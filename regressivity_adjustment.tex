\documentclass[]{article}
\usepackage{lmodern}
\usepackage{amssymb,amsmath}
\usepackage{ifxetex,ifluatex}
\usepackage{fixltx2e} % provides \textsubscript
\ifnum 0\ifxetex 1\fi\ifluatex 1\fi=0 % if pdftex
  \usepackage[T1]{fontenc}
  \usepackage[utf8]{inputenc}
\else % if luatex or xelatex
  \ifxetex
    \usepackage{mathspec}
  \else
    \usepackage{fontspec}
  \fi
  \defaultfontfeatures{Ligatures=TeX,Scale=MatchLowercase}
\fi
% use upquote if available, for straight quotes in verbatim environments
\IfFileExists{upquote.sty}{\usepackage{upquote}}{}
% use microtype if available
\IfFileExists{microtype.sty}{%
\usepackage{microtype}
\UseMicrotypeSet[protrusion]{basicmath} % disable protrusion for tt fonts
}{}
\usepackage[margin=1in]{geometry}
\usepackage{hyperref}
\hypersetup{unicode=true,
            pdftitle={Regressivity Adjustment Instructions},
            pdfauthor={Ta-Yun Yang},
            pdfborder={0 0 0},
            breaklinks=true}
\urlstyle{same}  % don't use monospace font for urls
\usepackage{graphicx,grffile}
\makeatletter
\def\maxwidth{\ifdim\Gin@nat@width>\linewidth\linewidth\else\Gin@nat@width\fi}
\def\maxheight{\ifdim\Gin@nat@height>\textheight\textheight\else\Gin@nat@height\fi}
\makeatother
% Scale images if necessary, so that they will not overflow the page
% margins by default, and it is still possible to overwrite the defaults
% using explicit options in \includegraphics[width, height, ...]{}
\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\IfFileExists{parskip.sty}{%
\usepackage{parskip}
}{% else
\setlength{\parindent}{0pt}
\setlength{\parskip}{6pt plus 2pt minus 1pt}
}
\setlength{\emergencystretch}{3em}  % prevent overfull lines
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
\setcounter{secnumdepth}{0}
% Redefines (sub)paragraphs to behave more like sections
\ifx\paragraph\undefined\else
\let\oldparagraph\paragraph
\renewcommand{\paragraph}[1]{\oldparagraph{#1}\mbox{}}
\fi
\ifx\subparagraph\undefined\else
\let\oldsubparagraph\subparagraph
\renewcommand{\subparagraph}[1]{\oldsubparagraph{#1}\mbox{}}
\fi

%%% Use protect on footnotes to avoid problems with footnotes in titles
\let\rmarkdownfootnote\footnote%
\def\footnote{\protect\rmarkdownfootnote}

%%% Change title format to be more compact
\usepackage{titling}

% Create subtitle command for use in maketitle
\providecommand{\subtitle}[1]{
  \posttitle{
    \begin{center}\large#1\end{center}
    }
}

\setlength{\droptitle}{-2em}

  \title{Regressivity Adjustment Instructions}
    \pretitle{\vspace{\droptitle}\centering\huge}
  \posttitle{\par}
    \author{Ta-Yun Yang}
    \preauthor{\centering\large\emph}
  \postauthor{\par}
      \predate{\centering\large\emph}
  \postdate{\par}
    \date{July 22, 2019}


\begin{document}
\maketitle

Before knitting the file, please make sure to include the following
scripts in the utility

\begin{verbatim}
,"caret", "ggpubr", "rlang" (in the packages)
, images=paste0(common.drive, "/images/") (in the directory)
\end{verbatim}

\subsubsection{Part 1: Adjustment
Methods}\label{part-1-adjustment-methods}

\paragraph{Conditions:}\label{conditions}

All of the following adjustment methods need to satisfy the conditions
that the summation of the fitted value should not vary more than a given
threshold, and the median of the ratio (which is calculated by the
fitted values divided by the most recent sale
price(\(\frac{\hat{y}}{y}\))) should not be different from 1 with given
tolerance. With all of the methods, we have limit observations in the
recent sale price which restricts the the ratio we can actually observe.
In order to simplify the situation, we assume the observe sale prices
are drawn randomly and they are capable to represent the population for
a given township with specific class.

The purpose of this adjustment is to reduce the regressivity or
progressivity in our assessed values. Optimally, the OLS curve from
\(y_hat\) and y should be close enough to 45 degree line (with
\(\beta_1\) close to 1 and \(\beta_0\) close to 0), the PRD should be
close enough to 1 and there should not have increase in COD.

\paragraph{1.1 Four Sections Median
Adjustment}\label{four-sections-median-adjustment}

The main concept of this method is to adjust the fitted values in
different sectors displayed in the follow graphs. In this function, we
regress the recent sale prices on the fitted values and extract a fitted
OLS line. The 45 degree line depicts the optimal fitted line where the
predicted values are equals to the fitted sale prices. In order to shift
the fitted OLS curve to the optimal line, we divide the points to four
groups with the optimal fitted line and the fitted value at the
intersection between OLS and optimal line (vertical line in the graphs).
In the first case on the left hand side, the fitted OLS curve has slope
lower than 1 where the property tax are considered to be progressive.
Fitted values in the upper left-hand side are systematically
underestimated. As the result, we increase the fitted values for these
points. Simultaneously, we decrease the fitted values for the group on
the lower right-hand side where they are systematically overestimated
(blue areas). On the other hand, on the right-hand side graph, the
fitted OLS curve contains slope greater than 1, we can expect
regressivity in the valuation. In this case, we decrease the fitted
values which are overestimated on the lower left-hand side and increase
the values which are overestimated on the upper right-hand side (gray
areas). This method implicitly shifts the OLS curve to the optimal line
without shifting the median ratio from 1. The magnitude of the single
shift for a point is decided by the amount it is underestimated or
overestimated.

\includegraphics[width=11.32in]{O:/CCAODATA/images/reg_picture1}
\includegraphics[width=10.82in]{O:/CCAODATA/images/reg_picture2}

\paragraph{1.2 Adjustment with Pivoting}\label{adjustment-with-pivoting}

The shift can be done directly from pivoting through adjusting the
fitted values according to the values of \(\beta0\) and \(\beta1\) from
OLS (regressing recent sale prices on fitted values). After
\(\hat{\beta_0}\) and \(\hat{\beta_1}\) are estimated from the binary
OLS with \[y = \beta_0 + \beta_1 \hat{y} + u\], we can shift the
\(\hat{y}\) by \[\hat{y}' = \hat{y} * \beta_1 + \beta_0\]. The change
for every point is identical in this method. The shift from pivoting an
be done in one step by \texttt{analytical\_pivot} (left-hand side) or in
gradual steps with \texttt{numerical\_pivot\_one\_step}(right-hand
side).

Notes: \textbf{red points}: points before adjustment, \textbf{green
points}; points after adjustment, \textbf{yellow curve}: origin fitted
line, \textbf{blue curve}: new fitted line.

\includegraphics[width=9.69in]{O:/CCAODATA/images/reg_picture_pivot}
\includegraphics[width=9.72in]{O:/CCAODATA/images/reg_picture_pivot2}

\paragraph{1.3 Local Median Adjustment}\label{local-median-adjustment}

Considering the difference in the sub-groups of properties. We also
provide an adjustment method accommodating the local characteristics of
the properties. For every given bin which is grouped according to the
percentiles of the original fitted values, points are shifted towards
the local median in every bins until the overall median ratio is close
enough to 1. The visualized instruction is presented as the following
graph. Since the method does not implicitly implement adjustment for
regressivity and progressivity, the combination with pivoting function
is necessary in this case.

\includegraphics[width=0.6\linewidth]{O:/CCAODATA/images/reg_picture3}

\paragraph{1.4 quantile value
adjustment}\label{quantile-value-adjustment}

There are some cases that the relationship of real sale prices and the
assesment values are seriously different by the different level of
quantile.(25th, 50th and 75th) Hydepark is the example of such kind of
cases. If it is the case, methods that rely on OLS do not work well,
because the correlation of sale price and characteristics of the
properties are inconsistent through different price zones. For such kind
of case, this method is used to adjust assesment values so that they
become close to quantile value of sale price. It will stop when the
difference of sale price and assesment value become closer enough. It
also take care the sum of total value change and change of median ratio,
and it will stop when these limit are reached. The detalil of how this
function works is as follows; First, it will compare the median assessed
values in each quantile group with the real sale price and if the
assesed value is larger than real sale price, all points in the bin are
regarded as overstimated and labeld 1. Similary, the if the assesed
value is smaller than median sale price, all points in the quantile bin
are regarded as underestimated and labeled 0. Then, overestimated values
should be decreased and understimated ones should be increased. However,
those move should be adjusted based on the magnitude of their effect. In
order for that, the function compares the total amount of difference
from over-allm median to each points between overestimated group and
underestimated group, and put heavy weight on the percentage of change
for the group with larger amount of difference.

\includegraphics[width=0.6\linewidth]{O:/CCAODATA/images/}

\paragraph{1.5 Implementation}\label{implementation}

The adjustment functions will end when max iteration is reached or the
stopping conditions are met. In order to guarantee that these changes
are feasible and reasonable to satisfy the strict criterian, we have
also designed several functions to set the threshold of the adjustments
and implement the integrity checks. The best adjustment model is
selected if the model is performing the best in reducing PRD, shifting
\(\beta_1\) to 1 and shifting \(beta_0\) to zero without increasing COD.
In order to avoid structural changes to single property values, we set a
limit percentage what single value can be changed. For example, given
the threshold equals to 0.05, if a single unit have changes in its
fitted value more than 5\% in our adjustment, the percentage of this
adjustment will be set to 5\% instead. Simultaneously, if the total
changes exceed the tolerance of sum values, the function will reset the
threshold of implemetation to 90\% and calculate the changes in sum
values until the condition is satisfied. These settings allow us to make
regressivity adjustments without violating critical conditions in origin
settings.

\subsubsection{2. Functions}\label{functions}

\paragraph{General parameters:}\label{general-parameters}

\begin{itemize}
\tightlist
\item
  \texttt{y}: vector, most recent sale prices
\item
  \texttt{y\_hat}: vector, origin fitted values
\item
  \texttt{origin\_yhat}: vector, origin fitted values from fitted value
  2
\item
  \texttt{df}: dataframe, dataframe from the valuation data
\item
  \texttt{adj\_perc}: numeric, percentage of the changes made in a
  single step
\item
  \texttt{threshold}: numeric, tolerance for the median in percentage
\item
  \texttt{max\_iter}: integer, the maximum number of iterations
\item
  \texttt{scale}: by default, ``\textbf{distance}''.

  \begin{itemize}
  \tightlist
  \item
    ``\textbf{y\_hat}'' use y\_hat as the base for the
    \texttt{adj\_perc}.
  \item
    ``\textbf{distance}'': use the difference between fitted values and
    the optimal values (on the 45 degree line) as the base for
    \texttt{adj\_perc}.
  \item
    ``\textbf{distance\_priority}'': only shift the points which contain
    the highest 20\% of the difference to the optimal level.
  \end{itemize}
\item
  \texttt{use\_prd}: by default, \texttt{FALSE}. If \texttt{TRUE} use
  whether PRD close enough to 1 as the stopping condition.
\item
  \texttt{prd\_thres}: by default, 0.05. The tolerance for the PRD
  condition (in percentage)
\item
  \texttt{sumv\_thres}: by default, 0.01. The maximum in the variatio
  for summation of fitted values
\item
  \texttt{b\_thres0}: by default, 1000. The tolerance for the
  \(\beta_0\)
\item
  \texttt{b\_thres1}: by default, 0.05. The tolerance for the
  \(\beta_1\)
\item
  \texttt{differentiate}: by default, FALSE. If it is TRUE, the shift of
  each points are adjusted based on their distance from median
\item
  \texttt{cumulated\_diff}: cumulative ratio of total change value
\item
  \texttt{converge\_rate}: by default, 0.8. Converge rate for changing
  value in the \texttt{execute\_adjustment\_filter} function
\item
  \texttt{med\_limit}: by default, c(0.02,0.05). The median ratio can
  move within this range.
\item
  \texttt{prior\_limit}: by default, list(0.45,0.55). Points within this
  quantile range do not move.
\item
  \texttt{balance}: by default, FALSE. The shift of points are adjusted
  by balance shift function
\item
  \texttt{onestep\_perc}: by default, 0.8. The shift of points are
  adjusted by this percentage in each iteration
\end{itemize}

All the functions are returning the vector of new fitted values after
adjustment

\paragraph{2.1
four\_sec\_median\_adj\_while:}\label{four_sec_median_adj_while}

\texttt{four\_sec\_median\_adj\_while} is a iterative function to adjust
the fitted values with four sectors median adjustment function which
implicitly pivot the OLS curce. The function can simply be called from

\begin{verbatim}
new_yhat <- four_sec_median_adj_while(y, y_hat, df, adj_perc, threshold, max_iter=20, scale="distance", use_prd="no", prd_thres=0.05, sumv_thres=0.01, b_thres0=1000, b_thres1=0.01, differentiate=FALSE)
\end{verbatim}

The example of the result is presented as the following:

\includegraphics[width=0.6\linewidth]{O:/CCAODATA/images/reg_picture_numeric_four_sec}

\paragraph{2.2
numerical\_pivot\_four\_sec\_median:}\label{numerical_pivot_four_sec_median}

\texttt{numerical\_pivot\_four\_sec\_median} is a recursive function to
adjust the fitted values with four sectors median adjustment
incorporated with numerical pivoting. The function can be called from

\begin{verbatim}
new_yhat <- numerical_pivot_four_sec_median(y, y_hat, df, adj_perc, threshold, cumulated_diff, b_thres0, b_thres1, origin_yhat, iter=0, max_iter=100, scale="distance", use_prd="no", prd_thres=0.05, sumv_thres=0.01, differentiate=FALSE)
\end{verbatim}

The example of the result is presented as the following:

\includegraphics[width=0.6\linewidth]{O:/CCAODATA/images/reg_picture_four_sec}

\paragraph{2.3 local\_median\_adj:}\label{local_median_adj}

\texttt{local\_median\_adj} is a iterative function using analytical (or
numerical) pivoting with local median adjustment. The function can be
called from

\begin{verbatim}
new_yhat <- local_median_adj(y, y_hat, df, bins, adj_perc, threshold, differentiate=FALSE, balance=FALSE, pivot="analytical", iter=0, max_iter=100, use_prd="no", prd_thres=0.05, sumv_thres=0.01, b_thres0=1000, b_thres1=0.01)
\end{verbatim}

The example of the result is presented as the following:

\includegraphics[width=0.6\linewidth]{O:/CCAODATA/images/reg_picture_local}

\paragraph{2.4
quantile\_diff\_spliter\_overall\_rec:}\label{quantile_diff_spliter_overall_rec}

\texttt{quantile\_diff\_spliter\_overall\_rec} is a recursive function
to adjust points to be close to quantile value of sale price. It stops
when the difference between sale price and assesment value become small
enough, or it reaches the median ratio limit. The function can be called
from

\begin{verbatim}
new_yhat <- quantile_diff_spliter_overall_rec(y, y_hat, origin_yhat, df, adj_perc, quantile_list=seq(0,1,0.01), by=0.01, 
                                              iter=0, maxiter=3000, differentiate=FALSE, med_limit=c(0.02,0.05),
                                              prior_limit=list(0.45,0.55))
\end{verbatim}

The example of the result is presented as the following:

\includegraphics[width=0.6\linewidth]{O:/CCAODATA/images/reg_picture_quantile}

\paragraph{2.5 balance\_shift:}\label{balance_shift}

\texttt{balance\_shift} is a function to balance the total amount of
change between overestimated values and underestimated values. The
function can be called from

\begin{verbatim}
balance_shift <- function(origin_yhat, current_diff, onestep_perc=0.8)
\end{verbatim}

\subsubsection{Part 2: Plots, Execute and
Thresholds}\label{part-2-plots-execute-and-thresholds}

\paragraph{make\_plot:}\label{make_plot}

\texttt{make\_plot} is a function used to generate plots depicting the
summary graphs for the percentage changes in the fitted values. The
function can be called from

\begin{verbatim}
make_plot(y_pred_orig, y_pred_adj, y_real, df)
\end{verbatim}

The example of the result is presented as the following:

\includegraphics[width=9.72in]{O:/CCAODATA/images/reg_picture_plot1}
\includegraphics[width=9.72in]{O:/CCAODATA/images/reg_picture_plot2}

Notes: Two plots at the top depict the distribution of the total and
percentages changes for the fitted values after the regressivity
adjustments. Two plots at the bottom are the corresponding changes
comparing to the recent sale price.

\paragraph{best\_yhat:}\label{best_yhat}

\texttt{choose\_best\_fv} is a function to extract the best adjustment
function with prioritizing COD, PRD, \(\beta_1\), \(\beta_0\), median
ratio, changes in summation of fitted valued. We can get the vector of
best fitted values after this function is implemented.

\begin{verbatim}
best_yhat <- choose_best_fv(df, y_col, methods_list, COD_tol)
\end{verbatim}

Notes: \textbf{COD\_tol}: the tolerance for the COD to increase slightly
in the comparison of best model. \textbf{methods\_list}: list of return
values from given regressivity adjustment functions. For example

\begin{verbatim}
methods_list <- list(four_sec_median_adj_while(y, y_hat, valuationdata, adj_perc, median_thres, max_iter=50),
                        four_sec_median_adj_while(y, y_hat, valuationdata, adj_perc, median_thres, max_iter=50, scale="distance_priority"),
                        four_sec_median_adj_while(y, y_hat, valuationdata, adj_perc, median_thres, max_iter=50, scale="y_hat"),
                        four_sec_median_adj_while(y, y_hat, valuationdata, adj_perc, median_thres, max_iter=50, scale="distance", use_prd="yes"),
                        four_sec_median_adj_while(y, y_hat, valuationdata, adj_perc, median_thres, max_iter=50, scale="y_hat", use_prd="yes"),
                        four_sec_median_adj_while(y, y_hat, valuationdata, adj_perc, median_thres, max_iter=50, scale="y_hat", use_prd="yes", differentiate = TRUE),
                        numerical_pivot_four_sec_median(y, y_hat, valuationdata, adj_perc, median_thres,0, beta0_thres, beta1_thres, y_hat,
                                                        iter=0, max_iter=100, scale="y_hat"),
                        numerical_pivot_four_sec_median(y, y_hat, valuationdata, adj_perc, median_thres,0, beta0_thres, beta1_thres, y_hat,
                                                        iter=0, max_iter=100, scale="y_hat", differentiate = TRUE),
                        numerical_pivot_four_sec_median(y, y_hat, valuationdata, adj_perc, median_thres,0, beta0_thres, beta1_thres, y_hat,
                                                        iter=0, max_iter=100, scale="distance"),
                        numerical_pivot_four_sec_median(y, y_hat, valuationdata, adj_perc, median_thres,0, beta0_thres, beta1_thres, y_hat,
                                                        iter=0, max_iter=100, use_prd="yes", scale="distance"),
                        numerical_pivot_four_sec_median(y, y_hat, valuationdata, adj_perc, median_thres,0, beta0_thres, beta1_thres, y_hat,
                                                        iter=0, max_iter=100, use_prd="yes", scale="y_hat"),
                        numerical_pivot_four_sec_median(y, y_hat, valuationdata, adj_perc, median_thres,0, beta0_thres, beta1_thres, y_hat,
                                                        iter=0, max_iter=100, scale="distance_priority"),
                        numerical_pivot_four_sec_median(y, y_hat, valuationdata, adj_perc, median_thres,0, beta0_thres, beta1_thres, y_hat,
                                                        iter=0, max_iter=100, use_prd="yes", scale="distance_priority"),
                        local_median_adj(y, y_hat, valuationdata, 10, adj_perc, median_thres),
                        local_median_adj(y, y_hat, valuationdata, 10, adj_perc, median_thres, use_prd="yes"),
                        local_median_adj(y, y_hat, valuationdata, 10, adj_perc, median_thres, pivot="analytical"),
                        local_median_adj(y, y_hat, valuationdata, 10, adj_perc, median_thres, use_prd="yes", pivot="analytical"),
                        local_median_adj(y, y_hat, valuationdata, 10, adj_perc, median_thres, pivot="numerical"),
                        local_median_adj(y, y_hat, valuationdata, 10, adj_perc, median_thres, use_prd="yes", pivot="numerical"),
                        local_median_adj(y, y_hat, valuationdata, 10, adj_perc, median_thres, use_prd="yes", pivot="numerical", differentiate = TRUE, balance=TRUE),
                        quantile_diff_spliter_overall_rec(y, y_hat, y_hat, valuationdata, 1, quantile_list=seq(0,1,0.05), by=0.05, iter=0, maxiter=1000, differentiate=TRUE, med_limit=c(0,median_thres)),
                        quantile_diff_spliter_overall_rec(y, y_hat, y_hat, valuationdata, 1, quantile_list=seq(0,1,0.05), by=0.05, iter=0, maxiter=1000, differentiate=FALSE, med_limit=c(0,median_thres)),
                        quantile_diff_spliter_overall_rec(y, y_hat, y_hat, valuationdata, 1, quantile_list=seq(0,1,0.01), by=0.01, iter=0, maxiter=1000, differentiate=TRUE, med_limit=c(0,median_thres)))
\end{verbatim}

\paragraph{execute\_adjustment\_filter:}\label{execute_adjustment_filter}

\texttt{execute\_adjustment\_filter} is a function to set a threshold
for the variation in single fitted value

\begin{verbatim}
limited_yhat <- execute_adjustment_filter(best_yhat, old_yhat, y, df, threshold=0.02, exe_threshold=0.05, converge_rate=0.8, sumv_thres=0.01, max_iter=10)
\end{verbatim}

If the total changes exceed the tolerance for sum values, the function
will reset the ``\textbf{threshold}''" of implemetation to 90\% and
calculate the changes in sum values until the condition is satisfied.

\paragraph{reg\_adj\_method class}\label{reg_adj_method-class}

After we run \texttt{choose\_best\_fv} function get the best fitted
values and ratio 3, the information of the regressivity adjustments can
be found in the object \texttt{reg\_adj\_method} named
\texttt{models\_info} in the example.

\begin{verbatim}
best_reg_models <- choose_best_fv(valuationdata, "most_recent_sale_price", methods_list, cod_thres)
valuationdata$fitted_value_3 <- best_reg_models[[1]]
valuationdata$ratio_3 <- valuationdata$fitted_value_3 / y
models_info <- best_reg_models[[2]]
\end{verbatim}

In general, \textbf{name}, \textbf{method}, \textbf{COD}, \textbf{PRD},
\textbf{med\_ratio}: median ratio, \textbf{sumdiff}: variation in
summation of values, \textbf{beta0}, \textbf{beta1} are stored in the
class. Specific information of models\_info can be extracted by

\begin{verbatim}
models_info@COD
\end{verbatim}

\paragraph{tract\_cod\_prd:}\label{tract_cod_prd}

\texttt{tract\_cod\_prd} is a function to to trace the change of COD and
PRD in each phase of sf\_valuation

\begin{verbatim}
tract_cod_prd <- function(df, y, c("ratio_1","ratio_2","ratio_3","ratio_4","ratio_5","ratio_6"))
\end{verbatim}


\end{document}
