# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

# Top comments ----

# This script allows an analyst to revalue condo buildings with undetected parking space using condo strata and an analysts defined
# parking space value threshold based on prior year valuation.

source(paste0("C:/Users/", Sys.info()[['user']], "/Documents/ccao_utility/code.r/99_utility_2.r"))
invisible(check.packages(libs))
database <- 1

# connect to retrieve data
CCAODATA <- dbConnect(odbc(),
                      driver   = "SQL Server",
                      server   = odbc.credentials("server"),
                      database = odbc.credentials("database"),
                      uid      = odbc.credentials("uid"),
                      pwd      = odbc.credentials("pwd"))

# has the building been mailed yet?
mailed <- FALSE

# declare building PIN
PIN <- '2220300041'

# declare prior value threshold for parking spaces
parking_space <- 3000

# declare desired year of revalue
year <- 2020

if (mailed == TRUE) {

  # retrieve data
  output <- dbGetQuery(CCAODATA, paste0("
  SELECT HEAD.PIN, DT_CLASS, STRATA.condo_strata_10 AS STRATA, DT_PER_ASS AS PER_ASS, HD_PRI_BLD + HD_PRI_LND AS PRI_AV,
  HD_ASS_BLD + HD_ASS_LND AS MAILED_AV,
  CASE WHEN (HD_PRI_BLD + HD_PRI_LND) >= ", parking_space, " THEN strata_10_minsp
    ELSE 1000 END AS STRATA_MIN,
  CASE WHEN (HD_PRI_BLD + HD_PRI_LND) >= ", parking_space, " THEN strata_10_medsp
    ELSE 1000 END AS STRATA_MED,
  CASE WHEN (HD_PRI_BLD + HD_PRI_LND) >= ", parking_space, " THEN strata_10_maxsp
    ELSE 1000 END AS STRATA_MAX

  FROM AS_HEADT HEAD

  LEFT JOIN AS_DETAILT DEETS ON HEAD.PIN = DEETS.PIN AND HEAD.TAX_YEAR = DEETS.TAX_YEAR

  LEFT JOIN DTBL_CONDOSTRATA STRATA ON LEFT(HEAD.PIN, 10) = STRATA.PIN10 AND HEAD.TAX_YEAR = STRATA.ASSESSMENT_YEAR + 1

  LEFT JOIN DTBL_CONDOSTRATA_STATS10 STATS
    ON STRATA.condo_strata_10 = STATS.condo_strata_10 AND STRATA.ASSESSMENT_YEAR = STATS.ASSESSMENT_YEAR

  WHERE LEFT(HEAD.PIN, 10) = '", PIN, "'
    AND DT_CLASS = 299
    AND HEAD.TAX_YEAR = ", lubridate::year(Sys.Date()), "
                     ")) %>%
    mutate("PER_ASS_RES" = PER_ASS / sum(PER_ASS),
           "NEW_AV" = floor(sum(STRATA_MED) * PER_ASS_RES / 10)) %>%
    arrange(PIN)

}

if (mailed == FALSE) {

  output <- dbGetQuery(CCAODATA, paste0("
  SELECT VALS.PIN, DT_CLASS, STRATA.condo_strata_10 AS STRATA,
  DT_PER_ASS AS PER_ASS, HD_ASS_BLD + HD_ASS_LND AS PRI_AV,
    fitted_value_6 AS MODEL_FMV,
  CASE WHEN (HD_ASS_BLD + HD_ASS_LND) >= ", parking_space, " THEN strata_10_minsp
    ELSE 1000 END AS STRATA_MIN,
  CASE WHEN (HD_ASS_BLD + HD_ASS_LND) >= ", parking_space, " THEN strata_10_medsp
    ELSE 1000 END AS STRATA_MED,
  CASE WHEN (HD_ASS_BLD + HD_ASS_LND) >= ", parking_space, " THEN strata_10_maxsp
    ELSE 1000 END AS STRATA_MAX

    FROM DTBL_MODELVALS VALS

  LEFT JOIN AS_HEADBR HEAD ON VALS.PIN = HEAD.PIN AND VALS.TAX_YEAR = HEAD.TAX_YEAR + 1

  LEFT JOIN AS_DETAILT DEETS ON VALS.PIN = DEETS.PIN AND VALS.TAX_YEAR = DEETS.TAX_YEAR + 1

  LEFT JOIN DTBL_CONDOSTRATA STRATA ON LEFT(VALS.PIN, 10) = STRATA.PIN10 AND VALS.TAX_YEAR = STRATA.ASSESSMENT_YEAR + 1

  LEFT JOIN DTBL_CONDOSTRATA_STATS10 STATS
      ON STRATA.condo_strata_10 = STATS.condo_strata_10 AND STRATA.ASSESSMENT_YEAR = STATS.ASSESSMENT_YEAR

  WHERE LEFT(VALS.PIN, 10) = '", PIN, "'
  AND DT_CLASS = 299
  AND VALS.TAX_YEAR = ", year, "
                     ")) %>%
    mutate("PER_ASS_RES" = PER_ASS / sum(PER_ASS),
           "NEW_AV_MIN" = floor(sum(STRATA_MIN) * PER_ASS_RES / 10),
           "NEW_AV_MED" = floor(sum(STRATA_MED) * PER_ASS_RES / 10),
           "NEW_AV_MAX" = floor(sum(STRATA_MAX) * PER_ASS_RES / 10)) %>%
  arrange(PIN)

}

# export
write_xlsx(output, paste0("O:/CCAODATA/data/bad_data/", PIN, "_reval.xlsx"))

writeLines(paste0("new BUILDING AV:\n",
                  "strata MIN = ", sum(output$NEW_AV_MIN), "\n",
                  "strata MED = ", sum(output$NEW_AV_MED), "\n",
                  "strata MAX = ", sum(output$NEW_AV_MAX)))