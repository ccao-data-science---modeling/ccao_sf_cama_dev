# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/.

# Top comments ----

# This script allows an analyst to value pins that were not present in the head file when the SQL datasets were created.

load(destfile)

# calculate mean percentage difference by class and neighborhood
for (i in levels(valuationdata$NBHD)) {
  for (c in levels(valuationdata$CLASS)) {
    valuationdata$ave_fitted_diff[valuationdata$NBHD == i & valuationdata$CLASS == c] <- 1 - (mean(valuationdata$fitted_value_6[valuationdata$NBHD == i & valuationdata$CLASS == c] / valuationdata$fitted_value_1[valuationdata$NBHD == i & valuationdata$CLASS == c]))
  }
}

# import pins
new_pins<-read.xlsx2(file = paste0("O:/CCAODATA/data/bad_data/missing_pins_", scope_target, "_", tax_year, ".xlsx"), sheetName = "manual_input", stringsAsFactors=FALSE)
new_pins$PIN[nchar(new_pins$PIN) == 13]<-paste0("0", new_pins$PIN)

# connect to retrieve data
CCAODATA <- dbConnect(odbc()
                      , driver   = "SQL Server"
                      , server   = odbc.credentials("server")
                      , database = odbc.credentials("database")
                      , uid      = odbc.credentials("uid")
                      , pwd      = odbc.credentials("pwd"))

# import PIN xy coordinates or use lat long from new_pins if necessary
coordinates <- dbGetQuery(CCAODATA, paste0("
                                           SELECT PIN, centroid_x as long, centroid_y as lat FROM DTBL_PINLOCATIONS
                                           WHERE PIN IN ('", paste(noquote(new_pins$PIN), collapse = "', '"), "')
                                           "))

# disconnect after pulls
dbDisconnect(CCAODATA)

coordinates <- rbind(coordinates, subset(new_pins, new_pins$PIN %ni% coordinates$PIN, select = c("PIN", "long", "lat")))
names(coordinates)<-c("PIN", "centroid_x", "centroid_y")

# generate missing location factor
coordinates$sale_date <- as.Date("01/01/2019", "%m/%d/%Y")
coordinates$centroid_x<-as.numeric(coordinates$centroid_x)
coordinates$centroid_y<-as.numeric(coordinates$centroid_y)
coordinates$sale_quarterofyear <- as.factor(1)
coordinates$sale_price<-NA
coordinates$LOCF_PRED<-as.numeric(predict(LOCF_MODEL, newdata=coordinates))
coordinates$LOCF1<-coordinates$LOCF_PRED/mean(assmntdata$LOC_PRED, na.rm = TRUE)

# add LOCF1
new_pins<-merge(new_pins, dplyr::select(coordinates, c("PIN", "LOCF1")), by = "PIN")

# create dataset of missing pins
new_pins$LOCF1<-as.numeric(new_pins$LOCF1)
new_pins$BLDG_SF<-as.numeric(new_pins$BLDG_SF)
new_pins$HD_SF<-as.numeric(new_pins$HD_SF)
new_pins$FBATH<-as.numeric(new_pins$FBATH)
new_pins$HBATH<-as.numeric(new_pins$HBATH)
new_pins$BEDS<-as.numeric(new_pins$BEDS)
new_pins$AGE_DECADE<-as.numeric(new_pins$AGE_DECADE)
new_pins$AGE_DECADE_SQRD<-as.numeric(new_pins$AGE_DECADE_SQRD)
new_pins$GAR1_SIZE<-as.numeric(new_pins$GAR1_SIZE)
new_pins$sale_year<-as.numeric(new_pins$sale_year)
new_pins$sale_date <- as.Date(as.numeric(new_pins$sale_date), origin = "1899-12-30")
new_pins$town_nbhd_SF<-new_pins$town_nbhd
new_pins$town_nbhd_MF<-new_pins$town_nbhd
new_pins$town_nbhd_NCHARS<-new_pins$town_nbhd
new_pins$modeling_group<-as.factor(new_pins$modeling_group)
new_pins$sale_quarter<-as.numeric(new_pins$sale_quarter)
new_pins$floodplain <- as.numeric(new_pins$floodplain)
new_pins$withinmr100 <- as.numeric(new_pins$withinmr100)
new_pins$withinmr101300 <- as.numeric(new_pins$withinmr101300)
#new_pins$ROOMS<-as.numeric(new_pins$ROOMS)
#new_pins$AGE<-as.numeric(new_pins$AGE)
#new_pins$AGE_SQRD<-as.numeric(new_pins$AGE_SQRD)
#new_pins$RENOVATION<-as.integer(new_pins$RENOVATION)
#new_pins$sale_quarterofyear<-as.factor(new_pins$sale_quarterofyear)
#new_pins$CONDO_CLASS_FACTOR<-as.factor(new_pins$CONDO_CLASS_FACTOR)
#new_pins$PIN10F<-as.factor(new_pins$PIN10F)
#new_pins$condo_strata_10<-as.factor(new_pins$condo_strata_10)
#new_pins$condo_strata_100<-as.factor(new_pins$condo_strata_100)

# price tercile
new_pins$price_tercile[new_pins$modeling_group == 'SF'] <- predict(TERC_MOD_SF, newdata=subset(new_pins, new_pins$modeling_group == 'SF'))
new_pins$price_tercile[new_pins$modeling_group == 'MF'] <- predict(TERC_MOD_MF, newdata=subset(new_pins, new_pins$modeling_group == 'MF'))

new_pins$price_tercile[new_pins$price_tercile > 3] <- 3
new_pins$price_tercile <- fct_explicit_na(ordered(factor(new_pins$price_tercile), c(1, 2, 3)))

# create ave_fitted_diff for new pins
for(x in levels(valuationdata$town_nbhd)){
  for(y in levels(valuationdata$CLASS)){
    new_pins$ave_fitted_diff[new_pins$town_nbhd == x & new_pins$CLASS == y] <- mean(valuationdata$ave_fitted_diff[valuationdata$town_nbhd == x & valuationdata$CLASS == y], na.rm=TRUE)
  }
}

# predict new values after corrected erroneous fields
for (i in levels(new_pins$modeling_group)){

  model <- eval(parse(text = paste0('final_', tolower(i) ,'_model')))

  # if ntree is too large for these predictions it will crash R
  ntree <- 100

  if (best_models$type[best_models$modeling_group == i] == 'log-log'){
    if (best_models$estimation_method[best_models$modeling_group == i] %in% c('OLS', 'Quantile')){
      new_pins$fitted_value_1[new_pins$modeling_group == i] <- exp(predict(model, newdata=new_pins[new_pins$modeling_group == i,]))
    }
    if (best_models$estimation_method[best_models$modeling_group == i] == 'GBM'){
      new_pins$fitted_value_1[new_pins$modeling_group == i] <- exp(predict(model, newdata=new_pins[new_pins$modeling_group == i,], n.trees=ntree))
    }
  }
  if (best_models$type[best_models$modeling_group == i] == 'lin-lin'){
    if (best_models$estimation_method[best_models$modeling_group == i]  %in% c('OLS', 'Quantile')){
      new_pins$fitted_value_1[new_pins$modeling_group == i] <- predict(model, newdata=new_pins[new_pins$modeling_group == i,])
    }
    if (best_models$estimation_method[best_models$modeling_group == i] == 'GBM'){
      new_pins$fitted_value_1[new_pins$modeling_group == i] <- predict(model, newdata=new_pins[new_pins$modeling_group == i,], n.trees=ntree)
    }
  }
}

new_pins$fitted_value_6 <- new_pins$fitted_value_1*(1-new_pins$ave_fitted_diff)

# export table
wb <- xlsx::loadWorkbook(paste0("O:/CCAODATA/data/bad_data/missing_pins_", scope_target, "_", tax_year, ".xlsx"))
xlsx::removeSheet(wb, sheetName = names(xlsx::getSheets(wb)[2]))
xlsx::saveWorkbook(wb, paste0("O:/CCAODATA/data/bad_data/missing_pins_", scope_target, "_", tax_year, ".xlsx"))
write.xlsx2(new_pins, file = paste0("O:/CCAODATA/data/bad_data/missing_pins_", scope_target, "_", tax_year, ".xlsx"), sheetName = 'valuations', row.names = FALSE, sep = ",", append = TRUE)

#save(list = scope_list[sapply(scope_list, exists)==TRUE], file=destfile)