# License notice ----

# This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 3 of the License, or (at your option) any later version.
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/. 

# Top comments ----

# This script allows an analyst to amend outliers detected during modeling by updating incorrect characteristics
# and running the pin back through the model with corrected information.

load(destfile)

# function for quickly correcting outliers during desktop review
outlier_correction <- function(pin, fields, new_values) {
  
  # example use case
  # outlier_corrections <- valuation_data
  # outlier_corrections <- outlier_correction("05072130360000", c("FBATH", "HBATH"), c("5", "2"))
  
  n <- length(fields)
  for (i in 1:n) {
    col <- which(colnames(outlier_corrections)==fields[i])
    row <- which(outlier_corrections$PIN==pin)
    outlier_corrections[row,col] <- as.numeric(new_values[i])
    outlier_corrections$fields_adjusted[outlier_corrections$PIN==pin] <- toString(fields)
    outlier_corrections$new_values[outlier_corrections$PIN==pin] <- toString(new_values)
  }
  return(outlier_corrections)
}

outlier_corrections <- valuationdata

# calculate mean percentage difference by class and neighborhood
for (i in levels(outlier_corrections$NBHD)) {
  
  for (c in levels(outlier_corrections$CLASS)) {
    
    outlier_corrections$ave_fitted_diff[outlier_corrections$NBHD == i & outlier_corrections$CLASS == c] <- 1 - (mean(outlier_corrections$fitted_value_6[outlier_corrections$NBHD == i & outlier_corrections$CLASS == c] / outlier_corrections$fitted_value_1[outlier_corrections$NBHD == i & outlier_corrections$CLASS == c]))
    
  }
  
}

# import lists of changes
changes_data <- read.xlsx2(file = paste0("O:/CCAODATA/data/bad_data/outliers_to_correct_", scope_target ,".xlsx"), sheetIndex = 1, header = TRUE, stringsAsFactors = FALSE)
changes_data$PIN[nchar(changes_data$PIN) == 13]<-paste0("0", changes_data$PIN)

# list of new modeling groups
modeling_group_changes <- changes_data[changes_data$modeling_group != "",colnames(changes_data) %in% c("PIN", "modeling_group") == TRUE]
modeling_group_changes$modeling_group <- as.factor(modeling_group_changes$modeling_group)

# create lists to loop through
outlier_pins <- changes_data$PIN
outlier_fields <- NULL
outlier_changes <- NULL

for (i in 1:nrow(changes_data)){
  outlier_fields[i] <- as.list(strsplit(changes_data$outlier_fields[i], ", "))
  outlier_changes[i] <- as.list(strsplit(changes_data$outlier_changes[i], ", "))
}

# create subset of VALUATIONDATA only including those pins
outlier_corrections <- subset(outlier_corrections, PIN %in% outlier_pins)
outlier_corrections$fields_adjusted <- NA
outlier_corrections$new_values <- NA

# loop through pins and apply changes
for (t in 1:length(outlier_pins)){
  outlier_corrections <- outlier_correction(outlier_pins[[t]], outlier_fields[[t]], outlier_changes[[t]])
}

# change modeling groups for certain class changes
outlier_corrections$modeling_group[outlier_corrections$PIN %in% modeling_group_changes$PIN] <- modeling_group_changes$modeling_group

# make sure that PINs that have changed modeling groups have an appropriate town_nbhd value
outlier_corrections$town_nbhd_MF <- outlier_corrections$town_nbhd_SF <- outlier_corrections$town_nbhd

# predict new values after corrected erroneous fields
for (i in levels(droplevels(outlier_corrections$modeling_group))){
  if (best_models$type[best_models$modeling_group == i] == 'log-log'){
    if (best_models$estimation_method[best_models$modeling_group == i] == 'OLS'){
      outlier_corrections$corrected_fitted_value_1[outlier_corrections$modeling_group == i] <- exp(predict(eval(parse(text = paste0("final_", tolower(i) ,"_model"))), newdata=outlier_corrections[outlier_corrections$modeling_group == i,]))  
    }
    if (best_models$estimation_method[best_models$modeling_group == i] == 'GBM'){
      outlier_corrections$corrected_fitted_value_1[outlier_corrections$modeling_group == i] <- exp(predict(eval(parse(text = paste0("final_", tolower(i) ,"_model"))), newdata=outlier_corrections[outlier_corrections$modeling_group == i,], n.trees=ntree))  
    }
    if (best_models$estimation_method[best_models$modeling_group == i] == 'Quantile'){
      outlier_corrections$corrected_fitted_value_1[outlier_corrections$modeling_group == i] <- exp(predict(eval(parse(text = paste0("final_", tolower(i) ,"_model"))), newdata=outlier_corrections[outlier_corrections$modeling_group == i,]))
    }
  }
  if (best_models$type[best_models$modeling_group == i] == 'lin-lin'){
    if (best_models$estimation_method[best_models$modeling_group == i] == 'OLS'){
      outlier_corrections$corrected_fitted_value_1[outlier_corrections$modeling_group == i] <- predict(eval(parse(text = paste0("final_", tolower(i) ,"_model"))), newdata=outlier_corrections[outlier_corrections$modeling_group == i,])  
    }
    if (best_models$estimation_method[best_models$modeling_group == i] == 'GBM'){
      outlier_corrections$corrected_fitted_value_1[outlier_corrections$modeling_group == i] <- predict(eval(parse(text = paste0("final_", tolower(i) ,"_model"))), newdata=outlier_corrections[outlier_corrections$modeling_group == i,], n.trees=ntree)
    }
    if (best_models$estimation_method[best_models$modeling_group == i] == 'Quantile'){
      outlier_corrections$corrected_fitted_value_1[outlier_corrections$modeling_group == i] <- predict(eval(parse(text = paste0("final_", tolower(i) ,"_model"))), newdata=outlier_corrections[outlier_corrections$modeling_group == i,])
    }
  }
}
outlier_corrections$corrected_fitted_value_6 <- outlier_corrections$corrected_fitted_value_1*(1-outlier_corrections$ave_fitted_diff)
outlier_corrections$net_change <- outlier_corrections$corrected_fitted_value_6-outlier_corrections$fitted_value_6

# print output for most recently added pin
outlier_corrections$output <- paste0("original fitted value for PIN ", outlier_corrections$PIN, " was $", formatC(round(outlier_corrections$fitted_value_6, 0), format="d", big.mark=","), ", corrected predicted value is $", formatC(round(outlier_corrections$corrected_fitted_value_6, 0), format="d", big.mark=","))
print(outlier_corrections$output[outlier_corrections$PIN == outlier_pins[length(outlier_pins)]])

if (length(outlier_pins) == length(outlier_fields) & length(outlier_fields) == length(outlier_changes)){
  print("input lists still have equal lengths, you haven't bungled it yet")
  
  # simplify data frame
  outlier_corrections <- dplyr::select(outlier_corrections, c("PIN", "fitted_value_1","fitted_value_6", "corrected_fitted_value_1", "ave_fitted_diff", "corrected_fitted_value_6", "net_change", "TOWN_CODE", "NBHD", "PROPERTY_ADDRESS", "CLASS", "fields_adjusted", "new_values"))
  
  # export table
  write.table(outlier_corrections, file = paste0("O:/CCAODATA/data/bad_data/corrected_outliers_", scope_target, "_2019.csv"), row.names = FALSE, sep = ",")
  
}else{
  print("ERROR | input lists are NOT EQUAL; you've bungled it, good work")
}

# import condo outlier adjustments
if(file.exists(paste0("O:/CCAODATA/data/bad_data/strata_adjustments_", scope_target,".xlsx"))){
  condo_adjustments <- read.xlsx2(file=paste0("O:/CCAODATA/data/bad_data/strata_adjustments_", scope_target,".xlsx"), sheetIndex = 1)
}

save(list = scope_list[sapply(scope_list, exists)==TRUE], file=destfile)

# combine existing outliers from all towns
combined <- NULL
for(x in paste0("O:/CCAODATA/data/bad_data/"
                , grep("combined"
                       , grep(paste0(tax_year)
                                        , grep(".csv", list.files(path = "O:/CCAODATA/data/bad_data", pattern = "corrected_outliers"), value = TRUE), value = TRUE), value = TRUE, invert = TRUE))){
  combined <- rbind(temp, read.csv(x, stringsAsFactors = FALSE))
}
combined$PIN <- as.character(ifelse(nchar(combined$PIN) == 14, combined$PIN, paste0("0", combined$PIN)))
write.table(combined, file = paste0("O:/CCAODATA/data/bad_data/corrected_outliers_combined_2019.csv"), row.names = FALSE, sep = ",")