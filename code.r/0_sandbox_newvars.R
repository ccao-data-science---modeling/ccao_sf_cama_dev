######################################################################################################
# This file supports the residential modeling sandbox (residential_modeling_sanbox.Rmd)
# This file is designed to be edited by the sandbox user to create new variables for modeling
# See the template below for an example of this process
######################################################################################################

# Example function

# In order to include a new variable in modeling data, the user must create a function that constructs
# this new variable, and exports it ready for use
# In the example below, we are joining the quarterly foreclosure rate by block into
# the sales sample. The general outline of the function is:
#       1) The function specifies both the database it is drawing from, as well as the original sales_sample
#       2) The function generates a data frame with the new data and join fields
#       3) The function joins the new data against the sales_sample
#       4) The function returns either the new data, or the old data if the join is bad

new_sandbox_data_1 <- function(database, sales_sample){

  if (database == 2) {
    message("Data loaded from memory without alteration")
    return(sales_sample)
  }
  if(database == 1) {
    
  # The foreclosure data is stored in the SQL server
  df <- try(dbGetQuery(CCAODATA, paste0("
  SELECT FORECLOSURES.PIN
  , date_of_sale
  , YEAR(date_of_sale) AS [YEAR]
  , ((YEAR(date_of_sale) - 1997) * 12) + MONTH(date_of_sale) AS [MONTH]
  , ((YEAR(date_of_sale) - 1997) * 4) +
  CASE WHEN MONTH(date_of_sale) BETWEEN 1  AND 3  THEN 1
  	 WHEN MONTH(date_of_sale) BETWEEN 4  AND 6  THEN 2
  	 WHEN MONTH(date_of_sale) BETWEEN 7  AND 9  THEN 3
  	 WHEN MONTH(date_of_sale) BETWEEN 10 AND 12 THEN 4 END AS [sale_quarter]
  , TOWN, NBHD, CAST(TOWN AS varchar) + CAST(NBHD AS varchar) AS town_nbhd
  , LEFT(FORECLOSURES.PIN, 7) AS block
  FROM DTBL_FORECLOSURES AS FORECLOSURES
  LEFT JOIN
  (SELECT PIN, TAX_YEAR, LEFT(HD_TOWN, 2) AS TOWN, HD_NBHD AS NBHD FROM AS_HEADT) AS HEAD
  	ON FORECLOSURES.PIN = HEAD.PIN AND YEAR(FORECLOSURES.date_of_sale) = HEAD.TAX_YEAR
  WHERE TOWN IS NOT NULL
  AND NBHD IS NOT NULL
  AND sale_results IN ('REO', 'REO BANK OWNED', 'REO SOLD', 'SOLD')
  ")))
  
  # Error handling
  if (class(df)=="try-error"){ 
    return(sales_sample)
    stop("Error getting new data 1")
    }else{
  
  # aggregate foreclosure data
  df <- df %>% dplyr::group_by(block, sale_quarter) %>% tally(name = "foreclosures_quarter_block")
  
  # Modify sales_sample to accept new data
  sales_sample$block <- substr(sales_sample$PIN, 1, 7)
  sales_sample$sale_year <- lubridate::year(sales_sample$sale_date)
  sales_sample$sale_quarter <- (sales_sample$sale_year - 1997) * 4 + lubridate::quarter(sales_sample$sale_date)
  
  # Join into sales sample
  n1 <- nrow(sales_sample)
  sales_sample2 <- dplyr::left_join(sales_sample, df)
  
  if(n1 != nrow(sales_sample)){
    return(sales_sample)
    stop("Bad join: no changes made to sales sample")}else{
      save(sales_sample2, file =  paste0(tempdir(), "sales_sample.Rda"))
      message("Local foreclosure rate included in modeling sample")
      return(sales_sample2)}
    }
  }
}


new_sandbox_data_2_USER_POPULATE <- function(database, sales_sample){
  message("Nothing additional included - user must populate this function")
  return(sales_sample)
}


new_sandbox_data_3_oak_park <- function(use, sales_sample, file=""){
  # Example function created to test out sandbox. Its possible that our model in oak
  #  park could be improved by including the proximity to Austin Boulevard, which separates
  #  oak park from austin. to test this theory, this function adds distance_to_austin_blvd
  # to one of the datasets, while the other one uses regular adjustments for location.
  # Then, we run the sandbox and see if adding this new variable can help improve modeling
  # in oak park
  # use = 1 if user wants to skip this function, use = 2 if user wants to add new variable
  # sales_sample: the sample that user wants to attach distance_to_austin_blvd to
  # file: geojson file containing the coordinates of austin_blvd. 
    # For other, similar uses, these coordinates can be obtained by using OpenStreetMap
    # via the Overpass API: https://overpass-turbo.eu/
    # To find a specific street: go into https://www.openstreetmap.org/#map=13/41.9178/-87.7691, 
    # click the little pointer with the question mark on it on the right side of the screen, 
    # click the street you want to query, find the way or relation ID in the query results on 
    # the left side, copy that ID into the overpass API
  
  if (use == 2) {
    message("Nothing additional included - user must populate this function")
  }
  if (use == 1) {
    # read in geojson
    austin_blvd <- st_read(file)
    # geojson file will likely have POINTS and LINESTRINGS. we extract just the line strings
    lines <- st_cast(austin_blvd$geometry %>% st_collection_extract(type = c("LINESTRING")),"LINESTRING")
    # Merge multiple linestrings into one
    # https://gis.stackexchange.com/questions/272813/merge-multilinestrings-that-are-out-of-order
    lines_merged <- st_cast(st_line_merge(
      st_union(st_cast(lines, "MULTILINESTRING"))), "LINESTRING")
    
    # check that now the lines are only sfc_linestring, not another format
    print(class(lines_merged)[1]=="sfc_LINESTRING")
    
    pins_missing_xy <- subset(sales_sample[, c("PIN", "centroid_x", "centroid_y")], is.na(centroid_x))
    integ_check_f(nrow(pins_missing_xy) > 0,
                  paste0("All PINs should be on the Clerk's shape file"),
                  paste0("Bad: ", nrow(pins_missing_xy), " PINs are missing xy data"),
                  "GOOD: All PINs have xy coordinates",
                  "0")
    
    # Remove PINs missing location
    sales_sample <- sales_sample %>% 
      filter(!is.na(centroid_x) & !is.na(centroid_y))
    
    # limit to data needed to calculate distance variable
    data <- sales_sample %>% 
      select(PIN,centroid_x,centroid_y) %>% 
      distinct()
    
    # turn data into an sf object using the centroid x and centroid y
    sf_model <- st_as_sf(data, coords = c("centroid_x","centroid_y"),
                         crs=st_crs(lines_merged))
    
    # calculate minimum distance from each PIN to austin blvd
    sf_model$distance_austin_blvd <- apply(as.matrix(st_distance(sf_model,lines_merged)) , 1, min)
    sf_model <- sf_model %>% select(PIN,distance_austin_blvd)
    
    # create deciles of distance variable
    sf_model$cut_distance <- cut(sf_model$distance_austin_blvd, 
                                      breaks = quantile(sf_model$distance_austin_blvd,
                                                        probs = seq(0,1,1/10)),
                                      labels=1:10,
                                      include.lowest = TRUE)
    
    # join back onto sales sample
    sales_sample <- left_join(sales_sample,sf_model)
    message("Distance to Austin Blvd variable (meters) added as distance_austin_blvd")  
  }

  return(sales_sample)
}


