get_nearest_neighborhoods <- function(df, k=3, target_township, ori_nbhd=c("TOWN_CODE", "NBHD_mapping", "modeling_group")){
  # Given the target township, the function is used to determined the nearest neighborhood for specific modeling group. 
  # In the function, we calculate the centroid of the given neighborhood by taking average of longitude and latitude from 
  # all of the pin locations. We then calculate the distance between neighborhoods with the centroids. The nearest k neighborhoods 
  # will be stored in the column "nearests" in the returned dataframe which uses towncode, neighborhood and modeling group 
  # as the primary key. This function will generate the distance dataframe used in the following functions
  
  # Inputs:
  #    df: (dataframe) assmntdata
  #    k: (integer) number of the nearest blocks considered
  #    target_twonship: (integer) the id for the target township
  #    ori_nbhd: (list of string) columns in assmntdata used for grouping
  # Returns: dataframe includes information of the nearest neighborhoods for every given neighborhoods
  
  # Example: dist_frame <- get_nearest_neighborhoods(assmntdata, 3, 17)
  df <- subset(df, df$TOWN_CODE == target_township & df$modeling_group != "FIXED")
  dist_list <- df %>% group_by(!!!syms(ori_nbhd)) %>% summarise(nbhd_centroidx = mean(centroid_x), nbhd_centroidy = mean(centroid_y))
  dist_frame <- data.frame(dist_list)
  dist_frame$NB_TYPE <- paste0(paste(dist_frame$TOWN_CODE), paste(dist_frame$NBHD_mapping), paste(dist_frame$modeling_group))
  dist_frame$nearests <- 0
  
  for (nbhd_idx in unique(dist_frame$NB_TYPE)){
    center_nbhd <- dist_frame[dist_frame$NB_TYPE == nbhd_idx, ]
    other_nbhd <- dist_frame[dist_frame$NB_TYPE != nbhd_idx, ]
    other_nbhd$center_x <- center_nbhd$nbhd_centroidx
    other_nbhd$center_y <- center_nbhd$nbhd_centroidy
    other_nbhd <- as.matrix(other_nbhd)
    
    distance_to_center <- apply(other_nbhd, 1, function(obk) distHaversine(c(as.numeric(obk[4]), as.numeric(obk[5])), 
                                                                           c(as.numeric(obk[8]), as.numeric(obk[9]))))
    other_nbhd <- as.data.frame(other_nbhd)
    other_nbhd$distance_to_center <- distance_to_center
    
    other_nbhd <- subset(other_nbhd, modeling_group == as.character(center_nbhd$modeling_group))
    
    nearest_nbhds <- list(as.character(other_nbhd[order(other_nbhd$distance_to_center), ][1:min(k, nrow(other_nbhd)), ]$NB_TYPE))
    dist_frame$nearests[dist_frame$NB_TYPE == nbhd_idx] <- nearest_nbhds
  }
  return(dist_frame)
}

get_merge_in_nb <- function(df, target_township, thres=10, ori_nbhd=c("TOWN_CODE", "NBHD_mapping", "modeling_group")){
  # The function is designed to find the specific neighhoods with specific modeling groups which satisfy the minimum
  # threshold of the sample size. The function will be used in the following function to find out the neighborhoods
  # that can be merged into.
  
  # Inputs:
  #    df: (dataframe) assmntdata
  #    target_township: (integer) ID for the target township
  #    thres: (integer) minimum rows for a given combination of neighborhood and modeling group
  #    ori_nbhd: (list of string) columns in assmntdata used for grouping
  # Return: the dataframe with the count of each neighborhood
  sub_df <- subset(df, df$TOWN_CODE == target_township & !is.na(most_recent_sale_price) & df$modeling_group != "FIXED")
  nb_count_frame <- sub_df %>% group_by(!!!syms(ori_nbhd)) %>% summarise(count = n())
  nb_count_frame$NB_TYPE <- paste0(paste(nb_count_frame$TOWN_CODE), paste(nb_count_frame$NBHD_mapping), paste(nb_count_frame$modeling_group))
  
  return(nb_count_frame)
  #verified_nb <- nb_count_frame[nb_count_frame$count >= thres, ]
  #return(verified_nb)
}


automated_neighborhood_recode <- function(df, k, target_township, thres=10, ori_nbhd=c("TOWN_CODE", "NBHD_mapping", "modeling_group")){
  # This function is used to provide a single step recoding for the neighborhood ID. We first calculate the nearests
  # neighborhoods for a specific combination of neighborhood and modeling group, and detect the neighborhoods which
  # could be merged into (via get_nearest_neighborhoods and get_merge_in_nb functions). For neighborhoods that cannot
  # satisfy the threshold of minimum rows. We search for the nearest neighborhoods which contains the most similar 
  # assessed value in the previous year and merge two neighborhoods. The neighborhood ID (with specific modeling group)
  # of the smaller set will be re-defined as which of the larger set. If the available neighborhoods cannot be found,
  # the neighborhood ID will remain unchanged, and the further operation will be done in the following function.
  
  # If we cannot find any neighborhood (for a specific modeling group) which satisfies the threshold, we select the
  # neighborhood that contains the most cases as the target neighborhood to be merged into
  
  # Inputs:
  #    df: (dataframe) assmntdata
  #    k: (integer) number of the nearest blocks considered
  #    target_township: (integer) ID for the target township
  #    thres: (integer) minimum rows for a given combination of neighborhood and modeling group
  #    ori_nbhd: (list of string) columns in assmntdata used for grouping
  # Return: updated assmntdata
  
  # Example: assmntdata <- automated_neighborhood_recode(assmntdata, 3, 17, 10)
  nb_count <- get_merge_in_nb(df, target_township, thres, ori_nbhd)  
  verified_nb <- nb_count[nb_count$count >= thres, ]$NB_TYPE

  for (mdg in unique(nb_count$modeling_group)){
    if (nrow(subset(nb_count, modeling_group == mdg) == 0)){
      mdg_sub <- subset(nb_count, modeling_group == mdg)
      top_nb <- mdg_sub[order(mdg_sub$count, decreasing=TRUE), ][1, ]$NB_TYPE
      verified_nb <- rbind(verified_nb, top_nb)
    }
  }
  
  df$previous_av <- df$PRI_EST_BLDG + df$PRI_EST_LAND
  sub_df <- subset(df, df$TOWN_CODE == target_township & df$modeling_group != "FIXED")
  nbhd_median_frame <- sub_df %>% group_by(!!!syms(ori_nbhd)) %>% summarise(nbhd_median = median(previous_av))
  nbhd_median_frame$NB_TYPE <- paste0(paste(nbhd_median_frame$TOWN_CODE), paste(nbhd_median_frame$NBHD_mapping), 
                                      paste(nbhd_median_frame$modeling_group))
  dist_frame <- get_nearest_neighborhoods(df, k, target_township, ori_nbhd)
  
  df$NB_TYPE <- paste0(paste(df$TOWN_CODE), paste(df$NBHD_mapping), paste(df$modeling_group))
  df$new_nb <- df$NB_TYPE

  #print(unique(nbhd_median_frame$NB_TYPE))
  #for (nb in unique(nbhd_median_frame$NB_TYPE)){
  for (nb in unique(dist_frame$NB_TYPE)){
    df$new_nb <- ifelse((df$TOWN_CODE == target_township) & (df$NB_TYPE == nb), nb, df$new_nb)
    nearest_distance <- Inf
    nearest_nb <- nb
    if (nrow(subset(df, df$TOWN_CODE == target_township & df$NB_TYPE == nb & !is.na(most_recent_sale_price))) < thres){
      for (near_nb in dist_frame[dist_frame$NB_TYPE == nb,]$nearests[[1]]){
        nb_distance <- abs(nbhd_median_frame[nbhd_median_frame$NB_TYPE == nb,]$nbhd_median[[1]] - 
                        nbhd_median_frame[nbhd_median_frame$NB_TYPE == near_nb,]$nbhd_median[[1]])

        if (!is.na(nb_distance)){
          if ((nb_distance < nearest_distance) & (near_nb %in% verified_nb)){
            nearest_distance <- nb_distance
            nearest_nb <- near_nb}}
        else{nearest_nb <- near_nb 
             break}

      }
      if (nb != nearest_nb){print(paste0("neighborhood: ", nb, " has been recoded to ", nearest_nb))}
      else{print(paste0("neighborhood: ", nb, " remain since there is no available nearest neighborhood"))}
    }
    df$new_nb <- ifelse(df$TOWN_CODE == target_township & df$NB_TYPE == nb, nearest_nb, df$new_nb)
  }
  return(df)
}


automated_neighborhood_recode_while <- function(df, k_list, target_township, thres=10, nbhd_col="new_nb"){
  # The function is used as the iterating loop to reassure that all the neighborhoods that fail to satisfy the given
  # size can be merged to neighborhood which satisfies the condition. For every round of the search (via automated_neighborhood_recode), 
  # if there exists neighborhoods which does not satisfy the condition, we will increase the number of nearest neighborhood
  # we are searching.
  
  # Inputs:
  #    df: (dataframe) assmntdata
  #    k_list: (list of integers) list of numbers of the nearest blocks considered
  #    target_township: (integer) ID for the target township
  #    thres: (integer) minimum rows for a given combination of neighborhood and modeling group
  #    nbhd_col: (string) the name of the new neighborhood label
  # Return: updated assmntdata

  # Example: assmntdata <- automated_neighborhood_recode_while(assmntdata, c(3,5,7,10), 17, 10)
  index <- 1
  df <- automated_neighborhood_recode(df, as.integer(k_list[[index]]), target_township, thres)
  existed_nb <- unique(subset(df, df$TOWN_CODE == target_township & modeling_group != "FIXED")[, nbhd_col])
  sub_df <- subset(df, df$TOWN_CODE == target_township & !is.na(most_recent_sale_price) & modeling_group != "FIXED")
  
  nb_count_frame <- sub_df %>% group_by(!!sym(nbhd_col)) %>% summarise(count = n())
  zero_nb <- data.frame(new_nb=existed_nb[existed_nb %ni% pull(nb_count_frame, nbhd_col)], count=rep(0, length(existed_nb[existed_nb %ni% pull(nb_count_frame, nbhd_col)])))
  nb_count_frame <- rbind(nb_count_frame, zero_nb)
  
  while (nrow(subset(nb_count_frame, nb_count_frame$count < thres)) > 0){
    
    if (index == length(k_list)){print("this is the final round, all the neighborhood (with specific modeling group) 
                                       which does not satisfied the condition will be merged to the neighborhood that
                                       contains the most cases")}
    index <- index + 1
    print(paste0("this is round ", paste(index), " for the recoding, using ", paste(k_list[[index]]), " nearest neighborhoods"))
    print(paste0("there is ", nrow(subset(nb_count_frame, nb_count_frame$count < thres)), " neighborhoods need to be merged"))
    df <- automated_neighborhood_recode(df, as.integer(k_list[[index]]), target_township, thres)
    existed_nb <- unique(subset(df, df$TOWN_CODE == target_township & modeling_group != "FIXED")[, nbhd_col])
    sub_df <- subset(df, df$TOWN_CODE == target_township & !is.na(most_recent_sale_price) & modeling_group != "FIXED")
    nb_count_frame <- sub_df %>% group_by(!!!sym(nbhd_col)) %>% summarise(count = n())
    zero_nb <- data.frame(new_nb=existed_nb[existed_nb %ni% pull(nb_count_frame, nbhd_col)], count=rep(0, length(existed_nb[existed_nb %ni% pull(nb_count_frame, nbhd_col)])))
    nb_count_frame <- rbind(nb_count_frame, zero_nb)}
  
  return(df)
}