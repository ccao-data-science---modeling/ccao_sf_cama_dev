﻿GET FILE = 'S:\2015 City_condo_valuations\Rogerspark\RogersParkD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 
*changed saleyr to 50 so that condos that were sold in FC sales won't be used to value condo's.
*Any number could be used as long as it isn't one that is already on the file for a sale year.

if saleyr=14 amount=salepric.
if saleyr=13 amount=salepric.
if saleyr=12 amount=salepric.
*if salpric<saleamt amount=saleamt.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=75.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.

IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2015 City_condo_valuations\Rogerspark\temp_t75.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\Rogerspark\temp_t75.sav'
                /by PIN10.
*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.
If (pin>10253030521000 and pin<10253030529999) newmv_median=1222776.
If (pin>10253030531000 and pin<10253030539999) newmv_median=1222776.
If (pin>10254100151000 and pin<10254100159999) newmv_median=357250.
If (pin>10254140261000 and pin<10254140269999) newmv_median=733740.
If (pin>10254250501000 and pin<10254250509999) newmv_median=1534722.
If (pin>10254270401000 and pin<10254270499999) newmv_median=415812.
If (pin>10254300111000 and pin<10254300119999) newmv_median=3860000.
If (pin>10362000461000 and pin<10362000469999) newmv_median=3514439.
If (pin>10362010311000 and pin<10362010319999) newmv_median=619444.
If (pin>10362010321000 and pin<10362010329999) newmv_median=860285.
If (pin>10362010331000 and pin<10362010339999) newmv_median=258000.
If (pin>10362060381000 and pin<10362060389999) newmv_median=194548.
If (pin>10362060411000 and pin<10362060419999) newmv_median=960337.
If (pin>10362080621000 and pin<10362080629999) newmv_median=560000.
If (pin>10362080631000 and pin<10362080639999) newmv_median=1367761.
If (pin>10362080641000 and pin<10362080649999) newmv_median=648043.
If (pin>10362090441000 and pin<10362090449999) newmv_median=551890.
If (pin>10362090461000 and pin<10362090469999) newmv_median=592000.
If (pin>10362090471000 and pin<10362090479999) newmv_median=1020000.
If (pin>10362090481000 and pin<10362090489999) newmv_median=2102218.
If (pin>10362100421000 and pin<10362100429999) newmv_median=591854.
If (pin>10362100431000 and pin<10362100439999) newmv_median=541732.
If (pin>10362100441000 and pin<10362100449999) newmv_median=1256211.
If (pin>10362100451000 and pin<10362100459999) newmv_median=660000.
If (pin>10362100461000 and pin<10362100469999) newmv_median=969584.
If (pin>10362110311000 and pin<10362110319999) newmv_median=636000.
If (pin>10362110331000 and pin<10362110339999) newmv_median=825000.
If (pin>10362160361000 and pin<10362160369999) newmv_median=501900.
If (pin>10362180441000 and pin<10362180449999) newmv_median=596370.
If (pin>10362180451000 and pin<10362180459999) newmv_median=1454382.
If (pin>10363110401000 and pin<10363110469999) newmv_median=452000.
If (pin>10363160371000 and pin<10363160379999) newmv_median=778894.
If (pin>10363170391000 and pin<10363170399999) newmv_median=559105.
If (pin>10363170411000 and pin<10363170419999) newmv_median=761750.
If (pin>10363180341000 and pin<10363180349999) newmv_median=649036.
If (pin>10363180371000 and pin<10363180379999) newmv_median=1547906.
If (pin>10363190321000 and pin<10363190329999) newmv_median=605882.
If (pin>10363190331000 and pin<10363190339999) newmv_median=1251158.
If (pin>10363200511000 and pin<10363200519999) newmv_median=664400.
If (pin>10363200521000 and pin<10363200529999) newmv_median=480300.
If (pin>10363200531000 and pin<10363200539999) newmv_median=545570.
If (pin>10363200561000 and pin<10363200579999) newmv_median=528000.
If (pin>10363200581000 and pin<10363200589999) newmv_median=528000.
If (pin>10363200591000 and pin<10363200599999) newmv_median=513400.
If (pin>10363200601000 and pin<10363200609999) newmv_median=842697.
If (pin>10363200621000 and pin<10363200629999) newmv_median=1750000.
If (pin>10363210711000 and pin<10363210719999) newmv_median=781415.
If (pin>10363230591000 and pin<10363230599999) newmv_median=2298140.
If (pin>10363250311000 and pin<10363250319999) newmv_median=482660.
If (pin>10363260451000 and pin<10363260459999) newmv_median=1193218.
If (pin>10363260481000 and pin<10363260489999) newmv_median=880000.
If (pin>10364000411000 and pin<10364000419999) newmv_median=472296.
If (pin>10364050391000 and pin<10364050399999) newmv_median=2331730.
If (pin>10364130381000 and pin<10364130389999) newmv_median=253120.
If (pin>10364140331000 and pin<10364140339999) newmv_median=932602.
If (pin>10364160411000 and pin<10364160419999) newmv_median=659456.
If (pin>10364160441000 and pin<10364160449999) newmv_median=480984.
If (pin>10364160451000 and pin<10364160459999) newmv_median=588888.
If (pin>10364160461000 and pin<10364160469999) newmv_median=476292.
If (pin>10364160481000 and pin<10364160489999) newmv_median=738000.
If (pin>10364160491000 and pin<10364160499999) newmv_median=699620.
If (pin>10364160501000 and pin<10364160509999) newmv_median=701888.
If (pin>10364170351000 and pin<10364170359999) newmv_median=2537600.
If (pin>10364170371000 and pin<10364170379999) newmv_median=1480263.
If (pin>10364190371000 and pin<10364190379999) newmv_median=540000.
If (pin>10364240341000 and pin<10364240349999) newmv_median=2036936.
If (pin>10364240381000 and pin<10364240389999) newmv_median=911685.
If (pin>10364280341000 and pin<10364280349999) newmv_median=792563.
If (pin>10364280361000 and pin<10364280369999) newmv_median=1463580.
If (pin>11291020501000 and pin<11291020509999) newmv_median=1125240.
If (pin>11291030231000 and pin<11291030239999) newmv_median=1143076.
If (pin>11291030241000 and pin<11291030249999) newmv_median=697752.
If (pin>11291030261000 and pin<11291030269999) newmv_median=647058.
If (pin>11291030271000 and pin<11291030279999) newmv_median=1159771.
If (pin>11291060221000 and pin<11291060229999) newmv_median=1157169.
If (pin>11291060271000 and pin<11291060279999) newmv_median=496050.
If (pin>11291060321000 and pin<11291060329999) newmv_median=750900.
If (pin>11291060341000 and pin<11291060349999) newmv_median=969131.
If (pin>11291060361000 and pin<11291060369999) newmv_median=1609311.
If (pin>11291060381000 and pin<11291060389999) newmv_median=828880.
If (pin>11291060391000 and pin<11291060399999) newmv_median=893181.
If (pin>11291070331000 and pin<11291070339999) newmv_median=1012183.
If (pin>11291070381000 and pin<11291070389999) newmv_median=4413490.
If (pin>11291070391000 and pin<11291070399999) newmv_median=1223404.

*Check this building for an error with comma placement. 399.
If (pin>11291090151000 and pin<11291090159999) newmv_median=1287005.


If (pin>11293070231000 and pin<11293070239999) newmv_median=1070833.
If (pin>11293070241000 and pin<11293070249999) newmv_median=1239978.
If (pin>11293070251000 and pin<11293070259999) newmv_median=3665654.
If (pin>11293080161000 and pin<11293080169999) newmv_median=1580136.
If (pin>11293110271000 and pin<11293110279999) newmv_median=1242630.
If (pin>11293110281000 and pin<11293110289999) newmv_median=881662.
If (pin>11293120151000 and pin<11293120159999) newmv_median=845530.
If (pin>11293130171000 and pin<11293130179999) newmv_median=1249740.
If (pin>11293140421000 and pin<11293140429999) newmv_median=1341256.
If (pin>11293150161000 and pin<11293150169999) newmv_median=900000.
If (pin>11293150171000 and pin<11293150179999) newmv_median=1369010.
If (pin>11293150181000 and pin<11293150189999) newmv_median=731250.
If (pin>11293160261000 and pin<11293160269999) newmv_median=1538061.
If (pin>11293180191000 and pin<11293180199999) newmv_median=795000.
If (pin>11293190191000 and pin<11293190199999) newmv_median=946642.
If (pin>11293190201000 and pin<11293190209999) newmv_median=1385075.
If (pin>11293200491000 and pin<11293200499999) newmv_median=680000.
If (pin>11293200501000 and pin<11293200509999) newmv_median=743040.
If (pin>11302050271000 and pin<11302050279999) newmv_median=1060500.
If (pin>11302050281000 and pin<11302050289999) newmv_median=1459847.
If (pin>11302150151000 and pin<11302150159999) newmv_median=908928.
If (pin>11302150181000 and pin<11302150189999) newmv_median=502300.
If (pin>11302160151000 and pin<11302160159999) newmv_median=794666.
If (pin>11302160161000 and pin<11302160169999) newmv_median=650670.
If (pin>11302160171000 and pin<11302160179999) newmv_median=817800.

*Is this the treasurer's house?.
If (pin>11302170201000 and pin<11302170209999) newmv_median=666273.

If (pin>11302170211000 and pin<11302170219999) newmv_median=1036442.
If (pin>11302170241000 and pin<11302170249999) newmv_median=651724.
If (pin>11302170251000 and pin<11302170259999) newmv_median=625000.
If (pin>11302180331000 and pin<11302180339999) newmv_median=7909744.
If (pin>11303010441000 and pin<11303010449999) newmv_median=355666.
If (pin>11303010451000 and pin<11303010459999) newmv_median=1410690.
If (pin>11303030651000 and pin<11303030659999) newmv_median=809609.
If (pin>11303030671000 and pin<11303030679999) newmv_median=675182.
If (pin>11303030681000 and pin<11303030689999) newmv_median=586500.
If (pin>11303030691000 and pin<11303030699999) newmv_median=514486.
If (pin>11303030701000 and pin<11303030709999) newmv_median=829813.
If (pin>11303060291000 and pin<11303060299999) newmv_median=854220.
If (pin>11303072141000 and pin<11303072149999) newmv_median=.
If (pin>11303072161000 and pin<11303072169999) newmv_median=.
If (pin>11303072181000 and pin<11303072189999) newmv_median=.
If (pin>11303072191000 and pin<11303072199999) newmv_median=.
If (pin>11303072201000 and pin<11303072209999) newmv_median=.
If (pin>11303080271000 and pin<11303080279999) newmv_median=.
If (pin>11303100301000 and pin<11303100309999) newmv_median=.
If (pin>11303100341000 and pin<11303100349999) newmv_median=.
If (pin>11303100351000 and pin<11303100359999) newmv_median=.
If (pin>11303100371000 and pin<11303100379999) newmv_median=.
If (pin>11303110231000 and pin<11303110239999) newmv_median=.
If (pin>11303110251000 and pin<11303110259999) newmv_median=.
If (pin>11303110271000 and pin<11303110279999) newmv_median=.
If (pin>11303120231000 and pin<11303120239999) newmv_median=.
If (pin>11303120241000 and pin<11303120249999) newmv_median=.
If (pin>11303120251000 and pin<11303120259999) newmv_median=.
If (pin>11303120261000 and pin<11303120269999) newmv_median=.
If (pin>11303120271000 and pin<11303120279999) newmv_median=.
If (pin>11303120291000 and pin<11303120299999) newmv_median=.
If (pin>11303120301000 and pin<11303120319999) newmv_median=.
If (pin>11303130231000 and pin<11303130239999) newmv_median=.
If (pin>11303140131000 and pin<11303140139999) newmv_median=.
If (pin>11303150191000 and pin<11303150199999) newmv_median=.
If (pin>11303150221000 and pin<11303150229999) newmv_median=.
If (pin>11303150241000 and pin<11303150249999) newmv_median=.
If (pin>11303150261000 and pin<11303150269999) newmv_median=.
If (pin>11303160181000 and pin<11303160189999) newmv_median=.
If (pin>11303160201000 and pin<11303160209999) newmv_median=.
If (pin>11303160211000 and pin<11303160219999) newmv_median=.
If (pin>11303170451000 and pin<11303170459999) newmv_median=.
If (pin>11303170461000 and pin<11303170469999) newmv_median=.
If (pin>11303170481000 and pin<11303170489999) newmv_median=.
If (pin>11303170491000 and pin<11303170499999) newmv_median=.
If (pin>11303170511000 and pin<11303170519999) newmv_median=.
If (pin>11303190311000 and pin<11303190319999) newmv_median=.

If (pin>11303190361000 and pin<11303190369999) newmv_median=.

If (pin>11303190371000 and pin<11303190379999) newmv_median=.
If (pin>11303200401000 and pin<11303200409999) newmv_median=.
If (pin>11303200411000 and pin<11303200419999) newmv_median=.
If (pin>11303210661000 and pin<11303210669999) newmv_median=.
If (pin>11303220381000 and pin<11303220389999) newmv_median=.
If (pin>11303220421000 and pin<11303220429999) newmv_median=.
If (pin>11303220431000 and pin<11303220439999) newmv_median=.
If (pin>11303230641000 and pin<11303230649999) newmv_median=.
If (pin>11303231051000 and pin<11303231059999) newmv_median=.
If (pin>11304010321000 and pin<11304010329999) newmv_median=.
If (pin>11304010331000 and pin<11304010339999) newmv_median=.
If (pin>11304010341000 and pin<11304010349999) newmv_median=.
If (pin>11304030431000 and pin<11304030439999) newmv_median=.
If (pin>11304060251000 and pin<11304060259999) newmv_median=.
If (pin>11304060261000 and pin<11304060269999) newmv_median=.
If (pin>11304060271000 and pin<11304060279999) newmv_median=.
If (pin>11304060291000 and pin<11304060299999) newmv_median=.
If (pin>11304060301000 and pin<11304060309999) newmv_median=.
If (pin>11304080771000 and pin<11304080779999) newmv_median=.
If (pin>11304080781000 and pin<11304080789999) newmv_median=.
If (pin>11304080791000 and pin<11304080799999) newmv_median=.
If (pin>11304080821000 and pin<11304080829999) newmv_median=.
If (pin>11304080861000 and pin<11304080869999) newmv_median=.
If (pin>11304130291000 and pin<11304130319999) newmv_median=.
If (pin>11304140271000 and pin<11304140279999) newmv_median=.
If (pin>11304140311000 and pin<11304140319999) newmv_median=.
If (pin>11304150391000 and pin<11304150399999) newmv_median=.
If (pin>11304160241000 and pin<11304160249999) newmv_median=.
If (pin>11304160251000 and pin<11304160259999) newmv_median=.
If (pin>11304170081000 and pin<11304170089999) newmv_median=.
If (pin>11304180391000 and pin<11304180399999) newmv_median=.
If (pin>11304180411000 and pin<11304180419999) newmv_median=.
If (pin>11304190321000 and pin<11304190329999) newmv_median=.
If (pin>11304200661000 and pin<11304200669999) newmv_median=.
If (pin>11304200681000 and pin<11304200689999) newmv_median=.
If (pin>11304200691000 and pin<11304200699999) newmv_median=.
If (pin>11304200701000 and pin<11304200709999) newmv_median=.
If (pin>11304200731000 and pin<11304200739999) newmv_median=.
If (pin>11304200741000 and pin<11304200749999) newmv_median=.
If (pin>11304220281000 and pin<11304220289999) newmv_median=.
If (pin>11304220301000 and pin<11304220309999) newmv_median=.
If (pin>11304220311000 and pin<11304220319999) newmv_median=.
If (pin>11304220331000 and pin<11304220339999) newmv_median=.
If (pin>11304230351000 and pin<11304230359999) newmv_median=.
If (pin>11304240301000 and pin<11304240309999) newmv_median=.
If (pin>11304240391000 and pin<11304240399999) newmv_median=.
If (pin>11311030311000 and pin<11311030319999) newmv_median=.
If (pin>11311040441000 and pin<11311040449999) newmv_median=.
If (pin>11311070281000 and pin<11311070289999) newmv_median=.
If (pin>11311070291000 and pin<11311070299999) newmv_median=.
If (pin>11311120321000 and pin<11311120329999) newmv_median=.
If (pin>11311130281000 and pin<11311130289999) newmv_median=.
If (pin>11311130561000 and pin<11311130569999) newmv_median=.
If (pin>11311140231000 and pin<11311140239999) newmv_median=.
If (pin>11311140241000 and pin<11311140249999) newmv_median=.
If (pin>11311150441000 and pin<11311150449999) newmv_median=.
If (pin>11311150451000 and pin<11311150459999) newmv_median=.
If (pin>11311160461000 and pin<11311160469999) newmv_median=.
If (pin>11311160481000 and pin<11311160489999) newmv_median=.
If (pin>11311160491000 and pin<11311160499999) newmv_median=.
If (pin>11311160511000 and pin<11311160519999) newmv_median=.
If (pin>11311160521000 and pin<11311160529999) newmv_median=.
If (pin>11311160531000 and pin<11311160539999) newmv_median=.
*multiple buildings.
If (pin>11311170201000 and pin<11311170239999) newmv_median=.


If (pin>11311170241000 and pin<11311170249999) newmv_median=.
If (pin>11311170251000 and pin<11311170259999) newmv_median=.
If (pin>11311170261000 and pin<11311170269999) newmv_median=.
If (pin>11311170271000 and pin<11311170279999) newmv_median=.
If (pin>11311170281000 and pin<11311170289999) newmv_median=.
If (pin>11311170291000 and pin<11311170299999) newmv_median=.
If (pin>11311200561000 and pin<11311200569999) newmv_median=.
If (pin>11311200571000 and pin<11311200579999) newmv_median=.
If (pin>11311200651000 and pin<11311200659999) newmv_median=.
*multiple buildings.
If (pin>11311210221000 and pin<11311210249999) newmv_median=.
If (pin>11311220251000 and pin<11311220259999) newmv_median=.
If (pin>11311220311000 and pin<11311220319999) newmv_median=.
If (pin>11311240171000 and pin<11311240179999) newmv_median=.
If (pin>11311240181000 and pin<11311240189999) newmv_median=.
If (pin>11311240191000 and pin<11311240199999) newmv_median=.
If (pin>11311250171000 and pin<11311250179999) newmv_median=.
If (pin>11312000321000 and pin<11312000329999) newmv_median=.
If (pin>11312000341000 and pin<11312000349999) newmv_median=.
If (pin>11312000361000 and pin<11312000369999) newmv_median=.
If (pin>11312020291000 and pin<11312020299999) newmv_median=.
If (pin>11312020301000 and pin<11312020309999) newmv_median=.
If (pin>11312020311000 and pin<11312020319999) newmv_median=.
If (pin>11312030221000 and pin<11312030229999) newmv_median=.
If (pin>11312040211000 and pin<11312040219999) newmv_median=.
If (pin>11312040221000 and pin<11312040229999) newmv_median=.
If (pin>11312080301000 and pin<11312080309999) newmv_median=.
If (pin>11312080331000 and pin<11312080339999) newmv_median=.
If (pin>11312080361000 and pin<11312080369999) newmv_median=.
If (pin>11312080411000 and pin<11312080419999) newmv_median=3320000.
If (pin>11312090221000 and pin<11312090229999) newmv_median=.
If (pin>11312100381000 and pin<11312100389999) newmv_median=.
If (pin>11312100391000 and pin<11312100399999) newmv_median=.
If (pin>11312130391000 and pin<11312130399999) newmv_median=.
If (pin>11312130411000 and pin<11312130419999) newmv_median=.
If (pin>11312140551000 and pin<11312140559999) newmv_median=.
If (pin>11312140561000 and pin<11312140569999) newmv_median=.
If (pin>11312140581000 and pin<11312140589999) newmv_median=.
If (pin>11312180381000 and pin<11312180389999) newmv_median=.
If (pin>11312210291000 and pin<11312210299999) newmv_median=.
If (pin>11312220351000 and pin<11312220359999) newmv_median=.
If (pin>11312220401000 and pin<11312220409999) newmv_median=.
If (pin>11312220411000 and pin<11312220419999) newmv_median=.
If (pin>11312230351000 and pin<11312230359999) newmv_median=.
If (pin>11312260311000 and pin<11312260319999) newmv_median=.

*Very Large Block.
If (pin>11313010451000 and pin<11313010459999) newmv_median=388000.

If (pin>11313050081000 and pin<11313050089999) newmv_median=.
If (pin>11313060071000 and pin<11313060079999) newmv_median=.
If (pin>11313080071000 and pin<11313080079999) newmv_median=.
If (pin>11313090071000 and pin<11313090079999) newmv_median=.
If (pin>11313100061000 and pin<11313100069999) newmv_median=.
If (pin>11313110091000 and pin<11313110099999) newmv_median=.
If (pin>11313120451000 and pin<11313120459999) newmv_median=.
If (pin>11313120471000 and pin<11313120479999) newmv_median=.
If (pin>11313130381000 and pin<11313130389999) newmv_median=.
If (pin>11313140371000 and pin<11313140379999) newmv_median=.
If (pin>11313140381000 and pin<11313140389999) newmv_median=.
If (pin>11313160441000 and pin<11313160449999) newmv_median=.
If (pin>11313160471000 and pin<11313160479999) newmv_median=.
If (pin>11313160541000 and pin<11313160549999) newmv_median=.
If (pin>11313160561000 and pin<11313160569999) newmv_median=.
If (pin>11313160571000 and pin<11313160579999) newmv_median=.
If (pin>11313170351000 and pin<11313170359999) newmv_median=.
If (pin>11313190391000 and pin<11313190399999) newmv_median=.
If (pin>11313190401000 and pin<11313190409999) newmv_median=.
If (pin>11313190411000 and pin<11313190419999) newmv_median=.
If (pin>11314010911000 and pin<11314010919999) newmv_median=.
If (pin>11314011041000 and pin<11314011049999) newmv_median=.
If (pin>11314020881000 and pin<11314020889999) newmv_median=.
If (pin>11314080191000 and pin<11314080199999) newmv_median=.
If (pin>11314080231000 and pin<11314080239999) newmv_median=.
If (pin>11314090281000 and pin<11314090289999) newmv_median=.
If (pin>11314110191000 and pin<11314110199999) newmv_median=.
If (pin>11314110211000 and pin<11314110219999) newmv_median=.

*just check it.
If (pin>11314110221000 and pin<11314110229999) newmv_median=.

If (pin>11314140521000 and pin<11314140529999) newmv_median=.
If (pin>11314140531000 and pin<11314140539999) newmv_median=.
If (pin>11314180191000 and pin<11314180199999) newmv_median=.
If (pin>11321020171000 and pin<11321020179999) newmv_median=.
If (pin>11321020181000 and pin<11321020189999) newmv_median=.
If (pin>11321040341000 and pin<11321040349999) newmv_median=.
If (pin>11321040351000 and pin<11321040359999) newmv_median=.
If (pin>11321040361000 and pin<11321040369999) newmv_median=.
If (pin>11321040371000 and pin<11321040379999) newmv_median=.
If (pin>11321040391000 and pin<11321040399999) newmv_median=.
If (pin>11321060281000 and pin<11321060289999) newmv_median=.
If (pin>11321090211000 and pin<11321090219999) newmv_median=.
If (pin>11321100361000 and pin<11321100369999) newmv_median=.
If (pin>11321100401000 and pin<11321100409999) newmv_median=.
If (pin>11321120241000 and pin<11321120249999) newmv_median=.
If (pin>11321140311000 and pin<11321140319999) newmv_median=.
If (pin>11321140321000 and pin<11321140329999) newmv_median=.
If (pin>11321140341000 and pin<11321140349999) newmv_median=.
If (pin>11321140371000 and pin<11321140379999) newmv_median=.
If (pin>11321150201000 and pin<11321150209999) newmv_median=.
If (pin>11321150211000 and pin<11321150219999) newmv_median=.
If (pin>11321150251000 and pin<11321150259999) newmv_median=.
If (pin>11321160291000 and pin<11321160299999) newmv_median=.
If (pin>11321190201000 and pin<11321190209999) newmv_median=.
If (pin>11321190291000 and pin<11321190299999) newmv_median=.
If (pin>11321200381000 and pin<11321200389999) newmv_median=.
If (pin>11321200391000 and pin<11321200399999) newmv_median=.
If (pin>11321200401000 and pin<11321200409999) newmv_median=.
If (pin>11321230251000 and pin<11321230259999) newmv_median=.
If (pin>11321230251000 and pin<11321230259999) newmv_median=.
If (pin>11321240261000 and pin<11321240269999) newmv_median=.
If (pin>11321240271000 and pin<11321240279999) newmv_median=.
If (pin>11321240281000 and pin<11321240289999) newmv_median=.
If (pin>11321240301000 and pin<11321240309999) newmv_median=.
If (pin>11321240311000 and pin<11321240319999) newmv_median=.
If (pin>11321240391000 and pin<11321240399999) newmv_median=.
If (pin>11322000341000 and pin<11322000349999) newmv_median=.
If (pin>11322000351000 and pin<11322000359999) newmv_median=.
If (pin>11322000391000 and pin<11322000399999) newmv_median=.
If (pin>11322000401000 and pin<11322000409999) newmv_median=.
If (pin>11322000421000 and pin<11322000429999) newmv_median=.
If (pin>11322010271000 and pin<11322010279999) newmv_median=.
If (pin>11322010291000 and pin<11322010299999) newmv_median=.
If (pin>11322020211000 and pin<11322020219999) newmv_median=.
If (pin>11322020301000 and pin<11322020309999) newmv_median=.
If (pin>11323000261000 and pin<11323000269999) newmv_median=.
If (pin>11323000271000 and pin<11323000279999) newmv_median=.
If (pin>11323010191000 and pin<11323010199999) newmv_median=.
If (pin>11323030281000 and pin<11323030289999) newmv_median=.
If (pin>11323040301000 and pin<11323040309999) newmv_median=.
If (pin>11323060171000 and pin<11323060179999) newmv_median=.
If (pin>11323060201000 and pin<11323060209999) newmv_median=.
If (pin>11323070321000 and pin<11323070329999) newmv_median=.
If (pin>11323100371000 and pin<11323100379999) newmv_median=.
If (pin>11323110271000 and pin<11323110279999) newmv_median=.
If (pin>11323110281000 and pin<11323110289999) newmv_median=.

*check.
If (pin>11323110301000 and pin<11323110309999) newmv_median=.

If (pin>11323120161000 and pin<11323120169999) newmv_median=.
If (pin>11323120191000 and pin<11323120199999) newmv_median=.
If (pin>11323120201000 and pin<11323120209999) newmv_median=.
If (pin>11323120211000 and pin<11323120219999) newmv_median=.
If (pin>11323130341000 and pin<11323130349999) newmv_median=.
If (pin>11323130371000 and pin<11323130379999) newmv_median=.
If (pin>11323130381000 and pin<11323130389999) newmv_median=.
If (pin>11323140341000 and pin<11323140349999) newmv_median=.
If (pin>11323140351000 and pin<11323140359999) newmv_median=.
If (pin>11323140401000 and pin<11323140409999) newmv_median=.
If (pin>11323140411000 and pin<11323140419999) newmv_median=.
If (pin>11323150371000 and pin<11323150379999) newmv_median=.
If (pin>11323150381000 and pin<11323150389999) newmv_median=.
If (pin>11323140341000 and pin<11323140349999) newmv_median=.
If (pin>11323140351000 and pin<11323140359999) newmv_median=.
If (pin>11323140401000 and pin<11323140409999) newmv_median=.
If (pin>11323140411000 and pin<11323140419999) newmv_median=.
If (pin>11323150371000 and pin<11323150379999) newmv_median=.
If (pin>11323150381000 and pin<11323150389999) newmv_median=.
If (pin>11323150421000 and pin<11323150429999) newmv_median=.
If (pin>11323170281000 and pin<11323170289999) newmv_median=.
If (pin>11323170301000 and pin<11323170309999) newmv_median=.
If (pin>11323180131000 and pin<11323180139999) newmv_median=.
If (pin>11323180141000 and pin<11323180149999) newmv_median=.
If (pin>11323180161000 and pin<11323180169999) newmv_median=.
If (pin>11323200271000 and pin<11323200279999) newmv_median=.
If (pin>11323210321000 and pin<11323210329999) newmv_median=.
If (pin>11323230161000 and pin<11323230169999) newmv_median=.
If (pin>11323230171000 and pin<11323230179999) newmv_median=.
If (pin>11323260301000 and pin<11323260309999) newmv_median=.
If (pin>11323270271000 and pin<11323270279999) newmv_median=.
If (pin>11323270301000 and pin<11323270309999) newmv_median=.
If (pin>11323300461000 and pin<11323300469999) newmv_median=.
If (pin>11324000361000 and pin<11324000369999) newmv_median=.
*check.
If (pin>11324000371000 and pin<11324000379999) newmv_median=.

If (pin>11324000391000 and pin<11324000399999) newmv_median=.
If (pin>11324000411000 and pin<11324000419999) newmv_median=.
If (pin>11324000421000 and pin<11324000429999) newmv_median=.
If (pin>11324010441000 and pin<11324010449999) newmv_median=.
If (pin>11324010461000 and pin<11324010469999) newmv_median=.
If (pin>11324010471000 and pin<11324010479999) newmv_median=.
If (pin>11324010511000 and pin<11324010519999) newmv_median=.
If (pin>11324010521000 and pin<11324010529999) newmv_median=.
If (pin>11324020371000 and pin<11324020379999) newmv_median=.
If (pin>11324030191000 and pin<11324030199999) newmv_median=.
If (pin>11324030201000 and pin<11324030209999) newmv_median=.
If (pin>11324040181000 and pin<11324040189999) newmv_median=.
If (pin>11324040191000 and pin<11324040199999) newmv_median=.



If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.

*399.
If (pin>11312210311000 and pin<11312210319999) newmv_median=1630000.
If (pin>11321200351000 and pin<11321200359999) newmv_median=630978.
If (pin>11323170311000 and pin<11323170319999) newmv_median=3044740.
*599.
If (pin>10361010441000 and pin<10361010449999) newmv_median=795723.
If (pin>10363270331000 and pin<10363270339999) newmv_median=743800.
If (pin>11304040261000 and pin<11304040269999) newmv_median=5977167.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.
If (pin>1000 and pin<9999) newmv_median=.



*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
*compute newmv_median=newmv_median+(.12*oldmv).
*if (pin=1001) newmv_median=.
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.

If (pin>10362110321000 and pin<10362110329999) newmv_median=1166530.
If (pin>10362180461000 and pin<10362180469999) newmv_median=491456.
If (pin>10363190511000 and pin<10363190519999) newmv_median=2046727.
If (pin>10363230601000 and pin<10363230609999) newmv_median=865356.
If (pin>10363260461000 and pin<10363260469999) newmv_median=536602.
If (pin>11303072121000 and pin<11303072129999) newmv_median=2810156.


Compute newav = newmv_median*percent*.10.
if class=599 newav=newmv_median*percent*.10.
if class=589 newav=newmv_median*percent*.10.
if class=399 newav=newmv_median*percent*.10.
if class=499 newav=newmv_median*percent*.10.
if class=899 newav=newmv_median*percent*.10.
if class=679 newav=newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=1001 newmv=9999.

Save OUTFILE = 'S:\2015 City_condo_valuations\Rogerspark\condos75.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'Rogers Park CONDOS' 'USING 2012-2014 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'Lake'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'Rogers Park ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.






