﻿GET FILE = 'S:\2015 City_condo_valuations\lake\lakeD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 

if (saleyr ge 30) saleyr=saleyr+1900.
execute.
if (saleyr lt 30) saleyr=saleyr+2000.
execute.

compute saledate= date.mdy (salemo,saleday,saleyr).
Formats saledate (date).
execute.

compute fc_indicator=0.
if (RANGE (filing_date,date.dmy(1,1,2012),date.dmy(31,12,2015)) and (saledate ge filing_date)) fc_indicator=1.
if (RANGE (auction_date,date.dmy(1,1,2012),date.dmy(31,12,2015)) and (saledate ge auction_date)) fc_indicator=1.

if (fc_indicator=1) saleyr=0.


if saleyr=2014 amount=salepric.
if saleyr=2013 amount=salepric.
if saleyr=2012 amount=salepric.
*if salpric<saleamt amount=saleamt.

*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=72.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.
IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2015 City_condo_valuations\lake\temp_t72sales.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\lake\temp_t72sales.sav'
                /by PIN10.
SORT CASES by Pin.
Save Outfile = 'S:\2015 City_condo_valuations\lake\SalesOutput.sav'.

SELECT IF MISSING(newmv_median) or newmv_median<.55*oldmv.
execute.

Compute newmortv=(mort/1)/percent.
**************************The division by .8 is the assumption that the mortgage represents 80% of value with 20% down****************.

Format newmortv (comma9.0).
SORT CASES by Pin10.
Aggregate
                /outfile= 'S:\2015 City_condo_valuations\lake\temp_t72Morts.sav'
               /break=PIN10
               /newmortv_median=median(newmortv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\lake\temp_t72Morts.sav'
                /by PIN10.
sort cases by pin.
Save Outfile='S:\2015 City_condo_valuations\lake\MortsOutput.sav'.

MATCH FILES FILE= 'S:\2015 City_condo_valuations\lake\MortsOutput.sav'
           /FILE= 'S:\2015 City_condo_valuations\lake\SalesOutput.sav'
            /BY=PIN.
sort cases by pin.
Save Outfile=  'S:\2015 City_condo_valuations\lake\SalesandMortsOutput.sav'.

if (newmv_median<(.55*oldmv)) or (MISSING(newmv_median))  newmv_median=newmortv_median.



*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.
If (pin>19012151641000 and pin<19012151769999) newmv_median=568182.
If (pin>19023110471000 and pin<19023110479999) newmv_median=1043588.
If (pin>19023110491000 and pin<19023110499999) newmv_median=867454.
If (pin>19084241321000 and pin<19084241329999) newmv_median=815033.
If (pin>19084241381000 and pin<19084241389999) newmv_median=664946.
If (pin>19084260181000 and pin<19084260199999) newmv_median=307692.
If (pin>19084270111000 and pin<19084270119999) newmv_median=307692.
If (pin>19084270121000 and pin<19084270129999) newmv_median=466532.
If (pin>19093140301000 and pin<19093140309999) newmv_median=1149488.
If (pin>19094060181000 and pin<19094060189999) newmv_median=534097.
If (pin>19094060191000 and pin<19094060199999) newmv_median=534097.
If (pin>19094060201000 and pin<19094060209999) newmv_median=534097.
If (pin>19094090611000 and pin<19094090619999) newmv_median=554120.
If (pin>19094110411000 and pin<19094110419999) newmv_median=612771.
If (pin>19103240911000 and pin<19103240949999) newmv_median=251592.
If (pin>19104050591000 and pin<19104050599999) newmv_median=328140.
If (pin>19111020411000 and pin<19111020419999) newmv_median=927452.
If (pin>19113330441000 and pin<19113330469999) newmv_median=422776.
If (pin>19114180471000 and pin<19114180479999) newmv_median=649962.
If (pin>19114200621000 and pin<19114200629999) newmv_median=308000.
If (pin>19133240361000 and pin<19133240369999) newmv_median=786939.
If (pin>19143280421000 and pin<19143280429999) newmv_median=583736.
If (pin>19143280451000 and pin<19143280459999) newmv_median=210320.
If (pin>19143280471000 and pin<19143280479999) newmv_median=572441.
If (pin>19151070451000 and pin<19151070459999) newmv_median=1080448.
If (pin>19151190341000 and pin<19151190419999) newmv_median=312572.
If (pin>19153080381000 and pin<19153080389999) newmv_median=665481.
If (pin>19153180331000 and pin<19153180339999) newmv_median=442270.
If (pin>19153240461000 and pin<19153240479999) newmv_median=348432.
If (pin>19154230391000 and pin<19154230399999) newmv_median=308300.
If (pin>19172050411000 and pin<19172050419999) newmv_median=572365.
If (pin>19173290421000 and pin<19173290429999) newmv_median=2160034.
If (pin>19174300441000 and pin<19174300449999) newmv_median=1261329.
If (pin>19181080471000 and pin<19181080479999) newmv_median=1054687.
If (pin>19182150381000 and pin<19182150389999) newmv_median=1381554.
If (pin>19182150421000 and pin<19182150429999) newmv_median=1083978.
If (pin>19183120481000 and pin<19183120489999) newmv_median=1364987.
If (pin>19183120521000 and pin<19183120529999) newmv_median=2277542.
If (pin>19184210591000 and pin<19184210599999) newmv_median=559879.
If (pin>19184290431000 and pin<19184290439999) newmv_median=392500.
If (pin>19191140361000 and pin<19191140369999) newmv_median=1162964.
If (pin>19191140391000 and pin<19191140399999) newmv_median=584642.
If (pin>19191140401000 and pin<19191140409999) newmv_median=690649.
If (pin>19192000601000 and pin<19192000609999) newmv_median=996967.
If (pin>19192010681000 and pin<19192010689999) newmv_median=406601.
If (pin>19192020761000 and pin<19192020769999) newmv_median=771300.
If (pin>19192080421000 and pin<19192080499999) newmv_median=479541.
If (pin>19192090381000 and pin<19192090489999) newmv_median=479541.
If (pin>19192080501000 and pin<19192080509999) newmv_median=2705479.
If (pin>19192110561000 and pin<19192110569999) newmv_median=550650.
If (pin>19192130161000 and pin<19192130169999) newmv_median=1214285.
If (pin>19192140261000 and pin<19192140269999) newmv_median=2332450.
If (pin>19192150191000 and pin<19192150239999) newmv_median=420000.
If (pin>19201000581000 and pin<19201000589999) newmv_median=2692740.
If (pin>19201030491000 and pin<19201030499999) newmv_median=541798.
If (pin>19201120481000 and pin<19201120489999) newmv_median=676738.
If (pin>19201120491000 and pin<19201120499999) newmv_median=592672.
If (pin>19201140321000 and pin<19201140329999) newmv_median=1265237.
If (pin>19201140341000 and pin<19201150349999) newmv_median=1265237.
If (pin>19201140331000 and pin<19201140339999) newmv_median=559091.
If (pin>19202000501000 and pin<19202000509999) newmv_median=672877.
If (pin>19202020451000 and pin<19202020489999) newmv_median=450000.
If (pin>19222000451000 and pin<19222000459999) newmv_median=471270.
If (pin>19222010411000 and pin<19222010419999) newmv_median=529284.
If (pin>19222060411000 and pin<19222060419999) newmv_median=556453.
If (pin>19223000171000 and pin<19223000179999) newmv_median=1791415.
If (pin>19233080411000 and pin<19233080419999) newmv_median=867437.
If (pin>19233120391000 and pin<19233120399999) newmv_median=629213.
If (pin>19233200431000 and pin<19233200439999) newmv_median=879310.
If (pin>19241270291000 and pin<19241270299999) newmv_median=400768.
If (pin>19251160601000 and pin<19251160609999) newmv_median=496171.
If (pin>19342150851000 and pin<19342150859999) newmv_median=1134531.
If (pin>19343190341000 and pin<19343190349999) newmv_median=1134531.
If (pin>19342061471000 and pin<19342150849999) newmv_median=1020702.
If (pin>19342150871000 and pin<19342150879999) newmv_median=1020702.
If (pin>19344300421000 and pin<19344300459999) newmv_median=745892.
If (pin>19353260451000 and pin<19353260459999) newmv_median=257168. 
If (pin>19361000421000 and pin<19361000429999) newmv_median=579442.
If (pin>19363060251000 and pin<19363060259999) newmv_median=1004712.
If (pin>20042020341000 and pin<20042020359999) newmv_median=436139.
If (pin>20043220511000 and pin<20043220519999) newmv_median=172186.
If (pin>20043220521000 and pin<20043220529999) newmv_median=265000.
If (pin>20061001231000 and pin<20061001239999) newmv_median=17806267.
If (pin>20081080441000 and pin<20081080449999) newmv_median=1041667.
If (pin>20163130391000 and pin<20163130399999) newmv_median=442463.
If (pin>20163260281000 and pin<20163260289999) newmv_median=247650.
If (pin>20171020391000 and pin<20171020399999) newmv_median=405894.
If (pin>20202040391000 and pin<20202040399999) newmv_median=142622.
If (pin>20202280491000 and pin<20202280499999) newmv_median=169830.
If (pin>20212180481000 and pin<20212180489999) newmv_median=877120.
If (pin>20213140631000 and pin<20213140639999) newmv_median=150000.
If (pin>20213170201000 and pin<20213170209999) newmv_median=125000.
If (pin>20282060461000 and pin<20282060469999) newmv_median=343053.
If (pin>20282110351000 and pin<20282110359999) newmv_median=867311.
If (pin>20283320321000 and pin<20283320329999) newmv_median=611765.
If (pin>20304280571000 and pin<20304280579999) newmv_median=152894.
If (pin>20304310421000 and pin<20304310429999) newmv_median=1114517.
If (pin>24132230321000 and pin<24132230339999) newmv_median=840000.
If (pin>24132240451000 and pin<24132240469999) newmv_median=840000.
If (pin>24133120261000 and pin<24133120269999) newmv_median=840000.
If (pin>24134250411000 and pin<24134250419999) newmv_median=478603.
If (pin>24143000271000 and pin<24143000279999) newmv_median=2253615.
If (pin>24143000281000 and pin<24143000289999) newmv_median=1813784.
If (pin>24143160841000 and pin<24143160849999) newmv_median=1864690.
If (pin>24143160891000 and pin<24143160899999) newmv_median=1326277.
If (pin>24143170831000 and pin<24143170839999) newmv_median=424790.
If (pin>24143230281000 and pin<24143230289999) newmv_median=314761.
If (pin>24232070901000 and pin<24232070909999) newmv_median=362727.
If (pin>24242030401000 and pin<24242030409999) newmv_median=811545.
If (pin>24244090441000 and pin<24244090449999) newmv_median=897648.
If (pin>25072040151000 and pin<25072040159999) newmv_median=1704745.
If (pin>25074160741000 and pin<25074160749999) newmv_median=1719984.
If (pin>25173040481000 and pin<25173040489999) newmv_median=600000.
If (pin>25173100421000 and pin<25173100429999) newmv_median=692619.
If (pin>25182000511000 and pin<25182000519999) newmv_median=520142.
If (pin>25182020401000 and pin<25182020409999) newmv_median=1268221.
If (pin>25182020411000 and pin<25182020419999) newmv_median=2435567.
If (pin>25182060691000 and pin<25182060739999) newmv_median=228866.
If (pin>25182060751000 and pin<25182060759999) newmv_median=410981.
If (pin>25183180341000 and pin<25183180349999) newmv_median=510920.
If (pin>25191020411000 and pin<25191020419999) newmv_median=1060095.

*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
*compute newmv_median=newmv_median+(.12*oldmv).
*if (pin=14082110491008) newmv_median=32901056. 
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.



Compute newav = newmv_median*percent*.10.
if class=599 newav = newmv_median*percent*.10.
if class=589 newav = newmv_median*percent*.10.
if class=399 newav = newmv_median*percent*.10.
if class=499 newav = newmv_median*percent*.10.
if class=899 newav = newmv_median*percent*.10.
if class=679 newav = newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=1001 newmv=9999.
select if class = 299.
execute.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
Save OUTFILE = 'S:\2015 City_condo_valuations\lake\condos72.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'Lake CONDOS' 'USING 2012-2014 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'Lake'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'Lake ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.




