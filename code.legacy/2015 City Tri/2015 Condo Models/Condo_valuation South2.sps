﻿GET FILE = 'S:\2015 City_condo_valuations\South\SouthD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 

if (saleyr ge 30) saleyr=saleyr+1900.
execute.
if (saleyr lt 30) saleyr=saleyr+2000.
execute.

compute saledate= date.mdy (salemo,saleday,saleyr).
Formats saledate (date).
execute.

compute fc_indicator=0.
if (RANGE (filing_date,date.dmy(1,1,2012),date.dmy(31,12,2015)) and (saledate ge filing_date)) fc_indicator=1.
if (RANGE (auction_date,date.dmy(1,1,2012),date.dmy(31,12,2015)) and (saledate ge auction_date)) fc_indicator=1.

if (fc_indicator=1) saleyr=0.


if saleyr=2014 amount=salepric.
if saleyr=2013 amount=salepric.
if saleyr=2012 amount=salepric.
*if salpric<saleamt amount=saleamt.

*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=76.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.
IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2015 City_condo_valuations\South\temp_t76sales.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\South\temp_t76sales.sav'
                /by PIN10.
SORT CASES by Pin.
Save Outfile = 'S:\2015 City_condo_valuations\South\SalesOutput.sav'.

SELECT IF MISSING(newmv_median) or newmv_median<.55*oldmv.
execute.

Compute newmortv=(mort/1)/percent.
**************************The division by .8 is the assumption that the mortgage represents 80% of value with 20% down****************.

Format newmortv (comma9.0).
SORT CASES by Pin10.
Aggregate
                /outfile= 'S:\2015 City_condo_valuations\South\temp_t76Morts.sav'
               /break=PIN10
               /newmortv_median=median(newmortv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\South\temp_t76Morts.sav'
                /by PIN10.
sort cases by pin.
Save Outfile='S:\2015 City_condo_valuations\South\MortsOutput.sav'.

MATCH FILES FILE= 'S:\2015 City_condo_valuations\South\MortsOutput.sav'
           /FILE= 'S:\2015 City_condo_valuations\South\SalesOutput.sav'
            /BY=PIN.
sort cases by pin.
Save Outfile=  'S:\2015 City_condo_valuations\South\SalesandMortsOutput.sav'.

if (newmv_median<(.55*oldmv)) or (MISSING(newmv_median))  newmv_median=newmortv_median.



*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.
If (pin>17103180841000 and pin<17103180849999) newmv_median=18107623.
If (pin>17151030341000 and pin<17151030349999) newmv_median=111198325.
If (pin>17162470531000 and pin<17162470539999) newmv_median=7578773.
If (pin>17162130221000 and pin<17162130229999) newmv_median=7936508.
If (pin>17164060251000 and pin<17164060259999) newmv_median=5989352.
If (pin>17164190061000 and pin<17164190069999) newmv_median=15265573.
If (pin>17164240061000 and pin<17164240069999) newmv_median=1820000.
If (pin>17164240081000 and pin<17164240089999) newmv_median=1820000.
If (pin>17212101481000 and pin<17212101489999) newmv_median=127560782.
If (pin>17214350691000 and pin<17214350699999) newmv_median=2626050.
If (pin>17214360521000 and pin<17214360529999) newmv_median=1481504.
If (pin>17221010391000 and pin<17221010399999) newmv_median=1802435.
If (pin>17221050421000 and pin<17221050429999) newmv_median=5000000.
If (pin>17221050531000 and pin<17221050539999) newmv_median=72954321.
If (pin>17221060731000 and pin<17221060739999) newmv_median=2354238.
If (pin>17221060961000 and pin<17221060969999) newmv_median=1918649.
If (pin>17221070801000 and pin<17221070809999) newmv_median=82806898.
If (pin>17221101061000 and pin<17221101069999) newmv_median=6601467.
If (pin>17223010561000 and pin<17223010569999) newmv_median=1235484.
If (pin>17223010711000 and pin<17223010719999) newmv_median=1120000.
If (pin>17223060461000 and pin<17223060469999) newmv_median=6703751.
If (pin>17223060481000 and pin<17223060489999) newmv_median=5460005.
*Landmark with a 2015 sale of $1,185,000.
If (pin>17223091221000 and pin<17223091229999) newmv_median=6771429.
*OK
If (pin>17223100251000 and pin<17223100259999) newmv_median=.
If (pin>17223150661000 and pin<17223150669999) newmv_median=22643253.
*Landmark.
If (pin>17271090281000 and pin<17271090289999) newmv_median=9003382.
*Landmark.
If (pin>17271100351000 and pin<17271100359999) newmv_median=18423423.
If (pin>17271230261000 and pin<17271230269999) newmv_median=10651372.
If (pin>17271290231000 and pin<17271290239999) newmv_median=3787305.
If (pin>17273000551000 and pin<17273000619999) newmv_median=957243.
If (pin>17273051401000 and pin<17273051409999) newmv_median=22146042.
If (pin>17281050281000 and pin<17281050289999) newmv_median=2691388.
If (pin>17281150461000 and pin<17281150469999) newmv_median=10778288.
If (pin>17281250461000 and pin<17281250469999) newmv_median=3116883.
If (pin>17281320441000 and pin<17281320449999) newmv_median=886532.
If (pin>17282020541000 and pin<17282020549999) newmv_median=2457380.
If (pin>17282120301000 and pin<17282120989999) newmv_median=515196.
If (pin>17282220591000 and pin<17282220609999) newmv_median=750000.
If (pin>17282230491000 and pin<17282230499999) newmv_median=2893158.
If (pin>17282330761000 and pin<17282330769999) newmv_median=3211111.
If (pin>17282330771000 and pin<17282330779999) newmv_median=745629.
If (pin>17282330781000 and pin<17282330789999) newmv_median=739898.
If (pin>17282370381000 and pin<17282370389999) newmv_median=19060375.
If (pin>17283010591000 and pin<17283010599999) newmv_median=700000.
If (pin>17283030481000 and pin<17283030489999) newmv_median=700746.
If (pin>17283030551000 and pin<17283030559999) newmv_median=1695760.
If (pin>17283100491000 and pin<17283100499999) newmv_median=1131428.
If (pin>17283120511000 and pin<17283120519999) newmv_median=570127.
If (pin>17284010441000 and pin<17284010449999) newmv_median=430102.
If (pin>17284030321000 and pin<17284030329999) newmv_median=581818.
If (pin>17284140261000 and pin<17284140269999) newmv_median=6143958.
If (pin>17284320301000 and pin<17284320309999) newmv_median=623096.
If (pin>17284360481000 and pin<17284360489999) newmv_median=2863128.
If (pin>17294230601000 and pin<17294230609999) newmv_median=1000000.
If (pin>17312270511000 and pin<17312270519999) newmv_median=913705.
If (pin>17313000341000 and pin<17313000349999) newmv_median=858085.
If (pin>17313060701000 and pin<17313060709999) newmv_median=1689342.
If (pin>17313130441000 and pin<17313130449999) newmv_median=365184.
If (pin>17313130451000 and pin<17313130469999) newmv_median=675770.
If (pin>17313160491000 and pin<17313160499999) newmv_median=616258.
If (pin>17314160441000 and pin<17314160449999) newmv_median=616258.
If (pin>17321030551000 and pin<17321030559999) newmv_median=562500.
If (pin>17322020451000 and pin<17322020459999) newmv_median=585000.
If (pin>17322030471000 and pin<17322030479999) newmv_median=634105.
If (pin>17322162051000 and pin<17322162059999) newmv_median=449746.
If (pin>17322200481000 and pin<17322200489999) newmv_median=600000.
If (pin>17322220471000 and pin<17322220479999) newmv_median=792253.
If (pin>17322230481000 and pin<17322230489999) newmv_median=541400.
If (pin>17324030441000 and pin<17324030459999) newmv_median=975437.
If (pin>17324070391000 and pin<17324070399999) newmv_median=887966.
If (pin>17331010441000 and pin<17331010499999) newmv_median=739726.
If (pin>17331010501000 and pin<17331010509999) newmv_median=876627.
If (pin>17331010511000 and pin<17331010519999) newmv_median=1458107.
If (pin>17331040531000 and pin<17331040549999) newmv_median=730000.
If (pin>17331110491000 and pin<17331110499999) newmv_median=700000.
If (pin>17331140601000 and pin<17331140609999) newmv_median=730000.
If (pin>17331200951000 and pin<17331200959999) newmv_median=1435248.
If (pin>17332010371000 and pin<17332010379999) newmv_median=928058.
If (pin>17332020611000 and pin<17332020619999) newmv_median=800000.
If (pin>17332020581000 and pin<17332020589999) newmv_median=866998.
If (pin>17332030581000 and pin<17332030589999) newmv_median=866998.
If (pin>17333140511000 and pin<17333140519999) newmv_median=954880.
If (pin>17333160521000 and pin<17333160529999) newmv_median=871560.
If (pin>17333190621000 and pin<17333190629999) newmv_median=829752.
If (pin>17333210451000 and pin<17333210459999) newmv_median=1120000.
If (pin>17334230201000 and pin<17334230209999) newmv_median=705260.
If (pin>17334230231000 and pin<17334230239999) newmv_median=10560143.
If (pin>17334230241000 and pin<17334230249999) newmv_median=5469872.
If (pin>17334260141000 and pin<17334260149999) newmv_median=5560089.
If (pin>17341020511000 and pin<17341020519999) newmv_median=25833333.
If (pin>17341030581000 and pin<17341030589999) newmv_median=1865673.
If (pin>17341030651000 and pin<17341030659999) newmv_median=547445.
If (pin>17341030691000 and pin<17341030699999) newmv_median=488145.
If (pin>17341100621000 and pin<17341100629999) newmv_median=642529.
If (pin>17341100641000 and pin<17341100649999) newmv_median=1195219.
If (pin>17341211261000 and pin<17341211269999) newmv_median=1019600.
If (pin>17341221151000 and pin<17341221159999) newmv_median=450000.
If (pin>17341221291000 and pin<17341221299999) newmv_median=353333.
If (pin>17343091091000 and pin<17343091099999) newmv_median=445454.
If (pin>17343091101000 and pin<17343091109999) newmv_median=445454.
If (pin>17343091211000 and pin<17343091219999) newmv_median=461320.
If (pin>17343091221000 and pin<17343091229999) newmv_median=461320.
If (pin>17343091231000 and pin<17343091239999) newmv_median=431111.
If (pin>17343101341000 and pin<17343101349999) newmv_median=860532.
If (pin>17343101351000 and pin<17343101359999) newmv_median=942349.
If (pin>17343101361000 and pin<17343101369999) newmv_median=1125000.
If (pin>17343101371000 and pin<17343101379999) newmv_median=736181.
If (pin>17343111011000 and pin<17343111019999) newmv_median=337000.
If (pin>17343120901000 and pin<17343120909999) newmv_median=943376.
If (pin>17343120911000 and pin<17343120919999) newmv_median=678546.
If (pin>17343120921000 and pin<17343120929999) newmv_median=2186567.
If (pin>17343190211000 and pin<17343190219999) newmv_median=459770.
If (pin>17343210421000 and pin<17343210429999) newmv_median=496160.
If (pin>17343220641000 and pin<17343220649999) newmv_median=622058.
If (pin>17343220651000 and pin<17343220659999) newmv_median=423000.
If (pin>17343220671000 and pin<17343220679999) newmv_median=325384.
If (pin>17343230641000 and pin<17343230649999) newmv_median=468913.
If (pin>17343230691000 and pin<17343230729999) newmv_median=371041.
If (pin>17343270471000 and pin<17343270479999) newmv_median=467379.
If (pin>17344000911000 and pin<17344000919999) newmv_median=377924.
If (pin>17344000921000 and pin<17344000929999) newmv_median=252000.
If (pin>17344000951000 and pin<17344000959999) newmv_median=703668.
If (pin>17344000961000 and pin<17344000969999) newmv_median=954198.
If (pin>17344090371000 and pin<17344090379999) newmv_median=705093.
If (pin>17344090361000 and pin<17344090369999) newmv_median=1451421.
If (pin>17344100551000 and pin<17344100559999) newmv_median=652000.
If (pin>17344230141000 and pin<17344230159999) newmv_median=736000.
If (pin>17344240101000 and pin<17344240109999) newmv_median=736000.
If (pin>17351011231000 and pin<17351011239999) newmv_median=725490.
If (pin>17351011251000 and pin<17351011259999) newmv_median=628306.
If (pin>17351011261000 and pin<17351011269999) newmv_median=637958.
If (pin>17351011271000 and pin<17351011279999) newmv_median=1196911.
If (pin>17351020371000 and pin<17351020379999) newmv_median=1280133.
*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
*compute newmv_median=newmv_median+(.12*oldmv).
*if (pin=14082110491008) newmv_median=32901056. 
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.



Compute newav = newmv_median*percent*.10.
if class=599 newav = newmv_median*percent*.10.
if class=589 newav = newmv_median*percent*.10.
if class=399 newav = newmv_median*percent*.10.
if class=499 newav = newmv_median*percent*.10.
if class=899 newav = newmv_median*percent*.10.
if class=679 newav = newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=1001 newmv=9999.
select if class = 299.
execute.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
Save OUTFILE = 'S:\2015 City_condo_valuations\South\condos76.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'South CONDOS' 'USING 2012-2014 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'South'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'South ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.


