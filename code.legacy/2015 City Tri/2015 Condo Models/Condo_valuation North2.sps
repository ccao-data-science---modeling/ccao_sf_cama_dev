﻿GET FILE = 'S:\2015 City_condo_valuations\North\NorthD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 

if (saleyr ge 30) saleyr=saleyr+1900.
execute.
if (saleyr lt 30) saleyr=saleyr+2000.
execute.

compute saledate= date.mdy (salemo,saleday,saleyr).
Formats saledate (date).
execute.

compute fc_indicator=0.
if (RANGE (filing_date,date.dmy(1,1,2012),date.dmy(31,12,2015)) and (saledate ge filing_date)) fc_indicator=1.
if (RANGE (auction_date,date.dmy(1,1,2012),date.dmy(31,12,2015)) and (saledate ge auction_date)) fc_indicator=1.

if (fc_indicator=1) saleyr=0.

if saleyr=2015 amount=salepric.
if saleyr=2014 amount=salepric.
if saleyr=2013 amount=salepric.
if saleyr=2012 amount=salepric.
*if salpric<saleamt amount=saleamt.

*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=74.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.
IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2015 City_condo_valuations\North\temp_t74sales.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\North\temp_t74sales.sav'
                /by PIN10.
SORT CASES by Pin.
Save Outfile = 'S:\2015 City_condo_valuations\North\SalesOutput.sav'.

SELECT IF MISSING(newmv_median) or newmv_median<.55*oldmv.
execute.

Compute newmortv=(mort/.8)/percent.
**************************The division by .8 is the assumption that the mortgage represents 80% of value with 20% down****************.

Format newmortv (comma9.0).
SORT CASES by Pin10.
Aggregate
                /outfile= 'S:\2015 City_condo_valuations\North\temp_t74Morts.sav'
               /break=PIN10
               /newmortv_median=median(newmortv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\North\temp_t74Morts.sav'
                /by PIN10.
sort cases by pin.
Save Outfile='S:\2015 City_condo_valuations\North\MortsOutput.sav'.

MATCH FILES FILE= 'S:\2015 City_condo_valuations\North\MortsOutput.sav'
           /FILE= 'S:\2015 City_condo_valuations\North\SalesOutput.sav'
            /BY=PIN.
sort cases by pin.
Save Outfile=  'S:\2015 City_condo_valuations\North\SalesandMortsOutput.sav'.

if (newmv_median<(.55*oldmv)) or (MISSING(newmv_median))  newmv_median=newmortv_median.



*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.
If (pin>14321030461000 and pin<14321030469999) newmv_median=1863636.
If (pin>14321070551000 and pin<14321070559999) newmv_median=1442620.
If (pin>14321120621000 and pin<14321120629999) newmv_median=1634000.
If (pin>14321270451000 and pin<14321270459999) newmv_median=2429630.
If (pin>14321270461000 and pin<14321270469999) newmv_median=1890000.
If (pin>14321270491000 and pin<14321270499999) newmv_median=1846500.
If (pin>14321340531000 and pin<14321340539999) newmv_median=1037064.
If (pin>14322060481000 and pin<14322060489999) newmv_median=1124100.
If (pin>14322060541000 and pin<14322060549999) newmv_median=2105000.
If (pin>14322070491000 and pin<14322070499999) newmv_median=1660714.
If (pin>14322070511000 and pin<14322070519999) newmv_median=2092352.
If (pin>14322070541000 and pin<14322070549999) newmv_median=1052632.
If (pin>14322090571000 and pin<14322090579999) newmv_median=1645840.
If (pin>14322100461000 and pin<14322100469999) newmv_median=1545000.
If (pin>14322110431000 and pin<14322110439999) newmv_median=3577818.
If (pin>14322120481000 and pin<14322120489999) newmv_median=950278.
If (pin>14322130481000 and pin<14322130489999) newmv_median=2002168.
If (pin>14322130501000 and pin<14322130509999) newmv_median=1581257.
If (pin>14322140411000 and pin<14322140419999) newmv_median=709040.
If (pin>14322140441000 and pin<14322140449999) newmv_median=1027274.
If (pin>14322160451000 and pin<14322160459999) newmv_median=1079701.
If (pin>14322160471000 and pin<14322160479999) newmv_median=935321.
If (pin>14322160481000 and pin<14322160489999) newmv_median=1925573.
If (pin>14322160571000 and pin<14322160579999) newmv_median=2175000.
If (pin>14322170471000 and pin<14322170479999) newmv_median=1640385.
If (pin>14322170501000 and pin<14322170509999) newmv_median=1042293.
If (pin>14322180521000 and pin<14322180529999) newmv_median=2538383.
If (pin>14322180531000 and pin<14322180539999) newmv_median=1090205.
If (pin>14322200511000 and pin<14322200519999) newmv_median=1842908.
If (pin>14322200531000 and pin<14322200539999) newmv_median=4500000.
If (pin>14322210501000 and pin<14322210509999) newmv_median=2496875.
If (pin>14322230371000 and pin<14322230379999) newmv_median=833947.
If (pin>14322230411000 and pin<14322230419999) newmv_median=1308363.
If (pin>14322240621000 and pin<14322240629999) newmv_median=877692.
If (pin>14322240651000 and pin<14322240659999) newmv_median=1668473.
If (pin>14322240701000 and pin<14322240709999) newmv_median=1978000.
If (pin>14322240711000 and pin<14322240719999) newmv_median=2379630.
If (pin>14322240721000 and pin<14322240729999) newmv_median=2222222.
If (pin>14322260521000 and pin<14322260529999) newmv_median=640000.
If (pin>14322270511000 and pin<14322270519999) newmv_median=2916667.
If (pin>14322280481000 and pin<14322280489999) newmv_median=1742400.

*All 2-99 units listed on MLS.
If (pin>14322280511000 and pin<14322280519999) newmv_median=4663333.
If (pin>14324000901000 and pin<14324000909999) newmv_median=1601423.
If (pin>14324020261000 and pin<14324020269999) newmv_median=1321071.
If (pin>14324030811000 and pin<14324030819999) newmv_median=1785357.
If (pin>14324030871000 and pin<14324030879999) newmv_median=2020202.
If (pin>14324070971000 and pin<14324070979999) newmv_median=4486310.
If (pin>14324100611000 and pin<14324100619999) newmv_median=4314183.
If (pin>14324100671000 and pin<14324100679999) newmv_median=849000.
If (pin>14324110851000 and pin<14324110859999) newmv_median=1013760.
If (pin>14324110871000 and pin<14324110879999) newmv_median=1293333.
If (pin>14324110901000 and pin<14324110909999) newmv_median=1098536.
If (pin>14324120661000 and pin<14324120669999) newmv_median=1318848.
If (pin>14324120721000 and pin<14324120729999) newmv_median=1534450.
If (pin>14324140711000 and pin<14324140719999) newmv_median=10830424.
If (pin>14324140771000 and pin<14324140779999) newmv_median=1497311.
If (pin>14324160731000 and pin<14324160739999) newmv_median=1032000.
If (pin>14324220371000 and pin<14324220379999) newmv_median=1186000.
If (pin>14324220391000 and pin<14324220399999) newmv_median=1067346.
If (pin>14324230551000 and pin<14324230559999) newmv_median=1590975.
If (pin>14324230561000 and pin<14324230569999) newmv_median=1981048.
If (pin>14324230581000 and pin<14324230589999) newmv_median=1782753.
If (pin>14324230621000 and pin<14324230629999) newmv_median=1338000.
If (pin>14324230661000 and pin<14324230669999) newmv_median=2986255.
If (pin>14324251351000 and pin<14324251359999) newmv_median=4362058.
If (pin>14324251361000 and pin<14324251369999) newmv_median=889820.
If (pin>14324260691000 and pin<14324260699999) newmv_median=1281916.
If (pin>14331020401000 and pin<14331020409999) newmv_median=2836388.
If (pin>14331040791000 and pin<14331040799999) newmv_median=1501953.
If (pin>14331070461000 and pin<14331070469999) newmv_median=1358550.
If (pin>14331080401000 and pin<14331080409999) newmv_median=3343450.
If (pin>14331090421000 and pin<14331090429999) newmv_median=3320000.
If (pin>14331090441000 and pin<14331090449999) newmv_median=1100000.
If (pin>14331100471000 and pin<14331100479999) newmv_median=1935819.
If (pin>14331110471000 and pin<14331110479999) newmv_median=1075396.
If (pin>14331110531000 and pin<14331110539999) newmv_median=1887500.
*Sale Confirmed for unit A in CCRD
If (pin>14331130321000 and pin<14331130329999) newmv_median=.
If (pin>14331140491000 and pin<14331140499999) newmv_median=1023692.
If (pin>14331210771000 and pin<14331210799999) newmv_median=735294.
If (pin>14331230601000 and pin<14331230609999) newmv_median=3515625.
If (pin>14331230611000 and pin<14331230619999) newmv_median=3190000.
If (pin>14331250511000 and pin<14331250519999) newmv_median=2387294.
If (pin>14331250541000 and pin<14331250549999) newmv_median=1803987.
If (pin>14331250551000 and pin<14331250559999) newmv_median=2359269.
If (pin>14331280791000 and pin<14331280859999) newmv_median=705882.
If (pin>14331290861000 and pin<14331290879999) newmv_median=705882.
If (pin>14331300631000 and pin<14331300639999) newmv_median=957180.
*Sale confirmed for unit 4 coach house in CCRD
If (pin>14331300691000 and pin<14331300699999) newmv_median=.
*Sale confirmed for unit 3 in CCRD
If (pin>14331300761000 and pin<14331300769999) newmv_median=.
If (pin>14331310481000 and pin<14331310489999) newmv_median=1481818.
If (pin>14332060561000 and pin<14332060569999) newmv_median=8538900.
If (pin>14332070511000 and pin<14332070519999) newmv_median=1334729.
If (pin>14333001101000 and pin<14333001109999) newmv_median=1532425.
If (pin>14333021291000 and pin<14333021299999) newmv_median=2560000.
If (pin>14333021331000 and pin<14333021339999) newmv_median=2138706.
If (pin>14333021561000 and pin<14333021569999) newmv_median=1284853.
If (pin>14333031421000 and pin<14333031429999) newmv_median=10344828.
If (pin>14333031531000 and pin<14333031539999) newmv_median=1169347.
If (pin>14333031621000 and pin<14333031629999) newmv_median=901579.
If (pin>14333040601000 and pin<14333040609999) newmv_median=2072608.
If (pin>14333050711000 and pin<14333050719999) newmv_median=1507447.
If (pin>14333050771000 and pin<14333050779999) newmv_median=1759531.
If (pin>14333060461000 and pin<14333060469999) newmv_median=3784722.
If (pin>14333060481000 and pin<14333060489999) newmv_median=1475000.
If (pin>14333060491000 and pin<14333060499999) newmv_median=3232759.
*MLS listing unit B $1,149,500.
If (pin>14333060531000 and pin<14333060539999) newmv_median=5717009.
If (pin>14333070721000 and pin<14333070729999) newmv_median=2170362.
If (pin>14333070741000 and pin<14333070749999) newmv_median=2347058.
If (pin>14333080631000 and pin<14333080639999) newmv_median=3250000.
If (pin>14333090491000 and pin<14333090499999) newmv_median=1647914.
If (pin>14333110551000 and pin<14333110559999) newmv_median=1131773.
If (pin>14333130771000 and pin<14333130779999) newmv_median=1020000.
If (pin>14333130851000 and pin<14333130859999) newmv_median=1462982.
If (pin>14333140801000 and pin<14333140809999) newmv_median=1700000.
If (pin>14333140821000 and pin<14333140829999) newmv_median=1128258.
If (pin>14333140841000 and pin<14333140849999) newmv_median=1528959.
If (pin>14333170551000 and pin<14333170559999) newmv_median=2424251.
If (pin>14333170591000 and pin<14333170599999) newmv_median=3950465.
If (pin>14333180641000 and pin<14333180649999) newmv_median=1788938.
If (pin>14333240341000 and pin<14333240349999) newmv_median=1049667.
*Sale confirmed
If (pin>14333240551000 and pin<14333240559999) newmv_median=.
If (pin>14333290271000 and pin<14333290279999) newmv_median=888000.
If (pin>14333300131000 and pin<14333300139999) newmv_median=3413371.
If (pin>14333310571000 and pin<14333310579999) newmv_median=1077512.
If (pin>14334070471000 and pin<14334070479999) newmv_median=2320000.
If (pin>14334070491000 and pin<14334070499999) newmv_median=6779874.
If (pin>14334080401000 and pin<14334080409999) newmv_median=1058000.
If (pin>14334080411000 and pin<14334080419999) newmv_median=4694485.
If (pin>14334090261000 and pin<14334090269999) newmv_median=1094655.
If (pin>14334120421000 and pin<14334120429999) newmv_median=2409383.
If (pin>14334160121000 and pin<14334160129999) newmv_median=1021291.
If (pin>14334170331000 and pin<14334170339999) newmv_median=3777110.
If (pin>14334200511000 and pin<14334200519999) newmv_median=1278430.
If (pin>14334210491000 and pin<14334210499999) newmv_median=1400000.
If (pin>17031010301000 and pin<17031010309999) newmv_median=4000000.
If (pin>17031020401000 and pin<17031020419999) newmv_median=3750000.
*MLS listing.
If (pin>17031050201000 and pin<17031050209999) newmv_median=7455268.
If (pin>17031060301000 and pin<17031060309999) newmv_median=1835800.
If (pin>17031090311000 and pin<17031090319999) newmv_median=4258862.
*MLS listing.
If (pin>17031090321000 and pin<17031090329999) newmv_median=3788635.
If (pin>17031090341000 and pin<17031090349999) newmv_median=2302974.
If (pin>17031120321000 and pin<17031120329999) newmv_median=18330063.
If (pin>17031120351000 and pin<17031120359999) newmv_median=2588150.
If (pin>17031120421000 and pin<17031120429999) newmv_median=8177874.
If (pin>17031120441000 and pin<17031120449999) newmv_median=16364095.
If (pin>17031130211000 and pin<17031130219999) newmv_median=8666667.
If (pin>17032000771000 and pin<17032000779999) newmv_median=1327820.
If (pin>17032000791000 and pin<17032000799999) newmv_median=1205333.
*MLS listing unit 1 $485000
If (pin>17032010771000 and pin<17032010779999) newmv_median=1600554.
If (pin>17032020711000 and pin<17032020719999) newmv_median=4617544.
If (pin>17032080331000 and pin<17032080339999) newmv_median=4275006.
*OK
If (pin>17032130201000 and pin<17032130209999) newmv_median=.
If (pin>17032250791000 and pin<17032250799999) newmv_median=18711923.
*OK
If (pin>17032280331000 and pin<17032280339999) newmv_median=
If (pin>17032280341000 and pin<17032280349999) newmv_median=
If (pin>17032280351000 and pin<17032280359999) newmv_median=.
If (pin>17041080551000 and pin<17041080559999) newmv_median=4943558.
If (pin>17041090421000 and pin<17041090429999) newmv_median=915128.
If (pin>17041090561000 and pin<17041090569999) newmv_median=1662877.
If (pin>17041100481000 and pin<17041100519999) newmv_median=1861796.
If (pin>17041100591000 and pin<17041100599999) newmv_median=1829420.
*This should be a 3-99
If (pin>17041100661000 and pin<17041100669999) newmv_median=857818.
If (pin>17041100681000 and pin<17041100689999) newmv_median=1992988.
If (pin>17041110551000 and pin<17041110559999) newmv_median=4056175.
If (pin>17041210901000 and pin<17041210909999) newmv_median=1861859.
If (pin>17041221151000 and pin<17041221159999) newmv_median=1330532.
*MLS pending at 625,000.
If (pin>17041221161000 and pin<17041221169999) newmv_median=1507537.
If (pin>17041221171000 and pin<17041221179999) newmv_median=1441666.
If (pin>17041121181000 and pin<17041121189999) newmv_median=1819848.
If (pin>17041221191000 and pin<17041221199999) newmv_median=1551128.
If (pin>17041221361000 and pin<17041221369999) newmv_median=1351726.
If (pin>17041221451000 and pin<17041221459999) newmv_median=1485521.
If (pin>17041300481000 and pin<17041300489999) newmv_median=2907084.
If (pin>17042001001000 and pin<17042001009999) newmv_median=861857.
If (pin>17042040461000 and pin<17042040469999) newmv_median=1955591.
If (pin>17042050531000 and pin<17042050539999) newmv_median=1350000.
If (pin>17042050561000 and pin<17042050569999) newmv_median=1261748.
If (pin>17042050571000 and pin<17042050579999) newmv_median=1158276.
If (pin>17042120371000 and pin<17042120379999) newmv_median=1532508.
If (pin>17042180441000 and pin<17042180449999) newmv_median=1590000.
*OK landmark reduced building
If (pin>17042180511000 and pin<17042180519999) newmv_median=.
If (pin>17042180521000 and pin<17042180529999) newmv_median=11913105.
If (pin>17042200481000 and pin<17042200489999) newmv_median=1414634.
If (pin>17042240461000 and pin<17042240469999) newmv_median=2500000.
If (pin>17042240481000 and pin<17042240489999) newmv_median=2435837.
If (pin>17043241101000 and pin<17043241109999) newmv_median=3040072.
If (pin>17044040261000 and pin<17044040269999) newmv_median=7659235.
If (pin>17044040351000 and pin<17044040359999) newmv_median=17692308.
If (pin>17044140371000 and pin<17044140379999) newmv_median=6756756.
If (pin>17044150251000 and pin<17044150259999) newmv_median=1413043.
If (pin>17044180411000 and pin<17044180419999) newmv_median=1489768.
If (pin>17044220401000 and pin<17044220409999) newmv_median=10367578.
If (pin>17044310201000 and pin<17044310209999) newmv_median=5000000.
If (pin>17044310231000 and pin<17044310239999) newmv_median=4506270.
If (pin>17044420541000 and pin<17044420549999) newmv_median=1011614.
If (pin>17044500321000 and pin<17044500329999) newmv_median=2385482.
*Should this be a 299?.
If (pin>17091200151000 and pin<17091200159999) newmv_median=4986400.
If (pin>17092020241000 and pin<17092020249999) newmv_median=1538496.
If (pin>17092030261000 and pin<17092030269999) newmv_median=1855001.
If (pin>17092100201000 and pin<17092100209999) newmv_median=9223810.
If (pin>17092220211000 and pin<17092220219999) newmv_median=10037174.
If (pin>17092510101000 and pin<17092510109999) newmv_median=18054261.
If (pin>17092580201000 and pin<17092580209999) newmv_median=2325001.
If (pin>17101320401000 and pin<17101320409999) newmv_median=9605309.
If (pin>17102020851000 and pin<17102020859999) newmv_median=8677897.
If (pin>17102120391000 and pin<17102120399999) newmv_median=8245533.
If (pin>17102140191000 and pin<17102140199999) newmv_median=39218052.


*******Make sure Trump Penthouse is put at 10 ratio********


*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
*compute newmv_median=newmv_median+(.09*oldmv).
*if (pin=14082110491008) newmv_median=32901056. 
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.



Compute newav = newmv_median*percent*.10.
if class=599 newav = newmv_median*percent*.10.
if class=589 newav = newmv_median*percent*.10.
if class=399 newav = newmv_median*percent*.10.
if class=499 newav = newmv_median*percent*.10.
if class=899 newav = newmv_median*percent*.10.
if class=679 newav = newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=1001 newmv=9999.
select if class = 299.
execute.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
Save OUTFILE = 'S:\2015 City_condo_valuations\North\condos74.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'North CONDOS' 'USING 2012-2015 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'North'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'North ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.


