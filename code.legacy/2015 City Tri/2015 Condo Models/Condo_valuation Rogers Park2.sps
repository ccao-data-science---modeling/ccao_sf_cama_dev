﻿GET FILE = 'S:\2015 City_condo_valuations\Rogerspark\RogersParkD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 

if (saleyr ge 30) saleyr=saleyr+1900.
execute.
if (saleyr lt 30) saleyr=saleyr+2000.
execute.

compute saledate= date.mdy (salemo,saleday,saleyr).
Formats saledate (date).
execute.

compute fc_indicator=0.
if (RANGE (filing_date,date.dmy(1,1,2012),date.dmy(31,12,2015)) and (saledate ge filing_date)) fc_indicator=1.
if (RANGE (auction_date,date.dmy(1,1,2012),date.dmy(31,12,2015)) and (saledate ge auction_date)) fc_indicator=1.

if (fc_indicator=1) saleyr=0.


if saleyr=2014 amount=salepric.
if saleyr=2013 amount=salepric.
if saleyr=2012 amount=salepric.
*if salpric<saleamt amount=saleamt.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=75.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.
IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2015 City_condo_valuations\Rogerspark\temp_t75sales.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\Rogerspark\temp_t75sales.sav'
                /by PIN10.
SORT CASES by Pin.
Save Outfile = 'S:\2015 City_condo_valuations\Rogerspark\SalesOutput.sav'.

SELECT IF MISSING(newmv_median) or newmv_median<.55*oldmv.
execute.

Compute newmortv=(mort/.8)/percent.
**************************The division by .8 is the assumption that the mortgage represents 80% of value with 20% down****************.

Format newmortv (comma9.0).
SORT CASES by Pin10.
Aggregate
                /outfile= 'S:\2015 City_condo_valuations\Rogerspark\temp_t75Morts.sav'
               /break=PIN10
               /newmortv_median=median(newmortv).
Match files
                 /file= *
                /table='S:\2015 City_condo_valuations\Rogerspark\temp_t75Morts.sav'
                /by PIN10.
sort cases by pin.
Save Outfile='S:\2015 City_condo_valuations\Rogerspark\MortsOutput.sav'.

MATCH FILES FILE= 'S:\2015 City_condo_valuations\Rogerspark\MortsOutput.sav'
           /FILE= 'S:\2015 City_condo_valuations\Rogerspark\SalesOutput.sav'
            /BY=PIN.
sort cases by pin.
Save Outfile=  'S:\2015 City_condo_valuations\Rogerspark\SalesandMortsOutput.sav'.

if (newmv_median<(.55*oldmv)) or (MISSING(newmv_median))  newmv_median=newmortv_median.
*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.
If (pin>10253030521000 and pin<10253030529999) newmv_median=1222776.
If (pin>10253030531000 and pin<10253030539999) newmv_median=1222776.
If (pin>10254100151000 and pin<10254100159999) newmv_median=357250.
If (pin>10254140261000 and pin<10254140269999) newmv_median=733740.
If (pin>10254250501000 and pin<10254250509999) newmv_median=1534722.
If (pin>10254270401000 and pin<10254270499999) newmv_median=415812.
If (pin>10254300111000 and pin<10254300119999) newmv_median=3860000.
If (pin>10362000461000 and pin<10362000469999) newmv_median=3514439.
If (pin>10362010311000 and pin<10362010319999) newmv_median=619444.
If (pin>10362010321000 and pin<10362010329999) newmv_median=860285.
If (pin>10362010331000 and pin<10362010339999) newmv_median=258000.
If (pin>10362060381000 and pin<10362060389999) newmv_median=194548.
If (pin>10362060411000 and pin<10362060419999) newmv_median=960337.
If (pin>10362080621000 and pin<10362080629999) newmv_median=560000.
If (pin>10362080631000 and pin<10362080639999) newmv_median=1367761.
If (pin>10362080641000 and pin<10362080649999) newmv_median=648043.
If (pin>10362090441000 and pin<10362090449999) newmv_median=551890.
If (pin>10362090461000 and pin<10362090469999) newmv_median=592000.
If (pin>10362090471000 and pin<10362090479999) newmv_median=1020000.
If (pin>10362090481000 and pin<10362090489999) newmv_median=2102218.
If (pin>10362100421000 and pin<10362100429999) newmv_median=591854.
If (pin>10362100431000 and pin<10362100439999) newmv_median=541732.
If (pin>10362100441000 and pin<10362100449999) newmv_median=1256211.
If (pin>10362100451000 and pin<10362100459999) newmv_median=660000.
If (pin>10362100461000 and pin<10362100469999) newmv_median=969584.
If (pin>10362110311000 and pin<10362110319999) newmv_median=636000.
If (pin>10362110331000 and pin<10362110339999) newmv_median=825000.
If (pin>10362160361000 and pin<10362160369999) newmv_median=501900.
If (pin>10362180441000 and pin<10362180449999) newmv_median=596370.
If (pin>10362180451000 and pin<10362180459999) newmv_median=1454382.
If (pin>10363110401000 and pin<10363110469999) newmv_median=452000.
If (pin>10363160371000 and pin<10363160379999) newmv_median=778894.
If (pin>10363170391000 and pin<10363170399999) newmv_median=559105.
If (pin>10363170411000 and pin<10363170419999) newmv_median=761750.
If (pin>10363180341000 and pin<10363180349999) newmv_median=649036.
If (pin>10363180371000 and pin<10363180379999) newmv_median=1547906.
If (pin>10363190321000 and pin<10363190329999) newmv_median=605882.
If (pin>10363190331000 and pin<10363190339999) newmv_median=1251158.
If (pin>10363200511000 and pin<10363200519999) newmv_median=664400.
If (pin>10363200521000 and pin<10363200529999) newmv_median=480300.
If (pin>10363200531000 and pin<10363200539999) newmv_median=545570.
If (pin>10363200561000 and pin<10363200579999) newmv_median=528000.
If (pin>10363200581000 and pin<10363200589999) newmv_median=528000.
If (pin>10363200591000 and pin<10363200599999) newmv_median=513400.
If (pin>10363200601000 and pin<10363200609999) newmv_median=842697.
If (pin>10363200621000 and pin<10363200629999) newmv_median=1750000.
If (pin>10363210711000 and pin<10363210719999) newmv_median=781415.
If (pin>10363230591000 and pin<10363230599999) newmv_median=2298140.
If (pin>10363250311000 and pin<10363250319999) newmv_median=482660.
If (pin>10363260451000 and pin<10363260459999) newmv_median=1193218.
If (pin>10363260481000 and pin<10363260489999) newmv_median=880000.
If (pin>10364000411000 and pin<10364000419999) newmv_median=472296.
If (pin>10364050391000 and pin<10364050399999) newmv_median=2331730.
If (pin>10364130381000 and pin<10364130389999) newmv_median=253120.
If (pin>10364140331000 and pin<10364140339999) newmv_median=932602.
If (pin>10364160411000 and pin<10364160419999) newmv_median=659456.
If (pin>10364160441000 and pin<10364160449999) newmv_median=480984.
If (pin>10364160451000 and pin<10364160459999) newmv_median=588888.
If (pin>10364160461000 and pin<10364160469999) newmv_median=476292.
If (pin>10364160481000 and pin<10364160489999) newmv_median=738000.
If (pin>10364160491000 and pin<10364160499999) newmv_median=699620.
If (pin>10364160501000 and pin<10364160509999) newmv_median=701888.
If (pin>10364170351000 and pin<10364170359999) newmv_median=2537600.
If (pin>10364170371000 and pin<10364170379999) newmv_median=1480263.
If (pin>10364190371000 and pin<10364190379999) newmv_median=540000.
If (pin>10364240341000 and pin<10364240349999) newmv_median=2036936.
If (pin>10364240381000 and pin<10364240389999) newmv_median=911685.
If (pin>10364280341000 and pin<10364280349999) newmv_median=792563.
If (pin>10364280361000 and pin<10364280369999) newmv_median=1463580.
If (pin>11291020501000 and pin<11291020509999) newmv_median=1125240.
If (pin>11291030231000 and pin<11291030239999) newmv_median=1143076.
If (pin>11291030241000 and pin<11291030249999) newmv_median=697752.
If (pin>11291030261000 and pin<11291030269999) newmv_median=647058.
If (pin>11291030271000 and pin<11291030279999) newmv_median=1159771.
If (pin>11291060221000 and pin<11291060229999) newmv_median=1157169.
If (pin>11291060271000 and pin<11291060279999) newmv_median=496050.
If (pin>11291060321000 and pin<11291060329999) newmv_median=750900.
If (pin>11291060341000 and pin<11291060349999) newmv_median=969131.
If (pin>11291060361000 and pin<11291060369999) newmv_median=1609311.
If (pin>11291060381000 and pin<11291060389999) newmv_median=828880.
If (pin>11291060391000 and pin<11291060399999) newmv_median=893181.
If (pin>11291070331000 and pin<11291070339999) newmv_median=1012183.
If (pin>11291070381000 and pin<11291070389999) newmv_median=4413490.
If (pin>11291070391000 and pin<11291070399999) newmv_median=1223404.

*Check this building for an error with comma placement. 399.
If (pin>11291090151000 and pin<11291090159999) newmv_median=1287005.
If (pin>11293070231000 and pin<11293070239999) newmv_median=1070833.
If (pin>11293070241000 and pin<11293070249999) newmv_median=1239978.
If (pin>11293070251000 and pin<11293070259999) newmv_median=3665654.
If (pin>11293080161000 and pin<11293080169999) newmv_median=1580136.
If (pin>11293110271000 and pin<11293110279999) newmv_median=1242630.
If (pin>11293110281000 and pin<11293110289999) newmv_median=881662.
If (pin>11293120151000 and pin<11293120159999) newmv_median=845530.
If (pin>11293130171000 and pin<11293130179999) newmv_median=1249740.
If (pin>11293140421000 and pin<11293140429999) newmv_median=1341256.
If (pin>11293150161000 and pin<11293150169999) newmv_median=900000.
If (pin>11293150171000 and pin<11293150179999) newmv_median=1369010.
If (pin>11293150181000 and pin<11293150189999) newmv_median=731250.
If (pin>11293160261000 and pin<11293160269999) newmv_median=1538061.
If (pin>11293180191000 and pin<11293180199999) newmv_median=795000.
If (pin>11293190191000 and pin<11293190199999) newmv_median=946642.
If (pin>11293190201000 and pin<11293190209999) newmv_median=1385075.
If (pin>11293200491000 and pin<11293200499999) newmv_median=680000.
If (pin>11293200501000 and pin<11293200509999) newmv_median=743040.
If (pin>11302050271000 and pin<11302050279999) newmv_median=1060500.
If (pin>11302050281000 and pin<11302050289999) newmv_median=1459847.
If (pin>11302150151000 and pin<11302150159999) newmv_median=908928.
If (pin>11302150181000 and pin<11302150189999) newmv_median=502300.
If (pin>11302160151000 and pin<11302160159999) newmv_median=794666.
If (pin>11302160161000 and pin<11302160169999) newmv_median=650670.
If (pin>11302160171000 and pin<11302160179999) newmv_median=817800.

*Is this the treasurer's house?.
If (pin>11302170201000 and pin<11302170209999) newmv_median=666273.
If (pin>11302170211000 and pin<11302170219999) newmv_median=1036442.
If (pin>11302170241000 and pin<11302170249999) newmv_median=651724.
If (pin>11302170251000 and pin<11302170259999) newmv_median=625000.
If (pin>11302180331000 and pin<11302180339999) newmv_median=7909744.
If (pin>11303010441000 and pin<11303010449999) newmv_median=355666.
If (pin>11303010451000 and pin<11303010459999) newmv_median=1410690.
If (pin>11303030651000 and pin<11303030659999) newmv_median=809609.
If (pin>11303030671000 and pin<11303030679999) newmv_median=675182.
If (pin>11303030681000 and pin<11303030689999) newmv_median=586500.
If (pin>11303030691000 and pin<11303030699999) newmv_median=514486.
If (pin>11303030701000 and pin<11303030709999) newmv_median=829813.
If (pin>11303060291000 and pin<11303060299999) newmv_median=854220.
If (pin>11303072141000 and pin<11303072149999) newmv_median=1636033.
If (pin>11303072181000 and pin<11303072189999) newmv_median=708764.
If (pin>11303072191000 and pin<11303072199999) newmv_median=5043918.
If (pin>11303072201000 and pin<11303072209999) newmv_median=744692.
If (pin>11303072241000 and pin<11303072249999) newmv_median=3711048.
If (pin>11303080271000 and pin<11303080279999) newmv_median=1984624.
If (pin>11303100341000 and pin<11303100349999) newmv_median=308651.
If (pin>11303100371000 and pin<11303100379999) newmv_median=1770892.
If (pin>11303110241000 and pin<11303110249999) newmv_median=1350650.
If (pin>11303120301000 and pin<11303130219999) newmv_median=448815.
If (pin>11303130231000 and pin<11303130239999) newmv_median=509810.
If (pin>11303150241000 and pin<11303150249999) newmv_median=4166667.
If (pin>11303150261000 and pin<11303150269999) newmv_median=978571.
If (pin>11303160181000 and pin<11303160189999) newmv_median=314818.
If (pin>11303170491000 and pin<11303170499999) newmv_median=740219.
If (pin>11303170511000 and pin<11303170519999) newmv_median=851852.
If (pin>11303200411000 and pin<11303200419999) newmv_median=3156176.
If (pin>11303220421000 and pin<11303220429999) newmv_median=1057692.
If (pin>11303220431000 and pin<11303220439999) newmv_median=793760.
If (pin>11304060251000 and pin<11304060259999) newmv_median=436000.
If (pin>11304060261000 and pin<11304060269999) newmv_median=416912.
If (pin>11304060271000 and pin<11304060279999) newmv_median=860862.
If (pin>11304060291000 and pin<11304060299999) newmv_median=589333.
If (pin>11304060301000 and pin<11304060309999) newmv_median=1296109.
If (pin>11304080821000 and pin<11304080829999) newmv_median=4754957.
If (pin>11304080861000 and pin<11304080869999) newmv_median=1213235.
If (pin>11304150391000 and pin<11304150399999) newmv_median=5639782.
If (pin>11304180391000 and pin<11304180399999) newmv_median=1164185.
If (pin>11304200731000 and pin<11304200739999) newmv_median=1822430.
If (pin>11304200741000 and pin<11304200749999) newmv_median=532275.
If (pin>11304220331000 and pin<11304220339999) newmv_median=1163239.
If (pin>11304240301000 and pin<11304240309999) newmv_median=1199287.
If (pin>11311070281000 and pin<11311070289999) newmv_median=354351.
If (pin>11311150451000 and pin<11311150459999) newmv_median=1065843.
If (pin>11311160461000 and pin<11311160469999) newmv_median=691950.
If (pin>11311160481000 and pin<11311160489999) newmv_median=713091.
*multiple buildings.
If (pin>11311170201000 and pin<11311170239999) newmv_median=505556.
If (pin>11311200571000 and pin<11311200579999) newmv_median=1100830.
If (pin>11311200651000 and pin<11311200659999) newmv_median=657366.
*multiple buildings.
If (pin>11311210221000 and pin<11311210249999) newmv_median=545596.
If (pin>11311220251000 and pin<11311220259999) newmv_median=1009460.
If (pin>11311240171000 and pin<11311240179999) newmv_median=589374.
If (pin>11311250171000 and pin<11311250179999) newmv_median=1360000.
If (pin>11312000361000 and pin<11312000369999) newmv_median=621500.
If (pin>11312020291000 and pin<11312020299999) newmv_median=716130.
If (pin>11312020301000 and pin<11312020309999) newmv_median=716129.
If (pin>11312030221000 and pin<11312030229999) newmv_median=1396277.
If (pin>11312040211000 and pin<11312040219999) newmv_median=448944.
If (pin>11312080361000 and pin<11312080369999) newmv_median=1133883.
If (pin>11312080411000 and pin<11312080419999) newmv_median=3320000.
If (pin>11312130411000 and pin<11312130419999) newmv_median=1890550.
If (pin>11312140561000 and pin<11312140569999) newmv_median=1376843.
If (pin>11312210291000 and pin<11312210299999) newmv_median=2412500.
If (pin>11312210311000 and pin<11312210319999) newmv_median=1630000.
If (pin>11312220351000 and pin<11312220359999) newmv_median=3357400.

*Very Large Block.
If (pin>11313010451000 and pin<11313030769999) newmv_median=504791.

If (pin>11313080071000 and pin<11313080079999) newmv_median=572037.
If (pin>11313110091000 and pin<11313110099999) newmv_median=675210.
If (pin>11313120471000 and pin<11313120479999) newmv_median=1269841.
If (pin>11313140371000 and pin<11313140379999) newmv_median=363970.
If (pin>11313140381000 and pin<11313140389999) newmv_median=650000.
If (pin>11313160441000 and pin<11313160449999) newmv_median=535572.
If (pin>11313160471000 and pin<11313160479999) newmv_median=1051564.
If (pin>11313160561000 and pin<11313160569999) newmv_median=599932.
If (pin>11313160571000 and pin<11313160579999) newmv_median=1358092.
If (pin>11313190391000 and pin<11313190399999) newmv_median=1105962.
If (pin>11313190401000 and pin<11313190409999) newmv_median=1395186.
If (pin>11314010911000 and pin<11314010919999) newmv_median=1837992.
If (pin>11314011041000 and pin<11314011049999) newmv_median=6027200.
If (pin>11314020881000 and pin<11314020889999) newmv_median=435508.
If (pin>11314080211000 and pin<11314080219999) newmv_median=3136646.
If (pin>11314080231000 and pin<11314080239999) newmv_median=376651.
If (pin>11314090281000 and pin<11314090289999) newmv_median=1939266.
If (pin>11314110201000 and pin<11314110209999) newmv_median=3138075.
If (pin>11314110211000 and pin<11314110219999) newmv_median=2369010.
If (pin>11314110221000 and pin<11314110229999) newmv_median=2374100.
If (pin>11314140531000 and pin<11314140539999) newmv_median=858468.
If (pin>11321020181000 and pin<11321020189999) newmv_median=6561486.
If (pin>11321040391000 and pin<11321040399999) newmv_median=273726.
If (pin>11321090211000 and pin<11321090219999) newmv_median=1833062.
If (pin>11321100361000 and pin<11321100369999) newmv_median=375000.
If (pin>11321100401000 and pin<11321100409999) newmv_median=764784.
If (pin>11321120241000 and pin<11321120249999) newmv_median=1179131.
If (pin>11321140321000 and pin<11321140329999) newmv_median=950207.
If (pin>11321150211000 and pin<11321150219999) newmv_median=1242337.
If (pin>11321160291000 and pin<11321160299999) newmv_median=771260.
If (pin>11321190201000 and pin<11321190209999) newmv_median=296778.
If (pin>11321190291000 and pin<11321190299999) newmv_median=993506.
If (pin>11321200391000 and pin<11321200399999) newmv_median=650000.
If (pin>11321200401000 and pin<11321200409999) newmv_median=1910560.
If (pin>11321240261000 and pin<11321240269999) newmv_median=1171137.
If (pin>11322000351000 and pin<11322000359999) newmv_median=1083207.
If (pin>11323000271000 and pin<11323000279999) newmv_median=589050.
If (pin>11323110301000 and pin<11323110309999) newmv_median=2578460.
If (pin>11323140401000 and pin<11323140409999) newmv_median=1111111.
If (pin>11323150401000 and pin<11323150409999) newmv_median=1033700.
If (pin>11323170301000 and pin<11323170309999) newmv_median=1141961.
If (pin>11323180141000 and pin<11323180149999) newmv_median=1242939.
If (pin>11323210321000 and pin<11323210329999) newmv_median=314718.
If (pin>11323230171000 and pin<11323230179999) newmv_median=1065994.
If (pin>11323300461000 and pin<11323300469999) newmv_median=391672.
If (pin>11324000361000 and pin<11324000369999) newmv_median=1553848.
If (pin>11324000371000 and pin<11324000379999) newmv_median=4728160.
If (pin>11324000391000 and pin<11324000399999) newmv_median=891237.
If (pin>11324000411000 and pin<11324000419999) newmv_median=1466666.
If (pin>11324000421000 and pin<11324000429999) newmv_median=754566.
If (pin>11324010441000 and pin<11324010449999) newmv_median=1152025.
If (pin>11324010461000 and pin<11324010469999) newmv_median=1055040.
If (pin>11324010471000 and pin<11324010479999) newmv_median=687054.
If (pin>11324030191000 and pin<11324030199999) newmv_median=1341541.
If (pin>11324030201000 and pin<11324030209999) newmv_median=1688097.
If (pin>11324040181000 and pin<11324040189999) newmv_median=628170.





*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
compute newmv_median=newmv_median+(.05*oldmv).
*if (pin=1001) newmv_median=.
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.


If (pin>10362110321000 and pin<10362110329999) newmv_median=1166530.
If (pin>10362180461000 and pin<10362180469999) newmv_median=491456.
If (pin>10363190511000 and pin<10363190519999) newmv_median=2046727.
If (pin>10363230601000 and pin<10363230609999) newmv_median=865356.
If (pin>10363260461000 and pin<10363260469999) newmv_median=536602.
If (pin>11303072121000 and pin<11303072129999) newmv_median=2810156.

*399.
If (pin>11321150251000 and pin<11321150259999) newmv_median=1000000.
If (pin>11321200351000 and pin<11321200359999) newmv_median=1000000.
If (pin>11312210311000 and pin<11312210319999) newmv_median=1630000.
If (pin>11321200351000 and pin<11321200359999) newmv_median=630978.
If (pin>11323150421000 and pin<11323150429999) newmv_median=31649838.
If (pin>11323170311000 and pin<11323170319999) newmv_median=3044740.


*599.
If (pin>10361010441000 and pin<10361010449999) newmv_median=795723.
If (pin>10363270331000 and pin<10363270339999) newmv_median=743800.
If (pin>11304040261000 and pin<11304040269999) newmv_median=5977167.
If (pin>11311130561000 and pin<11311130569999) newmv_median=441821.



Compute newav = newmv_median*percent*.10.
if class=599 newav=newmv_median*percent*.10.
if class=589 newav=newmv_median*percent*.10.
if class=399 newav=newmv_median*percent*.10.
if class=499 newav=newmv_median*percent*.10.
if class=899 newav=newmv_median*percent*.10.
if class=679 newav=newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=1001 newmv=9999.

Save OUTFILE = 'S:\2015 City_condo_valuations\Rogerspark\condos75.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'Rogers Park CONDOS' 'USING 2012-2014 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'Lake'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'Rogers Park ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.






