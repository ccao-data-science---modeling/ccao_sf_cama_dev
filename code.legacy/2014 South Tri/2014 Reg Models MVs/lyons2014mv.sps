			 	*SPSS Regression.
				*LYONS Regression 2014.

Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt21mergefcl2.sav'.
Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
if  (yr=13 and amount1>0)  year1=2013.
if  (yr=12 and amount1>0)  year1=2012.
if  (yr=11 and amount1>0)  year1=2011. 
if  (yr=10 and amount1>0)  year1=2010.
if  (yr=9 and amount1>0)   year1=2009.
if  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

COMPUTE FX = cumfile11121314.
RECODE FX (SYSMIS=0).
If  FX  > 8  FX = 8.
COMPUTE SRFX = sqrt(fx).

*******************************************************************************************************************.

*select if (year1 > 2008).
*select if puremarket=1.

Compute N=1.
*select if (amount1>145000).
*select if (amount1<990000).
*select if (multi<1).
*select if sqftb<6000.
Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.

*if (pin=	18013030030000
or pin=	18031150380000
or pin=	18031200730000
or pin=	18032020020000
or pin=	18033030490000
or pin=	18033100100000
or pin=	18033150300000
or pin=	18033220020000
or pin=	18034050210000
or pin=	18034160110000
or pin=	18041050140000
or pin=	18041110520000
or pin=	18041310060000
or pin=	18041330090000
or pin=	18042310050000
or pin=	18042310290000
or pin=	18043000060000
or pin=	18043070160000
or pin=	18043160260000
or pin=	18043170010000
or pin=	18043220040000
or pin=	18043280120000
or pin=	18043300240000
or pin=	18043300250000
or pin=	18044060200000
or pin=	18044150170000
or pin=	18044160140000
or pin=	18051000260000
or pin=	18051180030000
or pin=	18051180100000
or pin=	18051300130000
or pin=	18052000120000
or pin=	18053110120000
or pin=	18053190160000
or pin=	18053190240000
or pin=	18053210170000
or pin=	18054100120000
or pin=	18054230010000
or pin=	18054230020000
or pin=	18061150140000
or pin=	18061150170000
or pin=	18061300250000
or pin=	18061300320000
or pin=	18062060030000
or pin=	18062120220000
or pin=	18062130090000
or pin=	18062130320000
or pin=	18062150060000
or pin=	18062160380000
or pin=	18063020170000
or pin=	18063020230000
or pin=	18063030230000
or pin=	18063130220000
or pin=	18063200030000
or pin=	18064010020000
or pin=	18064030200000
or pin=	18071060010000
or pin=	18072040010000
or pin=	18072050070000
or pin=	18072050250000
or pin=	18072160280000
or pin=	18072160320000
or pin=	18073000590000
or pin=	18073090410000
or pin=	18074230120000
or pin=	18083120210000
or pin=	18083250060000
or pin=	18091050130000
or pin=	18091150070000
or pin=	18091230210000
or pin=	18091240010000
or pin=	18091290190000
or pin=	18091310220000
or pin=	18092020030000
or pin=	18092190040000
or pin=	18093130260000
or pin=	18093170180000
or pin=	18132020700000
or pin=	18134210120000
or pin=	18161060070000
or pin=	18162070060000
or pin=	18172000160000
or pin=	18172020210000
or pin=	18172120040000
or pin=	18173060320000
or pin=	18174000090000
or pin=	18174010080000
or pin=	18174020120000
or pin=	18174040050000
or pin=	18174070010000
or pin=	18182190260000
or pin=	18184000090000
or pin=	18192100090000
or pin=	18192100110000
or pin=	18193020030000
or pin=	18193050030000
or pin=	18202000490000
or pin=	18221120020000
or pin=	18241090200000
or pin=	18243070190000
or pin=	18291000480000
or pin=	18302010580000
or pin=	18304060040000
or pin=	18321060020000
or pin=	18321080260000
or pin=	18323040010000
or pin=	18334210120000
or pin=	18344080180000
or pin=	18344090380000
or pin=	18351010260000
or pin=	18354120040000
or pin=	23062030010000
or pin=	23063031110000)    bs=1.
*select if bs=0.

compute bsf=sqftb.

compute n11=0.
compute n12=0.
compute n13=0.
compute n22=0.
compute n31=0.
compute n32=0.
compute n41=0. 
compute n42=0.
compute n52=0.
compute n53=0.
compute n61=0.
compute n62=0.
compute n64=0.
compute n65=0.
compute n67=0.		
compute n71=0.
compute n82=0. 
compute n84=0. 
compute n91=0. 
compute n101=0.
compute n102=0. 
compute n111=0. 
compute n120=0. 
compute n121=0. 
compute n131=0. 
compute n132=0.
compute n133=0. 
compute n151=0. 
compute n161=0. 
compute n162=0. 
compute n165=0. 
compute n166=0. 
compute n200=0. 

if (nghcde=11) n11=1.
if (nghcde=12) n12=1.
if (nghcde=13) n13=1.
if (nghcde=22) n22=1.
if (nghcde=31) n31=1.
if (nghcde=32) n32=1.
if (nghcde=41) n41=1.
if (nghcde=42) n42=1.
if (nghcde=52) n52=1.
if (nghcde=53) n53=1.
if (nghcde=61) n61=1.
if (nghcde=62) n62=1.
if (nghcde=64) n64=1.
if (nghcde=65) n65=1.
if (nghcde=67) n67=1.
if (nghcde=71) n71=1.
if (nghcde=82) n82=1.
if (nghcde=84) n84=1.
if (nghcde=91) n91=1.
if (nghcde=101) n101=1.
if (nghcde=102) n102=1.
if (nghcde=111) n111=1.
if (nghcde=120) n120=1.
if (nghcde=121) n121=1.
if (nghcde=131) n131=1.
if (nghcde=132) n132=1.
if (nghcde=133) n133=1.
if (nghcde=151) n151=1.
if (nghcde=161) n161=1.
if (nghcde=162) n162=1.
if (nghcde=165) n165=1.
if (nghcde=166) n166=1.
if (nghcde=200) n200=1.

If town=21 and nghcde=11   n=7.60.
If town=21 and nghcde=12   n=5.49.
If town=21 and nghcde=13   n=4.49.
If town=21 and nghcde=22   n=4.11.
If town=21 and nghcde=31   n=4.29.
If town=21 and nghcde=32   n=6.70.
If town=21 and nghcde=41   n=2.06. 
If town=21 and nghcde=42   n=2.07.
If town=21 and nghcde=52   n=1.66.
If town=21 and nghcde=53   n=3.89.
If town=21 and nghcde=61   n=7.55.
If town=21 and nghcde=62   n=8.59.
If town=21 and nghcde=64   n=4.94.
If town=21 and nghcde=65   n=1.91.
If town=21 and nghcde=67   n=6.57.		
If town=21 and nghcde=71   n=4.48.
If town=21 and nghcde=82   n=3.27. 
If town=21 and nghcde=84   n=2.98. 
If town=21 and nghcde=91   n=1.75. 
If town=21 and nghcde=101  n=2.37.
If town=21 and nghcde=102  n=2.17. 
If town=21 and nghcde=111  n=1.81. 
If town=21 and nghcde=120  n=2.03. 
If town=21 and nghcde=121  n=1.98. 
If town=21 and nghcde=131  n=2.48. 
If town=21 and nghcde=132  n=2.63.
If town=21 and nghcde=133  n=3.85. 
If town=21 and nghcde=151  n=2.95. 
If town=21 and nghcde=161  n=3.02. 
If town=21 and nghcde=162  n=2.68. 
If town=21 and nghcde=165  n=3.65. 
If town=21 and nghcde=166  n=4.82. 
If town=21 and nghcde=200  n=9.60.

compute sb11=0.
compute sb12=0.
compute sb13=0.
compute sb22=0.
compute sb31=0.
compute sb32=0.
compute sb41=0.
compute sb42=0.
compute sb52=0.
compute sb53=0.
compute sb61=0.
compute sb62=0.
compute sb64=0.
compute sb65=0.
compute sb67=0.
compute sb71=0.
compute sb82=0.
compute sb84=0.
compute sb91=0.
compute sb101=0.
compute sb102=0.
compute sb111=0.
compute sb120=0.
compute sb121=0.
compute sb131=0.
compute sb132=0.
compute sb133=0.
compute sb151=0.
compute sb161=0.
compute sb162=0.
compute sb165=0.
compute sb166=0.
compute sb200=0.

if nghcde=11 sb11=sqrt(bsf).
if nghcde=12 sb12=sqrt(bsf).
if nghcde=13 sb13=sqrt(bsf).
if nghcde=22 sb22=sqrt(bsf).
if nghcde=31 sb31=sqrt(bsf).
if nghcde=32 sb32=sqrt(bsf).
if nghcde=41 sb41=sqrt(bsf).
if nghcde=42 sb42=sqrt(bsf).
if nghcde=52 sb52=sqrt(bsf).
if nghcde=53 sb53=sqrt(bsf).
if nghcde=61 sb61=sqrt(bsf).
if nghcde=62 sb62=sqrt(bsf).
if nghcde=64 sb64=sqrt(bsf).
if nghcde=65 sb65=sqrt(bsf).
if nghcde=67 sb67=sqrt(bsf).
if nghcde=71 sb71=sqrt(bsf).
if nghcde=82 sb82=sqrt(bsf).
if nghcde=84 sb84=sqrt(bsf).
if nghcde=91 sb91=sqrt(bsf).
if nghcde=101 sb101=sqrt(bsf).
if nghcde=102 sb102=sqrt(bsf).
if nghcde=111 sb111=sqrt(bsf).
if nghcde=120 sb120=sqrt(bsf).
if nghcde=121 sb121=sqrt(bsf).
if nghcde=131 sb131=sqrt(bsf).
if nghcde=132 sb132=sqrt(bsf).
if nghcde=133 sb133=sqrt(bsf).
if nghcde=151 sb151=sqrt(bsf).
if nghcde=161 sb161=sqrt(bsf).
if nghcde=162 sb162=sqrt(bsf).
if nghcde=165 sb165=sqrt(bsf).
if nghcde=166 sb166=sqrt(bsf).
if nghcde=200 sb200=sqrt(bsf).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.


if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute nbsf=n*bsf.
Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nsrbsf=n*srbsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

If firepl>1 firepl=1.
compute nfirepl=n*firepl.  

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

compute lowzonelyons=0.

compute midzone=0.
if (nghcde=41 or nghcde=52 or nghcde=91 or 
    nghcde=102 or nghcde=111 or nghcde=121) midzone=1.  
if midzone=0  lowzonelyons=1.

Compute midzonelyons=0.
If midzone = 1 midzonelyons=midzone.

Compute srfxlowblocklyons=0.
if lowzonelyons=1 srfxlowblocklyons=srfx*lowzonelyons.

Compute srfxmidblocklyons=0.
if midzonelyons=1 srfxmidblocklyons=srfx*midzonelyons.

Compute lsf=sqftl.
Compute srlsf=sqrt(sqftl).
Compute nsrlsf=n*srlsf.

if class=95 and nghcde=42  lsf=1700.
if class=95 and nghcde=65  lsf=2700.
if class=95 and nghcde=84  lsf=2600.
if class=95 and nghcde=102 lsf=1600.
if class=95 and nghcde=133 lsf=1850.
if class=95 and nghcde=151 lsf=1350.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1. 
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute jantmar09=0.
if (year1=2009 and (mos>=1 and mos<=3)) jantmar09=1. 
Compute octtdec13=0.
if (year1=2013 and (mos>=11 and mos<=13)) octtdec13=1.

Compute jantmar09cl234=jantmar09*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute octtdec13cl234=octtdec13*cl234.

Compute jantmar09cl56=jantmar09*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute octtdec13cl56=octtdec13*cl56.

Compute jantmar09cl778=jantmar09*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute octtdec13cl778=octtdec13*cl778.

Compute jantmar09cl89=jantmar09*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute octtdec13cl89=octtdec13*cl89.

Compute jantmar09cl1112=jantmar09*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute octtdec13cl1112=octtdec13*cl1112.

Compute jantmar09cl1095=jantmar09*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute octtdec13cl1095=octtdec13*cl1095.

Compute jantmar09clsplt=jantmar09*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute octtdec13clsplt=octtdec13*clsplt.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.

compute frbsf=frame*bsf.
 
Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1. 

Compute biggar=0.
if gar=5 or gar=6 or gar=8  biggar=1. 

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute srlsf=sqrt(sqftl).
compute nsrlsf=n*srlsf.
compute nmasbsf=n*mason*bsf.
compute nfrbsf=n*frame*bsf.
compute nfrmsbsf=n*framas*bsf.



Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.


compute mv= (124511.9121	
+ 10.44679102*nbsf
- 2764.38293*sb62
- 5524.718612*sb200
+ 450.58193121*nbsf1112
+ 2427.028838*sb31
+ 1499.54938*nbsf89
+ 2155.433278*sb13
+ 1307.493438*sb22
+ 930.9205812*nbsf778
+ 913.889897*nbsf56
+ 977.5217101*sb71
+ 1085.295345*sb12
+ 3925.383558*nbathsum
+ 55138.11024*biggar
+ 23049.37338*garage2
+ 2078.591713*sb166
- 3154.664408*nsrage
+ 772.9363695*nbsf234
+ 669.5576428*nbsf34
+ 11765.32388*garage1
- 20221.8656*midzonelyons
+ 7525.59714*nbasfull
+ 5837.533173*nbaspart
- 1213.765123*sb101
- 6073.071675*srfx
+ 1113.659874*sb151
+ 470.175554*sb84
- 842.9657478*sb67
- 4991.15595*srfxmidblocklyons
+ 64.80601694*nsrlsf
+ 443.9154544*nbsf1095
+ 0.83350138*nfrbsf
+ 1.65111111*nmasbsf
+ 2.061414192*nluxbsf
+ 4929.111735*garnogar
+ 1.252880027*nbsfair
+ 1476.851555*nfirepl
+ 212.1395681*sb65)*1.0.

save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv21.sav'/keep town pin mv.



