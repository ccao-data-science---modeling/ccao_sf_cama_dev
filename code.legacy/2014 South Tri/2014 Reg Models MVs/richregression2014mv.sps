                    		*SPSS Regression.
						*RICH TOWNSHIP REGRESSION 2014.

Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt32mergefcl2.sav'.
*select if (amount1>65000).
*select if (amount1<560000).
*select if (multi<1).
*select if sqftb<7000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=13 and amount1>0)  year1=2013.
if  (yr=12 and amount1>0)  year1=2012.
if  (yr=11 and amount1>0)  year1=2011.
If  (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.     
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

*select if (year1>2008).
*select if puremarket=1.


COMPUTE FX = cumfile11121314.
if FX >=8  FX=8.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).

*******************************************************************************************************************.

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	31011040380000
or pin=	31011040460000
or pin=	31012130030000
or pin=	31012210190000
or pin=	31012220020000
or pin=	31013010230000
or pin=	31013020040000
or pin=	31013020110000
or pin=	31013020130000
or pin=	31013030060000
or pin=	31013050070000
or pin=	31014000120000
or pin=	31014010060000
or pin=	31014030050000
or pin=	31014050070000
or pin=	31014060110000
or pin=	31014220180000
or pin=	31022000730000
or pin=	31022030430000
or pin=	31022030560000
or pin=	31022030580000
or pin=	31031030260000
or pin=	31031050040000
or pin=	31033050040000
or pin=	31034020170000
or pin=	31062000310000
or pin=	31062030050000
or pin=	31074020450000
or pin=	31074050020000
or pin=	31074051210000
or pin=	31111090110000
or pin=	31112200070000
or pin=	31113020040000
or pin=	31113020260000
or pin=	31122060130000
or pin=	31122090320000
or pin=	31122100300000
or pin=	31122110170000
or pin=	31122110260000
or pin=	31122120080000
or pin=	31122140120000
or pin=	31123060100000
or pin=	31123160280000
or pin=	31124050150000
or pin=	31124070140000
or pin=	31134001060000
or pin=	31134001390000
or pin=	31134002010000
or pin=	31144030110000
or pin=	31144090090000
or pin=	31152020030000
or pin=	31152070090000
or pin=	31152090250000
or pin=	31153050070000
or pin=	31154030210000
or pin=	31154050100000
or pin=	31154070160000
or pin=	31172060120000
or pin=	31172110070000
or pin=	31172140170000
or pin=	31202070020000
or pin=	31211030540000
or pin=	31211070150000
or pin=	31211080060000
or pin=	31211100150000
or pin=	31211110060000
or pin=	31221000150000
or pin=	31231070370000
or pin=	31231070430000
or pin=	31233030100000
or pin=	31233060230000
or pin=	31234350010000
or pin=	31234350190000
or pin=	31234400100000
or pin=	31242070200000
or pin=	31244240030000
or pin=	31253020150000
or pin=	31262120030000
or pin=	31264200120000
or pin=	31331010330000
or pin=	31331060170000
or pin=	31332030070000
or pin=	31332030760000
or pin=	31332100010000
or pin=	31332100180000
or pin=	31333140350000
or pin=	31334060140000
or pin=	31341010250000
or pin=	31344040050000
or pin=	31344050210000
or pin=	31351020290000
or pin=	31352010380000
or pin=	31364030070000)  bs=1.
*select if bs=0.

Compute bsf=sqftb.
Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.
compute bsf=sqftb.
compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute n10=0.
If nghcde=10 n10=1.
Compute n20=0.
If nghcde=20 n20=1.
Compute n40=0.
If nghcde=40 n40=1.
Compute n41=0.
If nghcde=41 n41=1.
Compute n42=0.
If nghcde=42 n42=1.
Compute n43=0.
If nghcde=43 n43=1.
Compute n45=0.
If nghcde=45 n45=1.
Compute n50=0.
If nghcde=50 n50=1.
Compute n55=0.
If nghcde=55 n55=1.
Compute n60=0.
If nghcde=60 n60=1.
Compute n61=0.
If nghcde=61 n61=1.
Compute n70=0.
If nghcde=70 n70=1.
Compute n75=0.
If nghcde=75 n75=1.
Compute n80=0.
If nghcde=80 n80=1.
Compute n90=0.
If nghcde=90 n90=1.
Compute n100=0.
If nghcde=100 n100=1.
Compute n110=0.
If nghcde=110 n110=1.
Compute n120=0.
If nghcde=120 n120=1.
Compute n130=0.
If nghcde=130 n130=1.
Compute n150=0.
If nghcde=150 n150=1.
Compute n151=0.
If nghcde=151 n151=1.
Compute n160=0.
If nghcde=160 n160=1.
Compute n161=0.
If nghcde=161 n161=1.
Compute n170=0.
If nghcde=170 n170=1.
Compute n175=0.
If nghcde=175 n175=1.
Compute n180=0.
If nghcde=180 n180=1.
Compute n181=0.
If nghcde=181 n181=1.
Compute n190=0.
If nghcde=190 n190=1.
Compute n200=0.
If nghcde=200 n200=1.
Compute n201=0.
If nghcde=201 n201=1.
Compute n220=0.
If nghcde=220 n220=1.
Compute n222=0.
If nghcde=222 n222=1.
Compute n224=0.
If nghcde=224 n224=1.
Compute n226=0.
If nghcde=226 n226=1.
Compute n227=0.
If nghcde=227 n227=1.
Compute n230=0.
If nghcde=230 n230=1.
 Compute n232=0.
If nghcde=232 n232=1.
Compute n240=0.
If nghcde=240 n240=1.
Compute n250=0.
If nghcde=250 n250=1.
Compute n255=0.
If nghcde=255 n255=1.
Compute n257=0.
If nghcde=257 n257=1.
Compute n262=0.
If nghcde=262 n262=1.
Compute n270=0.
If nghcde=270 n270=1.
Compute n275=0.
If nghcde=275 n275=1.
Compute n300=0.
If nghcde=300 n300=1.
Compute n310=0.
If nghcde=310 n310=1.
Compute n320=0.
If nghcde=320 n320=1.
Compute n330=0.
If nghcde=330 n330=1.
Compute n340=0.
If nghcde=340 n340=1.
Compute n350=0.
If nghcde=350 n350=1.
Compute n360=0.
If nghcde=360 n360=1.

Compute sb10=0.
If nghcde=10 sb10=sqrt(bsf).
Compute sb20=0.
If nghcde=20 sb20=sqrt(bsf).
Compute sb40=0.
If nghcde=40 sb40=sqrt(bsf).
Compute sb41=0.
If nghcde=41 sb41=sqrt(bsf).
Compute sb42=0.
If nghcde=42 sb42=sqrt(bsf).
Compute sb43=0.
If nghcde=43 sb43=sqrt(bsf).
Compute sb45=0.
If nghcde=45 sb45=sqrt(bsf).
Compute sb50=0.
If nghcde=50 sb50=sqrt(bsf).
Compute sb55=0.
If nghcde=55 sb55=sqrt(bsf).
Compute sb60=0.
If nghcde=60 sb60=sqrt(bsf).
Compute sb61=0.
If nghcde=61 sb61=sqrt(bsf).
Compute sb70=0.
If nghcde=70 sb70=sqrt(bsf).
Compute sb75=0.
If nghcde=75 sb75=sqrt(bsf).
Compute sb80=0.
If nghcde=80 sb80=sqrt(bsf).
Compute sb90=0.
If nghcde=90 sb90=sqrt(bsf).
Compute sb100=0.
If nghcde=100 sb100=sqrt(bsf).
Compute sb110=0.
If nghcde=110 sb110=sqrt(bsf).
Compute sb120=0.
If nghcde=120 sb120=sqrt(bsf).
Compute sb130=0.
If nghcde=130 sb130=sqrt(bsf).
Compute sb150=0.
If nghcde=150 sb150=sqrt(bsf).
Compute sb151=0.
If nghcde=151 sb151=sqrt(bsf).
Compute sb160=0.
If nghcde=160 sb160=sqrt(bsf).
Compute sb161=0.
If nghcde=161 sb161=sqrt(bsf).
Compute sb170=0.
If nghcde=170 sb170=sqrt(bsf).
Compute sb175=0.
If nghcde=175 sb175=sqrt(bsf).
Compute sb180=0.
If nghcde=180 sb180=sqrt(bsf).
Compute sb181=0.
If nghcde=181 sb181=sqrt(bsf).
Compute sb190=0.
If nghcde=190 sb190=sqrt(bsf).
Compute sb200=0.
If nghcde=200 sb200=sqrt(bsf).
Compute sb201=0.
If nghcde=201 sb201=sqrt(bsf).
Compute sb220=0.
If nghcde=220 sb220=sqrt(bsf).
Compute sb222=0.
If nghcde=222 sb222=sqrt(bsf).
Compute sb224=0.
If nghcde=224 sb224=sqrt(bsf).
Compute sb226=0.
If nghcde=226 sb226=sqrt(bsf).
Compute sb227=0.
If nghcde=227 sb227=sqrt(bsf).
Compute sb230=0.
If nghcde=230 sb230=sqrt(bsf).
 Compute sb232=0.
If nghcde=232 sb232=sqrt(bsf).
Compute sb250=0.
If nghcde=250 sb250=sqrt(bsf).
Compute sb255=0.
If nghcde=255 sb255=sqrt(bsf).
Compute sb257=0.
If nghcde=257 sb257=sqrt(bsf).
Compute sb262=0.
If nghcde=262 sb262=sqrt(bsf).
Compute sb270=0.
If nghcde=270 sb270=sqrt(bsf).
Compute sb275=0.
If nghcde=275 sb275=sqrt(bsf).
Compute sb300=0.
If nghcde=300 sb300=sqrt(bsf).
Compute sb310=0.
If nghcde=310 sb310=sqrt(bsf).
Compute sb320=0.
If nghcde=320 sb320=sqrt(bsf).
Compute sb330=0.
If nghcde=330 sb330=sqrt(bsf).
Compute sb340=0.
If nghcde=340 sb340=sqrt(bsf).
Compute sb350=0.
If nghcde=350 sb350=sqrt(bsf).
Compute sb360=0.
If nghcde=360 sb360=sqrt(bsf).

If nghcde=10 N=1.97.
If nghcde=20 N=1.59.
If nghcde=40 N=1.39.
If nghcde=41 N=1.56.
If nghcde=42 N=1.58.
If nghcde=43 N=1.72.
If nghcde=45 N=2.48.
If nghcde=50 N=2.02.
If nghcde=55 N=2.14.
If nghcde=60 N=1.75.
If nghcde=61 N=1.62.
If nghcde=70 N=2.30.
If nghcde=75 N=2.93.
If nghcde=80 N=3.65.
If nghcde=90 N=2.20.
If nghcde=100 N=3.89.
If nghcde=110 N=1.51.
If nghcde=120 N=2.24.
If nghcde=130 N=1.73.
If nghcde=150 N=1.59.
If nghcde=151 N=3.15.
If nghcde=160 N=1.70.
If nghcde=161 N=3.94.
If nghcde=170 N=1.63.
If nghcde=175 N=1.60.
If nghcde=180 N=1.66.
If nghcde=181 N=1.63.
If nghcde=190 N=1.58.
If nghcde=200 N=2.01.
If nghcde=201 N=1.88.
If nghcde=220 N=2.39. 
If nghcde=222 N=2.13.
If nghcde=224 N=2.43.
If nghcde=226 N=1.76.
If nghcde=227 N=3.05.
If nghcde=230 N=2.11.
If nghcde=232 N=1.95.
If nghcde=250 N=2.71.
If nghcde=255 N=2.22.
If nghcde=257 N=3.43.
If nghcde=262 N=1.87.
If nghcde=270 N=2.65.
If nghcde=275 N=3.08.
If nghcde=300 N=4.05.
If nghcde=310 N=1.86.
If nghcde=320 N=2.97.
If nghcde=330 N=2.00.
If nghcde=340 N=1.62.
If nghcde=350 N=2.01.
If nghcde=360 N=3.25.

Compute lsf=sqftl.
Compute srbsf=sqrt(sqftb).
Compute srlsf=sqrt(lsf).
Compute nsrbsf=N*srbsf.
Compute nsrlsf=N*srlsf.

If firepl>1 firepl = 1.  

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.


Compute lsf=sqftl.
If  lsf > 18650  lsf = 18650 + ((lsf - 18650)/3).
If (class=8 or class=9) lsf=sqftl.

if class=95 and nghcde=40 lsf=2900.
if class=95 and nghcde=41 lsf=1500.
if class=95 and nghcde=42 lsf=3100.
if class=95 and nghcde=45 lsf=1500.
if class=95 and nghcde=220 lsf=1550.
if class=95 and nghcde=230 lsf=3750.
if class=95 and nghcde=310 lsf=4800.

Compute nage=n*age.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=0.
If frame=1 frabsf=frame*bsf.
Compute stubsf=0.
If stucco=1 stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute masbsf=0.
If mason=1 masbsf=mason*bsf.
Compute frbsf=0.
If frame=1 frbsf=frame*bsf.
Compute frmsbsf=0.
If framas=1 frmsbsf=framas*bsf.
Compute stbsf=0.
If stucco=1 stbsf=stucco*bsf.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nfirepl=n*firepl.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.


Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if gar=4 or gar=5 or gar=6 or gar=8 biggar=1.
Compute nogar=0.
If gar=7 nogar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute cathdral=0.
If ceiling=1 cathdral=1.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1. 
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute jantmar09=0.
if (year1=2009 and (mos>=1 and mos<=3)) jantmar09=1. 
Compute octtdec13=0.
if (year1=2013 and (mos>=11 and mos<=13)) octtdec13=1.

Compute jantmar09cl234=jantmar09*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute octtdec13cl234=octtdec13*cl234.

Compute jantmar09cl56=jantmar09*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute octtdec13cl56=octtdec13*cl56.

Compute jantmar09cl778=jantmar09*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute octtdec13cl778=octtdec13*cl778.

Compute jantmar09cl89=jantmar09*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute octtdec13cl89=octtdec13*cl89.

Compute jantmar09cl1112=jantmar09*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute octtdec13cl1112=octtdec13*cl1112.

Compute jantmar09cl1095=jantmar09*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute octtdec13cl1095=octtdec13*cl1095.

Compute jantmar09clsplt=jantmar09*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute octtdec13clsplt=octtdec13*clsplt.


Compute lowzonerich=0.
if town=32 and (nghcde=43 or nghcde=50 or nghcde=60  or nghcde=61 
or nghcde=70 or nghcde=80 or nghcde=201 or nghcde=224  or nghcde=227
or nghcde=230  or nghcde=255  or nghcde=257  or nghcde=275) lowzonerich=1.                         	
Compute midzonerich=0.
if town=32 and (nghcde=20 or nghcde=42 or nghcde=55  or nghcde=75  
or nghcde=90  or nghcde=100  or nghcde=110   or nghcde=130   or nghcde=150   
or nghcde=160 or nghcde=170  or nghcde=180 or nghcde=200 or nghcde=222)  midzonerich=1. 
Compute highzonerich=0.
if town=32 and (nghcde=10 or nghcde=40 or nghcde=41  or nghcde=45  or nghcde=120  
or nghcde=151 or nghcde=175  or nghcde=181  or nghcde=190  or nghcde=220 or nghcde=226  
or nghcde=232 or nghcde=250  or nghcde=262  or nghcde=270  or nghcde=300
or nghcde=310 or nghcde=320  or nghcde=330  or nghcde=340 or nghcde=350 or nghcde=360)  highzonerich=1.

Compute srfxlowblockrich=0.
if lowzonerich=1 srfxlowblockrich=srfx*lowzonerich.
Compute srfxmidblockrich=0.
if midzonerich=1 srfxmidblockrich=srfx*midzonerich.
Compute srfxhighblockrich=0.
if highzonerich=1  srfxhighblockrich=srfx*highzonerich.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.


Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute b=1.

compute mv = (167004.2423	
+ 2669.954231*nbathsum
+ 10373.87235*biggar
+ 1523.582604*sb255
+ 7861.806202*cathdral
+ 106.2460173*nsrlsf
+ 3256.230702*srfxlowblockrich
- 30437.24027*nnobase
+ 554.310025*sb201
+ 13.17271099*frmsbsf
+ 16.00494403*masbsf
+ 8.477777599*frastbsf
+ 612.0943198*sb160
+ 1730.271025*sb257
+ 1548.694076*sb80
- 4399.187213*nsrage
+ 805.4432849*sb55
+ 723.4772778*sb200
+ 295.392671*sb70
+ 364.5068538*sb50
- 17781.90714*nbasfull
- 943.6623938*sb270
- 22576.30666*nbaspart
- 1066.342239*sb161
- 866.1412219*sb226
+ 742.5293835*nbsf778
+ 985.3827871*nbsf1112
- 465.0020455*sb310
+ 1300.554257*nbsf1095
- 4030.372507*srfxmidblockrich
- 4607.77936*srfxhighblockrich
+ 1131.106998*sb20
- 23601.83134*midzonerich
- 13628.41171*highzonerich
+ 1948.91492*nfirepl
+ 97.39800363*sb151
- 326.0062039*sb250
- 295.5252836*sb330
+ 886.9121342*nbsf56
+ 998.1877357*nbsf34
+ 1070.855844*nbsf89)*1.0.





save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv32.sav'/keep town pin mv.
