                        *SPSS Regression.
				           *THORNTON REGRESSION 2014.	

Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt37mergefcl2a.sav'.

*select if (amount1>40000).
*select if (amount1<450000).
*select if (multi<1).

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=13 and amount1>0)  year1=2013.
If  (yr=12 and amount1>0)  year1=2012.
If  (yr=11 and amount1>0)  year1=2011.
If  (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.  
If  (yr=7 and amount1>0)  year1=2007.   
If  (yr=6 and amount1>0)  year1=2006.
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.


*select if sqftb<6000.
*select if (year1>2008).
*select if puremarket=1.

COMPUTE FX = cumfile11121314.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
If  FX  >= 6  FX = 6.
*******************************************************************************************************************.

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	29064120030000
or pin=	29081210480000
or pin=	29061020370000
or pin=	29024130620000
or pin=	29033110070000
or pin=	29033150340000
or pin=	29102190370000
or pin=	29102220440000
or pin=	29102260050000
or pin=	29111110320000
or pin=	29112270570000
or pin=	29092310350000
or pin=	29103000360000
or pin=	29113080440000
or pin=	29113090440000
or pin=	29113120520000
or pin=	29141550320000
or pin=	29152050270000
or pin=	29152100190000
or pin=	29121050570000
or pin=	29121100520000
or pin=	29121190240000
or pin=	29121230590000
or pin=	29123080360000
or pin=	29123100710000
or pin=	29123140100000
or pin=	29123180170000
or pin=	29123180180000
or pin=	29123200440000
or pin=	29122040310000
or pin=	29122090100000
or pin=	29122210130000
or pin=	30071100130000
or pin=	30071120340000
or pin=	30071190260000
or pin=	30071200180000
or pin=	30071260030000
or pin=	30073210390000
or pin=	30171270010000
or pin=	30171270080000
or pin=	30181300080000
or pin=	30072170030000
or pin=	30083050010000
or pin=	30083170130000
or pin=	30084130460000
or pin=	30171270240000
or pin=	30171280080000
or pin=	30172120070000
or pin=	30173000400000
or pin=	30173070020000
or pin=	30174010100000
or pin=	30182080190000
or pin=	30182260510000
or pin=	29083040500000
or pin=	29173120620000
or pin=	29184090200000
or pin=	29143050480000
or pin=	29143050590000
or pin=	29143140020000
or pin=	29144030060000
or pin=	29144050010000
or pin=	29151000340000
or pin=	29154070650000
or pin=	29154090210000
or pin=	29154120590000
or pin=	29154130040000
or pin=	29154140090000
or pin=	29221000070000
or pin=	29221010120000
or pin=	29221140350000
or pin=	29221170050000
or pin=	29222010620000
or pin=	29222040060000
or pin=	29222040170000
or pin=	29222050130000
or pin=	29222080430000
or pin=	29223050020000
or pin=	29223070180000
or pin=	29223080240000
or pin=	29231030030000
or pin=	29271030090000
or pin=	30192220140000
or pin=	30203130040000
or pin=	30204080410000
or pin=	30204090130000
or pin=	30204090380000
or pin=	30291070380000
or pin=	30291080540000
or pin=	30291090510000
or pin=	30293030540000
or pin=	30302010290000
or pin=	30302020460000
or pin=	30302150790000
or pin=	30302150810000
or pin=	30291250190000
or pin=	29201030200000
or pin=	29202140230000
or pin=	29301170080000
or pin=	29301300050000
or pin=	29304090240000
or pin=	29224070090000
or pin=	29224070410000
or pin=	29224180020000
or pin=	29232070150000
or pin=	29233020200000
or pin=	29261000230000
or pin=	29261000490000
or pin=	29261010030000
or pin=	29261070410000
or pin=	29262020170000
or pin=	29262050060000
or pin=	29272100070000
or pin=	29314000550000
or pin=	29314010170000
or pin=	29314020540000
or pin=	29314030190000
or pin=	29314040200000
or pin=	29314070020000
or pin=	29314090320000
or pin=	29314090350000
or pin=	29314100070000
or pin=	29314100130000
or pin=	29314110260000
or pin=	29314120400000
or pin=	29314120430000
or pin=	29314140010000
or pin=	29314160340000
or pin=	29314160760000
or pin=	29323000410000
or pin=	29323010190000
or pin=	29323010540000
or pin=	29323040270000
or pin=	29323050010000
or pin=	29323070570000
or pin=	29324020220000
or pin=	29324040450000
or pin=	29324040500000
or pin=	29333040240000
or pin=	29273040730000
or pin=	29274000670000
or pin=	29352000030000
or pin=	29354060370000
or pin=	29354150510000
or pin=	29311230090000
or pin=	29313010060000
or pin=	29254000320000
or pin=	29362000050000
or pin=	29362000100000
or pin=	30293160480000
or pin=	30293230470000
or pin=	30293260480000
or pin=	30303120440000
or pin=	30303130340000
or pin=	30311000360000
or pin=	30311050460000
or pin=	30311210400000
or pin=	30312010490000
or pin=	30312040150000
or pin=	30312040460000
or pin=	30314000010000
or pin=	30314060400000
or pin=	30314170260000
or pin=	30321110390000
or pin=	30321170040000
or pin=	30321180210000
or pin=	30322030060000
or pin=	29364050140000
or pin=	30313160440000
or pin=	30323150100000
or pin=	30323270080000
or pin=	29363040190000
or pin=	29363110120000
or pin=	29364110250000
or pin=	29364120010000
or pin=	29364150060000
or pin=	30324030750000
or pin=	30324040470000
or pin=	29244040130000
or pin=	29251100090000) bs=1.
*select if bs=0.

Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1. 
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute jantmar09=0.
if (year1=2009 and (mos>=1 and mos<=3)) jantmar09=1. 
Compute octtdec13=0.
if (year1=2013 and (mos>=11 and mos<=13)) octtdec13=1.

Compute jantmar09cl234=jantmar09*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute octtdec13cl234=octtdec13*cl234.

Compute jantmar09cl56=jantmar09*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute octtdec13cl56=octtdec13*cl56.

Compute jantmar09cl778=jantmar09*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute octtdec13cl778=octtdec13*cl778.

Compute jantmar09cl89=jantmar09*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute octtdec13cl89=octtdec13*cl89.

Compute jantmar09cl1112=jantmar09*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute octtdec13cl1112=octtdec13*cl1112.

Compute jantmar09cl1095=jantmar09*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute octtdec13cl1095=octtdec13*cl1095.

Compute jantmar09clsplt=jantmar09*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute octtdec13clsplt=octtdec13*clsplt.


Compute lowzonethornton=0.
if town=37 and  (nghcde=75 or nghcde=161  or nghcde=162  or nghcde=164 
or nghcde=171 or nghcde=180 or nghcde=183 or nghcde=185)  lowzonethornton=1.                         	

Compute midzonethornton=0.
if town=37 and (nghcde=11 or nghcde=51 or nghcde=63 
or nghcde=102 or nghcde=103 or nghcde=150  or nghcde=163 
or nghcde=181 or nghcde=190 or nghcde=200)   midzonethornton=1. 

Compute highzonethornton=0.
if town=37 and (nghcde=10 or nghcde=21 or nghcde=22 or nghcde=23
or nghcde=24 or nghcde=33  or nghcde=52  or nghcde=53     
or nghcde=54  or nghcde=61 or nghcde=70 or nghcde=92 or nghcde=101
or nghcde=121 or nghcde=130 or nghcde=134)   highzonethornton=1.


Compute srfxlowblockthornton=0.
if lowzonethornton=1 srfxlowblockthornton=srfx*lowzonethornton.

Compute srfxmidblockthornton=0.
if midzonethornton=1 srfxmidblockthornton=srfx*midzonethornton.

Compute srfxhighblockthornton=0.
if highzonethornton=1 srfxhighblockthornton=srfx*highzonethornton.

Compute n10=0.
Compute n11=0.
Compute n21=0.
Compute n22=0.
Compute n23=0.
Compute n24=0.
Compute n33=0.
Compute n51=0.
Compute n52=0.
Compute n53=0.
Compute n54=0.
Compute n61=0.
Compute n63=0.
Compute n70=0.
Compute n75=0.
Compute n92=0.
Compute n101=0.
Compute n102=0.
Compute n103=0.
Compute n121=0.
Compute n130=0.
Compute n134=0.
Compute n150=0.
Compute n161=0.
Compute n162=0.
Compute n163=0.
Compute n164=0.
Compute n171=0.
Compute n180=0.
Compute n181=0.
Compute n183=0.
Compute n185=0.
Compute n190=0.
Compute n200=0.

Compute sb10=0.
Compute sb11=0.
Compute sb21=0.
Compute sb22=0.
Compute sb23=0.
Compute sb24=0.
Compute sb33=0.
Compute sb51=0.
Compute sb52=0.
Compute sb53=0.
Compute sb54=0.
Compute sb61=0.
Compute sb63=0.
Compute sb70=0.
Compute sb75=0.
Compute sb92=0.
Compute sb101=0.
Compute sb102=0.
Compute sb103=0.
Compute sb121=0.
Compute sb130=0.
Compute sb134=0.
Compute sb150=0.
Compute sb161=0.
Compute sb162=0.
Compute sb163=0.
Compute sb164=0.
Compute sb171=0.
Compute sb180=0.
Compute sb181=0.
Compute sb183=0.
Compute sb185=0.
Compute sb190=0.
Compute sb200=0.

If nghcde=10 n10=1.
if nghcde=11 n11=1.
If nghcde=21 n21=1.
if nghcde=22 n22=1.
if nghcde=23 n23=1.
If nghcde=24 n24=1.
if nghcde=33 n33=1.
If nghcde=51 n51=1.
if nghcde=52 n52=1.
If nghcde=53 n53=1.
if nghcde=54 n54=1.
if nghcde=61 n61=1.
If nghcde=63 n63=1.
if nghcde=70 n70=1.
If nghcde=75 n75=1.
if nghcde=92 n92=1.
If nghcde=101 n101=1.
if nghcde=102 n102=1.
If nghcde=103 n103=1.
if nghcde=121 n121=1.
If nghcde=130 n130=1.
If nghcde=134 n134=1.
if nghcde=150 n150=1.
if nghcde=161 n161=1.
If nghcde=162 n162=1.
if nghcde=163 n163=1.
If nghcde=164 n164=1.
if nghcde=171 n171=1.
If nghcde=180 n180=1.
if nghcde=181 n181=1.
If nghcde=183 n183=1.
if nghcde=185 n185=1.
If nghcde=190 n190=1.
if nghcde=200 n200=1.

If nghcde=10 sb10=sqrt(bsf).
if nghcde=11 sb11=sqrt(bsf).
If nghcde=21 sb21=sqrt(bsf).
if nghcde=22 sb22=sqrt(bsf).
if nghcde=23 sb23=sqrt(bsf).
If nghcde=24 sb24=sqrt(bsf).
if nghcde=33 sb33=sqrt(bsf).
If nghcde=51 sb51=sqrt(bsf).
if nghcde=52 sb52=sqrt(bsf).
If nghcde=53 sb53=sqrt(bsf).
if nghcde=54 sb54=sqrt(bsf).
if nghcde=61 sb61=sqrt(bsf).
If nghcde=63 sb63=sqrt(bsf).
if nghcde=70 sb70=sqrt(bsf).
If nghcde=75 sb75=sqrt(bsf).
if nghcde=92 sb92=sqrt(bsf).
If nghcde=101 sb101=sqrt(bsf).
if nghcde=102 sb102=sqrt(bsf).
If nghcde=103 sb103=sqrt(bsf).
if nghcde=121 sb121=sqrt(bsf).
If nghcde=130 sb130=sqrt(bsf).
If nghcde=134 sb134=sqrt(bsf).
if nghcde=150 sb150=sqrt(bsf).
if nghcde=161 sb161=sqrt(bsf).
If nghcde=162 sb162=sqrt(bsf).
if nghcde=163 sb163=sqrt(bsf).
If nghcde=164 sb164=sqrt(bsf).
if nghcde=171 sb171=sqrt(bsf).
If nghcde=180 sb180=sqrt(bsf).
if nghcde=181 sb181=sqrt(bsf).
If nghcde=183 sb183=sqrt(bsf).
if nghcde=185 sb185=sqrt(bsf).
If nghcde=190 sb190=sqrt(bsf).
if nghcde=200 sb200=sqrt(bsf).

Compute nbsf=n*bsf.
Compute nsrbsf=n*sqrt(bsf).

If firepl>1 firepl=1 .  
compute nfirepl=n*firepl.

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

Compute lsf = sqftl. 
Compute srlsf=sqrt(lsf).
Compute nsrlsf=n*sqrt(lsf).

If nghcde=10 N=1.08.
if nghcde=11 N=1.17.
If nghcde=21 N=1.05.
if nghcde=22 N=1.35.
if nghcde=23 N=1.20.
If nghcde=24 N=1.17.
if nghcde=33 N=1.24.
If nghcde=51 N=1.07.
if nghcde=52 N=1.60.
If nghcde=53 N=1.16.
if nghcde=54 N=1.16.
if nghcde=61 N=1.27.
If nghcde=63 N=1.28.
if nghcde=70 N=1.15.
If nghcde=75 N=1.16.
if nghcde=92 N=1.26.
If nghcde=101 N=1.10.
if nghcde=102 N=1.04.
If nghcde=103 N=1.18.
if nghcde=121 N=1.10.
If nghcde=130 N=1.18.
If nghcde=134 N=1.24.
if nghcde=150 N=1.39.
if nghcde=161 N=1.31.
If nghcde=162 N=1.46.
if nghcde=163 N=1.53.
If nghcde=164 N=1.17.
if nghcde=171 N=1.14.
If nghcde=180 N=1.87.
if nghcde=181 N=1.15.
If nghcde=183 N=1.19.
if nghcde=185 N=1.13.
If nghcde=190 N=1.59.
if nghcde=200 N=1.69.


Compute srage=sqrt(age).
Compute nsrage=n*srage.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute frmsbsf=framas*bsf.
compute masonbsf=mason*bsf.


if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.


Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.


Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.
Compute biggar=0.
If garage3=1 or garage4=1 biggar=1.  

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.


Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute deluxbsf = qualdlux*bsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute b=1.


compute mv = (40556.3987	
+ 0.931835211*nbsfair
+ 9750.340233*nbathsum
+ 792.7290036*sb162
+ 1492.651056*sb180
+ 22167.81557*biggar
+ 1008.986829*sb190
+ 710.9238683*sb150
+ 316.5716969*nsrlsf
+ 846.0250594*sb200
- 3837.770904*nsrage
+ 363.5312626*sb92
+ 992.6565171*sb52
- 166.025536*sb54
+ 15073.66674*nbasfull
+ 963.890542*nbsf234
- 245.29000*sb75
+ 311.0964452*sb181
+ 627.1029172*sb163
+ 172.0427546*sb164
+ 1.795602509*frastbsf
+ 1836.821902*nbsf778
+ 15.88745957*sb183
+ 3.160044*masonbsf
+ 1455.298198*nbsf89
- 9016.375274*garnogar
+ 156.6020142*sb61
+ 2.563888*frmsbsf
- 9293.613976*midzonethornton
- 9949.511191*highzonethornton
- 1020.079544*srfxlowblockthornton
- 1072.996318*srfxmidblockthornton
- 613.3330765*srfxhighblockthornton
+ 6235.442199*nbaspart
- 3788.425406*garage1
- 850.6914655*sb161
- 325.9124334*sb171
- 1.585394664*stubsf
- 399.3412321*sb22
+ 1886.486765*nbsf1095
+ 4589.232881*garage2
+ 2171.487734*nnobase
+ 1528.984807*nbsf34
+ 1233.929243*nbsf56
+ 1462.2654564*nbsf1112
+ 3017.791531*nfirepl
- 279.0527688*sb10)*1.0.

	
	* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=37.
*sel if puremarket=1.
*sel if  yr1=13.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv37.sav'/keep town pin mv.

 
