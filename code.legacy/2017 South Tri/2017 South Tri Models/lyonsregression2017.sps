			 	*SPSS Regression.
				*LYONS Regression 2017.

Get file='C:\Users\daaaron\documents\regt21mergefcl2b.sav'.
Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
if  (yr=16 and amount1>0)  year1=2016.
if  (yr=15 and amount1>0)  year1=2015.
if  (yr=14 and amount1>0)  year1=2014.
if  (yr=13 and amount1>0)  year1=2013.
if  (yr=12 and amount1>0)  year1=2012.
if  (yr=11 and amount1>0)  year1=2011. 
if  (yr=10 and amount1>0)  year1=2010.


COMPUTE FX = cumfile14151617.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
If  FX  > 8  FX = 8.
*******************************************************************************************************************.

select if (year1 > 2011).
select if puremarket=1.

Compute N=1.
select if (amount1>145000).
select if (amount1<990000).
select if (multi<1).
select if sqftb<6000.
Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.

if (pin=	18022060530000
or pin=	18031110020000
or pin=	18032170290000
or pin=	18032220350000
or pin=	18041070160000
or pin=	18041080160000
or pin=	18041080170000
or pin=	18041090100000
or pin=	18041110520000
or pin=	18042160070000
or pin=	18042310290000
or pin=	18043000060000
or pin=	18043070160000
or pin=	18043140170000
or pin=	18043220200000
or pin=	18043270250000
or pin=	18043280120000
or pin=	18043300220000
or pin=	18044060200000
or pin=	18044090130000
or pin=	18044170020000
or pin=	18044190040000
or pin=	18044220260000
or pin=	18051010070000
or pin=	18051020200000
or pin=	18051050090000
or pin=	18051080180000
or pin=	18051260060000
or pin=	18051300130000
or pin=	18052000120000
or pin=	18052020060000
or pin=	18052080170000
or pin=	18052150080000
or pin=	18052250010000
or pin=	18053070270000
or pin=	18053090320000
or pin=	18053090340000
or pin=	18053100160000
or pin=	18053110190000
or pin=	18053150250000
or pin=	18053160410000
or pin=	18053170120000
or pin=	18053180200000
or pin=	18053190240000
or pin=	18053200250000
or pin=	18053210230000
or pin=	18054190060000
or pin=	18054230020000
or pin=	18054230090000
or pin=	18054230120000
or pin=	18054230180000
or pin=	18054290200000
or pin=	18061050110000
or pin=	18061080240000
or pin=	18061170170000
or pin=	18061190200000
or pin=	18061200220000
or pin=	18061280100000
or pin=	18061280150000
or pin=	18062050010000
or pin=	18062060030000
or pin=	18062130320000
or pin=	18062170100000
or pin=	18062200290000
or pin=	18063010110000
or pin=	18063020170000
or pin=	18063020230000
or pin=	18063050340000
or pin=	18063100030000
or pin=	18063120210000
or pin=	18063140250000
or pin=	18064040160000
or pin=	18071060010000
or pin=	18071080240000
or pin=	18072010220000
or pin=	18073000220000
or pin=	18074110230000
or pin=	18074210210000
or pin=	18074230120000
or pin=	18074230140000
or pin=	18081000130000
or pin=	18083090630000
or pin=	18083100430000
or pin=	18083120270000
or pin=	18083220040000
or pin=	18091050170000
or pin=	18091200170000
or pin=	18091230180000
or pin=	18091230210000
or pin=	18091230220000
or pin=	18091280230000
or pin=	18091290190000
or pin=	18091310190000
or pin=	18092020030000
or pin=	18092070080000
or pin=	18092140050000
or pin=	18092150120000
or pin=	18092190080000
or pin=	18092200140000
or pin=	18092240020000
or pin=	18093050120000
or pin=	18093130070000
or pin=	18093140080000
or pin=	18093220100000
or pin=	18093280060000
or pin=	18094050580000
or pin=	18094130070000
or pin=	18124040350000
or pin=	18132020700000
or pin=	18133040100000
or pin=	18161080070000
or pin=	18161080130000
or pin=	18162050170000
or pin=	18162070060000
or pin=	18162070400000
or pin=	18163020600000
or pin=	18163050070000
or pin=	18163080020000
or pin=	18171120210000
or pin=	18172080070000
or pin=	18172110090000
or pin=	18172120190000
or pin=	18172150050000
or pin=	18174040250000
or pin=	18174070120000
or pin=	18183040200000
or pin=	18184060240000
or pin=	18192010060000
or pin=	18192010260000
or pin=	18192010660000
or pin=	18192080120000
or pin=	18192090030000
or pin=	18192090040000
or pin=	18192110030000
or pin=	18192110040000
or pin=	18193020030000
or pin=	18193040210000
or pin=	18202000480000
or pin=	18204090010000
or pin=	18221100120000
or pin=	18291000480000
or pin=	18302060170000
or pin=	18304020320000
or pin=	18304060040000
or pin=	18304060100000
or pin=	18311090080000
or pin=	18321060020000
or pin=	18323040010000
or pin=	18323050120000
or pin=	18331030170000
or pin=	18333260090000
or pin=	18342050080000
or pin=	18344080180000
or pin=	18351000070000
or pin=	18363050090000
or pin=	23062020090000
or pin=	23062030010000
or pin=	23063031110000)     bs=1.
select if bs=0.

compute bsf=sqftb.

compute n11=0.
compute n12=0.
compute n13=0.
compute n22=0.
compute n31=0.
compute n32=0.
compute n41=0. 
compute n42=0.
compute n52=0.
compute n53=0.
compute n61=0.
compute n62=0.
compute n64=0.
compute n65=0.
compute n67=0.		
compute n71=0.
compute n82=0. 
compute n84=0. 
compute n91=0. 
compute n101=0.
compute n102=0. 
compute n111=0. 
compute n120=0. 
compute n121=0. 
compute n131=0. 
compute n132=0.
compute n133=0. 
compute n151=0. 
compute n161=0. 
compute n162=0. 
compute n165=0. 
compute n166=0. 
compute n200=0. 

if (nghcde=11) n11=1.
if (nghcde=12) n12=1.
if (nghcde=13) n13=1.
if (nghcde=22) n22=1.
if (nghcde=31) n31=1.
if (nghcde=32) n32=1.
if (nghcde=41) n41=1.
if (nghcde=42) n42=1.
if (nghcde=52) n52=1.
if (nghcde=53) n53=1.
if (nghcde=61) n61=1.
if (nghcde=62) n62=1.
if (nghcde=64) n64=1.
if (nghcde=65) n65=1.
if (nghcde=67) n67=1.
if (nghcde=71) n71=1.
if (nghcde=82) n82=1.
if (nghcde=84) n84=1.
if (nghcde=91) n91=1.
if (nghcde=101) n101=1.
if (nghcde=102) n102=1.
if (nghcde=111) n111=1.
if (nghcde=120) n120=1.
if (nghcde=121) n121=1.
if (nghcde=131) n131=1.
if (nghcde=132) n132=1.
if (nghcde=133) n133=1.
if (nghcde=151) n151=1.
if (nghcde=161) n161=1.
if (nghcde=162) n162=1.
if (nghcde=165) n165=1.
if (nghcde=166) n166=1.
if (nghcde=200) n200=1.

If town=21 and nghcde=11   n=7.90.
If town=21 and nghcde=12   n=6.03.
If town=21 and nghcde=13   n=4.99.
If town=21 and nghcde=22   n=4.65.
If town=21 and nghcde=31   n=5.09.
If town=21 and nghcde=32   n=7.10.
If town=21 and nghcde=41   n=2.39. 
If town=21 and nghcde=42   n=2.55.
If town=21 and nghcde=52   n=1.90.
If town=21 and nghcde=53   n=4.39.
If town=21 and nghcde=61   n=7.80.
If town=21 and nghcde=62   n=8.59.
If town=21 and nghcde=64   n=5.44.
If town=21 and nghcde=65   n=2.14.
If town=21 and nghcde=67   n=6.57.		
If town=21 and nghcde=71   n=5.08.
If town=21 and nghcde=82   n=3.47. 
If town=21 and nghcde=84   n=3.34. 
If town=21 and nghcde=91   n=2.04. 
If town=21 and nghcde=101  n=2.57.
If town=21 and nghcde=102  n=2.37. 
If town=21 and nghcde=111  n=2.07. 
If town=21 and nghcde=120  n=2.43. 
If town=21 and nghcde=121  n=2.28. 
If town=21 and nghcde=131  n=2.98. 
If town=21 and nghcde=132  n=2.93.
If town=21 and nghcde=133  n=4.15. 
If town=21 and nghcde=151  n=3.45. 
If town=21 and nghcde=161  n=4.32. 
If town=21 and nghcde=162  n=2.98. 
If town=21 and nghcde=165  n=3.95. 
If town=21 and nghcde=166  n=5.32. 
If town=21 and nghcde=200  n=9.60.

compute sb11=0.
compute sb12=0.
compute sb13=0.
compute sb22=0.
compute sb31=0.
compute sb32=0.
compute sb41=0.
compute sb42=0.
compute sb52=0.
compute sb53=0.
compute sb61=0.
compute sb62=0.
compute sb64=0.
compute sb65=0.
compute sb67=0.
compute sb71=0.
compute sb82=0.
compute sb84=0.
compute sb91=0.
compute sb101=0.
compute sb102=0.
compute sb111=0.
compute sb120=0.
compute sb121=0.
compute sb131=0.
compute sb132=0.
compute sb133=0.
compute sb151=0.
compute sb161=0.
compute sb162=0.
compute sb165=0.
compute sb166=0.
compute sb200=0.

if nghcde=11 sb11=sqrt(bsf).
if nghcde=12 sb12=sqrt(bsf).
if nghcde=13 sb13=sqrt(bsf).
if nghcde=22 sb22=sqrt(bsf).
if nghcde=31 sb31=sqrt(bsf).
if nghcde=32 sb32=sqrt(bsf).
if nghcde=41 sb41=sqrt(bsf).
if nghcde=42 sb42=sqrt(bsf).
if nghcde=52 sb52=sqrt(bsf).
if nghcde=53 sb53=sqrt(bsf).
if nghcde=61 sb61=sqrt(bsf).
if nghcde=62 sb62=sqrt(bsf).
if nghcde=64 sb64=sqrt(bsf).
if nghcde=65 sb65=sqrt(bsf).
if nghcde=67 sb67=sqrt(bsf).
if nghcde=71 sb71=sqrt(bsf).
if nghcde=82 sb82=sqrt(bsf).
if nghcde=84 sb84=sqrt(bsf).
if nghcde=91 sb91=sqrt(bsf).
if nghcde=101 sb101=sqrt(bsf).
if nghcde=102 sb102=sqrt(bsf).
if nghcde=111 sb111=sqrt(bsf).
if nghcde=120 sb120=sqrt(bsf).
if nghcde=121 sb121=sqrt(bsf).
if nghcde=131 sb131=sqrt(bsf).
if nghcde=132 sb132=sqrt(bsf).
if nghcde=133 sb133=sqrt(bsf).
if nghcde=151 sb151=sqrt(bsf).
if nghcde=161 sb161=sqrt(bsf).
if nghcde=162 sb162=sqrt(bsf).
if nghcde=165 sb165=sqrt(bsf).
if nghcde=166 sb166=sqrt(bsf).
if nghcde=200 sb200=sqrt(bsf).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.


if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute nbsf=n*bsf.
Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nsrbsf=n*srbsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

If firepl>1 firepl=1.
compute nfirepl=n*firepl.  

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

compute lowzonelyons=0.

compute midzone=0.
if (nghcde=41 or nghcde=52 or nghcde=91 or 
    nghcde=102 or nghcde=111 or nghcde=121) midzone=1.  
if midzone=0  lowzonelyons=1.

Compute midzonelyons=0.
If midzone = 1 midzonelyons=midzone.

Compute srfxlowblocklyons=0.
if lowzonelyons=1 srfxlowblocklyons=srfx*lowzonelyons.

Compute srfxmidblocklyons=0.
if midzonelyons=1 srfxmidblocklyons=srfx*midzonelyons.

Compute lsf=sqftl.
Compute srlsf=sqrt(sqftl).
Compute nsrlsf=n*srlsf.

if class=95 and nghcde=42  lsf=1700.
if class=95 and nghcde=65  lsf=2700.
if class=95 and nghcde=84  lsf=2600.
if class=95 and nghcde=102 lsf=1600.
if class=95 and nghcde=133 lsf=1850.
if class=95 and nghcde=151 lsf=1350.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute winter1516=0.
if (mos > 9 and yr=15) or (mos <= 3 and yr=16) winter1516=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute summer16=0.
if (mos > 3 and yr=16) and (mos <= 9 and yr=16) summer16=1.
Compute jantmar12=0.
if (year1=2012 and (mos>=1 and mos<=3)) jantmar12=1. 
Compute octtdec16=0.
if (year1=2016 and (mos>=10 and mos<=12)) octtdec16=1.

Compute jantmar12cl234=jantmar12*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute winter1516cl234=winter1516*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute summer16cl234=summer16*cl234.
Compute octtdec16cl234=octtdec16*cl234.

Compute jantmar12cl56=jantmar12*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute winter1516cl56=winter1516*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute summer16cl56=summer16*cl56.
Compute octtdec16cl56=octtdec16*cl56.

Compute jantmar12cl778=jantmar12*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute winter1516cl778=winter1516*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute summer16cl778=summer16*cl778.
Compute octtdec16cl778=octtdec16*cl778.


Compute jantmar12cl89=jantmar12*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute winter1516cl89=winter1516*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute summer16cl89=summer16*cl89.
Compute octtdec16cl89=octtdec16*cl89.

Compute jantmar12cl1112=jantmar12*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute winter1516cl1112=winter1516*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute summer16cl1112=summer16*cl1112.
Compute octtdec16cl1112=octtdec16*cl1112.

Compute jantmar12cl1095=jantmar12*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute winter1516cl1095=winter1516*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute summer16cl1095=summer16*cl1095.
Compute octtdec16cl1095=octtdec16*cl1095.

Compute jantmar12clsplt=jantmar12*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute winter1516clsplt=winter1516*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute summer16clsplt=summer16*clsplt.
Compute octtdec16clsplt=octtdec16*clsplt.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.

compute frbsf=frame*bsf.
 
Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1. 

Compute biggar=0.
if gar=5 or gar=6 or gar=8  biggar=1. 

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
Compute nnobase=n*nobase.
compute srlsf=sqrt(sqftl).
compute nsrlsf=n*srlsf.
compute nmasbsf=n*mason*bsf.
compute nfrbsf=n*frame*bsf.
compute nfrmsbsf=n*framas*bsf.



Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute b=1.
*select if yr1 = 16.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

reg des=defaults cov
   /var=amount1 nbsf nsrlsf nsrage nbsf234 nbsf34 nbsf56 nbsf778
	nbsf1095 nbsf89 nfrbsf nfrmsbsf nmasbsf nbathsum bnumb nnobase
	nbasfull nbaspart  garage1 garage2 biggar garnogar nfirepl nrepabsf 
	npremrf  nbsfair sb11 sb12 sb13 sb22 sb31 sb32
	sb41 sb42 sb52 sb62 sb64 sb65 sb67 sb71 sb82 sb84 sb91 
	sb101 sb102 sb111 sb120 sb121 sb131 sb132 sb151 sb161
	sb162 sb165 sb166 sb200  jantmar12cl234  winter1213cl234 winter1314cl234 winter1415cl234 winter1516cl234 
 summer12cl234 summer13cl234 summer14cl234 summer15cl234 summer16cl234 octtdec16cl234
 winter1213cl56 winter1314cl56 winter1415cl56 winter1516cl56 summer12cl56
 summer13cl56 summer14cl56 summer15cl56 summer16cl56 octtdec16cl56 winter1213cl778
 winter1314cl778 winter1415cl778 winter1516cl778 summer12cl778 summer13cl778
 summer14cl778 summer15cl778 summer16cl778 octtdec16cl778  winter1213cl89
 winter1314cl89 winter1415cl89 winter1516cl89 summer12cl89 summer13cl89 summer14cl89
 summer15cl89 summer16cl89 octtdec16cl89  winter1213cl1112 winter1314cl1112 winter1415cl1112 winter1516cl1112 
 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112 summer16cl1112
 octtdec16cl1112  winter1213cl1095 winter1314cl1095 winter1415cl1095 winter1516cl1095
 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 summer16cl1095 octtdec16cl1095
 winter1213clsplt winter1314clsplt winter1415clsplt winter1516clsplt  summer12clsplt
 summer13clsplt summer14clsplt summer15clsplt summer16clsplt octtdec16clsplt 
 midzonelyons srfxlowblocklyons srfxmidblocklyons   
	/dep=amount1	
	/method=stepwise
 /method=enter  jantmar12cl234  winter1213cl234 winter1314cl234 winter1415cl234 winter1516cl234 
 summer12cl234 summer13cl234 summer14cl234 summer15cl234 summer16cl234 octtdec16cl234
 winter1213cl56 winter1314cl56 winter1415cl56 winter1516cl56 summer12cl56
 summer13cl56 summer14cl56 summer15cl56 summer16cl56 octtdec16cl56 winter1213cl778
 winter1314cl778 winter1415cl778 winter1516cl778 summer12cl778 summer13cl778
 summer14cl778 summer15cl778 summer16cl778 octtdec16cl778  winter1213cl89
 winter1314cl89 winter1415cl89 winter1516cl89 summer12cl89 summer13cl89 summer14cl89
 summer15cl89 summer16cl89 octtdec16cl89  winter1213cl1112 winter1314cl1112 winter1415cl1112 winter1516cl1112 
 summer12cl1112 summer13cl1112 summer14cl1112 summer15cl1112 summer16cl1112
 octtdec16cl1112  winter1213cl1095 winter1314cl1095 winter1415cl1095 winter1516cl1095
 summer12cl1095 summer13cl1095 summer14cl1095 summer15cl1095 summer16cl1095 octtdec16cl1095
 winter1213clsplt winter1314clsplt winter1415clsplt winter1516clsplt  summer12clsplt
 summer13clsplt summer14clsplt summer15clsplt summer16clsplt octtdec16clsplt 
	/method=enter  srfxlowblocklyons srfxmidblocklyons 
	/method=enter nbsf1095 nbsfair nsrlsf garage1 garnogar nfrbsf bnumb
	/method=enter nfirepl sb65 nmasbsf garage2 biggar nfrmsbsf nbaspart nnobase
 /method=enter sb42 sb52 sb132 sb162 
	/save pred (pred) resid (resid).
      sort cases by pin.	
      value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
      /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
      /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
      /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
      /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).

*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount1 with RESID.
*compute badsal=0.
*if perdif>50 badsal=1.
*if perdif<-50 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Lyons'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
exe,