TYLER_rawcounts <- function(display=FALSE) {
	N0 <- list(
		N=nrow(DAT),
		N_100000_990000=sum(DAT$mv>CONST$minmv & DAT$mv<CONST$maxmv),
		N_SingleFam=sum(DAT$mv>CONST$minmv & DAT$mv<CONST$maxmv & DAT$use==1),
		N_post2012=sum(DAT$mv>CONST$minmv & DAT$mv<CONST$maxmv & DAT$use==1 & ifelse(DAT$saledate==" ",0,as.numeric(substr(DAT$saledate,7,10))>=CONST$minyear)),
		N_market=sum(DAT$mv>CONST$minmv & DAT$mv<CONST$maxmv & DAT$use==1 & ifelse(DAT$saledate==" ",0,as.numeric(substr(DAT$saledate,7,10))>=CONST$minyear) & !is.na(DAT$puremarket) & DAT$puremarket==1)
		);
	out <- data.frame(N0);
	if (display) grid.table(out);
	invisible(out);
}

TYLER_rqos <- function(display=TRUE) {
	out <- data.frame(table(DAT$RQOS));
	if (display) grid.table(out);
	invisible(out);
}

TYLER_ppsf_rqos <- function(display=TRUE) {
	n <- data.frame(table(DAT$RQOS));
	med <- aggregate(subset(DAT,select=c(PPSF)),by=list(Var1=DAT$RQOS),median);
		n$Var1 <- as.numeric(as.character(n$Var1));
	
	dat <- merge(n,med,by="Var1");
	dat <- dat[order(-dat$Var1),];
	
	plot(NA,NA,xlim=c(20,1),ylim=c(0,250));
		points(dat$Var1,dat$Freq,col='blue',type='l',lwd=2);
		points(dat$Var1,dat$PPSF,col='red',type='l',lwd=2);
		grid();
		axis(1);axis(2);box();
}