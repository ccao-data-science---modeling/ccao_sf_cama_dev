                                  *SPSS Regression.
		        	                    *SCHAUMBURG  2016.

***Split block 07-28-313-001 thru 010 is in nbhd 112 should be in 040.
Get file='C:\Users\daaaron\documents\regt35jun17.sav'.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=15 and amount1>0) year1=2015.
If (yr=14 and amount1>0) year1=2014. 
If (yr=13 and amount1>0) year1=2013.
If (yr=12 and amount1>0) year1=2012.
If (yr=11 and amount1>0)  year1=2011.
If (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005. 
If  (yr=4 and amount1>0)  year1=2004. 
exe.
*select if (amount1 > 100000).
*select if (amount1 < 990000).
*select if year1 > 2010.

compute n=1.
Compute bs=0.
if (pin=	7171070530000
or pin=	7184120030000
or pin=	7141070160000
or pin=	7141180390000
or pin=	7141200170000
or pin=	7143060010000
or pin=	7153140250000
or pin=	7154120110000
or pin=	7154150130000
or pin=	7164220010000
or pin=	7212040290000
or pin=	7221040340000
or pin=	7221100090000
or pin=	7302050120000
or pin=	7303040060000
or pin=	7304060100000
or pin=	7304060160000
or pin=	7304070330000
or pin=	7304100140000
or pin=	7311020110000
or pin=	7311050170000
or pin=	7311050200000
or pin=	7312050150000
or pin=	7312070260000
or pin=	7312070340000
or pin=	7312070360000
or pin=	7312090540000
or pin=	7183030410000
or pin=	7353080540000
or pin=	7201060090000
or pin=	7201130130000
or pin=	7201130170000
or pin=	7202170220000
or pin=	7203060160000
or pin=	7203100060000
or pin=	7203110170000
or pin=	7203120220000
or pin=	7213000370000
or pin=	7213000560000
or pin=	7213030320000
or pin=	7213090030000
or pin=	7281110500000
or pin=	7281160210000
or pin=	7282050090000
or pin=	7282080560000
or pin=	7292100120000
or pin=	7294120150000
or pin=	7294130130000
or pin=	7294140290000
or pin=	7341050300000
or pin=	7341120210000
or pin=	7172080110000
or pin=	7261030110000
or pin=	7261050010000
or pin=	7261160090000
or pin=	7263030070000
or pin=	7214050010000
or pin=	7223130070000
or pin=	7264110130000
or pin=	7322030170000
or pin=	7322040060000
or pin=	7341210030000
or pin=	7342040190000
or pin=	7344010500000
or pin=	7351090130000
or pin=	7351100040000
or pin=	7351110110000
or pin=	7351130180000
or pin=	7351130200000
or pin=	7362000150000
or pin=	7362001020000
or pin=	7253090030000
or pin=	7363050030000
or pin=	7363060200000
or pin=	7364090230000
or pin=	7364120390000
or pin=	7243080680000
or pin=	7163210200000
or pin=	7163210250000
or pin=	7163210500000
or pin=	7151070120000
or pin=	7222030210000
or pin=	7293000550000
or pin=	7293100180000
or pin=	7293160020000
or pin=	7304180340000
or pin=	7241070090000
or pin=	7352010260000
or pin=	7194150010000
or pin=	7174150020000
or pin=	7012000840000
or pin=	7074040100000
or pin=	7042020330000
or pin=	7092100240000
or pin=	7101110040000)  bs=1.
*select if bs=0.

*COMPUTE FX = cumfile13141516.
*if FX >= 7   FX = 7.
*COMPUTE SRFX = sqrt(fx).
*RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute n=1.
compute bsf=sqftb.
compute nbsf=n*bsf.
Compute lsf=sqftl.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.
if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


compute tnb=(town*1000) + nghcde.

if (tnb=35011) n=2.37.
if (tnb=35012) n=3.10.
if (tnb=35020) n=2.23.
if (tnb=35030) n=2.10.
if (tnb=35031) n=2.37.
if (tnb=35032) n=1.80.
if (tnb=35035) n=4.25.
if (tnb=35040) n=2.45.
if (tnb=35060) n=4.90.
if (tnb=35065) n=2.95.
if (tnb=35071) n=2.88.
if (tnb=35072) n=3.45.
if (tnb=35074) n=2.86.
if (tnb=35075) n=4.09.
if (tnb=35080) n=3.13.
if (tnb=35085) n=2.64.
if (tnb=35090) n=3.06.
if (tnb=35091) n=3.65.
if (tnb=35101) n=3.42.
if (tnb=35102) n=2.20.
if (tnb=35103) n=1.96.
if (tnb=35104) n=1.92.
if (tnb=35105) n=1.55.
if (tnb=35106) n=2.79.
if (tnb=35107) n=1.98.
if (tnb=35108) n=1.49.
if (tnb=35109) n=2.17.
if (tnb=35110) n=3.95.
if (tnb=35111) n=3.45.
if (tnb=35112) n=3.94.
if (tnb=35113) n=3.30.
if (tnb=35114) n=3.25.
if (tnb=35115) n=3.05.
if (tnb=35116) n=3.55.
if (tnb=35117) n=5.83.
if (tnb=35140) n=1.99.
if (tnb=35150) n=5.19.
if (tnb=35151) n=6.65.
if (tnb=35160) n=2.68.
if (tnb=35170) n=2.25.



Compute sb35011=0.
Compute sb35012=0.
Compute sb35020=0.
Compute sb35030=0.
Compute sb35031=0.
Compute sb35032=0.
Compute sb35035=0.
Compute sb35040=0.
Compute sb35060=0.
Compute sb35065=0.
Compute sb35071=0.
Compute sb35072=0.
Compute sb35074=0.
Compute sb35075=0.
Compute sb35080=0.
Compute sb35085=0.
Compute sb35090=0.
Compute sb35091=0.
Compute sb35101=0.
Compute sb35102=0.
Compute sb35103=0.
Compute sb35104=0.
Compute sb35105=0.
Compute sb35106=0.
Compute sb35107=0.
Compute sb35108=0.
Compute sb35109=0.
Compute sb35110=0.
Compute sb35111=0.
Compute sb35112=0.
Compute sb35113=0.
Compute sb35114=0.
Compute sb35115=0.
Compute sb35116=0.
Compute sb35117=0.
Compute sb35140=0.
Compute sb35150=0.
Compute sb35151=0.
Compute sb35160=0.
Compute sb35170=0.


compute n35011=0.
compute n35012=0.
compute n35020=0.
compute n35030=0.
compute n35031=0.
compute n35032=0.
compute n35035=0.
compute n35040=0.
compute n35060=0.
compute n35065=0.
compute n35071=0.
compute n35072=0.
compute n35074=0.
compute n35075=0.
compute n35080=0.
compute n35085=0.
compute n35090=0.
compute n35091=0.
compute n35101=0.
compute n35102=0.
compute n35103=0.
compute n35104=0.
compute n35105=0.
compute n35106=0.
compute n35107=0.
compute n35108=0.
compute n35109=0.
compute n35110=0.
compute n35111=0.
compute n35112=0.
compute n35113=0.
compute n35114=0.
compute n35115=0.
compute n35116=0.
compute n35117=0.
compute n35140=0.
compute n35150=0.
compute n35151=0.
compute n35160=0.
compute n35170=0.

If (tnb=35011)  n35011=1.
If (tnb=35012)  n35012=1.
If (tnb=35020)  n35020=1.
if (tnb=35030)  n35030=1.
If (tnb=35031)  n35031=1.
If (tnb=35032)  n35032=1.
If (tnb=35035)  n35035=1.
if (tnb=35040)  n35040=1.
If (tnb=35060)  n35060=1.
If (tnb=35065)  n35065=1.
if (tnb=35071)  n35071=1.
If (tnb=35072)  n35072=1.
If (tnb=35074)  n35074=1.
If (tnb=35075)  n35075=1.
if (tnb=35080)  n35080=1.
If (tnb=35085)  n35085=1.
If (tnb=35090)  n35090=1.
If (tnb=35091)  n35091=1.
If (tnb=35101)  n35101=1.
If (tnb=35102)  n35102=1.
If (tnb=35103)  n35103=1.
If (tnb=35104)  n35104=1.
If (tnb=35105)  n35105=1.
If (tnb=35106)  n35106=1.
If (tnb=35107)  n35107=1.
If (tnb=35108)  n35108=1.
If (tnb=35109)  n35109=1.
If (tnb=35110)  n35110=1.
If (tnb=35111)  n35111=1.
If (tnb=35112)  n35112=1.
If (tnb=35113)  n35113=1.
If (tnb=35114)  n35114=1.
If (tnb=35115)  n35115=1.
If (tnb=35116)  n35116=1.
If (tnb=35117)  n35117=1.
If (tnb=35140)  n35140=1.
If (tnb=35150)  n35150=1.
If (tnb=35151)  n35151=1.
If (tnb=35160)  n35160=1.
If (tnb=35170)  n35170=1.


Compute n95=0.
If (tnb=35012) and class=95 n95=2.22.
if (tnb=35020) and class=95 n95=2.82. 
If (tnb=35030) and class=95 n95=3.07.
If (tnb=35032) and class=95 n95=1.66.
If (tnb=35040) and class=95 n95=1.72.
If (tnb=35065) and class=95 n95=2.16.
if (tnb=35071) and class=95 n95=2.24.
if (tnb=35072) and class=95 n95=2.18.
if (tnb=35074) and class=95 n95=2.17.
if (tnb=35080) and class=95 n95=1.97.
if (tnb=35090) and class=95 n95=2.16.
if (tnb=35101) and class=95 n95=2.89.
if (tnb=35102) and class=95 n95=2.11.
if (tnb=35103) and class=95 n95=1.78.
if (tnb=35104) and class=95 n95=1.87.
if (tnb=35105) and class=95 n95=1.31.
if (tnb=35106) and class=95 n95=2.66.
if (tnb=35107) and class=95 n95=1.89.
if (tnb=35108) and class=95 n95=1.55.
if (tnb=35109) and class=95 n95=1.52.
if (tnb=35116) and class=95 n95=3.22.
if (tnb=35140) and class=95 n95=1.73.
if (tnb=35160) and class=95 n95=2.65.
if (tnb=35170) and class=95 n95=1.91.

compute no95=1.
if class=95 no95=0.
Compute nbsf95=n95*sqrt(bsf).

Compute bsfs=sqftb*no95.
If (tnb=35011)  sb35011=sqrt(bsfs).
If (tnb=35012)  sb35012=sqrt(bsfs).
If (tnb=35020)  sb35020=sqrt(bsfs).
if (tnb=35030)  sb35030=sqrt(bsfs).
If (tnb=35031)  sb35031=sqrt(bsfs).
If (tnb=35032)  sb35032=sqrt(bsfs).
If (tnb=35035)  sb35035=sqrt(bsfs).
if (tnb=35040)  sb35040=sqrt(bsfs).
If (tnb=35060)  sb35060=sqrt(bsfs).
If (tnb=35065)  sb35065=sqrt(bsfs).
if (tnb=35071)  sb35071=sqrt(bsfs).
If (tnb=35072)  sb35072=sqrt(bsfs).
If (tnb=35074)  sb35074=sqrt(bsfs).
If (tnb=35075)  sb35075=sqrt(bsfs).
if (tnb=35080)  sb35080=sqrt(bsfs).
If (tnb=35085)  sb35085=sqrt(bsfs).
If (tnb=35090)  sb35090=sqrt(bsfs).
If (tnb=35091)  sb35091=sqrt(bsfs).
If (tnb=35101)  sb35101=sqrt(bsfs).
If (tnb=35102)  sb35102=sqrt(bsfs).
If (tnb=35103)  sb35103=sqrt(bsfs).
If (tnb=35104)  sb35104=sqrt(bsfs).
If (tnb=35105)  sb35105=sqrt(bsfs).
If (tnb=35106)  sb35106=sqrt(bsfs).
If (tnb=35107)  sb35107=sqrt(bsfs).
If (tnb=35108)  sb35108=sqrt(bsfs).
If (tnb=35109)  sb35109=sqrt(bsfs).
If (tnb=35110)  sb35110=sqrt(bsfs).
If (tnb=35111)  sb35111=sqrt(bsfs).
If (tnb=35112)  sb35112=sqrt(bsfs).
If (tnb=35113)  sb35113=sqrt(bsfs).
If (tnb=35114)  sb35114=sqrt(bsfs).
If (tnb=35115)  sb35115=sqrt(bsfs).
If (tnb=35116)  sb35116=sqrt(bsfs).
If (tnb=35117)  sb35117=sqrt(bsfs).
If (tnb=35140)  sb35140=sqrt(bsfs).
If (tnb=35150)  sb35150=sqrt(bsfs).
If (tnb=35151)  sb35151=sqrt(bsfs).
If (tnb=35160)  sb35160=sqrt(bsfs).
If (tnb=35170)  sb35170=sqrt(bsfs).


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95 cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

*Compute lowzoneschaumburg=0.
*if town=35 and  (nghcde=12 or nghcde=35 or nghcde=60 or nghcde=71 or nghcde=72 or nghcde=74
or nghcde=75 or nghcde=90 or nghcde =91 or nghcde=101 or nghcde=107 or nghcde=110 or nghcde=112
or nghcde=113 or nghcde=114 or nghcde=116 or nghcde=117 or nghcde=140 or nghcde=150
or nghcde=151 or nghcde=160)  lowzoneschaumburg=1.                         	

*Compute midzoneschaumburg=0.
*if town=35 and (nghcde=11 or nghcde=20 or nghcde=31 
or nghcde=32 or nghcde=40 or nghcde=65 or nghcde=80 or nghcde=85 or nghcde=102 or nghcde=103
or nghcde=104 or nghcde=106 or nghcde=111 or nghcde=115 or nghcde=170)    midzoneschaumburg=1.          
 
*Compute highzoneschaumburg=0.
*if town=35 and (nghcde=30  or nghcde=105 
or nghcde=108 or nghcde=109)   highzoneschaumburg=1.

*Compute srfxlowblockschaumburg=0.
*if lowzoneschaumburg=1 srfxlowblockschaumburg=srfx*lowzoneschaumburg.

*Compute srfxmidblockschaumburg=0.
*if midzoneschaumburg=1 srfxmidblockschaumburg=srfx*midzoneschaumburg.

*Compute srfxhighblockschaumburg=0.
*if highzoneschaumburg=1 srfxhighblockschaumburg=srfx*highzoneschaumburg.

Compute winter1112=0.
if (mos > 9 and yr=11 or (mos <= 3 and yr=12)) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1. 
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute jantmar11=0.
if (year1=2011 and (mos>=1 and mos<=3)) jantmar11=1. 
Compute octtdec15=0.
if (year1=2015 and (mos>=10 and mos<=12)) octtdec15=1.

Compute jantmar11cl234=jantmar11*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute octtdec15cl234=octtdec15*cl234.

Compute jantmar11cl56=jantmar11*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute octtdec15cl56=octtdec15*cl56.

Compute jantmar11cl778=jantmar11*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute octtdec15cl778=octtdec15*cl778.

Compute jantmar11cl89=jantmar11*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute octtdec15cl89=octtdec15*cl89.


Compute jantmar11cl1112=jantmar11*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute octtdec15cl1112=octtdec15*cl1112.

Compute jantmar11cl1095=jantmar11*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute octtdec15cl1095=octtdec15*cl1095.

Compute jantmar11clsplt=jantmar11*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute octtdec15clsplt=octtdec15*clsplt.


Compute lsf=sqftl.

if class=95 and tnb=35012   lsf=5594.
if class=95 and tnb=35020   lsf=3491.
if class=95 and tnb=35030   lsf=3146.
if class=95 and tnb=35032   lsf=2109.
if class=95 and tnb=35040   lsf=4047.
if class=95 and tnb=35065   lsf=5413.
if class=95 and tnb=35071   lsf=2365.	
if class=95 and tnb=35072   lsf=1947.
if class=95 and tnb=35074   lsf=3260.
if class=95 and tnb=35080   lsf=2314.
if class=95 and tnb=35090   lsf=2406.
if class=95 and tnb=35101   lsf=2962.
if class=95 and tnb=35102   lsf=2538.
if class=95 and tnb=35103   lsf=3160.	
if class=95 and tnb=35104   lsf=4585.
if class=95 and tnb=35105   lsf=1400.
if class=95 and tnb=35106   lsf=3535.
if class=95 and tnb=35107   lsf=2992.
if class=95 and tnb=35108   lsf=1998.
if class=95 and tnb=35109   lsf=1800.
if class=95 and tnb=35116   lsf=2255.
if class=95 and tnb=35140   lsf=670.
if class=95 and tnb=35160   lsf=4092.
if class=95 and tnb=35170   lsf=4072.	 	

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.


compute b=1.
*select if year1 >= 2015.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

compute mv = 143363.337	
+ 4063.507*nbathsum
+ 156.010*nsrlsf
- 10596.577*nsrage
+ .782*nbsfair
+ 1064.284*nbsf778
+ 4.239*masbsf
+ 17922.342*nnobase
+ 305.074*sb35030
+ 1258.993*nbsf89
+ 9433.880*garage1
+ 1117.486*nbsf34
+ 974.364*nbsf234
+ 1175.391*nbsf95
+ 1088.469*nbsf1112
+ 2853.734*nbsf56
+ 57134.462*biggar
+ 17676.113*garage2
+ 2073.458*sb35074
+ 2119.202*sb35071
+ 1711.922*sb35040
+ 2657.699*sb35085
+ 1712.698*sb35090
+ 2.840*nrepabsf
+ 1652.675*sb35011
+ 3448.679*sb35140
+ 1520.221*sb35116
+ 1361.906*sb35080
+ 1354.928*sb35112
+ 1443.210*sb35072
+ 1550.319*sb35075
+ 1402.494*sb35012
+ 1694.323*sb35110
+ 1370.211*sb35020
+ 1542.342*sb35170
+ 1534.264*sb35113
+ 1098.311*sb35114
+ 1188.735*sb35115
+ 1221.155*sb35150
+ 815.126*sb35065
+ 1253.217*sb35109
- 17.795*frabsf
+ 1937.357*nfirepl
+ 1.931*nluxbsf
+ 13.944*frastbsf
+ 28962.561*nbasfull
+ 24312.564*nbaspart
+ 2400.985*nsiteben.

save outfile='C:\Users\daaaron\documents\mv35jun22.sav'/keep town pin mv. 

	