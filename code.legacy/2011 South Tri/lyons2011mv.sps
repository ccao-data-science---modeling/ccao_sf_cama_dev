			 	*SPSS Regression.
				*LYONS Regression 2011.

Get file='C:\Program Files\SPSS\spssa\regt21mergefcl2.sav'.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.  
if  (yr=10 and amount1>0)  year1=2010.
if  (yr=9 and amount1>0)   year1=2009.
if  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
If  (yr=3 and amount1>0)  year1=2003.   
If  (yr=2 and amount1>0)  year1=2002.
If  (yr=1 and amount1>0)  year1=2001.
If  (yr=0 and amount1>0)  year1=2000.

COMPUTE FX = cumfile7891011.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if (year1=2006 or puremarket=1). 

Compute N=1.
*select if (amount1>145000).
*select if (amount1<990000).
*select if (multi<1).
*select if sqftb<6000.
Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin= 18021040320000
or pin=	18041050140000
or pin=	18042190080000
or pin=	18042290210000
or pin=	18043030120000
or pin=	18043130190000
or pin=	18043140200000
or pin=	18043210240000
or pin=	18043300240000
or pin=	18044160140000
or pin=	18051070070000
or pin=	18051130100000
or pin=	18051180230000
or pin=	18051230140000
or pin=	18053100170000
or pin=	18054090080000
or pin=	18054170140000
or pin=	18061090030000
or pin=	18061300220000
or pin=	18061310200000
or pin=	18062120190000
or pin=	18063000180000
or pin=	18063140130000
or pin=	18064070230000
or pin=	18071100020000
or pin=	18072050250000
or pin=	18072090280000
or pin=	18073090410000
or pin=	18073110050000
or pin=	18081070550000
or pin=	18082110040000
or pin=	18083150060000
or pin=	18091020110000
or pin=	18091050130000
or pin=	18091240010000
or pin=	18091310170000
or pin=	18091310220000
or pin=	18093070020000
or pin=	18094000290000
or pin=	18094090100000
or pin=	18094110340000
or pin=	18111230340000
or pin=	18111300180000
or pin=	18161070070000
or pin=	18162020190000
or pin=	18162050170000
or pin=	18163060070000
or pin=	18171090150000
or pin=	18172090100000
or pin=	18172120190000
or pin=	18172130260000
or pin=	18181030150000
or pin=	18182200310000
or pin=	18184010210000
or pin=	18192100070000
or pin=	18272010060000
or pin=	18323060110000
or pin=	18323150380000)    bs=1.
*select if bs=0.

compute bsf=sqftb.

compute n11=0.
compute n12=0.
compute n13=0.
compute n22=0.
compute n31=0.
compute n32=0.
compute n41=0. 
compute n42=0.
compute n52=0.
compute n53=0.
compute n61=0.
compute n62=0.
compute n64=0.
compute n65=0.
compute n67=0.		
compute n71=0.
compute n82=0. 
compute n84=0. 
compute n91=0. 
compute n101=0.
compute n102=0. 
compute n111=0. 
compute n120=0. 
compute n121=0. 
compute n131=0. 
compute n132=0.
compute n133=0. 
compute n151=0. 
compute n161=0. 
compute n162=0. 
compute n165=0. 
compute n166=0. 
compute n200=0. 

if (nghcde=11) n11=1.
if (nghcde=12) n12=1.
if (nghcde=13) n13=1.
if (nghcde=22) n22=1.
if (nghcde=31) n31=1.
if (nghcde=32) n32=1.
if (nghcde=41) n41=1.
if (nghcde=42) n42=1.
if (nghcde=52) n52=1.
if (nghcde=53) n53=1.
if (nghcde=61) n61=1.
if (nghcde=62) n62=1.
if (nghcde=64) n64=1.
if (nghcde=65) n65=1.
if (nghcde=67) n67=1.
if (nghcde=71) n71=1.
if (nghcde=82) n82=1.
if (nghcde=84) n84=1.
if (nghcde=91) n91=1.
if (nghcde=101) n101=1.
if (nghcde=102) n102=1.
if (nghcde=111) n111=1.
if (nghcde=120) n120=1.
if (nghcde=121) n121=1.
if (nghcde=131) n131=1.
if (nghcde=132) n132=1.
if (nghcde=133) n133=1.
if (nghcde=151) n151=1.
if (nghcde=161) n161=1.
if (nghcde=162) n162=1.
if (nghcde=165) n165=1.
if (nghcde=166) n166=1.
if (nghcde=200) n200=1.

If town=21 and nghcde=11   n=8.02.
If town=21 and nghcde=12   n=4.99.
If town=21 and nghcde=13   n=4.54.
If town=21 and nghcde=22   n=3.86.
If town=21 and nghcde=31   n=4.35.
If town=21 and nghcde=32   n=5.77.
If town=21 and nghcde=41   n=2.36. 
If town=21 and nghcde=42   n=2.25.
If town=21 and nghcde=52   n=1.86.
If town=21 and nghcde=53   n=4.10.
If town=21 and nghcde=61   n=7.30.
If town=21 and nghcde=62   n=8.89.
If town=21 and nghcde=64   n=4.54.
If town=21 and nghcde=65   n=2.15.
If town=21 and nghcde=67   n=6.10.		
If town=21 and nghcde=71   n=4.68.
If town=21 and nghcde=82   n=3.00. 
If town=21 and nghcde=84   n=2.75. 
If town=21 and nghcde=91   n=1.85. 
If town=21 and nghcde=101  n=2.17.
If town=21 and nghcde=102  n=1.91. 
If town=21 and nghcde=111  n=1.97. 
If town=21 and nghcde=120  n=2.33. 
If town=21 and nghcde=121  n=2.48. 
If town=21 and nghcde=131  n=2.88. 
If town=21 and nghcde=132  n=2.81.
If town=21 and nghcde=133  n=4.15. 
If town=21 and nghcde=151  n=3.25. 
If town=21 and nghcde=161  n=3.10. 
If town=21 and nghcde=162  n=2.72. 
If town=21 and nghcde=165  n=4.25. 
If town=21 and nghcde=166  n=5.42. 
If town=21 and nghcde=200  n=10.10.

compute sb11=0.
compute sb12=0.
compute sb13=0.
compute sb22=0.
compute sb31=0.
compute sb32=0.
compute sb41=0.
compute sb42=0.
compute sb52=0.
compute sb53=0.
compute sb61=0.
compute sb62=0.
compute sb64=0.
compute sb65=0.
compute sb67=0.
compute sb71=0.
compute sb82=0.
compute sb84=0.
compute sb91=0.
compute sb101=0.
compute sb102=0.
compute sb111=0.
compute sb120=0.
compute sb121=0.
compute sb131=0.
compute sb132=0.
compute sb133=0.
compute sb151=0.
compute sb161=0.
compute sb162=0.
compute sb165=0.
compute sb166=0.
compute sb200=0.

if nghcde=11 sb11=sqrt(bsf).
if nghcde=12 sb12=sqrt(bsf).
if nghcde=13 sb13=sqrt(bsf).
if nghcde=22 sb22=sqrt(bsf).
if nghcde=31 sb31=sqrt(bsf).
if nghcde=32 sb32=sqrt(bsf).
if nghcde=41 sb41=sqrt(bsf).
if nghcde=42 sb42=sqrt(bsf).
if nghcde=52 sb52=sqrt(bsf).
if nghcde=53 sb53=sqrt(bsf).
if nghcde=61 sb61=sqrt(bsf).
if nghcde=62 sb62=sqrt(bsf).
if nghcde=64 sb64=sqrt(bsf).
if nghcde=65 sb65=sqrt(bsf).
if nghcde=67 sb67=sqrt(bsf).
if nghcde=71 sb71=sqrt(bsf).
if nghcde=82 sb82=sqrt(bsf).
if nghcde=84 sb84=sqrt(bsf).
if nghcde=91 sb91=sqrt(bsf).
if nghcde=101 sb101=sqrt(bsf).
if nghcde=102 sb102=sqrt(bsf).
if nghcde=111 sb111=sqrt(bsf).
if nghcde=120 sb120=sqrt(bsf).
if nghcde=121 sb121=sqrt(bsf).
if nghcde=131 sb131=sqrt(bsf).
if nghcde=132 sb132=sqrt(bsf).
if nghcde=133 sb133=sqrt(bsf).
if nghcde=151 sb151=sqrt(bsf).
if nghcde=161 sb161=sqrt(bsf).
if nghcde=162 sb162=sqrt(bsf).
if nghcde=165 sb165=sqrt(bsf).
if nghcde=166 sb166=sqrt(bsf).
if nghcde=200 sb200=sqrt(bsf).

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.


if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute nbsf=n*bsf.
Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nsrbsf=n*srbsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.

If firepl>1 firepl=1.
compute nfirepl=n*firepl.  

Compute bathsum=fullbath + 0.25*halfbath.
Compute nbathsum=n*bathsum.

compute lowzonelyons=0.

compute midzone=0.
if (nghcde=41 or nghcde=52 or nghcde=91 or 
    nghcde=102 or nghcde=111 or nghcde=121) midzone=1.  
if midzone=0  lowzonelyons=1.

Compute midzonelyons=0.
Compute midzonelyons=midzone.

Compute srfxlowblocklyons=0.
if lowzonelyons=1 srfxlowblocklyons=srfx*lowzonelyons.

Compute srfxmidblocklyons=0.
if midzonelyons=1 srfxmidblocklyons=srfx*midzonelyons.

Compute lsf=sqftl.
Compute srlsf=sqrt(sqftl).
Compute nsrlsf=n*srlsf.

if class=95 and nghcde=42  lsf=1700.
if class=95 and nghcde=65  lsf=2700.
if class=95 and nghcde=84  lsf=2600.
if class=95 and nghcde=102 lsf=1600.
if class=95 and nghcde=133 lsf=1850.
if class=95 and nghcde=151 lsf=1350.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1. 
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute jantmar06=0.
if (year1=2006 and (mos>=1 and mos<=3)) jantmar06=1. 
Compute octtdec10=0.
if (year1=2010 and (mos>=10 and mos<=12)) octtdec10=1.

Compute jantmar06cl234=jantmar06*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute octtdec10cl234=octtdec10*cl234.

Compute jantmar06cl56=jantmar06*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute octtdec10cl56=octtdec10*cl56.

Compute jantmar06cl778=jantmar06*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute octtdec10cl778=octtdec10*cl778.

Compute jantmar06cl89=jantmar06*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute octtdec10cl89=octtdec10*cl89.

Compute jantmar06cl1112=jantmar06*cl1112. 
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute octtdec10cl1112=octtdec10*cl1112.

Compute jantmar06cl1095=jantmar06*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute octtdec10cl1095=octtdec10*cl1095.

Compute jantmar06clsplt=jantmar06*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute octtdec10clsplt=octtdec10*clsplt.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.

compute frbsf=frame*bsf.
 
Compute gar1car=0.
Compute gar1hcar=0.
Compute gar2car=0.
Compute gar2hcar=0.
Compute gar3car=0.
Compute gar3hcar=0.
Compute gar4car=0.	

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

compute garage1=0.
compute garage2=0.
compute garage3=0.
compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1. 

Compute biggar=0.
if gar=5 or gar=6 or gar=8  biggar=1. 

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

compute res=bnum - comm.
compute bnumb=bnum*bsf.
compute nres=n*res.

Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute srlsf=sqrt(sqftl).
compute nsrlsf=n*srlsf.
compute nmasbsf=n*mason*bsf.
compute nfrbsf=n*frame*bsf.
compute nfrmsbsf=n*framas*bsf.



Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute b=1.
*select if year1 = 2010.
*select if puremarket=1.
*Table observation = b
                amount1
               /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

compute mv = (249472.0046	
+ 9.486228978*nbsf
- 7869.165083*sb62
- 5681.867357	*srfxlowblocklyons
+ 819.8140671*nbsf778
+ 1332.288974*nbsf56
+ 1682.691756*sb22
+ 1928.025497*sb84
- 2145.899999*lowzonelyons
+ 6941.356208*nbathsum
- 2618.929344*sb11
+ 909.1179475	*bsf234
+ 786.2178261*nbsf34
+ 1552.013759*sb31
+ 51184.94772*biggar
+ 1127.29298*nbsf89
+ 1748.029557*sb151
- 3632.012465*sb200
- 2005.439963	*sb67
+ 1158.842617*sb82
+ 13698.84434*npremrf
+ 1019.935751*sb13
+ 684.277996*sb12
+ 1545.424523*sb132
- 2.729255516*nfrbsf
+ 4.356614301*nluxbsf
+ 8912.596554*garage2
- 1148.478979*sb165
+ 65.80374353*nsrlsf
- 2032.925424*nsrage
+ 8562.239542*nbaspart
+ 10097.82695*nbasfull
+ 783.1509207*sb161
+ 497.675984*sb102
- 14160.16697*midzonelyons
- 7761.681342	*srfx
+ 978.6245247*nbsf1095
+ 249.4711469*nbsf1112
- 0.589509014*nfrmsbsf
+ 5672.982028*garage1
+ 0.404111996*nbsfair
+ 1008.9758*nfirepl
+ 1245.927564*sb65)*1.0.




save outfile='C:\Program Files\SPSS\spssa\mv21.sav'/keep town pin mv.
 


  