                        *SPSS Regression.
	                       *HANOVER  2013.

Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt18mergefcl2.sav'.
*select if (amount1>90000).
*select if (amount1<750000).
*select if (multi<1).
*select if sqftb<7000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=12 and amount1>0) year1=2012.
If (yr=11 and amount1>0) year1=2011.
If (yr=10 and amount1>0) year1=2010. 
If  (yr=9 and amount1>0) year1=2009. 
If  (yr=8 and amount1>0) year1=2008.
If  (yr=7 and amount1>0) year1=2007.
If  (yr=6 and amount1>0) year1=2006. 
If  (yr=5 and amount1>0) year1=2005.
If  (yr=4 and amount1>0) year1=2004.

*select if (year1>2007).
set mxcells=2000500.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	6182140190000
or pin=	6192020080000
or pin=	6192030200000
or pin=	6283140140000
or pin=	6323060020000
or pin=	6273090120000
or pin=	6261020210000
or pin=	6362120110000
or pin=	6181040280000
or pin=	6084040290000) bs=1.
*select if bs=0.

compute bsf=sqftb.
Compute N=1.

COMPUTE FX = cumfile78910111213.
IF FX >= 8  FX = 8.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).

*select if puremarket=1. 

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000) + nghcde.

if (tnb= 18010 ) n=1.52.
if (tnb= 18011 ) n=1.48.
if (tnb= 18012 ) n=2.49.
if (tnb= 18013 ) n=2.20.
if (tnb= 18014 ) n=1.85.
if (tnb= 18015 ) n=2.45.
if (tnb= 18018 ) n=3.85.
if (tnb= 18019 ) n=3.39.
if (tnb= 18020 ) n=1.43.
if (tnb= 18030 ) n=3.76.
if (tnb= 18035 ) n=4.14.
if (tnb= 18040 ) n=1.44.
if (tnb= 18050 ) n=1.97.
if (tnb= 18060 ) n=1.49. 
if (tnb= 18070 ) n=1.75.
if (tnb= 18075 ) n=1.98.
if (tnb= 18080 ) n=1.74.
if (tnb= 18082 ) n=3.38.
if (tnb= 18090 ) n=3.28.


compute sb18010=0.
compute sb18011=0.
compute sb18012=0.
compute sb18013=0.
compute sb18014=0.
compute sb18015=0.
compute sb18018=0.
compute sb18019=0.
compute sb18020=0.
compute sb18030=0.
compute sb18035=0.
compute sb18040=0.
compute sb18050=0.
compute sb18060=0.
compute sb18070=0.
compute sb18075=0. 
compute sb18080=0.
compute sb18082=0.
compute sb18090=0.

compute n18010=0.
compute n18011=0.
compute n18012=0.
compute n18013=0.
compute n18014=0.
compute n18015=0.
compute n18018=0.
compute n18019=0.
compute n18020=0.
compute n18030=0.
compute n18035=0.
compute n18040=0.
compute n18050=0.
compute n18060=0. 
compute n18070=0.
compute n18075=0.
compute n18080=0.
compute n18082=0.
compute n18090=0.

if (tnb= 18010 ) n18010=1.
if (tnb= 18011 ) n18011=1.
if (tnb= 18012 ) n18012=1.
if (tnb= 18013 ) n18013=1.
if (tnb= 18014 ) n18014=1.
if (tnb= 18015 ) n18015=1.
if (tnb= 18018 ) n18018=1.
if (tnb= 18019 ) n18019=1.
if (tnb= 18020 ) n18020=1.
if (tnb= 18030 ) n18030=1.
if (tnb= 18035 ) n18035=1.
if (tnb= 18040 ) n18040=1.
if (tnb= 18050 ) n18050=1.
if (tnb= 18060 ) n18060=1.
if (tnb= 18070 ) n18070=1.
if (tnb= 18075 ) n18075=1.
if (tnb= 18080 ) n18080=1.
if (tnb= 18082 ) n18082=1.
if (tnb= 18090)  n18090=1.

if (tnb= 18010 ) sb18010=sqrt(bsf).
if (tnb= 18011 ) sb18011=sqrt(bsf).
if (tnb= 18012 ) sb18012=sqrt(bsf).
if (tnb= 18013 ) sb18013=sqrt(bsf).
if (tnb= 18014 ) sb18014=sqrt(bsf).
if (tnb= 18015 ) sb18015=sqrt(bsf).
if (tnb= 18018 ) sb18018=sqrt(bsf).
if (tnb= 18019 ) sb18019=sqrt(bsf).
if (tnb= 18020 ) sb18020=sqrt(bsf).
if (tnb= 18030 ) sb18030=sqrt(bsf).
if (tnb= 18035 ) sb18035=sqrt(bsf).
if (tnb= 18040 ) sb18040=sqrt(bsf).
if (tnb= 18050 ) sb18050=sqrt(bsf).
if (tnb= 18060 ) sb18060=sqrt(bsf).
if (tnb= 18070 ) sb18070=sqrt(bsf).
if (tnb= 18075 ) sb18075=sqrt(bsf).
if (tnb= 18080 ) sb18080=sqrt(bsf).
if (tnb= 18082 ) sb18082=sqrt(bsf).
if (tnb= 18090)  sb18090=sqrt(bsf).

Compute lowzonehanover=0.
if town=18 and  (nghcde=15  or nghcde=18  or nghcde=19 
or nghcde=35 or nghcde=50 or nghcde=82 or nghcde=90)  lowzonehanover=1.                         	

Compute midzonehanover=0.
if town=18 and (nghcde=10 or  nghcde=12 or nghcde=13 or nghcde=14 
or nghcde=30 or nghcde=75 or nghcde=80)   midzonehanover=1. 

Compute highzonehanover=0.
if town=18 and (nghcde=11 or nghcde=20 or nghcde=40 
or nghcde=60 or nghcde=70)   highzonehanover=1.


Compute srfxlowblockhanover=0.
if lowzonehanover=1 srfxlowblockhanover=srfx*lowzonehanover.

Compute srfxmidblockhanover=0.
if midzonehanover=1 srfxmidblockhanover=srfx*midzonehanover.

Compute srfxhighblockhanover=0.
if highzonehanover=1 srfxhighblockhanover=srfx*highzonehanover.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute jantmar08=0.
if (year1=2008 and (mos>=1 and mos<=3)) jantmar08=1. 
Compute octtdec12=0.
if (year1=2012 and (mos>=10 and mos<=12)) octtdec12=1.

Compute jantmar08cl234=jantmar08*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute octtdec12cl234=octtdec12*cl234.

Compute jantmar08cl56=jantmar08*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute octtdec12cl56=octtdec12*cl56.

Compute jantmar08cl778=jantmar08*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute octtdec12cl778=octtdec12*cl778.

Compute jantmar08cl89=jantmar08*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute octtdec12cl89=octtdec12*cl89.

Compute jantmar08cl1112=jantmar08*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute octtdec12cl1112=octtdec12*cl1112.

Compute jantmar08cl1095=jantmar08*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute octtdec12cl1095=octtdec12*cl1095.

Compute jantmar08clsplt=jantmar08*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute octtdec12clsplt=octtdec12*clsplt.

Compute lsf=sqftl.

if class=95 and tnb=18010   lsf=6300.
if class=95 and tnb=18011   lsf=7722.
if class=95 and tnb=18012   lsf=7668.
if class=95 and tnb=18013   lsf=7800.
if class=95 and tnb=18014   lsf=8838.
if class=95 and tnb=18015   lsf=8325.
if class=95 and tnb=18019   lsf=10580.	
if class=95 and tnb=18060   lsf=1320.
if class=95 and tnb=18070   lsf=7920.
if class=95 and tnb=18090   lsf=10781.

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.


compute b=1.

compute mv = (23661.49153	
+ 1065.494731*nbsf1095
+ 1495.064999*nbathsum
+ 56319.44216*biggar
- 6011.014827*nsrage
- 777.5896492*sb18030
+ 1.45655*nbsfair
+ 49764.57222*highzonehanover
+ 14.26891799*masbsf
+ 5791.392089*nnobase
+ 54.62658924*nsrlsf
+ 3880.947143*nbsf56
- 527.1701121*sb18011
+ 680.2736648*sb18015
+ 26294.94586*garage2
+ 1538.864749*sb18014
+ 1656.032866*nbsf778
+ 2086.791391*sb18050
- 8643.411436*srfxlowblockhanover
+ 1812.826508*nbsf234
+ 1886.64141*nbsf89
+ 1712.428743*nbsf34
+ 4576.144789*nsiteben
- 553.0109088*sb18040
+ 2518.673836*sb18010
- 503.3637151*sb18035
+ 1892.87119*sb18013
+ 1523.030373*sb18012
+ 1984.69196*sb18075
+ 2463.962631*nbsf1112
- 4479.771615*srfxmidblockhanover
+ 1725.302996*sb18080
- 27482.57127*midzonehanover
- 3649.549103*srfxhighblockhanover
+ 10583.53093*nbaspart
+ 12387.51745*nbasfull
+ 1130.968597*nfirepl
+ 14059.75134*garage1
+ 1.657757576*nprembsf
+ 139.2005488*sb18019)*1.0.



save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv18.sav'/keep town pin mv.

   











