					*SPSS Regression.
					*NORTHFIELD 2013.


Get file='C:\Program Files\SPSS\Statistics\19\1\regt25mergefcl2.sav'.
set mxcells=300000.
set wid=125.
set len=59.
*select if (amount1>140000).
*select if (amount1<1700000).
*select if (multi<1).
*select if sqftb<6000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=12 and amount1>0)  year1=2012.
If (yr=11 and amount1>0)  year1=2011.
If (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007. 	 
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005. 
If  (yr=4 and amount1>0)  year1=2004.

*select if (year1>2007).
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin= 4061010240000
or pin=	4074030120000
or pin=	4332060090000
or pin=	4333020200000
or pin=	4333080150000
or pin=	4334070040000
or pin=	4334080160000
or pin=	4061020140000
or pin=	4063090120000
or pin=	4064000460000
or pin=	4064060380000
or pin=	4074100180000
or pin=	4164040070000
or pin=	4102000500000
or pin=	4102000650000
or pin=	4102020150000
or pin=	4102020170000
or pin=	4173000950000
or pin=	4291002420000
or pin=	4032040030000
or pin=	4083030250000
or pin=	4084110150000
or pin=	4091000490000
or pin=	4163040170000
or pin=	4163060100000
or pin=	4163070050000
or pin=	4172020430000
or pin=	4172130250000
or pin=	4174080020000
or pin=	4174180030000
or pin=	4031060030000
or pin=	4031060350000
or pin=	4293000530000
or pin=	4302080050000
or pin=	4092010640000
or pin=	4093070190000
or pin=	4093110130000
or pin=	4093120140000
or pin=	4101080100000
or pin=	4101080330000
or pin=	4112110410000
or pin=	4112160220000
or pin=	4112180510000
or pin=	4112190240000
or pin=	4162010270000
or pin=	4162020320000
or pin=	4162080160000
or pin=	4162120080000
or pin=	4162120140000
or pin=	4162130140000
or pin=	4242000020000
or pin=	4243030090000
or pin=	4243050300000
or pin=	4244000440000
or pin=	4244070290000
or pin=	4244070330000
or pin=	4244140370000
or pin=	4244150240000
or pin=	4252000520000
or pin=	4252020910000
or pin=	4254020270000
or pin=	4263040070000
or pin=	4204080050000
or pin=	4211120260000
or pin=	4214090100000
or pin=	4251020290000
or pin=	4251030110000
or pin=	4251100030000
or pin=	4251160220000
or pin=	4253060140000
or pin=	4253060190000
or pin=	4253130310000
or pin=	4253140270000
or pin=	4253180030000
or pin=	4253180150000
or pin=	4341030090000
or pin=	4341030200000
or pin=	4341050080000
or pin=	4341060100000
or pin=	4342020260000
or pin=	4342020580000
or pin=	4342060330000
or pin=	4342150060000
or pin=	4351090070000
or pin=	4351100100000
or pin=	4351200100000
or pin=	4351230060000
or pin=	4352030170000
or pin=	4352040120000
or pin=	4092050150000
or pin=	4092050220000
or pin=	4092060060000
or pin=	4092070160000
or pin=	4092090080000
or pin=	4092120120000
or pin=	4094000090000
or pin=	4094100360000
or pin=	4094140140000
or pin=	4094140380000
or pin=	4094180110000
or pin=	4103010280000
or pin=	4103080190000
or pin=	4343020150000
or pin=	4343040060000
or pin=	4344020010000
or pin=	4344040300000
or pin=	4344060170000
or pin=	4344150090000
or pin=	4353010130000
or pin=	4353030100000
or pin=	4353060100000
or pin=	4353120550000
or pin=	4353180020000
or pin=	4353180160000
or pin=	4353190120000
or pin=	4353190300000
or pin=	4352070750000
or pin=	4361050220000
or pin=	4211010120000
or pin=	4211080190000
or pin=	4211080210000
or pin=	4212000320000
or pin=	4212090130000
or pin=	4212090220000
or pin=	4212100070000
or pin=	4212100110000
or pin=	4223000190000
or pin=	4271100240000
or pin=	4274220020000
or pin=	4284000420000
or pin=	4341130080000
or pin=	4341150020000
or pin=	4354040040000
or pin=	4354070080000
or pin=	4354083760000
or pin=	4363120260000
or pin=	4364030080000
or pin=	4133030600000
or pin=	4133030620000
or pin=	4234010910000
or pin=	4243000180000
or pin=	4243100110000
or pin=	4244120400000
or pin=	4251010100000
or pin=	4251150390000
or pin=	4251170040000
or pin=	4262030620000
or pin=	4203050340000
or pin=	4283040060000
or pin=	4283070050000
or pin=	4283070070000
or pin=	4284010260000
or pin=	4331120010000
or pin=	4334110050000
or pin=	4334110060000
or pin=	4334110070000
or pin=	4334120140000
or pin=	4171030220000
or pin=	4113000100000
or pin=	4023010240000
or pin=	4081030020000
or pin=	4083110380000
or pin=	4083110610000)    bs=1.                           
*select if bs=0.
compute bsf=sqftb.
Compute N=1.


COMPUTE FX = cumfile78910111213.
if FX >= 4  FX = 4.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute n=1.

Compute n10=0.
If nghcde=10 n10=1.
Compute n11=0.
If nghcde=11 n11=1.
Compute n21=0.
If nghcde=21 n21=1.
Compute n23=0.
If nghcde=23 n23=1.
Compute n30=0.
If nghcde=30 n30=1.
Compute n51=0.
If nghcde=51 n51=1.
Compute n52=0.
If nghcde=52 n52=1.
Compute n61=0.
If nghcde=61 n61=1.
Compute n72=0.
If nghcde=72 n72=1.
Compute n82=0.
If nghcde=82 n82=1.
Compute n90=0.
If nghcde=90 n90=1.
Compute n100=0.
If nghcde=100 n100=1.
Compute n102=0.
If nghcde=102 n102=1.
Compute n110=0.
If nghcde=110 n110=1.
Compute n131=0.
If nghcde=131 n131=1.
Compute n132=0.
If nghcde=132 n132=1.
Compute n140=0.
If nghcde=140 n140=1.
Compute n160=0.
If nghcde=160 n160=1.
Compute n180=0.
If nghcde=180 n180=1.
Compute n190=0.
If nghcde=190 n190=1.
Compute n200=0.
If nghcde=200 n200=1.
Compute n210=0.
If nghcde=210 n210=1.
Compute n220=0.
If nghcde=220 n220=1.
Compute n230=0.
If nghcde=230 n230=1.
Compute n250=0.
If nghcde=250 n250=1.
Compute n260=0.
If nghcde=260 n260=1.
Compute n270=0.
If nghcde=270 n270=1.
Compute n280=0.
If nghcde=280 n280=1.
Compute n300=0.
If nghcde=300 n300=1.
Compute n310=0.
If nghcde=310 n310=1.
Compute n320=0.
If nghcde=320 n320=1.
Compute n350=0.
If nghcde=350 n350=1.
Compute n400=0.
If nghcde=400 n400=1.
Compute n420=0.
If nghcde=420 n420=1.

If nghcde=10 N=4.37.
If nghcde=11 N=3.45.
If nghcde=21 N=5.25.
If nghcde=23 N=4.79.
If nghcde=51 N=5.79.
If nghcde=52 N=5.19.
If nghcde=61 N=3.45.
If nghcde=72 N=3.05.
If nghcde=82 N=12.24.
If nghcde=90 N=4.31.
If nghcde=100 N=5.53.
If nghcde=102 N=5.01.
If nghcde=110 N=7.90.
If nghcde=131 N=5.26.
If nghcde=132 N=3.90.
If nghcde=140 N=4.89.
If nghcde=160 N=5.70.
If nghcde=180 N=6.05.
If nghcde=190 N=4.70.
If nghcde=200 N=8.48.
If nghcde=210 N=6.68.
If nghcde=220 N=7.31.
If nghcde=230 N=5.82.
If nghcde=240 N=6.95.
If nghcde=250 N=4.71.
If nghcde=260 N=3.89.
If nghcde=270 N=3.78.
If nghcde=280 N=3.76.
If nghcde=300 N=8.57.
If nghcde=320 N=8.20.
If nghcde=350 N=3.93.
If nghcde=400 N=4.77.
If nghcde=420 N=5.86.

Compute sb10=0.
Compute sb11=0.
Compute sb21=0.
Compute sb23=0.
Compute sb30=0.
Compute sb51=0.
Compute sb52=0.
Compute sb61=0.
Compute sb72=0.
Compute sb82=0.
Compute sb90=0.
Compute sb100=0.
Compute sb102=0.
Compute sb110=0.
Compute sb131=0.
Compute sb132=0.
Compute sb140=0.
Compute sb160=0.
Compute sb180=0.
Compute sb190=0.
Compute sb200=0.
Compute sb210=0.
Compute sb220=0.
Compute sb230=0.
Compute sb250=0.
Compute sb260=0.
Compute sb270=0.
Compute sb280=0.
Compute sb300=0.
Compute sb310=0.
Compute sb320=0.
Compute sb350=0.
Compute sb400=0.
Compute sb420=0.
If nghcde=10 sb10=sqrt(bsf).
If nghcde=11 sb11=sqrt(bsf).
if nghcde=21 sb21=sqrt(bsf).
If nghcde=23 sb23=sqrt(bsf).
If nghcde=30 sb30=sqrt(bsf).
If nghcde=51 sb51=sqrt(bsf).
if nghcde=52 sb52=sqrt(bsf).
If nghcde=61 sb61=sqrt(bsf).
If nghcde=72 sb72=sqrt(bsf).
If nghcde=82 sb82=sqrt(bsf).
if nghcde=90 sb90=sqrt(bsf).
If nghcde=100 sb100=sqrt(bsf).
If nghcde=102 sb102=sqrt(bsf).
If nghcde=110 sb110=sqrt(bsf).
If nghcde=131 sb131=sqrt(bsf).
If nghcde=132 sb132=sqrt(bsf).
If nghcde=140 sb140=sqrt(bsf).
If nghcde=160 sb160=sqrt(bsf).
If nghcde=180 sb180=sqrt(bsf).
If nghcde=190 sb190=sqrt(bsf).
If nghcde=200 sb200=sqrt(bsf).
If nghcde=210 sb210=sqrt(bsf).
If nghcde=220 sb220=sqrt(bsf).
If nghcde=230 sb230=sqrt(bsf).
If nghcde=250 sb250=sqrt(bsf).
If nghcde=260 sb260=sqrt(bsf).
If nghcde=270 sb270=sqrt(bsf).
If nghcde=280 sb280=sqrt(bsf).
If nghcde=300 sb300=sqrt(bsf).
If nghcde=310 sb310=sqrt(bsf).
If nghcde=320 sb320=sqrt(bsf).
If nghcde=350 sb350=sqrt(bsf).
If nghcde=400 sb400=sqrt(bsf).
If nghcde=420 sb420=sqrt(bsf).
       	

Compute midzonenorthfield=0.
if town=25 and (nghcde=11 or nghcde=61 or nghcde=250 or nghcde=270)   midzonenorthfield=1. 

Compute lowzonenorthfield=0.
if midzonenorthfield=0  lowzonenorthfield=1.                    

Compute srfxlowblocknorthfield=0.
if lowzonenorthfield=1 srfxlowblocknorthfield=srfx*lowzonenorthfield.

Compute srfxmidblocknorthfield=0.
if midzonenorthfield=1 srfxmidblocknorthfield=srfx*midzonenorthfield.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute jantmar08=0.
if (year1=2008 and (mos>=1 and mos<=3)) jantmar08=1. 
Compute octtdec12=0.
if (year1=2012 and (mos>=10 and mos<=12)) octtdec12=1.

Compute jantmar08cl234=jantmar08*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute octtdec12cl234=octtdec12*cl234.

Compute jantmar08cl56=jantmar08*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute octtdec12cl56=octtdec12*cl56.

Compute jantmar08cl778=jantmar08*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute octtdec12cl778=octtdec12*cl778.

Compute jantmar08cl89=jantmar08*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute octtdec12cl89=octtdec12*cl89.

Compute jantmar08cl1112=jantmar08*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute octtdec12cl1112=octtdec12*cl1112.

Compute jantmar08cl1095=jantmar08*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute octtdec12cl1095=octtdec12*cl1095.

Compute jantmar08clsplt=jantmar08*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute octtdec12clsplt=octtdec12*clsplt.

Compute lsf=sqftl.

if class=95 and nghcde=21   lsf=3375.
if class=95 and nghcde=51   lsf=2570.
if class=95 and nghcde=54   lsf=2570.
if class=95 and nghcde=61   lsf=3100.
if class=95 and nghcde=72   lsf=2914.
if class=95 and nghcde=131  lsf=1792.
if class=95 and nghcde=132  lsf=3100.
if class=95 and nghcde=160  lsf=2621.
if class=95 and nghcde=180  lsf=1255.
if class=95 and nghcde=200  lsf=2509.
if class=95 and nghcde=230  lsf=3918.
if class=95 and nghcde=260  lsf=1688.
if class=95 and nghcde=270  lsf=3843.
if class=95 and nghcde=350  lsf=3202.
if class=95 and nghcde=400  lsf=3965.
if class=95 and nghcde=420  lsf=4228.
	

Compute nsrlsf=n*sqrt(lsf).


compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm=n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


if firepl > 2 firepl = (firepl +1)/2.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (year1 >= 2012).
*select if puremarket=1.
*Table observation = b
                amount1
             /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

    
compute mv = (-266429.1129	
+ 6499.520201*nbathsum
- 8337.591396*nsrage
+ 283.1325475*nsrlsf
+ 1211.621518*nsrbsf
+ 6384.542702*sb132
+ 2213.13043*sb180
+ 3355.659537*sb131
+ 98685.99479*biggar
+ 8.705552797*masbsf
- 2155.524918*sb51
+ 4610.947763*nfirepl
- 17824.05857*sb82
+ 7871.511462*sb72
+ 3977.294032*sb140
+ 4690.230483*sb90
+ 43761.5169*garage2
+ 7023.929741*sb11
+ 1641.679703*sb100
+ 3184.436785*sb23
- 55832.53473*SRFX
- 4176.707123*sb210
- 7852.628335*sb220
+ 14661.41703*sb310
+ 78186.4679*nnobase
+ 3394.285952*sb190
- 152.7446444*sb21
+ 2.144102094*nprembsf
+ 17.16027363*frabsf
- 2763.839641*sb420
+ 7026.433871*sb61
- 5699.069181*sb200
- 5088.318958*sb110
- 6113.938051*sb300
+ 6479.030271*sb270
+ 43843.72264*srfxlowblocknorthfield
+ 4716.620548*sb350
+ 3627.522061*sb260
+ 4100.95292*sb280
+ 1.904437747*nrepabsf
+ 85899.44679*nbasfull
+ 83404.28628*nbaspart
+ 2428.201982*sb10
+ 2973.270065*sb250
+ 822.614696*sb52
+ 1897.925471*sb400
+ 438.0427235*sb102
- 6069.939105*midzonenorthfield
+ 0.34439566*nbsfair
+ 22282.8548*garage1
- 11.21403713*frastbsf)*1.0.

* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=25.
*sel if puremarket=1 and yr1=12.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv25.sav'/keep town pin mv.
