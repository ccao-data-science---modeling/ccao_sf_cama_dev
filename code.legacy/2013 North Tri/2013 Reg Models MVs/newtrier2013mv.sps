                           	*SPSS Regression.
	   		             *EVANSTON & NEW TRIER 2013.

Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt17andregt23mergefcl2.sav'.
*select if (amount1>140000).
*select if (amount1<9700000).
*select if (multi<1).
*select if sqftb<12000.

compute bsf=sqftb.
compute lsf=sqftl.

Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
If  (yr=12 and amount1>0) year1=2012.
If  (yr=11 and amount1>0) year1=2011.
If  (yr=10 and amount1>0) year1=2010. 
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007.
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005. 
If  (yr=4 and amount1>0)  year1=2004. 
exe.
*select if (year1>2007).
set mxcells=2000500.

Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	10111000390000
or pin=	10111000600000
or pin=	10113000490000
or pin=	10113000510000
or pin=	10113070180000
or pin=	10113080250000
or pin=	10113150190000
or pin=	10113160080000
or pin=	10113160120000
or pin=	10114110350000
or pin=	10121030350000
or pin=	10121060210000
or pin=	10123030090000
or pin=	10123030190000
or pin=	10123080010000
or pin=	10123090210000
or pin=	10123120120000
or pin=	10123140080000
or pin=	10123150050000
or pin=	10142040520000
or pin=	11071000180000
or pin=	10112040160000
or pin=	10112040280000
or pin=	10113230210000
or pin=	10114010140000
or pin=	10114060090000
or pin=	11181030290000
or pin=	11181080290000
or pin=	11071060240000
or pin=	11071070030000
or pin=	11071160310000
or pin=	11184050170000
or pin=	11184150210000
or pin=	11192080250000
or pin=	11192240250000
or pin=	5354000020000
or pin=	11192060140000
or pin=	10124140060000
or pin=	10131070240000
or pin=	10131100360000
or pin=	10131140540000
or pin=	10131210080000
or pin=	10132000030000
or pin=	10132030060000
or pin=	10132040130000
or pin=	10132080090000
or pin=	10132110190000
or pin=	11194100200000
or pin=	11194130480000
or pin=	11194160130000
or pin=	11191100150000
or pin=	11193000200000
or pin=	11193090050000
or pin=	11193090060000
or pin=	11193090110000
or pin=	11193110030000
or pin=	11193120290000
or pin=	11193160290000
or pin=	11194060050000
or pin=	10132180210000
or pin=	10134100110000
or pin=	10134230060000
or pin=	10134230200000
or pin=	10242040020000
or pin=	11191070130000
or pin=	11191120060000
or pin=	11191190240000
or pin=	10244020050000
or pin=	10244030220000
or pin=	10244040380000
or pin=	10244070010000
or pin=	10244080380000
or pin=	10244100110000
or pin=	10244130310000
or pin=	10244130370000
or pin=	10244160210000
or pin=	10252010420000
or pin=	10252020260000
or pin=	10252080240000
or pin=	10252110230000
or pin=	10252130190000
or pin=	11193210360000
or pin=	11193280090000
or pin=	11301000270000
or pin=	11301200200000
or pin=	11302060140000
or pin=	5333210260000
or pin=	5334140050000
or pin=	5334160050000
or pin=	5334180090000
or pin=	5334200130000
or pin=	5334230070000
or pin=	5334260010000
or pin=	5334260030000
or pin=	5334280030000
or pin=	5334280080000
or pin=	5343100150000
or pin=	5343210350000
or pin=	5353130160000
or pin=	10133180170000
or pin=	10133210100000
or pin=	10134270070000
or pin=	10241130320000
or pin=	10241180120000
or pin=	10243080060000
or pin=	10113120070000
or pin=	10242010250000
or pin=	10242100110000
or pin=	10242110160000
or pin=	10242110250000
or pin=	10242180160000
or pin=	4014000560000
or pin=	4014010490000
or pin=	4014070020000
or pin=	4014120330000
or pin=	5063080020000
or pin=	5063080100000
or pin=	4122000070000
or pin=	4122000290000
or pin=	4122020030000
or pin=	4122130080000
or pin=	4122140080000
or pin=	5062000160000
or pin=	5062000260000
or pin=	5063010070000
or pin=	5063010100000
or pin=	5063040160000
or pin=	5064000180000
or pin=	5064000210000
or pin=	5064020270000
or pin=	5064070040000
or pin=	5083000030000
or pin=	5083060100000
or pin=	5083130160000
or pin=	5083170060000
or pin=	5083180280000
or pin=	5083190020000
or pin=	5083190080000
or pin=	5083190230000
or pin=	5083200080000
or pin=	5083200140000
or pin=	5171030080000
or pin=	5171030200000
or pin=	5172020190000
or pin=	5161040020000
or pin=	5171070040000
or pin=	5171070630000
or pin=	5171120040000
or pin=	5171140030000
or pin=	5173000350000
or pin=	5173010120000
or pin=	5173100040000
or pin=	5173100230000
or pin=	5174000060000
or pin=	5174000130000
or pin=	5174000280000
or pin=	5174020110000
or pin=	5174030280000
or pin=	5174040150000
or pin=	5174080240000
or pin=	5174130010000
or pin=	5174130060000
or pin=	5174150100000
or pin=	5174170150000
or pin=	5174170200000
or pin=	5211030090000
or pin=	5211110050000
or pin=	5211160010000
or pin=	5211240030000
or pin=	5213030020000
or pin=	5213200080000
or pin=	5214070130000
or pin=	5214070230000
or pin=	5282020010000
or pin=	5282020150000
or pin=	5074010020000
or pin=	5074010050000
or pin=	5074050170000
or pin=	5074110100000
or pin=	5074120330000
or pin=	5074190330000
or pin=	5182000060000
or pin=	5182030010000
or pin=	5182050210000
or pin=	5182070140000
or pin=	5182110060000
or pin=	5182110070000
or pin=	5182120090000
or pin=	5182200050000
or pin=	5074090040000
or pin=	5074090220000
or pin=	5074090270000
or pin=	5063140150000
or pin=	5071060070000
or pin=	5072000120000
or pin=	5201060060000
or pin=	5201090030000
or pin=	5201090080000
or pin=	5201170070000
or pin=	5201210030000
or pin=	5201210150000
or pin=	5202150240000
or pin=	5202150300000
or pin=	5202200220000
or pin=	5202220020000
or pin=	5201110080000
or pin=	5201150160000
or pin=	5203000370000
or pin=	5203000380000
or pin=	5181060110000
or pin=	5183080200000
or pin=	5183080440000
or pin=	5184020810000
or pin=	5184030760000
or pin=	5173130050000
or pin=	5173150080000
or pin=	5173150090000
or pin=	5174100050000
or pin=	5174100410000
or pin=	5174120070000
or pin=	5174120180000
or pin=	5203120060000
or pin=	5203180310000
or pin=	5204000380000
or pin=	5204070770000
or pin=	5204130040000
or pin=	5281060260000
or pin=	5291010040000
or pin=	5291010510000
or pin=	5291020880000
or pin=	5291030520000
or pin=	5293000170000
or pin=	5202280110000
or pin=	5204060200000
or pin=	5211270050000
or pin=	5281010030000
or pin=	5281020190000
or pin=	5281030890000
or pin=	5281080030000
or pin=	5282070240000
or pin=	5283050500000
or pin=	5283070780000
or pin=	5283080670000
or pin=	5283150200000
or pin=	5283160040000
or pin=	5283160070000
or pin=	5283160200000
or pin=	5283170050000
or pin=	5284100260000
or pin=	5284120120000
or pin=	5284150310000
or pin=	5284170020000
or pin=	5284170160000
or pin=	5284250220000
or pin=	5284250300000
or pin=	5294040460000
or pin=	5294140020000
or pin=	5331000580000
or pin=	5331000660000
or pin=	5331010020000
or pin=	5331020040000
or pin=	5331070570000
or pin=	5332010070000
or pin=	5332020350000
or pin=	5334020050000
or pin=	5334040050000
or pin=	5341040030000
or pin=	5341040060000
or pin=	5341120050000
or pin=	5341150220000
or pin=	5343000140000
or pin=	5343050210000
or pin=	5343060130000
or pin=	5344000250000
or pin=	5344080090000
or pin=	5353000090000
or pin=	5353020200000
or pin=	5342140090000
or pin=	5342170080000
or pin=	5342210230000
or pin=	5344030030000
or pin=	5351010080000
or pin=	5351090080000
or pin=	5304061030000
or pin=	5312030360000
or pin=	5312090020000
or pin=	5312090050000
or pin=	5312120150000
or pin=	5314080890000
or pin=	5323000020000
or pin=	5271000140000
or pin=	5271030070000
or pin=	5271070020000
or pin=	5282030040000
or pin=	5282060320000
or pin=	5282120060000
or pin=	5282140040000
or pin=	5282190150000
or pin=	5284010180000
or pin=	5284010190000
or pin=	5284010240000
or pin=	5284010300000
or pin=	5193130140000
or pin=	5193190030000
or pin=	5193260190000
or pin=	5193260210000
or pin=	5194130130000
or pin=	5303010250000
or pin=	5303020150000
or pin=	5303110030000
or pin=	5311110100000
or pin=	5312070190000
or pin=	5312220270000
or pin=	5313040400000
or pin=	5313040430000
or pin=	5313070330000
or pin=	5313090170000
or pin=	5313110520000
or pin=	5313120430000
or pin=	5313190010000
or pin=	5313230110000
or pin=	5313230370000
or pin=	5314060120000
or pin=	5314210190000
or pin=	5314210210000
or pin=	5293070370000
or pin=	5293150220000
or pin=	5294080090000
or pin=	5294080160000
or pin=	5294190070000
or pin=	5294250090000
or pin=	5302010470000
or pin=	5302010510000
or pin=	5302020590000
or pin=	5273040190000
or pin=	5273060090000
or pin=	5273180040000
or pin=	5273210020000
or pin=	5274010030000
or pin=	5274070010000
or pin=	5274080050000
or pin=	5274120050000
or pin=	5274150050000
or pin=	5321030480000
or pin=	5322000290000
or pin=	5322000830000
or pin=	5322010410000
or pin=	5322010800000
or pin=	5323100190000
or pin=	5323110020000
or pin=	5323110040000
or pin=	5323120160000
or pin=	5323120240000
or pin=	5323140190000
or pin=	5324001040000
or pin=	5331110470000
or pin=	5331160060000
or pin=	5331170350000
or pin=	5333000140000
or pin=	5333230080000
or pin=	5333230250000
or pin=	5081000080000
or pin=	5083140170000
or pin=	5083210210000
or pin=	5064040390000
or pin=	5161060340000) bs=1.
*select if bs=0.


Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.




compute tnb=(town*1000) + nghcde.

if (tnb= 17011 ) n=3.89.
if (tnb= 17013 ) n=4.79.
if (tnb= 17020 ) n=7.74.
if (tnb= 17041 ) n=4.51.
if (tnb= 17042 ) n=7.50.
if (tnb= 17043 ) n=8.22.
if (tnb= 17050 ) n=10.47.
if (tnb= 17060 ) n=3.13.
if (tnb= 17071 ) n=4.95.
if (tnb= 17080 ) n=4.30.
if (tnb= 17090 ) n=5.66.
if (tnb= 17110 ) n=3.23.
if (tnb= 17112 ) n=3.43.
if (tnb= 17120 ) n=5.05.
if (tnb= 17130 ) n=2.79. 
if (tnb= 17140 ) n=2.72. 
if (tnb= 17200 ) n=5.23.
if (tnb= 17210 ) n=3.28.
if (tnb= 23010 ) n=11.14.
if (tnb= 23011 ) n=6.28.
if (tnb= 23021 ) n=11.59.
if (tnb= 23022 ) n=9.90.
if (tnb= 23041 ) n=7.30.
if (tnb= 23042 ) n=8.21.
if (tnb= 23043 ) n=9.71.
if (tnb= 23044 ) n=7.77.
if (tnb= 23045 ) n=5.68.
if (tnb= 23050 ) n=12.00.
if (tnb= 23080 ) n=17.01.
if (tnb= 23091 ) n=8.98.
if (tnb= 23092 ) n=6.57.
if (tnb= 23093 ) n=6.11.
if (tnb= 23094 ) n=8.65.
if (tnb= 23100 ) n=3.2.
if (tnb= 23110 ) n=19.00.
if (tnb= 23120 ) n=3.62.
if (tnb= 23132 ) n=9.50.
if (tnb= 23141 ) n=9.22.
if (tnb= 23150 ) n=4.84.
if (tnb= 23170 ) n=15.23.
if (tnb= 23171 ) n=32.08.


COMPUTE FX = cumfile78910111213.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).


*select if puremarket=1.

compute sb17011=0.
compute sb17013=0.
compute sb17020=0.
compute sb17041=0.
compute sb17042=0.
compute sb17043=0.
compute sb17050=0.
compute sb17060=0.
compute sb17071=0.
compute sb17080=0.
compute sb17090=0.
compute sb17110=0.
compute sb17112=0.
compute sb17120=0.
compute sb17130=0.
compute sb17140=0. 
compute sb17200=0.
compute sb17210=0.
compute sb23010=0.
compute sb23011=0.
compute sb23021=0.
compute sb23022=0.
compute sb23041=0.
compute sb23042=0.
compute sb23043=0.
compute sb23044=0.
compute sb23045=0.
compute sb23050=0.
compute sb23080=0.
compute sb23091=0.
compute sb23092=0.
compute sb23093=0.
compute sb23094=0.
compute sb23100=0.
compute sb23110=0.
compute sb23120=0.
compute sb23132=0.
compute sb23141=0.
compute sb23150=0.
compute sb23170=0.
compute sb23171=0.

compute n17011=0.
compute n17013=0.
compute n17020=0.
compute n17041=0.
compute n17042=0.
compute n17043=0.
compute n17050=0.
compute n17060=0.
compute n17071=0.
compute n17080=0.
compute n17090=0.
compute n17110=0.
compute n17112=0.
compute n17120=0.
compute n17130=0.
compute n17140=0. 
compute n17200=0.
compute n17210=0.
compute n23010=0.
compute n23011=0.
compute n23021=0.
compute n23022=0.
compute n23041=0.
compute n23042=0.
compute n23043=0.
compute n23044=0.
compute n23045=0.
compute n23050=0.
compute n23080=0.
compute n23091=0.
compute n23092=0.
compute n23093=0.
compute n23094=0.
compute n23100=0.
compute n23110=0.
compute n23120=0.
compute n23132=0.
compute n23141=0.
compute n23150=0.
compute n23170=0.
compute n23171=0.


if (tnb= 17011 ) n17011=1.
if (tnb= 17013 ) n17013=1.
if (tnb= 17020 ) n17020=1.
if (tnb= 17041 ) n17041=1.
if (tnb= 17042 ) n17042=1.
if (tnb= 17043 ) n17043=1.
if (tnb= 17050 ) n17050=1.
if (tnb= 17060 ) n17060=1.
if (tnb= 17071 ) n17071=1.
if (tnb= 17080 ) n17080=1.
if (tnb= 17090 ) n17090=1.
if (tnb= 17110 ) n17110=1.
if (tnb= 17112 ) n17112=1.
if (tnb= 17120 ) n17120=1.
if (tnb= 17130 ) n17130=1.
if (tnb= 17140 ) n17140=1.
if (tnb= 17200 ) n17200=1.
if (tnb= 17210 ) n17210=1.
if (tnb= 23010 ) n23010=1.
if (tnb= 23011 ) n23011=1.
if (tnb= 23021 ) n23021=1.
if (tnb= 23022 ) n23022=1.
if (tnb= 23041 ) n23041=1.
if (tnb= 23042 ) n23042=1.
if (tnb= 23043 ) n23043=1.
if (tnb= 23044 ) n23044=1.
if (tnb= 23045 ) n23045=1.
if (tnb= 23050 ) n23050=1.
if (tnb= 23080 ) n23080=1.
if (tnb= 23091 ) n23091=1.
if (tnb= 23092 ) n23092=1.
if (tnb= 23093 ) n23093=1.
if (tnb= 23094 ) n23094=1.
if (tnb= 23100 ) n23100=1.
if (tnb= 23110 ) n23110=1.
if (tnb= 23120 ) n23120=1.
if (tnb= 23132 ) n23132=1.
if (tnb= 23141 ) n23141=1.
if (tnb= 23150 ) n23150=1.
if (tnb= 23170 ) n23170=1.
if (tnb= 23171 ) n23171=1.

if (tnb=17011) sb17011=sqrt(bsf).
if (tnb=17013) sb17013=sqrt(bsf).
if (tnb=17020) sb17020=sqrt(bsf).
if (tnb=17041) sb17041=sqrt(bsf).
if (tnb=17042) sb17042=sqrt(bsf).
if (tnb=17043) sb17043=sqrt(bsf).
if (tnb=17050) sb17050=sqrt(bsf).
if (tnb=17060) sb17060=sqrt(bsf).
if (tnb=17071) sb17071=sqrt(bsf).
if (tnb=17080) sb17080=sqrt(bsf).
if (tnb=17090) sb17090=sqrt(bsf).
if (tnb=17110) sb17110=sqrt(bsf).
if (tnb=17112) sb17112=sqrt(bsf).
if (tnb=17120) sb17120=sqrt(bsf).
if (tnb=17130) sb17130=sqrt(bsf).
if (tnb=17140) sb17140=sqrt(bsf).
if (tnb=17200) sb17200=sqrt(bsf).
if (tnb=17210) sb17210=sqrt(bsf).
if (tnb=23010) sb23010=sqrt(bsf).
if (tnb=23011) sb23011=sqrt(bsf).
if (tnb=23021) sb23021=sqrt(bsf).
if (tnb=23022) sb23022=sqrt(bsf).
if (tnb=23041) sb23041=sqrt(bsf).
if (tnb=23042) sb23042=sqrt(bsf).
if (tnb=23043) sb23043=sqrt(bsf).
if (tnb=23044) sb23044=sqrt(bsf).
if (tnb=23045) sb23045=sqrt(bsf).
if (tnb=23050) sb23050=sqrt(bsf).
if (tnb=23080) sb23080=sqrt(bsf).
if (tnb=23091) sb23091=sqrt(bsf).
if (tnb=23092) sb23092=sqrt(bsf).
if (tnb=23093) sb23093=sqrt(bsf).
if (tnb=23094) sb23094=sqrt(bsf).
if (tnb=23100) sb23100=sqrt(bsf).
if (tnb=23110) sb23110=sqrt(bsf).
if (tnb=23120) sb23120=sqrt(bsf).
if (tnb=23132) sb23132=sqrt(bsf).
if (tnb=23141) sb23141=sqrt(bsf).
if (tnb=23150) sb23150=sqrt(bsf).
if (tnb=23170) sb23170=sqrt(bsf).
if (tnb=23171) sb23171=sqrt(bsf).


compute evn=0.
compute newt=0.
if town=17 evn=1.
if town=23 newt=1.

Compute lowzoneevn=0.
if town=17 and  (nghcde=11 or nghcde=13  or nghcde=20  or nghcde=41 or nghcde=42
or nghcde=43 or nghcde=50 or nghcde=71 or nghcde=80 or nghcde=90 
or nghcde=120 or nghcde=200)  lowzoneevn=1.                         	

Compute midzoneevn=0.
if town=17 and (nghcde=110 or nghcde=112 or nghcde=140 or nghcde=210)  midzoneevn=1. 

Compute highzoneevn=0.
if town=17 and (nghcde=60 or nghcde=130)  highzoneevn=1.


Compute srfxlowblockevn=0.
if lowzoneevn=1 srfxlowblockevn=srfx*lowzoneevn.

Compute srfxmidblockevn=0.
if midzoneevn=1 srfxmidblockevn=srfx*midzoneevn.

Compute srfxhighblockevn=0.
if highzoneevn=1 srfxhighblockevn=srfx*highzoneevn.


Compute midzonenewt=0.
if town=23 and (nghcde=11 or nghcde=100 or nghcde=120)  midzonenewt=1. 

Compute lowzonenewt=0.
if (town=23 and  midzonenewt=0)  lowzonenewt=1.                         	


Compute srfxlowblocknewt=0.
if lowzonenewt=1 srfxlowblocknewt=srfx*lowzonenewt.

Compute srfxmidblocknewt=0.
if midzonenewt=1 srfxmidblocknewt=srfx*midzonenewt.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute winter1112=0.
if (mos > 9 and yr=11) or (mos <= 3 and yr=12) winter1112=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute jantmar08=0.
if (year1=2008 and (mos>=1 and mos<=3)) jantmar08=1. 
Compute octtdec12=0.
if (year1=2012 and (mos>=10 and mos<=12)) octtdec12=1.

Compute jantmar08cl234=jantmar08*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute octtdec12cl234=octtdec12*cl234.

Compute jantmar08cl56=jantmar08*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute octtdec12cl56=octtdec12*cl56.

Compute jantmar08cl778=jantmar08*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute octtdec12cl778=octtdec12*cl778.

Compute jantmar08cl89=jantmar08*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute octtdec12cl89=octtdec12*cl89.


Compute jantmar08cl1112=jantmar08*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute octtdec12cl1112=octtdec12*cl1112.

Compute jantmar08cl1095=jantmar08*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute octtdec12cl1095=octtdec12*cl1095.

Compute jantmar08clsplt=jantmar08*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute octtdec12clsplt=octtdec12*clsplt.


if class=95 and tnb=17011  	lsf=3980.
if class=95 and tnb=17013  	lsf=2814.
if class=95 and tnb=17060  	lsf=2315.
if class=95 and tnb=17071  	lsf=926.
if class=95 and tnb=17080  	lsf=1783.
if class=95 and tnb=17110  	lsf=2789.
if class=95 and tnb=17112 	lsf=2789.
if class=95 and tnb=17120  	lsf=2916.
if class=95 and tnb=17130  	lsf=3006.
if class=95 and tnb=17140  	lsf=2164.
if class=95 and tnb=23093  	lsf=2621.
if class=95 and tnb=23094 	lsf=1313.
if class=95 and tnb=23100  	lsf=2462.
if class=95 and tnb=23120  	lsf=1855.
if class=95 and tnb=23150  	lsf=2160.


compute nsrevnl=0.
compute nsrnewtl=0.
if evn=1 nsrevnl=n*evn*sqrt(lsf).
if newt=1 nsrnewtl=n*newt*sqrt(lsf).
compute srbsf=sqrt(bsf).
compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.50*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).


compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute nsitebsf=nsiteben*bsf.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.
*select if (town=17 and year1>=2012) or (town=23 and year1>=2012).
*select if puremarket=1. 
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').

select if town = 23.

compute mv = (-113767.3737177	
+ 5450.0422628*nbathsum
+ 961.4430066*nbsf89
- 13.8747466*nsrbsf
- 4469.2131385*nsrage
+ 1223.1075787*nbsf56
+ 1006.6366061*nbsf1095
+ 68304.2247901*biggar
+ 1048.6854044*nbsf778
+ 830.3930118*sb23110
+ 1611.8111996*sb23010
+ 2.4431489*nprembsf
+ 4181.6635029*nfirepl
- 12339.5025623*nbaspart
+ 318.4978133*nsrnewtl
+ 602.7706018*nsrevnl
+ 324634.3765324*midzoneevn
+ 3345.5225308*sb23050
- 48295.445554*garnogar
+ 11260.4308102*sb23093
+ 11650.7997011*sb23092
+ 11074.4365113*sb23141
+ 13110.8600821*sb23045
- 38230.5871953*garage1
+ 10979.4880083*sb23044
+ 9328.6012290*sb23022
+ 9703.6680860*sb23094
- 5554.1679370*nbasfull
+ 9758.0552794*sb23091
+ 10060.1940226*sb23150
+ 9099.7369570*sb23041
- 4384.2127303*sb17050
- 2027.5382206*sb17020
- 42049.3389982*srfxhighblockevn
+ 7167.3201940*sb23021
+ 7256.7415691*sb23132
+ 9624.4813725*sb23120
+ 8132.9157155*sb23011
+ 6067.1295776*sb23043
+ 9677.1190769*sb23100
+ 345924.9107749*lowzoneevn
+ 6680.1159370*sb23042
+ 395012.0598283*highzoneevn
+ 210.0437138*totunitb
- 19569.9899065*garage2
+ 1036.3642634*nbsf34
+ 1.3143974*nbsfair
+ 818.2517405*nbsf234
+ 1017.2696638*sb17013
+ 1360.0675945*sb17041
+ 1092.5650601*sb17120
- 9072.3391607*srfxlowblockevn
- 17982.8500913*srfxmidblockevn
- 2604.2294036*srfxlowblocknewt
- 5893.9048856*srfxmidblocknewt)*1.0.



save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv23.sav'/keep town pin mv.


