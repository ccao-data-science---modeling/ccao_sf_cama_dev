					*SPSS Regression.
					*ELK GROVE 2016.


Get file='C:\Program Files\IBM\SPSS\Statistics\19\1\regt16mar18.sav'.
set wid=125.
set len=59.
*select if (amount1>100000).
*select if (amount1<890000).
*select if (multi<1).
*select if sqftb<6000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If (yr=15 and amount1>0)  year1=2015.
If (yr=14 and amount1>0)  year1=2014.
If  (yr=13 and amount1>0) year1=2013. 
If (yr=12 and amount1>0)  year1=2012.
If (yr=11 and amount1>0)  year1=2011.
If (yr=10 and amount1>0)  year1=2010.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.
If  (yr=4 and amount1>0)  year1=2004.
exe.

*select if (year1>2010).
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	8084060570000
or pin=	8092020100000
or pin=	8092050190000
or pin=	8093160230000
or pin=	8101140520000
or pin=	8104010270000
or pin=	8104130060000
or pin=	8154040120000
or pin=	8143080160000
or pin=	8092160390000
or pin=	8092290290000
or pin=	8101010400000
or pin=	8102120230000
or pin=	8122110110000
or pin=	8122260200000
or pin=	8122260210000
or pin=	8132050220000
or pin=	8133140110000
or pin=	8241130220000
or pin=	8243090130000
or pin=	8244000130000
or pin=	8244020180000
or pin=	8244020770000
or pin=	8244021010000
or pin=	8244021050000
or pin=	8312010100000
or pin=	8321040090000
or pin=	8323090100000
or pin=	8323190260000
or pin=	8324040110000
or pin=	8324160270000
or pin=	8324180070000
or pin=	8324190060000
or pin=	8333120010000
or pin=	8214040070000
or pin=	8271070280000
or pin=	8271070290000
or pin=	8282130290000
or pin=	8282160110000
or pin=	8283110250000
or pin=	8332080020000
or pin=	8332090220000
or pin=	8332120400000
or pin=	8334080020000
or pin=	8334150080000
or pin=	8263050070000
or pin=	8284040100000
or pin=	8284040210000
or pin=	8323260400000
or pin=	8111040040000
or pin=	8112080110000
or pin=	8112100070000
or pin=	8112220220000
or pin=	8114310150000
or pin=	8121210460000
or pin=	8123000460000
or pin=	8123000550000
or pin=	8123000570000
or pin=	8123010170000
or pin=	8123140150000
or pin=	8123170170000
or pin=	8123180060000
or pin=	8124020270000
or pin=	8124190050000
or pin=	8124210090000
or pin=	8141040040000
or pin=	8141050050000
or pin=	8141070040000
or pin=	8141100030000
or pin=	8141120210000
or pin=	8142050010000
or pin=	8142060110000
or pin=	8142060120000
or pin=	8142080150000
or pin=	8143040100000
or pin=	8144050110000)   bs=1.
*select if bs=0.

Compute bsf=sqftb.
Compute lsf=sqftl.


compute n=1.
Compute n10=0.
If nghcde=10 n10=1.
Compute n11=0.
If nghcde=11 n11=1.
Compute n12=0.
If nghcde=12 n12=1.
Compute n30=0.
If nghcde=30 n30=1.
Compute n51=0.
If nghcde=51 n51=1.
Compute n61=0.
If nghcde=61 n61=1.
Compute n62=0.
If nghcde=62 n62=1.
Compute n70=0.
If nghcde=70 n70=1.
Compute n85=0.
If nghcde=85 n85=1.
Compute n91=0.
If nghcde=91 n91=1.
Compute n94=0.
If nghcde=94 n94=1.
Compute n95=0.
If nghcde=95 n95=1.
Compute n96=0.
If nghcde=96 n96=1.
Compute n100=0.
If nghcde=100 n100=1.

If nghcde=10 N=3.55.
If nghcde=11 N=3.12.
If nghcde=12 N=2.77.
If nghcde=30 N=2.65.
If nghcde=51 N=2.57.
If nghcde=61 N=2.63.
If nghcde=62 N=2.39.
If nghcde=70 N=4.50.
If nghcde=85 N=4.10.
If nghcde=91 N=3.22.
If nghcde=94 N=3.10.
If nghcde=95 N=4.79.
If nghcde=96 N=5.10.
If nghcde=100 N=3.25.

Compute sb10=0.
Compute sb11=0.
Compute sb12=0.
Compute sb30=0.
Compute sb51=0.
Compute sb61=0.
Compute sb62=0.
Compute sb70=0.
Compute sb85=0.
Compute sb91=0.
Compute sb94=0.
Compute sb95=0.
Compute sb96=0.
Compute sb100=0.

If nghcde=10 sb10=sqrt(bsf).
If nghcde=11 sb11=sqrt(bsf).
If nghcde=12 sb12=sqrt(bsf).
If nghcde=30 sb30=sqrt(bsf).
If nghcde=51 sb51=sqrt(bsf).
If nghcde=61 sb61=sqrt(bsf).
If nghcde=62 sb62=sqrt(bsf).
If nghcde=70 sb70=sqrt(bsf).
If nghcde=85 sb85=sqrt(bsf).
If nghcde=91 sb91=sqrt(bsf).
If nghcde=94 sb94=sqrt(bsf).
If nghcde=95 sb95=sqrt(bsf).
If nghcde=96 sb96=sqrt(bsf). 
If nghcde=100 sb100=sqrt(bsf).

compute bsf=sqftb.
Compute N=1.

*compute FX =  cumfile13141516.
*COMPUTE SRFX = sqrt(fx).
*RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

*Compute lowzoneelkgrove=0.
*if town=16 and  (nghcde=10 or nghcde=11  or nghcde=12 or nghcde=30
       or nghcde=61 or nghcde=70 or nghcde=94 or nghcde=95 or nghcde=96 or nghcde=100) lowzoneelkgrove=1.                         	

*Compute midzoneelkgrove=0.
*if town=16 and (nghcde=51 or nghcde=62 or nghcde=85 or nghcde=91) midzoneelkgrove=1. 

*Compute srfxlowblockelkgrove=0.
*if lowzoneelkgrove=1 srfxlowblockelkgrove=srfx*lowzoneelkgrove.

*Compute srfxmidblockelkgrove=0.
*if midzoneelkgrove=1 srfxmidblockelkgrove=srfx*midzoneelkgrove.



Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

Compute winter1112=0.
if (mos > 9 and yr=11 or (mos <= 3 and yr=12)) winter1112=1.
Compute winter1213=0.
if (mos > 9 and yr=12) or (mos <= 3 and yr=13) winter1213=1.
Compute winter1314=0.
if (mos > 9 and yr=13) or (mos <= 3 and yr=14) winter1314=1.
Compute winter1415=0.
if (mos > 9 and yr=14) or (mos <= 3 and yr=15) winter1415=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1. 
Compute summer12=0.
if (mos > 3 and yr=12) and (mos <= 9 and yr=12) summer12=1.
Compute summer13=0.
if (mos > 3 and yr=13) and (mos <= 9 and yr=13) summer13=1.
Compute summer14=0.
if (mos > 3 and yr=14) and (mos <= 9 and yr=14) summer14=1.
Compute summer15=0.
if (mos > 3 and yr=15) and (mos <= 9 and yr=15) summer15=1.
Compute jantmar11=0.
if (year1=2011 and (mos>=1 and mos<=3)) jantmar11=1. 
Compute octtdec15=0.
if (year1=2015 and (mos>=10 and mos<=12)) octtdec15=1.

Compute jantmar11cl234=jantmar11*cl234.
Compute winter1112cl234=winter1112*cl234.
Compute winter1213cl234=winter1213*cl234.
Compute winter1314cl234=winter1314*cl234.
Compute winter1415cl234=winter1415*cl234.
Compute summer11cl234=summer11*cl234.
Compute summer12cl234=summer12*cl234.
Compute summer13cl234=summer13*cl234.
Compute summer14cl234=summer14*cl234.
Compute summer15cl234=summer15*cl234.
Compute octtdec15cl234=octtdec15*cl234.

Compute jantmar11cl56=jantmar11*cl56.
Compute winter1112cl56=winter1112*cl56.
Compute winter1213cl56=winter1213*cl56.
Compute winter1314cl56=winter1314*cl56.
Compute winter1415cl56=winter1415*cl56.
Compute summer11cl56=summer11*cl56.
Compute summer12cl56=summer12*cl56.
Compute summer13cl56=summer13*cl56.
Compute summer14cl56=summer14*cl56.
Compute summer15cl56=summer15*cl56.
Compute octtdec15cl56=octtdec15*cl56.

Compute jantmar11cl778=jantmar11*cl778.
Compute winter1112cl778=winter1112*cl778.
Compute winter1213cl778=winter1213*cl778.
Compute winter1314cl778=winter1314*cl778.
Compute winter1415cl778=winter1415*cl778.
Compute summer11cl778=summer11*cl778.
Compute summer12cl778=summer12*cl778.
Compute summer13cl778=summer13*cl778.
Compute summer14cl778=summer14*cl778.
Compute summer15cl778=summer15*cl778.
Compute octtdec15cl778=octtdec15*cl778.

Compute jantmar11cl89=jantmar11*cl89.
Compute winter1112cl89=winter1112*cl89.
Compute winter1213cl89=winter1213*cl89.
Compute winter1314cl89=winter1314*cl89.
Compute winter1415cl89=winter1415*cl89.
Compute summer11cl89=summer11*cl89.
Compute summer12cl89=summer12*cl89.
Compute summer13cl89=summer13*cl89.
Compute summer14cl89=summer14*cl89.
Compute summer15cl89=summer15*cl89.
Compute octtdec15cl89=octtdec15*cl89.


Compute jantmar11cl1112=jantmar11*cl1112.
Compute winter1112cl1112=winter1112*cl1112.
Compute winter1213cl1112=winter1213*cl1112.
Compute winter1314cl1112=winter1314*cl1112.
Compute winter1415cl1112=winter1415*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute summer12cl1112=summer12*cl1112.
Compute summer13cl1112=summer13*cl1112.
Compute summer14cl1112=summer14*cl1112.
Compute summer15cl1112=summer15*cl1112.
Compute octtdec15cl1112=octtdec15*cl1112.

Compute jantmar11cl1095=jantmar11*cl1095.
Compute winter1112cl1095=winter1112*cl1095.
Compute winter1213cl1095=winter1213*cl1095.
Compute winter1314cl1095=winter1314*cl1095.
Compute winter1415cl1095=winter1415*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute summer12cl1095=summer12*cl1095.
Compute summer13cl1095=summer13*cl1095.
Compute summer14cl1095=summer14*cl1095.
Compute summer15cl1095=summer15*cl1095.
Compute octtdec15cl1095=octtdec15*cl1095.

Compute jantmar11clsplt=jantmar11*clsplt.
Compute winter1112clsplt=winter1112*clsplt.
Compute winter1213clsplt=winter1213*clsplt.
Compute winter1314clsplt=winter1314*clsplt.
Compute winter1415clsplt=winter1415*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute summer12clsplt=summer12*clsplt.
Compute summer13clsplt=summer13*clsplt.
Compute summer14clsplt=summer14*clsplt.
Compute summer15clsplt=summer15*clsplt.
Compute octtdec15clsplt=octtdec15*clsplt.



Compute lsf=sqftl.
*if lsf > 4500 lsf = 4500  + ((lsf - 4500)/4).

if class=95 and nghcde=12   lsf=2400.
if class=95 and nghcde=51   lsf=3350.
if class=95 and nghcde=61   lsf=3000.
if class=95 and nghcde=62   lsf=3529.
if class=95 and nghcde=94   lsf=3800.
if class=95 and nghcde=100  lsf=3100.
if class=10 and nghcde=100  lsf=3264.

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).

If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.


compute b=1.

*select if (town=16 and year1>=2015).
*select if puremarket=1. 
*Table observation = b
                amount1
          /table = nghcde by amount1 + b      
		/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').

compute mv = (144721.2302	
+ 7899.437408*	nbathsum
+ 4544.603415*nfirepl
+ 1487.74812*sb100
+ 8048.052934*nbsf89
+ 5482.749713*nbsf778
+ 966.9871202*sb30
+ 718.4544151*sb10
+ 3224.035757*sb95
- 19596.09906*nsrage
+ 7133.684434*nbsf56
+ 204.7965612*nsrlsf
+ 72357.06014*biggar
+ 9603.768446*garage2
+ 9.20831373*masbsf
+ 3751.554851*nbsf1095
+ 18840.03825*nbasfull
+ 4299.429011*nbaspart
+ 27911.67918*nsiteben
- 3244.320103*garage1
+ 5880.37498*nbsf34
+ 5110.961288*nbsf234
+ 4085.00595*nbsf1112
- 12828.03542*garnogar
- 424.9382621*sb11
- 248.5152443*sb51
- 196.5956096*sb62
+ 1.545926297*nbsfair
+ 357.9208238*sb91
- 4864.708741*nnobase
+ 5.57244449*frmsbsf
+ 12.25880608*frabsf
- 11.00581827*frastbsf
- 10.65883272*sb12
+ 190.0092924*sb85
+ 81.04641465*sb96)*1.0.

save outfile='C:\Program Files\IBM\SPSS\Statistics\19\1\mv16mar21.sav'/keep town pin mv.
