                        *SPSS Regression.
		   *BARRINGTON AND PALATINE REGRESSION  2010.


Get file='C:\Program Files\SPSS\spssa\regt10mergefcl2.sav'.
*select if (amount1>265000).
*select if (amount1<1900000).
*select if (multi<1).
*select if sqftb<9000.
Compute year1=0.
If  (amount1>0) year1=1900 + yr.
If  (yr=9 and amount1>0)  year1=2009.
If  (yr=8 and amount1>0)  year1=2008.
If  (yr=7 and amount1>0)  year1=2007. 
If  (yr=6 and amount1>0)  year1=2006. 
If  (yr=5 and amount1>0)  year1=2005.  
If  (yr=4 and amount1>0)  year1=2004. 
If  (yr=3 and amount1>0)  year1=2003.
If  (yr=2 and amount1>0)  year1=2002.  
If  (yr=1 and amount1>0)  year1=2001.   
If  (yr=0 and amount1>0)  year1=2000.

*select if (year1>2004).
set mxcells=2500000.
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	1122050080000
or pin=	1011000480000
or pin=	1011130180000
or pin=	1011180110000
or pin=	1011180160000
or pin=	1011200010000
or pin=	1011230460000
or pin=	1011230730000
or pin=	1011240550000
or pin=	1011250090000
or pin=	1011250460000
or pin=	1011250490000
or pin=	1012050060000
or pin=	1012050130000
or pin=	1012050180000
or pin=	1012090060000
or pin=	1012090110000
or pin=	1012150150000
or pin=	1012150230000
or pin=	1013050050000
or pin=	1013050070000
or pin=	1013110120000
or pin=	1013180080000
or pin=	1013200200000
or pin=	1013200290000
or pin=	1014010290000
or pin=	1014030160000
or pin=	1024050070000
or pin=	1044030100000
or pin=	1152000100000
or pin=	1161000110000
or pin=	1211000030000
or pin=	1243000370000
or pin=	1272040050000
or pin=	1134010280000
or pin=	1123030880000
or pin=	1132090090000
or pin=	1264040080000
or pin=	1341060050000
or pin=	1341070010000
or pin=	1344040130000
or pin=	1351010410000
or pin=	1352050090000
or pin=	2041050080000
or pin=	2052100030000
or pin=	2081020200000
or pin=	2083000250000
or pin=	2181040050000
or pin=	2062000430000
or pin=	2062000570000
or pin=	2201030060000
or pin=	2201040200000
or pin=	2184170100000
or pin=	2083060010000
or pin=	2162130040000
or pin=	2172080040000
or pin=	2201050140000
or pin=	2204000090000
or pin=	2211000320000
or pin=	2283011040000
or pin=	2292000060000
or pin=	2292050020000
or pin=	2031020060000
or pin=	2032070030000
or pin=	2032080090000
or pin=	2093020240000
or pin=	2094040040000
or pin=	2094070170000
or pin=	2162080090000
or pin=	2143060350000
or pin=	2144050270000
or pin=	2144120060000
or pin=	2144200060000
or pin=	2153030080000
or pin=	2153030090000
or pin=	2153030480000
or pin=	2154050030000
or pin=	2154250080000
or pin=	2154270010000
or pin=	2212110080000
or pin=	2221030090000
or pin=	2222010420000
or pin=	2222010450000
or pin=	2222050060000
or pin=	2222050060000
or pin=	2222150200000
or pin=	2224010270000
or pin=	2113140190000
or pin=	2121000620000
or pin=	2121000670000
or pin=	2122000350000
or pin=	2142150260000
or pin=	2224080150000
or pin=	2233050010000
or pin=	2274030150000
or pin=	2274081690000
or pin=	2321020120000
or pin=	2082070030000
or pin=	2091130130000
or pin=	2282060080000
or pin=	2282090060000
or pin=	2282110150000
or pin=	2353040070000
or pin=	2354020160000
or pin=	2164000190000
or pin=	2164190010000
or pin=	2102010220000)  bs=1.
*select if bs=0.

compute bsf=sqftb.
Compute N=1.


*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.

COMPUTE FX = cumfile78910.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

*select if puremarket=1.

compute bar=0.
compute pal=0.
if town=10 bar=1.
if town=29 pal=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

compute tnb=(town*1000) + nghcde.

if (tnb= 10011 ) n=3.89.
if (tnb= 10012 ) n=4.32.
if (tnb= 10014 ) n=4.98.
if (tnb= 10021 ) n=7.30.
if (tnb= 10022 ) n=3.55.
if (tnb= 10023 ) n=8.74.
if (tnb= 10024 ) n=6.26.
if (tnb= 10025 ) n=6.36.
if (tnb= 10030 ) n=6.22.
if (tnb= 10031 ) n=10.32.
if (tnb= 10040 ) n=3.61.
if (tnb= 29011 ) n=6.35.
if (tnb= 29013 ) n=4.85.
if (tnb= 29022 ) n=3.21.
if (tnb= 29031 ) n=5.10.
if (tnb= 29032 ) n=4.57.
if (tnb= 29033 ) n=3.10.
if (tnb= 29041 ) n=6.50.
if (tnb= 29042 ) n=5.73.
if (tnb= 29043 ) n=3.85.
if (tnb= 29050 ) n=3.57.
if (tnb= 29060 ) n=3.53.
if (tnb= 29070 ) n=3.14.
if (tnb= 29080 ) n=3.12.
if (tnb= 29082 ) n=5.13.
if (tnb= 29083 ) n=5.05.
if (tnb= 29084 ) n=4.93.
if (tnb= 29090 ) n=4.56.
if (tnb= 29100 ) n=2.85.
if (tnb= 29110 ) n=5.85.
if (tnb= 29120 ) n=7.60.
if (tnb= 29130 ) n=7.43.
if (tnb= 29140 ) n=5.20.
if (tnb= 29170 ) n=3.05.
if (tnb= 29180 ) n=4.85.

compute sb10011=0.
compute sb10012=0.
compute sb10014=0.
compute sb10021=0.
compute sb10022=0.
compute sb10023=0.
compute sb10024=0.
compute sb10025=0.
compute sb10030=0.
compute sb10031=0.
compute sb10040=0.
compute sb29011=0.
compute sb29013=0.
compute sb29021=0. 
compute sb29022=0.
compute sb29031=0.
compute sb29032=0.
compute sb29033=0.
compute sb29041=0.
compute sb29042=0.
compute sb29043=0.
compute sb29050=0.
compute sb29060=0.
compute sb29070=0.
compute sb29080=0.
compute sb29082=0.
compute sb29083=0.
compute sb29084=0.
compute sb29090=0.
compute sb29100=0.
compute sb29110=0.
compute sb29120=0.
compute sb29130=0.
compute sb29140=0.
compute sb29160=0.
compute sb29170=0.
compute sb29180=0.


compute n10011=0.
compute n10012=0.
compute n10014=0.
compute n10021=0.
compute n10022=0.
compute n10023=0.
compute n10024=0.
compute n10025=0.
compute n10030=0.
compute n10031=0.
compute n10040=0.
compute n29011=0.
compute n29013=0.
compute n29021=0.
compute n29022=0. 
compute n29031=0.
compute n29032=0.
compute n29033=0.
compute n29041=0.
compute n29042=0.
compute n29043=0.
compute n29050=0.
compute n29060=0.
compute n29070=0.
compute n29080=0.
compute n29082=0.
compute n29083=0.
compute n29084=0.
compute n29090=0.
compute n29100=0.
compute n29110=0.
compute n29120=0.
compute n29130=0.
compute n29140=0.
compute n29160=0.
compute n29170=0.
compute n29180=0.


if (tnb= 10011 )  n10011=1.
if (tnb= 10012 )  n10012=1.
if (tnb= 10014 )  n10014=1.
if (tnb= 10021 )  n10021=1.
if (tnb= 10022 )  n10022=1.
if (tnb= 10023 )  n10023=1.
if (tnb= 10024 )  n10024=1.
if (tnb= 10025 )  n10025=1.
if (tnb= 10030 )  n10030=1.
if (tnb= 10031 )  n10031=1.
if (tnb= 10040 )  n10040=1.
if (tnb= 29011 )  n29011=1.
if (tnb= 29013 )  n29013=1.
if (tnb= 29021 )  n29021=1.
if (tnb= 29022 )  n29022=1.
if (tnb= 29031 )  n29031=1.
if (tnb= 29032 )  n29032=1.
if (tnb= 29033 )  n29033=1.
if (tnb= 29041 )  n29041=1.
if (tnb= 29042 )  n29042=1.
if (tnb= 29043 )  n29043=1.
if (tnb= 29050 )  n29050=1.
if (tnb= 29060 )  n29060=1.
if (tnb= 29070 )  n29070=1.
if (tnb= 29080 )  n29080=1.
if (tnb= 29082 )  n29082=1.
if (tnb= 29083 )  n29083=1.
if (tnb= 29084 )  n29084=1.
if (tnb= 29090 )  n29090=1.
if (tnb= 29100 )  n29100=1.
if (tnb= 29110 )  n29110=1.
if (tnb= 29120 )  n29120=1.
if (tnb= 29130 )  n29130=1.
if (tnb= 29140 )  n29140=1.
if (tnb= 29160 )  n29160=1.
if (tnb= 29170 )  n29170=1.
if (tnb= 29180 )  n29180=1.

if (tnb= 10011 ) sb10011=sqrt(bsf).
if (tnb= 10012 ) sb10012=sqrt(bsf).
if (tnb= 10014 ) sb10014=sqrt(bsf).
if (tnb= 10021 ) sb10021=sqrt(bsf).
if (tnb= 10022 ) sb10022=sqrt(bsf).
if (tnb= 10023 ) sb10023=sqrt(bsf).
if (tnb= 10024 ) sb10024=sqrt(bsf).
if (tnb= 10025 ) sb10025=sqrt(bsf).
if (tnb= 10030 ) sb10030=sqrt(bsf).
if (tnb= 10031 ) sb10031=sqrt(bsf).
if (tnb= 10040 ) sb10040=sqrt(bsf).
if (tnb= 29011 ) sb29011=sqrt(bsf).
if (tnb= 29013 ) sb29013=sqrt(bsf).
if (tnb= 29022 ) sb29022=sqrt(bsf).
if (tnb= 29031 ) sb29031=sqrt(bsf).
if (tnb= 29032 ) sb29032=sqrt(bsf).
if (tnb= 29033 ) sb29033=sqrt(bsf).
if (tnb= 29041 ) sb29041=sqrt(bsf).
if (tnb= 29042 ) sb29042=sqrt(bsf).
if (tnb= 29043)  sb29043=sqrt(bsf).
if (tnb= 29050 ) sb29050=sqrt(bsf).
if (tnb= 29060 ) sb29060=sqrt(bsf).
if (tnb= 29070 ) sb29070=sqrt(bsf).
if (tnb= 29080 ) sb29080=sqrt(bsf).
if (tnb= 29082 ) sb29082=sqrt(bsf).
if (tnb= 29083 ) sb29083=sqrt(bsf).
if (tnb= 29084 ) sb29084=sqrt(bsf).
if (tnb= 29090 ) sb29090=sqrt(bsf).
if (tnb= 29100 ) sb29100=sqrt(bsf).
if (tnb= 29110 ) sb29110=sqrt(bsf).
if (tnb= 29120 ) sb29120=sqrt(bsf).
if (tnb= 29130 ) sb29130=sqrt(bsf).
if (tnb= 29140 ) sb29140=sqrt(bsf).
if (tnb= 29170 ) sb29170=sqrt(bsf).
if (tnb= 29180 ) sb29180=sqrt(bsf).



Compute lowzonebar=0.
if town=10 and  (nghcde=14 or nghcde=22  or nghcde=24) lowzonebar=1.                         	

Compute midzonebar=0.
if town=10 and (nghcde=12 or nghcde=21 or nghcde=23 or nghcde=30 
 or nghcde=31 or nghcde=40) midzonebar=1. 

Compute highzonebar=0.
if town=10 and (nghcde=11 or nghcde=25) highzonebar=1.


Compute srfxlowblockbar=0.
if lowzonebar=1 srfxlowblockbar=srfx*lowzonebar.

Compute srfxmidblockbar=0.
if midzonebar=1 srfxmidblockbar=srfx*midzonebar.

Compute srfxhighblockbar=0.
if highzonebar=1 srfxhighblockbar=srfx*highzonebar.


Compute lowzonepal=0.
if town=29 and  (nghcde=32 or nghcde=82  or nghcde=83  or nghcde=84 
 or nghcde=110 or nghcde=130)  lowzonepal=1.  

Compute highzonepal=0.
if town=29 and (nghcde=22 or nghcde=50 or nghcde=100) highzonepal=1.
                  	
Compute midzonepal=0.
if town=29 and (nghcde=11 or nghcde=13 or nghcde=21 or nghcde=31 or nghcde=33
  or nghcde=41 or nghcde=42 or nghcde=43 or nghcde=60 or nghcde=70 or nghcde=80
  or nghcde=90 or nghcde=120 or nghcde=140 or nghcde=160 or nghcde=170 or nghcde=180)  midzonepal=1.

Compute srfxlowblockpal=0.
if lowzonepal=1 srfxlowblockpal=srfx*lowzonepal.

Compute srfxmidblockpal=0.
if midzonepal=1 srfxmidblockpal=srfx*midzonepal.

Compute srfxhighblockpal=0.
if highzonepal=1 srfxhighblockpal=srfx*highzonepal.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).


Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.

Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.



Compute lsf=sqftl.

if class=95 and tnb=10031  	lsf=7375.
if class=95 and tnb=29021  	lsf=2340.
if class=95 and tnb=29022  	lsf=2665.
if class=95 and tnb=29033  	lsf=3298.
if class=95 and tnb=29043  	lsf=2988.
if class=95 and tnb=29050  	lsf=1179.
if class=95 and tnb=29060  	lsf=2244.
if class=95 and tnb=29070  	lsf=1760.
if class=95 and tnb=29080  	lsf=2516.
if class=95 and tnb=29084  	lsf=2630.
if class=95 and tnb=29090  	lsf=2887.
if class=95 and tnb=29160 	lsf=2593.
if class=95 and tnb=29170 	lsf=2340.


compute nsrpall=0.
compute nsrbarl=0.
if pal=1 nsrpall=n*pal*sqrt(lsf).
if bar=1 nsrbarl=n*bar*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrpalb=n*pal*sqrt(bsf).
Compute nsrbarb=n*bar*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.


Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frmsbsf=framas*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.


Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

compute b=1.

*select if (town=10 and year1>=2009) or (town=29 and year1>=2009).
*select if puremarket=1. 
*Table observation = b
                amount1
               /table = tnb by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
            mean (amount1 'MEAN SP')
            validn (b '# PROPS').


compute mv=(219440.9102	
+ 3.027888601*nbsf
- 2756.064798*sb10031
- 2041.415067*sb10023
+ 7405.587733*garage1
+ 4357.587086*sb10012
+ 102.3488663*nsrbarl
+ 221.9955159*nsrpall
- 5359.966687*nsrage
+ 68.80522516*masbsf
+ 769.5943314*nbsf56
+ 18401.20435*lowzonebar
+ 7150.213959*nnobase
+ 396.8108044*nbsf1095
+ 12583.15002*nsiteben
+ 2074.579675*nfirepl
+ 57.26800651*frmsbsf
+ 51.39183158*frastbsf
- 1095.284784*nbsf1112
+ 768.8360822*sb29060
+ 162.2917126*nsrbarb
- 39883.57739*srfxmidblockbar
+ 15345.29533*nbasfull
- 1848.236088*sb29011
+ 1.857108782*nprembsf
+ 12385.28549*nbaspart
+ 974.307715*sb29080
+ 2980.159434*sb10014
- 2539.012204*sb29130
+ 1681.071801*sb29170
- 1106.85091*sb29031
+ 2822.908309*nbathsum
- 861.4453982*sb29022
- 1133.342844*sb29041
- 1478.302099*sb29120
- 13322.40694*srfxhighblockpal
+ 31668.23768*biggar
- 1374.132956*sb29082
+ 233.6040008*nbsf34
+ 196.5024665*nbsf234
+ 1152.513941*sb10024
- 5846.428406*SRFX
+ 2210.385299*sb10011
- 315.2145246*sb29084
+ 129.3058133*nbsf778
+ 16869.67428*garage2
- 6.7060468*stubsf
+ 4389.427494*midzonebar
- 45506.50582*highzonebar
- 1744.176819*midzonepal
- 1915.074538*highzonepal)*.83780186.

  
* RUN THIS PROCEDURE TO CALCULATE THE ADJ FACTOR.
*sel if town=10.
*sel if puremarket=1 and year1=2009.
*compute av=mv*0.10.
*compute ratio=av/amount1.
*fre format=notable/var=ratio/sta=med.

save outfile='C:\Program Files\SPSS\spssa\mv10may14.sav'/keep town pin mv.

 	

 
