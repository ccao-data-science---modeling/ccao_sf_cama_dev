					*SPSS Regression.
					*WHEELING 2010.


Get file='C:\Program Files\SPSS\spssa\regt38repsales.sav'.
set mxcells=300000.
set wid=125.
set len=59.
select if (amount1>140000).
select if (amount1<890000).
select if (multi<1).
select if sqftb<7000.

Compute year1=0.
If  (amount1>0) year1=1900 + yr. 
If (yr=9 and amount1>0)  year1=2009.
If (yr=8 and amount1>0)  year1=2008.
If (yr=7 and amount1>0)  year1=2007. 
If (yr=6 and amount1>0)  year1=2006. 
If (yr=5 and amount1>0)  year1=2005.
If (yr=4 and amount1>0)  year1=2004.
If (yr=3 and amount1>0)  year1=2003.
If (yr=2 and amount1>0)  year1=2002.  
If (yr=1 and amount1>0)  year1=2001.   
If (yr=0 and amount1>0)  year1=2000.

select if (year1>2004).
Compute bs=0.
if age<10 and (AMOUNT1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	3354050070000
or pin=	3354170010000
or pin=	3053060010000
or pin=	3014020020000
or pin=	3023040110000
or pin=	3024060080000
or pin=	3362080160000
or pin=	3091020070000
or pin=	3181170490000
or pin=	3312160060000
or pin=	3084040180000
or pin=	3171190120000
or pin=	3174090120000
or pin=	3174110240000
or pin=	3201110090000
or pin=	3162110120000
or pin=	3284020260000
or pin=	3253100580000
or pin=	3321100070000
or pin=	3151080020000
or pin=	3252010010000
or pin=	3292000080000
or pin=	3294130200000
or pin=	3301130250000
or pin=	3302080100000
or pin=	3323000030000
or pin=	3324120060000
or pin=	3344060200000
or pin=	3291040060000
or pin=	3291060030000
or pin=	3291170090000
or pin=	3341250150000
or pin=	3342100170000
or pin=	3344020130000
or pin=	3223030260000
or pin=	3272120010000) bs=1.

select if bs=0.

Compute bsf=sqftb.
Compute lsf=sqftl.

Compute N=1.


*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 , 2008 and 2009 block filings.

COMPUTE FX = filings06 + filings07 + filings08 + filings09. 
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if puremarket=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf=sqftb.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.


If nghcde=21 nghcde=13.

If nghcde=10 N=3.20.
If nghcde=11 N=2.55.
If nghcde=12 N=2.44.
If nghcde=13 N=2.60.
If nghcde=14 N=4.75.
If nghcde=15 N=5.18.
If nghcde=22 N=1.73.
If nghcde=23 N=2.09.
If nghcde=26 N=3.36.
If nghcde=27 N=3.50.
If nghcde=31 N=3.24.
If nghcde=32 N=3.72.
If nghcde=40 N=3.80.
If nghcde=41 N=4.25.
If nghcde=45 N=5.65.
If nghcde=48 N=4.50.
If nghcde=50 N=3.25.
If nghcde=51 N=3.31.
If nghcde=52 N=4.20.
If nghcde=53 N=1.85.
If nghcde=55 N=3.05.
If nghcde=56 N=3.22.
If nghcde=70 N=3.24.
If nghcde=71 N=2.67.
If nghcde=72 N=3.58.
If nghcde=80 N=2.50.
If nghcde=81 N=3.22.
If nghcde=82 N=3.03.
If nghcde=90 N=3.78.
If nghcde=100 N=3.98.
If nghcde=110 N=4.11.




Compute sb10=0.
Compute sb11=0.
Compute sb12=0.
Compute sb13=0.
Compute sb14=0.
Compute sb15=0.
Compute sb22=0.
Compute sb23=0.
Compute sb26=0.
Compute sb27=0.
Compute sb31=0.
Compute sb32=0.
Compute sb40=0.
Compute sb41=0.
Compute sb45=0.
Compute sb48=0.
Compute sb50=0.
Compute sb51=0.
Compute sb52=0.
Compute sb53=0.
Compute sb55=0.
Compute sb56=0.
Compute sb70=0.
Compute sb71=0.
Compute sb72=0.
Compute sb80=0.
Compute sb81=0.
Compute sb82=0.
Compute sb90=0.
Compute sb100=0.
Compute sb110=0.

If nghcde=10 sb10=sqrt(bsf).
If nghcde=11 sb11=sqrt(bsf).
If nghcde=12 sb12=sqrt(bsf).
if nghcde=13 sb13=sqrt(bsf).
If nghcde=14 sb14=sqrt(bsf).
If nghcde=15 sb15=sqrt(bsf).
if nghcde=22 sb22=sqrt(bsf).
If nghcde=23 sb23=sqrt(bsf).
If nghcde=26 sb26=sqrt(bsf).
if nghcde=27 sb27=sqrt(bsf).
If nghcde=31 sb31=sqrt(bsf).
If nghcde=32 sb32=sqrt(bsf).
If nghcde=40 sb40=sqrt(bsf).
if nghcde=41 sb41=sqrt(bsf).
If nghcde=45 sb45=sqrt(bsf).
If nghcde=48 sb48=sqrt(bsf).
If nghcde=50 sb50=sqrt(bsf).
If nghcde=51 sb51=sqrt(bsf).
If nghcde=52 sb52=sqrt(bsf).
If nghcde=53 sb53=sqrt(bsf).
If nghcde=55 sb55=sqrt(bsf).
If nghcde=56 sb56=sqrt(bsf).
If nghcde=70 sb70=sqrt(bsf).
If nghcde=71 sb71=sqrt(bsf).
If nghcde=72 sb72=sqrt(bsf).
If nghcde=80 sb80=sqrt(bsf).
If nghcde=81 sb81=sqrt(bsf).
If nghcde=82 sb82=sqrt(bsf).
If nghcde=90 sb90=sqrt(bsf).
If nghcde=100 sb100=sqrt(bsf).
If nghcde=110 sb110=sqrt(bsf).


Compute lowzonewheeling=0.
if town=38 and  (nghcde=14 or nghcde=15  or nghcde=27  or nghcde=32 or nghcde=40
or nghcde=41 or nghcde=45 or nghcde=48 or nghcde=50 or nghcde=52 
or nghcde=56 or nghcde=72 or nghcde=90 or nghcde=100 or nghcde=110) lowzonewheeling=1.                         	

Compute midzonewheeling=0.
if town=38 and (nghcde=10 or nghcde=11 or nghcde=12 or nghcde=31 or nghcde=51 or nghcde=55
or nghcde=70 or nghcde=71 or nghcde=80 or nghcde=82) midzonewheeling=1. 

Compute highzonewheeling=0.
if town=38 and (nghcde=13 or nghcde=22 or nghcde=23 or nghcde=26  
or nghcde=53 or nghcde=81) highzonewheeling=1.


Compute srfxlowblockwheeling=0.
if lowzonewheeling=1 srfxlowblockwheeling=srfx*lowzonewheeling.

Compute srfxmidblockwheeling=0.
if midzonewheeling=1 srfxmidblockwheeling=srfx*midzonewheeling.

Compute srfxhighblockwheeling=0.
if highzonewheeling=1 srfxhighblockwheeling=srfx*highzonewheeling.


Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
Compute nbsf1095=n*cl1095*sqrt(bsf).

compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).



Compute winter0506=0.
if (mos > 9 and yr=5) or (mos <= 3 and yr=6) winter0506=1.
Compute winter0607=0.
if (mos > 9 and yr=6) or (mos <= 3 and yr=7) winter0607=1.
Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute summer05=0.
if (mos > 3 and yr=5) and (mos <= 9 and yr=5) summer05=1.
Compute summer06=0.
if (mos > 3 and yr=6) and (mos <= 9 and yr=6) summer06=1. 
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1.
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute jantmar05=0.
if (year1=2005 and (mos>=1 and mos<=3)) jantmar05=1. 
Compute octtdec09=0.
if (year1=2009 and (mos>=10 and mos<=12)) octtdec09=1.

Compute jantmar05cl234=jantmar05*cl234.
Compute winter0506cl234=winter0506*cl234.
Compute winter0607cl234=winter0607*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute summer05cl234=summer05*cl234.
Compute summer06cl234=summer06*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute octtdec09cl234=octtdec09*cl234.

Compute jantmar05cl56=jantmar05*cl56.
Compute winter0506cl56=winter0506*cl56.
Compute winter0607cl56=winter0607*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute summer05cl56=summer05*cl56.
Compute summer06cl56=summer06*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute octtdec09cl56=octtdec09*cl56.

Compute jantmar05cl778=jantmar05*cl778.
Compute winter0506cl778=winter0506*cl778.
Compute winter0607cl778=winter0607*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute summer05cl778=summer05*cl778.
Compute summer06cl778=summer06*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute octtdec09cl778=octtdec09*cl778.

Compute jantmar05cl89=jantmar05*cl89.
Compute winter0506cl89=winter0506*cl89.
Compute winter0607cl89=winter0607*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute summer05cl89=summer05*cl89.
Compute summer06cl89=summer06*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute octtdec09cl89=octtdec09*cl89.

Compute jantmar05cl1112=jantmar05*cl1112. 
Compute winter0506cl1112=winter0506*cl1112.
Compute winter0607cl1112=winter0607*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute summer05cl1112=summer05*cl1112.
Compute summer06cl1112=summer06*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute octtdec09cl1112=octtdec09*cl1112.

Compute jantmar05cl1095=jantmar05*cl1095.
Compute winter0506cl1095=winter0506*cl1095.
Compute winter0607cl1095=winter0607*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute summer05cl1095=summer05*cl1095.
Compute summer06cl1095=summer06*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute octtdec09cl1095=octtdec09*cl1095.

Compute jantmar05clsplt=jantmar05*clsplt.
Compute winter0506clsplt=winter0506*clsplt.
Compute winter0607clsplt=winter0607*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute summer05clsplt=summer05*clsplt.
Compute summer06clsplt=summer06*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute octtdec09clsplt=octtdec09*clsplt.

Compute lsf=sqftl.
*if lsf > 4500 lsf = 4500  + ((lsf - 4500)/4).

*if class=95 and nghcde=   lsf=    .
*if class=95 and nghcde=   lsf=    .
*if class=95 and nghcde=   lsf=    .
*if class=95 and nghcde=   lsf=    .
*if class=95 and nghcde=   lsf=    .
*if class=95 and nghcde=   lsf=    .
*if class=95 and nghcde=   lsf=    .	

Compute nsrlsf=n*sqrt(lsf).
compute srbsf=sqrt(bsf).
Compute nsrbsf=n*sqrt(bsf).
Compute nbsf=n*bsf.
Compute srage=sqrt(age).
Compute bsfage=bsf*age.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
compute masbsf=mason*bsf.
Compute stubsf=stucco*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.
compute nbathsum=n*bathsum.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
compute prembsf=premrf*bsf.
compute nprembsf=n*prembsf.

Compute garnogar=0.
Compute garage=0.
If gar=1 gar1car=1.
If gar=2 gar1hcar=1.
If gar=3 gar2car=1.
If gar=4 gar2hcar=1.
If gar=5 gar3car=1.
If gar=6 gar3hcar=1.
If gar=7 garnogar=1.
If gar=8 gar4car=1. 
If gar ne 7 garage=1.

Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute nsrage=n*srage.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=3 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.
compute deluxbsf=qualdlux*bsf.

if num=6 bnum=0.
if num=0 bnum=0.
if num=1 bnum=2.
if num=2 bnum=3.
if num=3 bnum=4.
if num=4 bnum=5.
if num=5 bnum=6.

Compute nbnum=n*bnum.
Compute ncomm = n*comm.
Compute totunit=bnum + comm.
Compute totunitb=totunit*sqrt(bsf).

If firepl>0 firepl=1.
compute nfirepl=n*firepl.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.

Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.


compute b=1.
*select if (year1 >= 2009).
*select if puremarket=1.
*Table observation = b
                amount1
             /table = nghcde by 
                           amount1 
                             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').

     
reg des=defaults cov
      /var=amount1 
	nsrlsf nbsf234 nbsf56 nbsf778 nbsf89 nbsf1112 nbsf1095 nbsf34 
     nbasfull nbaspart nnobase nfirepl nbsfair nsiteben nluxbsf nrepabsf garage1 garage2
	biggar nbathsum nprembsf nsrage masbsf frabsf stubsf frastbsf nbnum srfx
	sb10 sb11 sb12 sb13 sb14 sb15 sb22 sb23 sb26 sb27 sb31 sb32 sb40 sb41 sb45
	sb48 sb50 sb51 sb52 sb53 sb55 sb56 sb70 sb71 sb72 sb80 sb81 sb82 sb90 sb100 sb110 
	jantmar05cl234 winter0506cl234 winter0607cl234 winter0708cl234 winter0809cl234
	summer05cl234 summer06cl234 summer07cl234 summer08cl234 summer09cl234
	octtdec09cl234 jantmar05cl56 winter0506cl56 winter0607cl56 winter0708cl56
	winter0809cl56 summer05cl56 summer06cl56 summer07cl56 summer08cl56
	summer09cl56 octtdec09cl56 jantmar05cl778 winter0506cl778 winter0607cl778
	winter0708cl778 winter0809cl778 summer05cl778 summer06cl778 summer07cl778
    summer08cl778 summer09cl778 octtdec09cl778 jantmar05cl89 winter0506cl89
	winter0607cl89 winter0708cl89 winter0809cl89 summer05cl89 summer06cl89		
	summer07cl89 summer08cl89 summer09cl89 octtdec09cl89 jantmar05cl1112
	winter0506cl1112 winter0607cl1112 winter0708cl1112 winter0809cl1112
	summer05cl1112 summer06cl1112 summer07cl1112 summer08cl1112
	summer09cl1112 octtdec09cl1112 jantmar05cl1095 winter0506cl1095
	winter0607cl1095 winter0708cl1095 winter0809cl1095 summer05cl1095
	summer06cl1095 summer07cl1095 summer08cl1095 summer09cl1095
	octtdec09cl1095 jantmar05clsplt winter0506clsplt winter0607clsplt
	winter0708clsplt winter0809clsplt summer05clsplt summer06clsplt
	summer07clsplt summer08clsplt summer09clsplt octtdec09clsplt
	lowzonewheeling midzonewheeling highzonewheeling srfxlowblockwheeling
     srfxmidblockwheeling srfxhighblockwheeling
	/dep=amount1
    /method=stepwise
	/method=enter jantmar05cl234 winter0506cl234 winter0607cl234 winter0708cl234 
	winter0809cl234 summer05cl234 summer06cl234 summer07cl234 summer08cl234 summer09cl234
	octtdec09cl234 jantmar05cl56 winter0506cl56 winter0607cl56 winter0708cl56
	winter0809cl56 summer05cl56 summer06cl56 summer07cl56 summer08cl56
	summer09cl56 octtdec09cl56 jantmar05cl778 winter0506cl778 winter0607cl778
	winter0708cl778 winter0809cl778 summer05cl778 summer06cl778 summer07cl778
    summer08cl778 summer09cl778 octtdec09cl778 jantmar05cl89 winter0506cl89
	winter0607cl89 winter0708cl89 winter0809cl89 summer05cl89 summer06cl89		
	summer07cl89 summer08cl89 summer09cl89 octtdec09cl89 jantmar05cl1112
	winter0506cl1112 winter0607cl1112 winter0708cl1112 winter0809cl1112
	summer05cl1112 summer06cl1112 summer07cl1112 summer08cl1112
	summer09cl1112 octtdec09cl1112 jantmar05cl1095 winter0506cl1095
	winter0607cl1095 winter0708cl1095 winter0809cl1095 summer05cl1095
	summer06cl1095 summer07cl1095 summer08cl1095 summer09cl1095
	octtdec09cl1095 jantmar05clsplt winter0506clsplt winter0607clsplt
	winter0708clsplt winter0809clsplt summer05clsplt summer06clsplt
	summer07clsplt summer08clsplt summer09clsplt octtdec09clsplt
	/method=enter lowzonewheeling midzonewheeling highzonewheeling
      srfxlowblockwheeling srfxmidblockwheeling srfxhighblockwheeling
	/save pred (pred) resid (resid).
   	sort cases by nghcde pin.	
      	value labels extcon  1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
          /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
          /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
          /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
          /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11'2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).
exe.
*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount with RESID.
*compute badsal=0.
*if perdif>75 badsal=1.
*if perdif<-75 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.

*REPORT FORMAT=automatic list(1)
	/title='Office of the Assessor'
        	'Residential Regression Report'
        	'Town is Wheeling'
    	/ltitle 'Report Ran On)Date' 
    	/rtitle='PAGE)PAGE'
    	/string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          	date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhd'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).

