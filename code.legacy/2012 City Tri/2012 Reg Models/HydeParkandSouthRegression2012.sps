                                  *SPSS Regression.
				    *Hyde Park and South Multiple Regression 2012.


Get file='C:\Program Files\IBM\SPSS\statistics\19\regt70andregt76mergefcl3.sav'.
select if (amount1>75000).
select if (amount1<600000).
select if (multi<1).
select if sqftb<9000.

Compute year1=0.
If (amount1>0) year1=1900 + yr.
If (yr=11 and amount1>0) year1=2011.
If (yr=10 and amount1>0) year1=2010.
If (yr=9 and amount1>0)  year1=2009.
If (yr=8 and amount1>0)  year1=2008.
If (yr=7 and amount1>0)  year1=2007.   
If (yr=6 and amount1>0)  year1=2006. 
If (yr=5 and amount1>0)  year1=2005.
If (yr=4 and amount1>0)  year1=2004.
If (yr=3 and amount1>0)  year1=2003.   
If (yr=2 and amount1>0)  year1=2002.  
If (yr=1 and amount1>0)  year1=2001.   
If (yr=0 and amount1>0)  year1=2000.
select if (year1>2006).

Compute bs=0.
if age<10 and (amount1<1600000 and (amount1/sqftb)<75 and class<95) bs=1.
if (pin=	
or pin=
or pin=
or pin=
or pin=
or pin=
or pin=	                                  	) bs=1.
select if bs=0.

Compute aos=12*(2011-year1) + (13-mos).
compute bsf=sqftb.
Compute N=1.

compute class02=0.
compute class03=0.
compute class04=0.
compute class05=0.
compute class06=0.
compute class07=0.
compute class08=0.
compute class09=0.
compute class10=0.
compute class11=0.
compute class12=0.
compute class34=0.
compute class78=0.
compute class95=0.

compute bsf02=0.
compute bsf03=0.
compute bsf04=0.
compute bsf05=0.
compute bsf06=0.
compute bsf07=0.
compute bsf08=0.
compute bsf09=0.
compute bsf10=0.
compute bsf11=0.
compute bsf12=0.
compute bsf34=0.
compute bsf78=0.
compute bsf95=0.

if class=02 class02=1.
if class=03 class03=1.
if class=04 class04=1.
if class=05 class05=1.
if class=06 class06=1.
if class=07 class07=1.
if class=08 class08=1.
if class=09 class09=1.
if class=10 class10=1.
if class=11 class11=1.
if class=12 class12=1.
if class=34 class34=1.
if class=78 class78=1.
if class=95 class95=1.

Compute apt=0.
If class=11 or class=12 apt=1.
Compute aptbsf=apt*bsf.
Compute naptbsf=n*aptbsf.

compute bsf02=bsf*class02.
compute bsf03=bsf*class03.
compute bsf04=bsf*class04.
compute bsf05=bsf*class05.
compute bsf06=bsf*class06.
compute bsf07=bsf*class07.
compute bsf08=bsf*class08.
compute bsf09=bsf*class09.
compute bsf10=bsf*class10.
compute bsf11=bsf*class11.
compute bsf12=bsf*class12.
compute bsf34=bsf*class34.
compute bsf78=bsf*class78.
compute bsf95=bsf*class95.

Compute cl234=0.
Compute bsf234=0.
if class=2 or class=3 or class=4 cl234=1.
Compute nbsf234=n*cl234*sqrt(bsf).

Compute cl56=0.
compute bsf56=0.
if class=5 or class=6 cl56=1.
Compute nbsf56=n*cl56*sqrt(bsf).

Compute cl89=0.
Compute bsf89=0.
if class=8 or class=9 cl89=1.
Compute nbsf89=n*cl89*sqrt(bsf).

Compute cl778=0.
Compute bsf778=0.
if class=7 or class=78 cl778=1.
Compute nbsf778=n*cl778*sqrt(bsf).

compute cl1095=0.
if class=10 or class=95  cl1095=1.
compute nbsf1095=n*cl1095*sqrt(bsf).


compute clsplt=0.
if class=34 clsplt=1.
compute nbsf34=n*clsplt*sqrt(bsf).

compute cl1112=0.
if class=11 or class=12 cl1112=1.
Compute nbsf1112=n*cl1112*sqrt(bsf).

compute cl95=0.
if class=95 cl95=1.                  	
compute nbsf95=n*cl95*sqrt(bsf).

*******************************************************************************************************************.
***** Compute Block-Level Filings.
* FX includes the own property when counting "block-level filings".
*********** We have combined the filings so that we are now using 2006, 2007 and 2008 block filings.COMPUTE FX = cumfile789.

COMPUTE FX = cumfile789101112.
COMPUTE SRFX = sqrt(fx).
RECODE FX (SYSMIS=0).
*******************************************************************************************************************.

select if puremarket=1.


****************************************************************************************************************.
* The next section computes a  mid-frequency foreclosure zone .
* and a high frequency foreclosure zone based on foreclosure rates. 
* in specific Hyde Park and South neighborhoods (see Hyde Park and South foreclosure maps).
********************************************************************************************.

Compute lowzonehydepark=0.
if town=70 and (nghcde=20 or nghcde=91 or nghcde=101  or nghcde=150  or nghcde=151
or nghcde=240 or nghcde=241  or nghcde=280) lowzonehydepark=1.                         	

Compute midzonehydepark=0.
if town=70 and (nghcde=80 or nghcde=83  or nghcde=111  or nghcde=120 or  nghcde=130
or nghcde=170 or nghcde=180 or nghcde=220 or nghcde=230 or nghcde=250 or nghcde=260) midzonehydepark=1. 

Compute highzonehydepark=0.
if town=70 and (nghcde=10 or nghcde=30  or nghcde=70  or nghcde=100   or nghcde=121
or nghcde=140 or nghcde=210 ) highzonehydepark=1.


Compute srfxlowblockhydepark=0.
if lowzonehydepark=1 srfxlowblockhydepark=srfx*lowzonehydepark.

Compute srfxmidblockhydepark=0.
if midzonehydepark=1 srfxmidblockhydepark=srfx*midzonehydepark.

Compute srfxhighblockhydepark=0.
if highzonehydepark=1 srfxhighblockhydepark=srfx*highzonehydepark.


Compute lowzonesouth=0.
if town=76 and (nghcde=10 or nghcde=11 or nghcde=12  or nghcde=30  
or nghcde=42) lowzonesouth=1. 

Compute midzonesouth=0.
if town=76 and (nghcde=50  or nghcde=60)  midzonesouth=1.   

Compute highzonesouth=0.
if town=76 and (nghcde=40  or nghcde=41 )  highzonesouth=1.


Compute srfxlowblocksouth=0.
if lowzonesouth=1 srfxlowblocksouth=srfx*lowzonesouth.

Compute srfxmidblocksouth=0.
if midzonesouth=1 srfxmidblocksouth=srfx*midzonesouth.

Compute srfxhighblocksouth=0.
if highzonesouth=1 srfxhighblocksouth=srfx*highzonesouth.




*********************************************************************************************.

Compute winter0708=0.
if (mos > 9 and yr=7) or (mos <= 3 and yr=8) winter0708=1.
Compute winter0809=0.
if (mos > 9 and yr=8) or (mos <= 3 and yr=9) winter0809=1.
Compute winter0910=0.
if (mos > 9 and yr=9) or (mos <= 3 and yr=10) winter0910=1.
Compute winter1011=0.
if (mos > 9 and yr=10) or (mos <= 3 and yr=11) winter1011=1.
Compute summer07=0.
if (mos > 3 and yr=7) and (mos <= 9 and yr=7) summer07=1.
Compute summer08=0.
if (mos > 3 and yr=8) and (mos <= 9 and yr=8) summer08=1. 
Compute summer09=0.
if (mos > 3 and yr=9) and (mos <= 9 and yr=9) summer09=1.
Compute summer10=0.
if (mos > 3 and yr=10) and (mos <= 9 and yr=10) summer10=1.
Compute summer11=0.
if (mos > 3 and yr=11) and (mos <= 9 and yr=11) summer11=1.
Compute jantmar07=0.
if (year1=2007 and (mos>=1 and mos<=3)) jantmar07=1. 
Compute octtdec11=0.
if (year1=2011 and (mos>=10 and mos<=12)) octtdec11=1.


Compute jantmar07cl234=jantmar07*cl234.
Compute winter0708cl234=winter0708*cl234.
Compute winter0809cl234=winter0809*cl234.
Compute winter0910cl234=winter0910*cl234.
Compute winter1011cl234=winter1011*cl234.
Compute summer07cl234=summer07*cl234.
Compute summer08cl234=summer08*cl234.
Compute summer09cl234=summer09*cl234.
Compute summer10cl234=summer10*cl234.
Compute summer11cl234=summer11*cl234.
Compute octtdec11cl234=octtdec11*cl234.

Compute jantmar07cl56=jantmar07*cl56.
Compute winter0708cl56=winter0708*cl56.
Compute winter0809cl56=winter0809*cl56.
Compute winter0910cl56=winter0910*cl56.
Compute winter1011cl56=winter1011*cl56.
Compute summer07cl56=summer07*cl56.
Compute summer08cl56=summer08*cl56.
Compute summer09cl56=summer09*cl56.
Compute summer10cl56=summer10*cl56.
Compute summer11cl56=summer11*cl56.
Compute octtdec11cl56=octtdec11*cl56.

Compute jantmar07cl778=jantmar07*cl778.
Compute winter0708cl778=winter0708*cl778.
Compute winter0809cl778=winter0809*cl778.
Compute winter0910cl778=winter0910*cl778.
Compute winter1011cl778=winter1011*cl778.
Compute summer07cl778=summer07*cl778.
Compute summer08cl778=summer08*cl778.
Compute summer09cl778=summer09*cl778.
Compute summer10cl778=summer10*cl778.
Compute summer11cl778=summer11*cl778.
Compute octtdec11cl778=octtdec11*cl778.

Compute jantmar07cl89=jantmar07*cl89.
Compute winter0708cl89=winter0708*cl89.
Compute winter0809cl89=winter0809*cl89.
Compute winter0910cl89=winter0910*cl89.
Compute winter1011cl89=winter1011*cl89.
Compute summer07cl89=summer07*cl89.
Compute summer08cl89=summer08*cl89.
Compute summer09cl89=summer09*cl89.
Compute summer10cl89=summer10*cl89.
Compute summer11cl89=summer11*cl89.
Compute octtdec11cl89=octtdec11*cl89.


Compute jantmar07cl1112=jantmar07*cl1112.
Compute winter0708cl1112=winter0708*cl1112.
Compute winter0809cl1112=winter0809*cl1112.
Compute winter0910cl1112=winter0910*cl1112.
Compute winter1011cl1112=winter1011*cl1112.
Compute summer07cl1112=summer07*cl1112.
Compute summer08cl1112=summer08*cl1112.
Compute summer09cl1112=summer09*cl1112.
Compute summer10cl1112=summer10*cl1112.
Compute summer11cl1112=summer11*cl1112.
Compute octtdec11cl1112=octtdec11*cl1112.


Compute jantmar07cl1095=jantmar07*cl1095.
Compute winter0708cl1095=winter0708*cl1095.
Compute winter0809cl1095=winter0809*cl1095.
Compute winter0910cl1095=winter0910*cl1095.
Compute winter1011cl1095=winter1011*cl1095.
Compute summer07cl1095=summer07*cl1095.
Compute summer08cl1095=summer08*cl1095.
Compute summer09cl1095=summer09*cl1095.
Compute summer10cl1095=summer10*cl1095.
Compute summer11cl1095=summer11*cl1095.
Compute octtdec11cl1095=octtdec11*cl1095.

Compute jantmar07clsplt=jantmar07*clsplt.
Compute winter0708clsplt=winter0708*clsplt.
Compute winter0809clsplt=winter0809*clsplt.
Compute winter0910clsplt=winter0910*clsplt.
Compute winter1011clsplt=winter1011*clsplt.
Compute summer07clsplt=summer07*clsplt.
Compute summer08clsplt=summer08*clsplt.
Compute summer09clsplt=summer09*clsplt.
Compute summer10clsplt=summer10*clsplt.
Compute summer11clsplt=summer11*clsplt.
Compute octtdec11clsplt=octtdec11*clsplt.


compute tnb=(town*1000) + nghcde.

If (tnb=70010) n=2.95. 
If (tnb=70020) n=4.40.
If (tnb=70030) n=2.25.
If (tnb=70070) n=2.00. 
If (tnb=70080) n=1.80. 
If (tnb=70083) n=3.22. 
If (tnb=70091) n=1.70. 
If (tnb=70100) n=1.70. 
If (tnb=70101) n=1.50.
If (tnb=70111) n=1.72. 
If (tnb=70120) n=1.65. 
If (tnb=70121) n=1.51. 
If (tnb=70130) n=1.65. 
If (tnb=70140) n=1.73. 
If (tnb=70150) n=4.72.
If (tnb=70151) n=2.32. 
If (tnb=70170) n=1.42. 
If (tnb=70180) n=1.25. 
If (tnb=70210) n=1.80. 
If (tnb=70220) n=1.60. 
If (tnb=70230) n=1.53. 
If (tnb=70240) n=1.45. 
If (tnb=70241) n=2.53. 
If (tnb=70250) n=1.48. 
If (tnb=70260) n=1.00.
If (tnb=70280) n=1.58.
if (tnb=76010) n=5.25. 
if (tnb=76011) n=4.53.
if (tnb=76012) n=5.20.
if (tnb=76030) n=3.50.
if (tnb=76040) n=1.80.
if (tnb=76041) n=2.43.
if (tnb=76042) n=3.14.
if (tnb=76050) n=3.10.
if (tnb=76060) n=2.42.


compute sb70010=0.
compute sb70020=0.
compute sb70030=0.
compute sb70070=0.
compute sb70080=0.
compute sb70083=0.
compute sb70091=0.
compute sb70100=0.
compute sb70101=0.
compute sb70111=0.
compute sb70120=0.
compute sb70121=0.
compute sb70130=0.
compute sb70140=0.
compute sb70150=0.
compute sb70151=0.
compute sb70170=0.
compute sb70180=0.
compute sb70210=0.
compute sb70220=0.
compute sb70230=0.
compute sb70240=0.
compute sb70241=0.
compute sb70250=0.
compute sb70260=0.
compute sb70280=0.
compute sb70300=0.
compute sb76010=0.
compute sb76011=0.
compute sb76012=0.
compute sb76030=0.
compute sb76040=0.
compute sb76041=0.
compute sb76042=0.
compute sb76050=0.
compute sb76060=0.


if (tnb=70010) sb70010=sqrt(bsf).
if (tnb=70020) sb70020=sqrt(bsf).
if (tnb=70030) sb70030=sqrt(bsf).
if (tnb=70070) sb70070=sqrt(bsf).
if (tnb=70080) sb70080=sqrt(bsf).
if (tnb=70083) sb70083=sqrt(bsf).
if (tnb=70091) sb70091=sqrt(bsf).
if (tnb=70100) sb70100=sqrt(bsf).
if (tnb=70101) sb70101=sqrt(bsf).
if (tnb=70111) sb70111=sqrt(bsf).
if (tnb=70120) sb70120=sqrt(bsf).
if (tnb=70121) sb70121=sqrt(bsf).
if (tnb=70130) sb70130=sqrt(bsf).
if (tnb=70140) sb70140=sqrt(bsf).
if (tnb=70150) sb70150=sqrt(bsf).
if (tnb=70151) sb70151=sqrt(bsf).
if (tnb=70170) sb70170=sqrt(bsf).
if (tnb=70180) sb70180=sqrt(bsf).
if (tnb=70210) sb70210=sqrt(bsf).
if (tnb=70220) sb70220=sqrt(bsf).
if (tnb=70230) sb70230=sqrt(bsf).
if (tnb=70240) sb70240=sqrt(bsf).
if (tnb=70241) sb70241=sqrt(bsf).
if (tnb=70250) sb70250=sqrt(bsf).
if (tnb=70260) sb70260=sqrt(bsf).
if (tnb=70280) sb70280=sqrt(bsf).
if (tnb=76010) sb76010=sqrt(bsf).
if (tnb=76011) sb76011=sqrt(bsf).
if (tnb=76012) sb76012=sqrt(bsf).
if (tnb=76030) sb76030=sqrt(bsf).
if (tnb=76040) sb76040=sqrt(bsf).
if (tnb=76041) sb76041=sqrt(bsf).
if (tnb=76042) sb76042=sqrt(bsf).
if (tnb=76050) sb76050=sqrt(bsf).
if (tnb=76060) sb76060=sqrt(bsf).

compute n70010=0.
compute n70020=0.
compute n70030=0.
compute n70070=0.
compute n70080=0.
compute n70083=0.
compute n70091=0.
compute n70100=0.
compute n70101=0.
compute n70111=0.
compute n70120=0.
compute n70121=0.
compute n70130=0.
compute n70140=0.
compute n70150=0.
compute n70151=0.
compute n70170=0.
compute n70180=0.
compute n70210=0.
compute n70220=0.
compute n70230=0.
compute n70240=0.
compute n70241=0.
compute n70250=0.
compute n70260=0.
compute n70280=0.
compute n76010=0.
compute n76011=0.
compute n76012=0.
compute n76030=0.
compute n76040=0.
compute n76041=0.
compute n76042=0.
compute n76050=0.
compute n76060=0.


if (tnb=70010) n70010=1.0.
if (tnb=70020) n70020=1.0.
if (tnb=70030) n70030=1.0.
if (tnb=70070) n70070=1.0.
if (tnb=70080) n70080=1.0.
if (tnb=70083) n70083=1.0.
if (tnb=70091) n70091=1.0.
if (tnb=70100) n70100=1.0.
if (tnb=70101) n70101=1.0.
if (tnb=70111) n70111=1.0.
if (tnb=70120) n70120=1.0.
if (tnb=70121) n70121=1.0.
if (tnb=70130) n70130=1.0.
if (tnb=70140) n70140=1.0.
if (tnb=70150) n70150=1.0.
if (tnb=70151) n70151=1.0.
if (tnb=70170) n70170=1.0.
if (tnb=70180) n70180=1.0.
if (tnb=70210) n70210=1.0.
if (tnb=70220) n70220=1.0.
if (tnb=70230) n70230=1.0.
if (tnb=70240) n70240=1.0.
if (tnb=70241) n70241=1.0.
if (tnb=70250) n70250=1.0.
if (tnb=70260) n70260=1.0.
if (tnb=70280) n70280=1.0.
if (tnb=76010) n76010=1.0.
if (tnb=76011) n76011=1.0.
if (tnb=76012) n76012=1.0.
if (tnb=76030) n76030=1.0.
if (tnb=76040) n76040=1.0.
if (tnb=76041) n76041=1.0.
if (tnb=76042) n76042=1.0.
if (tnb=76050) n76050=1.0.
if (tnb=76060) n76060=1.0.
               	
Compute lsf=sqftl.
Compute nlsf=n*lsf.
Compute nbsf=n*bsf.
Compute srbsf=sqrt(bsf).
Compute nsrbsf=n*srbsf.
Compute srage=sqrt(age).
Compute nsrage=n*srage.
Compute cenair=0.
If aircond=1 cenair=1.
If aircond=2 cenair=0.
Compute bsfair=bsf*cenair.
Compute nbsfair=n*bsfair.

Compute frame=0.
Compute mason=0.
Compute framas=0.
Compute stucco=0.
If extcon=1 frame=1.
If extcon=2 mason=1.
If extcon=3 framas=1.
If extcon=4 stucco=1.
Compute frabsf=frame*bsf.
Compute frmsbsf=framas*bsf.
Compute stubsf=stucco*bsf.
compute masbsf=mason*bsf.
Compute frast=0.
If extcon=1 or extcon=4 frast=1.
Compute frastbsf=frast*bsf.
Compute bathsum=fullbath + 0.25*halfbath.

Compute shingle=0.
Compute tar=0.
Compute slate=0.
Compute shake=0.
Compute tile=0.
Compute premrf=0.
If rf=1 shingle=1.
If rf=2 tar=1.
If rf=3 slate=1.
If rf=4 shake=1.
If rf=5 tile=1.
If rf>=3 premrf=1.
Compute npremrf=n*premrf.
Compute nprembsf=npremrf*bsf.


Compute firp2=0.
If firepl=1 firp2=1.
If firepl>=2 firp2=2.
compute nfirpl=0.
compute nfirpl=n*firp2.


compute nogarage=0.
if gar=7 nogarage=1.
Compute garage1=0.
Compute garage2=0.
Compute garage3=0.
Compute garage4=0.
if gar=1 garage1=1.
if gar=2 garage1=1.
if gar=3 garage2=1.
if gar=4 garage2=1.
if gar=5 garage3=1.
if gar=6 garage3=1.
if gar=8 garage4=1.  
Compute biggar=0.
if garage3=1 or garage4=1 biggar=1.

Compute basefull=0.
Compute basepart=0.
Compute baseslab=0.
Compute basecraw=0.
If basment=1 basefull=1.
If basment=2 baseslab=1.
If basment=3 basepart=1.
If basment=4 basecraw=1.
Compute basement=0.
If basment=1 or basment=2 basement=1.
Compute nbasfull=n*basefull.
Compute nbaspart=n*basepart.
Compute bsfnrec=0.
Compute bsfnapt=0.
Compute bsunf=0.
If bsfn=1 bsfnrec=1.
If bsfn=2 bsfnapt=1.
If bsfn=3 bsunf=1.
Compute nobase=0.
If (baseslab=1 or basecraw=1) nobase=1.
compute nnobase=0.
compute nnobase=n*nobase.

Compute qualdlux=0.
Compute qualavg=0.
Compute qualpoor=0.
If qual=1 qualdlux=1.
If qual=2 qualavg=1.
If qual=3 qualpoor=1.
Compute nlux=n*qualdlux.
Compute luxbsf=qualdlux*bsf.
Compute nluxbsf=n*luxbsf.

Compute ren=0.
If renov=1 ren=1.
Compute nren=n*ren.
Compute renbsf=ren*bsf.
Compute nrenbsf=n*ren*bsf.
Compute siteben=0.
Compute sitedetr=0.
If site=1 siteben=1.
If site=3 sitedetr=1.
Compute nsiteben=n*siteben.
Compute repabove=0.
Compute repbelow=0.
Compute repave=0.
If rep=1 repabove=1.
If rep=3 repbelow=1.
Compute repabosf=repabove*bsf.
Compute repbelsf=repbelow*bsf.
Compute nrepabsf=n*repabove*bsf.
Compute nrepbesf=n*repbelow*bsf.
Compute onestory=0.
Compute twostory=0.
Compute threstor=0.
Compute multilev=0.
Compute rs1519=0.
If rs=1 onestory=1.
If rs=2 twostory=1.
If rs=3 threstor=1.
If rs=4 multilev=1.
If rs=5 rs1519=1.
Compute twosty=0.
If class07=1 or class08=1 or class78=1 twosty=1.

compute srlsf=sqrt(lsf).
compute nsrlsf=n*srlsf.
compute nfulbath=n*fullbath.
compute nhafbath=n*halfbath.
compute nbathsum=n*bathsum.

If num=6 bnum=0.
If num=0 bnum=0.
If num=1 bnum=2.
If num=2 bnum=3.
If num=3 bnum=4.
If num=4 bnum=5.
If num=5 bnum=6.
compute nbnum=n*bnum.
compute bnumb=bnum*bsf.


compute b=1.
*select if (puremarket=1 and year1=2011).
*Table observation = b
                amount1
     /table = tnb by 
              amount1 
 		             + b      
	/title = 'Ave & Med Sales'
     	/statistics =  median (amount1 'MED SP')
              mean (amount1 'MEAN SP')
                         validn (b '# PROPS').


reg des=defaults cov
     /var=amount1 nsrage nsrbsf nbaspart nnobase nbasfull nbsfair nsrlsf nrepabsf
     garage1 garage2 biggar nfirpl nluxbsf nbsf234 nbsf56 nbsf778 nbsf34 nbsf1112 nbsf1095
     nbathsum bnumb masbsf frmsbsf frabsf stubsf nrepbesf nprembsf   
     jantmar07cl234 winter0708cl234 winter0809cl234 winter0910cl234
    winter1011cl234 summer07cl234 summer08cl234 summer09cl234 summer10cl234
    summer11cl234 octtdec11cl234 jantmar07cl56 winter0708cl56 winter0809cl56 winter0910cl56
    winter1011cl56 summer07cl56 summer08cl56 summer09cl56 summer10cl56
    summer11cl56 octtdec11cl56 jantmar07cl778 winter0708cl778 winter0809cl778 winter0910cl778
    winter1011cl778 summer07cl778 summer08cl778 summer09cl778 summer10cl778
    summer11cl778 octtdec11cl778 jantmar07cl89 winter0708cl89  winter0809cl89 winter0910cl89
    winter1011cl89 summer07cl89 summer08cl89 summer09cl89 summer10cl89
    summer11cl89 octtdec11cl89 jantmar07cl1112 winter0708cl1112 winter0809cl1112 winter0910cl1112
    winter1011cl1112 summer07cl1112 summer08cl1112 summer09cl1112 summer10cl1112 summer11cl1112
    octtdec11cl1112 jantmar07cl1095 winter0708cl1095 winter0809cl1095 winter0910cl1095 winter1011cl1095
    summer07cl1095 summer08cl1095 summer09cl1095 summer10cl1095 summer11cl1095 octtdec11cl1095
    jantmar07clsplt winter0708clsplt winter0809clsplt winter0910clsplt winter1011clsplt summer07clsplt
    summer08clsplt summer09clsplt summer10clsplt summer11clsplt octtdec11clsplt
    sb70010 sb70020 sb70030 sb70070 sb70080 sb70083 sb70091 sb70100 sb70101
	   sb70111 sb70120 sb70121 sb70130 sb70140 sb70150 sb70151 sb70170 sb70180
		  sb70210 sb70220 sb70230 sb70240 sb70241 sb70250 sb70260 sb70280 sb76011
    sb76012 sb76030 sb76041 sb76042 sb76050 sb76060 
	   midzonesouth highzonesouth lowzonehydepark midzonehydepark 
    highzonehydepark srfxlowblockhydepark srfxlowblocksouth srfxmidblockhydepark
   	srfxmidblocksouth srfxhighblockhydepark srfxhighblocksouth   
	  /dep=amount1 
	  /method=stepwise   
	  /method=enter   
	  /method=enter  jantmar07cl234 winter0708cl234 winter0809cl234 winter0910cl234
    winter1011cl234 summer07cl234 summer08cl234 summer09cl234 summer10cl234
    summer11cl234 octtdec11cl234 jantmar07cl56 winter0708cl56 winter0809cl56 winter0910cl56
    winter1011cl56 summer07cl56 summer08cl56 summer09cl56 summer10cl56
    summer11cl56 octtdec11cl56 jantmar07cl778 winter0708cl778 winter0809cl778 winter0910cl778
    winter1011cl778 summer07cl778 summer08cl778 summer09cl778 summer10cl778
    summer11cl778 octtdec11cl778 jantmar07cl89 winter0708cl89  winter0809cl89 winter0910cl89
    winter1011cl89 summer07cl89 summer08cl89 summer09cl89 summer10cl89
    summer11cl89 octtdec11cl89 jantmar07cl1112 winter0708cl1112 winter0809cl1112 winter0910cl1112
    winter1011cl1112 summer07cl1112 summer08cl1112 summer09cl1112 summer10cl1112 summer11cl1112
    octtdec11cl1112 jantmar07cl1095 winter0708cl1095 winter0809cl1095 winter0910cl1095 winter1011cl1095
    summer07cl1095 summer08cl1095 summer09cl1095 summer10cl1095 summer11cl1095 octtdec11cl1095
    jantmar07clsplt winter0708clsplt winter0809clsplt winter0910clsplt winter1011clsplt summer07clsplt
    summer08clsplt summer09clsplt summer10clsplt summer11clsplt octtdec11clsplt
	  /method=enter midzonesouth highzonesouth lowzonehydepark midzonehydepark 
   highzonehydepark srfxlowblockhydepark srfxlowblocksouth srfxmidblockhydepark
   srfxmidblocksouth srfxhighblockhydepark srfxhighblocksouth   
	  /method=enter 
		 /save pred (pred) resid (resid).
           sort cases by tnb pin.	
           value labels extcon 1 'Frame' 2 'Mason' 3 'Framas' 4 'Stucco'
   /basment 1 'Full' 2 'Slab' 3 'Part' 4 'Crwl'
   /bsfn 1 'Rec' 2 'Apt' 3 'Unf'
   /gar 1 '1' 2 '1.5' 3 '2' 4 '2.5' 5 '3' 6 '3.5' 7 'non'
   /class 2 '2-02' 3 '2-03' 4 '2-04' 5 '2-05' 6 '2-06' 7 '2-07' 8 '2-08' 9 '2-09' 10 '2-10' 11 '2-11' 12 '2-12' 34 '2-34' 78 '2-78' 95 '2-95'.
*compute perdif=(resid)/(amount1)*100.
*formats pred(COMMA9.0)
          /resid (f6.0).

*plot
/vertical='Sales Price' MIN(50000) MAX(1000000)
/horizontal='RESID'
/vsize=25
/hsize=140
/plot=amount1 with RESID.
*compute badsal=0.
*if perdif>75 badsal=1.
*if perdif<-75 badsal=1.
*select if badsal=1.
set wid=125.
set len=59.
*REPORT FORMAT=automatic list(1)
/title='Office of the Assessor'
        'Residential Regression Report'
        'Town is Hyde Park'
    /ltitle 'Report Ran On)Date' 
    /rtitle='PAGE)PAGE'
    /string=prop(area(2) '-' subarea(2) '-' block(3) '-' parcel(3))
          date(mos(2) '-'yr(2))
   /var=prop 'Prop' 'Index Number'(13)
       class 'Class' (label) (5)
       nghcde 'nbhb'(5)
       amount1 'Price'(7)
       pred 'Predicted'(9)
       resid 'Resid'(7)
       perdif '% DIF' (5)
       aos 'AOS' (3)
       sqftb 'Sqftb'(5)
       age 'Age'(4)
       extcon 'Extcon' (label)(6)
       sqftl 'Land'(6).
	
