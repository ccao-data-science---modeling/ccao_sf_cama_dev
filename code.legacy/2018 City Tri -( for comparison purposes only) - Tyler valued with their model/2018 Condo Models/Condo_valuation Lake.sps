﻿GET FILE = 'S:\2018 City_condo_valuations\Lake\LakeD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 
*changed saleyr to 50 so that condos that were sold in FC sales won't be used to value condo's.
*Any number could be used as long as it isn't one that is already on the file for a sale year.

if saleyr=18 amount=salepric.
if saleyr=17 amount=salepric.
if saleyr=16 amount=salepric.
if saleyr=15 amount=salepric.

*if salpric<saleamt amount=saleamt.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=72.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.

IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2018 City_condo_valuations\Lake\temp_t72.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2018 City_condo_valuations\Lake\temp_t72.sav'
                /by PIN10.
*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.

compute newmv_median=newmv_median-(.05*oldmv).



If (pin>19012151641000 and pin<19012151649999) newmv_median=581818.
If (pin>19012151651000 and pin<19012151659999) newmv_median=581818.
If (pin>19012151661000 and pin<19012151669999) newmv_median=581818.
If (pin>19012151671000 and pin<19012151679999) newmv_median=581818.
If (pin>19012151681000 and pin<19012151689999) newmv_median=581818.
If (pin>19012151691000 and pin<19012151699999) newmv_median=581818.
If (pin>19012151701000 and pin<19012151709999) newmv_median=581818.
If (pin>19012151711000 and pin<19012151719999) newmv_median=581818.
If (pin>19012151721000 and pin<19012151729999) newmv_median=581818.
If (pin>19012151731000 and pin<19012151739999) newmv_median=581818.
If (pin>19012151741000 and pin<19012151749999) newmv_median=581818.
If (pin>19012151751000 and pin<19012151759999) newmv_median=581818.
If (pin>19012151761000 and pin<19012151769999) newmv_median=581818.
If (pin>19084241381000 and pin<19084241389999) newmv_median=713113.
If (pin>19084241391000 and pin<19084241399999) newmv_median=2668161.
If (pin>19084241401000 and pin<19084241409999) newmv_median=876344.
If (pin>19084250151000 and pin<19084250159999) newmv_median=716116.
If (pin>19084260201000 and pin<19084260209999) newmv_median=1408077.
If (pin>19084270121000 and pin<19084270129999) newmv_median=557260.
If (pin>19093140301000 and pin<19093140309999) newmv_median=1209987.
If (pin>19094060181000 and pin<19094060189999) newmv_median=562089.
If (pin>19094060191000 and pin<19094060199999) newmv_median=562089.
If (pin>19094060201000 and pin<19094060209999) newmv_median=562089.
If (pin>19094090611000 and pin<19094090619999) newmv_median=567614.
If (pin>19094090661000 and pin<19094090669999) newmv_median=562089.
If (pin>19103240921000 and pin<19103240929999) newmv_median=292000.
If (pin>19103240931000 and pin<19103240939999) newmv_median=292000.
If (pin>19103240941000 and pin<19103240949999) newmv_median=292000.
If (pin>19104050591000 and pin<19104050599999) newmv_median=475001.
If (pin>19113250501000 and pin<19113250509999) newmv_median=5849109.
If (pin>19113330441000 and pin<19113330449999) newmv_median=454564.
If (pin>19113330451000 and pin<19113330459999) newmv_median=454564.
If (pin>19113330461000 and pin<19113330469999) newmv_median=454564.
If (pin>19114180471000 and pin<19114180479999) newmv_median=1019314.
If (pin>19133240361000 and pin<19133240369999) newmv_median=828346.
If (pin>19143280421000 and pin<19143280429999) newmv_median=758457.
If (pin>19143280461000 and pin<19143280469999) newmv_median=607082.
If (pin>19143280471000 and pin<19143280479999) newmv_median=712964.
If (pin>19151070451000 and pin<19151070459999) newmv_median=1137130.
If (pin>19151190341000 and pin<19151190349999) newmv_median=393609.
If (pin>19151190351000 and pin<19151190359999) newmv_median=393609.
If (pin>19151190361000 and pin<19151190369999) newmv_median=393609.
If (pin>19151190371000 and pin<19151190379999) newmv_median=393609.
If (pin>19151190381000 and pin<19151190389999) newmv_median=393609.
If (pin>19151190391000 and pin<19151190399999) newmv_median=393609.
If (pin>19151190401000 and pin<19151190409999) newmv_median=393609.
If (pin>19151190411000 and pin<19151190419999) newmv_median=393609.
If (pin>19153180331000 and pin<19153180339999) newmv_median=604500.
If (pin>19153240461000 and pin<19153240469999) newmv_median=409887.
If (pin>19153240471000 and pin<19153240479999) newmv_median=409887.
If (pin>19154230391000 and pin<19154230399999) newmv_median=346388.
If (pin>19172050411000 and pin<19172050419999) newmv_median=413529.
If (pin>19174250441000 and pin<19174250449999) newmv_median=920502.
If (pin>19174300441000 and pin<19174300449999) newmv_median=1327714.
If (pin>19181080471000 and pin<19181080479999) newmv_median=848920.
If (pin>19182150381000 and pin<19182150389999) newmv_median=1400000.
If (pin>19183120481000 and pin<19183120489999) newmv_median=1499869.
If (pin>19184290431000 and pin<19184290439999) newmv_median=461705.
If (pin>19191140331000 and pin<19191140339999) newmv_median=1520546.
If (pin>19191140341000 and pin<19191140349999) newmv_median=1127251.
If (pin>19191140351000 and pin<19191140359999) newmv_median=1277913.
If (pin>19191140361000 and pin<19191140369999) newmv_median=1277913.
If (pin>19191140371000 and pin<19191140379999) newmv_median=1277913.
If (pin>19191140381000 and pin<19191140389999) newmv_median=1277913.
If (pin>19191140391000 and pin<19191140399999) newmv_median=764499.
If (pin>19192000601000 and pin<19192000609999) newmv_median=1001326.
If (pin>19192020761000 and pin<19192020769999) newmv_median=956799.
If (pin>19192080391000 and pin<19192080399999) newmv_median=548206.
If (pin>19192080401000 and pin<19192080409999) newmv_median=548206.
If (pin>19192080411000 and pin<19192080419999) newmv_median=548206.
If (pin>19192080421000 and pin<19192080429999) newmv_median=548206.
If (pin>19192080441000 and pin<19192080449999) newmv_median=548206.
If (pin>19192080451000 and pin<19192080459999) newmv_median=548206.
If (pin>19192080461000 and pin<19192080469999) newmv_median=548206.
If (pin>19192080471000 and pin<19192080479999) newmv_median=548206.
If (pin>19192080481000 and pin<19192080489999) newmv_median=548206.
If (pin>19192080491000 and pin<19192080499999) newmv_median=548206.
If (pin>19192090361000 and pin<19192090369999) newmv_median=769128.
If (pin>19192090381000 and pin<19192090389999) newmv_median=548206.
If (pin>19192090391000 and pin<19192090399999) newmv_median=548206.
If (pin>19192090421000 and pin<19192090429999) newmv_median=548206.
If (pin>19192090431000 and pin<19192090439999) newmv_median=548206.
If (pin>19192090441000 and pin<19192090449999) newmv_median=548206.
If (pin>19192090451000 and pin<19192090459999) newmv_median=548206.
If (pin>19192090461000 and pin<19192090469999) newmv_median=548206.
If (pin>19192090481000 and pin<19192090489999) newmv_median=556847.
If (pin>19192090491000 and pin<19192090499999) newmv_median=548206.
If (pin>19192130161000 and pin<19192130169999) newmv_median=1666666.
If (pin>19192130171000 and pin<19192130179999) newmv_median=1666666.
If (pin>19192140251000 and pin<19192140259999) newmv_median=2680805.
If (pin>19192140261000 and pin<19192140269999) newmv_median=2680805.
If (pin>19192150221000 and pin<19192150229999) newmv_median=518999.
If (pin>19192150231000 and pin<19192150239999) newmv_median=518999.
If (pin>19201010671000 and pin<19201010679999) newmv_median=1805555.
If (pin>19201120421000 and pin<19201120429999) newmv_median=1072164.
If (pin>19201120431000 and pin<19201120439999) newmv_median=859372.
If (pin>19201120481000 and pin<19201120489999) newmv_median=946293.
If (pin>19201140291000 and pin<19201140299999) newmv_median=1003907.
If (pin>19201140301000 and pin<19201140309999) newmv_median=1003907.
If (pin>19201140311000 and pin<19201140319999) newmv_median=1642335.
If (pin>19201140321000 and pin<19201140329999) newmv_median=1642335.
If (pin>19201140331000 and pin<19201140339999) newmv_median=976169.
If (pin>19201140341000 and pin<19201140349999) newmv_median=1642335.
If (pin>19201150341000 and pin<19201150349999) newmv_median=1642335.
If (pin>19202000501000 and pin<19202000509999) newmv_median=977636.
If (pin>19202020451000 and pin<19202020459999) newmv_median=509999.
If (pin>19202020471000 and pin<19202020479999) newmv_median=509999.
If (pin>19202020481000 and pin<19202020489999) newmv_median=509999.
If (pin>19222000441000 and pin<19222000449999) newmv_median=715417.
If (pin>19222000451000 and pin<19222000459999) newmv_median=517798.
If (pin>19222010411000 and pin<19222010419999) newmv_median=578989.
If (pin>19222060411000 and pin<19222060419999) newmv_median=611410.
If (pin>19223000171000 and pin<19223000179999) newmv_median=1812294.
If (pin>19232290401000 and pin<19232290409999) newmv_median=427264.
If (pin>19233080411000 and pin<19233080419999) newmv_median=1151684.
If (pin>19233120391000 and pin<19233120399999) newmv_median=951174.
If (pin>19233200431000 and pin<19233200439999) newmv_median=1101695.
If (pin>19241270291000 and pin<19241270299999) newmv_median=424308.
If (pin>19251160601000 and pin<19251160609999) newmv_median=522218.
If (pin>19274010381000 and pin<19274010389999) newmv_median=19371345.
If (pin>19342061471000 and pin<19342061479999) newmv_median=1074298.
If (pin>19342150841000 and pin<19342150849999) newmv_median=1071508.
If (pin>19342150851000 and pin<19342150859999) newmv_median=1194236.
If (pin>19342150861000 and pin<19342150869999) newmv_median=1074298.
If (pin>19342150871000 and pin<19342150879999) newmv_median=1071508.
If (pin>19343190341000 and pin<19343190349999) newmv_median=1194236.
If (pin>19344300421000 and pin<19344300429999) newmv_median=819569.
If (pin>19344300441000 and pin<19344300449999) newmv_median=819569.
If (pin>19344300451000 and pin<19344300459999) newmv_median=819569.
If (pin>19353180651000 and pin<19353180659999) newmv_median=5135005.
If (pin>19353260451000 and pin<19353260459999) newmv_median=424901.
If (pin>19361000421000 and pin<19361000429999) newmv_median=623058.
If (pin>19363020361000 and pin<19363020369999) newmv_median=1429800.
If (pin>19363060251000 and pin<19363060259999) newmv_median=927713.
If (pin>19364170361000 and pin<19364170369999) newmv_median=927713.
If (pin>20042010271000 and pin<20042010279999) newmv_median=797583.
If (pin>20042020341000 and pin<20042020349999) newmv_median=631394.
If (pin>20042020351000 and pin<20042020359999) newmv_median=631394.
If (pin>20043220511000 and pin<20043220519999) newmv_median=410036.
If (pin>20162120231000 and pin<20162120239999) newmv_median=146666.
If (pin>20163130391000 and pin<20163130399999) newmv_median=475044.
If (pin>20163260281000 and pin<20163260289999) newmv_median=291328.
If (pin>20171020391000 and pin<20171020399999) newmv_median=461227.
If (pin>20202040391000 and pin<20202040399999) newmv_median=167788.
If (pin>20202280491000 and pin<20202280499999) newmv_median=212271.
If (pin>20212180481000 and pin<20212180489999) newmv_median=923248.
If (pin>20213140631000 and pin<20213140639999) newmv_median=222915.
If (pin>20213170201000 and pin<20213170209999) newmv_median=200307.
If (pin>20282060461000 and pin<20282060469999) newmv_median=361073.
If (pin>20282250271000 and pin<20282250279999) newmv_median=168052.
If (pin>20283320321000 and pin<20283320329999) newmv_median=505074.
If (pin>20304280571000 and pin<20304280579999) newmv_median=247941.
If (pin>20304310421000 and pin<20304310429999) newmv_median=1198244.
If (pin>20311100071000 and pin<20311100079999) newmv_median=1713357.
If (pin>20314290421000 and pin<20314290429999) newmv_median=308112.
If (pin>24132230331000 and pin<24132230339999) newmv_median=880382.
If (pin>24132240451000 and pin<24132240459999) newmv_median=880382.
If (pin>24132240461000 and pin<24132240469999) newmv_median=884129.
If (pin>24132330541000 and pin<24132330549999) newmv_median=1487500.
If (pin>24133120261000 and pin<24133120269999) newmv_median=884135.
If (pin>24134250411000 and pin<24134250419999) newmv_median=600540.
If (pin>24134300401000 and pin<24134300409999) newmv_median=3224985.
If (pin>24134300411000 and pin<24134300419999) newmv_median=913583.
If (pin>24143000271000 and pin<24143000279999) newmv_median=2372461.
If (pin>24143160841000 and pin<24143160849999) newmv_median=2368257.
If (pin>24143160891000 and pin<24143160899999) newmv_median=1326232.
If (pin>24143170831000 and pin<24143170839999) newmv_median=529308.
If (pin>24232020861000 and pin<24232020869999) newmv_median=2163798.
If (pin>24232070901000 and pin<24232070909999) newmv_median=422933.
If (pin>24232070911000 and pin<24232070919999) newmv_median=422933.
If (pin>24242010401000 and pin<24242010409999) newmv_median=1735177.
If (pin>24242030401000 and pin<24242030409999) newmv_median=891658.
If (pin>24244090441000 and pin<24244090449999) newmv_median=937325.
If (pin>25062060471000 and pin<25062060479999) newmv_median=2550300.
If (pin>25062210371000 and pin<25062210379999) newmv_median=579967.
If (pin>25064030371000 and pin<25064030379999) newmv_median=793406.
If (pin>25072030091000 and pin<25072030099999) newmv_median=2247810.
If (pin>25072040151000 and pin<25072040159999) newmv_median=1794473.
If (pin>25074160741000 and pin<25074160749999) newmv_median=1794473.
If (pin>25082041031000 and pin<25082041039999) newmv_median=523540.
If (pin>25173040481000 and pin<25173040489999) newmv_median=391538.
If (pin>25173100421000 and pin<25173100429999) newmv_median=760701.
If (pin>25182000511000 and pin<25182000519999) newmv_median=583333.
If (pin>25182020391000 and pin<25182020399999) newmv_median=1006166.
If (pin>25182020401000 and pin<25182020409999) newmv_median=1393508.
If (pin>25182020411000 and pin<25182020419999) newmv_median=1081291.
If (pin>25182030661000 and pin<25182030669999) newmv_median=1190215.
If (pin>25182060691000 and pin<25182060699999) newmv_median=316380.
If (pin>25182060701000 and pin<25182060709999) newmv_median=316380.
If (pin>25182060711000 and pin<25182060719999) newmv_median=316380.
If (pin>25182060721000 and pin<25182060729999) newmv_median=316380.
If (pin>25182060731000 and pin<25182060739999) newmv_median=316380.
If (pin>25182060751000 and pin<25182060759999) newmv_median=442273.
If (pin>25183170541000 and pin<25183170549999) newmv_median=2103480.
If (pin>25183180341000 and pin<25183180349999) newmv_median=670941.
If (pin>25183190361000 and pin<25183190369999) newmv_median=1421125.
If (pin>25183190371000 and pin<25183190379999) newmv_median=1595267.
If (pin>25191030181000 and pin<25191030189999) newmv_median=1151260.
If (pin>25193000441000 and pin<25193000449999) newmv_median=1580352.








*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
*compute newmv_median=newmv_median+(.12*oldmv).
*if (pin=1001) newmv_median=.
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.

Compute newav = newmv_median*percent*.10.
if class=599 newav=newmv_median*percent*.10.
if class=589 newav=newmv_median*percent*.10.
if class=399 newav=newmv_median*percent*.10.
if class=499 newav=newmv_median*percent*.10.
if class=899 newav=newmv_median*percent*.10.
if class=679 newav=newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=99999999991001 newmv=99999.

Save OUTFILE = 'S:\2018 City_condo_valuations\Lake\condos72.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'Lake CONDOS' 'USING 2015-2018 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'Lake'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'Lake ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.






