﻿GET FILE = 'S:\2018 City_condo_valuations\South\SouthD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 
*changed saleyr to 50 so that condos that were sold in FC sales won't be used to value condo's.
*Any number could be used as long as it isn't one that is already on the file for a sale year.
if saleyr=18 amount=salepric.
if saleyr=17 amount=salepric.
if saleyr=16 amount=salepric.
if saleyr=15 amount=salepric.
*if salpric<saleamt amount=saleamt.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=76.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.

IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2018 City_condo_valuations\South\temp_t76.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2018 City_condo_valuations\South\temp_t76.sav'
                /by PIN10.
*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.

If (pin>17103180861000 and pin<17103180869999) newmv_median=44588105.
If (pin>17151070791000 and pin<17151070799999) newmv_median=9052941.
If (pin>17162130221000 and pin<17162130229999) newmv_median=8181838.
If (pin>17162470541000 and pin<17162470549999) newmv_median=877551.
If (pin>17164190061000 and pin<17164190069999) newmv_median=15124694.
If (pin>17164230021000 and pin<17164230029999) newmv_median=54651919.
If (pin>17164240081000 and pin<17164240089999) newmv_median=1956000.
If (pin>17214200661000 and pin<17214200669999) newmv_median=3004535.
If (pin>17214310731000 and pin<17214310739999) newmv_median=4605688.
If (pin>17214320691000 and pin<17214320699999) newmv_median=6129807.
If (pin>17214330371000 and pin<17214330379999) newmv_median=5219999.
If (pin>17214350691000 and pin<17214350699999) newmv_median=2711456.
If (pin>17221010391000 and pin<17221010399999) newmv_median=1994357.
If (pin>17221040381000 and pin<17221040389999) newmv_median=59859154.
If (pin>17221050421000 and pin<17221050429999) newmv_median=10403120.
If (pin>17221060961000 and pin<17221060969999) newmv_median=2111111.
If (pin>17221070591000 and pin<17221070599999) newmv_median=4283216.
If (pin>17221070631000 and pin<17221070639999) newmv_median=7115913.
If (pin>17223000781000 and pin<17223000789999) newmv_median=17250862.
If (pin>17223010571000 and pin<17223010579999) newmv_median=3349673.
If (pin>17223010591000 and pin<17223010599999) newmv_median=1260231.
If (pin>17223020501000 and pin<17223020509999) newmv_median=12909836.
If (pin>17223050561000 and pin<17223050569999) newmv_median=2292899.
If (pin>17223050631000 and pin<17223050639999) newmv_median=1420803.
If (pin>17223050641000 and pin<17223050649999) newmv_median=2897618.
If (pin>17223060451000 and pin<17223060459999) newmv_median=7022282.
If (pin>17223060461000 and pin<17223060469999) newmv_median=6906518.
If (pin>17223060501000 and pin<17223060509999) newmv_median=7793176.
If (pin>17223091221000 and pin<17223091229999) newmv_median=5934579.
If (pin>17223110301000 and pin<17223110309999) newmv_median=9420022.
If (pin>17271230261000 and pin<17271230269999) newmv_median=13608562.
If (pin>17271290931000 and pin<17271290939999) newmv_median=11180124.
If (pin>17273000561000 and pin<17273000569999) newmv_median=1448171.
If (pin>17273000571000 and pin<17273000579999) newmv_median=1448171.
If (pin>17273000581000 and pin<17273000589999) newmv_median=1448171.
If (pin>17273000591000 and pin<17273000599999) newmv_median=1448171.
If (pin>17273000611000 and pin<17273000619999) newmv_median=1448171.
If (pin>17273090521000 and pin<17273090529999) newmv_median=1094100.
If (pin>17281040361000 and pin<17281040369999) newmv_median=5514354.
If (pin>17281050281000 and pin<17281050289999) newmv_median=2691387.
If (pin>17281150461000 and pin<17281150469999) newmv_median=10891435.
If (pin>17281270251000 and pin<17281270259999) newmv_median=2322580.
If (pin>17281320441000 and pin<17281320449999) newmv_median=1470878.
If (pin>17282020541000 and pin<17282020549999) newmv_median=3311474.
If (pin>17282020561000 and pin<17282020569999) newmv_median=3529450.
If (pin>17282120301000 and pin<17282120309999) newmv_median=567761.
If (pin>17282120311000 and pin<17282120319999) newmv_median=567761.
If (pin>17282120321000 and pin<17282120329999) newmv_median=567761.
If (pin>17282120331000 and pin<17282120339999) newmv_median=567761.
If (pin>17282120341000 and pin<17282120349999) newmv_median=567761.
If (pin>17282120351000 and pin<17282120359999) newmv_median=567761.
If (pin>17282120361000 and pin<17282120369999) newmv_median=567761.
If (pin>17282120371000 and pin<17282120379999) newmv_median=567761.
If (pin>17282120381000 and pin<17282120389999) newmv_median=567761.
If (pin>17282120391000 and pin<17282120399999) newmv_median=567761.
If (pin>17282120401000 and pin<17282120409999) newmv_median=567761.
If (pin>17282120411000 and pin<17282120419999) newmv_median=567761.
If (pin>17282120421000 and pin<17282120429999) newmv_median=567761.
If (pin>17282120431000 and pin<17282120439999) newmv_median=567761.
If (pin>17282120441000 and pin<17282120449999) newmv_median=567761.
If (pin>17282120461000 and pin<17282120469999) newmv_median=567761.
If (pin>17282120471000 and pin<17282120479999) newmv_median=567761.
If (pin>17282120481000 and pin<17282120489999) newmv_median=567761.
If (pin>17282120491000 and pin<17282120499999) newmv_median=567761.
If (pin>17282120501000 and pin<17282120509999) newmv_median=567761.
If (pin>17282120511000 and pin<17282120519999) newmv_median=567761.
If (pin>17282120521000 and pin<17282120529999) newmv_median=567761.
If (pin>17282120531000 and pin<17282120539999) newmv_median=567761.
If (pin>17282120541000 and pin<17282120549999) newmv_median=567761.
If (pin>17282120551000 and pin<17282120559999) newmv_median=567761.
If (pin>17282120561000 and pin<17282120569999) newmv_median=567761.
If (pin>17282120571000 and pin<17282120579999) newmv_median=567761.
If (pin>17282120861000 and pin<17282120869999) newmv_median=567761.
If (pin>17282120871000 and pin<17282120879999) newmv_median=567761.
If (pin>17282120881000 and pin<17282120889999) newmv_median=567761.
If (pin>17282120891000 and pin<17282120899999) newmv_median=567761.
If (pin>17282120901000 and pin<17282120909999) newmv_median=567761.
If (pin>17282120911000 and pin<17282120919999) newmv_median=567761.
If (pin>17282120921000 and pin<17282120929999) newmv_median=567761.
If (pin>17282120931000 and pin<17282120939999) newmv_median=567761.
If (pin>17282120941000 and pin<17282120949999) newmv_median=567761.
If (pin>17282120951000 and pin<17282120959999) newmv_median=567761.
If (pin>17282120961000 and pin<17282120969999) newmv_median=567761.
If (pin>17282120971000 and pin<17282120979999) newmv_median=567761.
If (pin>17282120981000 and pin<17282120989999) newmv_median=567761.
If (pin>17282121001000 and pin<17282121009999) newmv_median=13842145.
If (pin>17282210361000 and pin<17282210369999) newmv_median=6779661.
If (pin>17282220591000 and pin<17282220599999) newmv_median=787354.
If (pin>17282330761000 and pin<17282330769999) newmv_median=3452697.
If (pin>17282330771000 and pin<17282330779999) newmv_median=793139.
If (pin>17282330781000 and pin<17282330789999) newmv_median=781809.
If (pin>17283000791000 and pin<17283000799999) newmv_median=7179487.
If (pin>17283030481000 and pin<17283030489999) newmv_median=771158.
If (pin>17283100491000 and pin<17283100499999) newmv_median=1243279.
If (pin>17283120511000 and pin<17283120519999) newmv_median=715377.
If (pin>17284010441000 and pin<17284010449999) newmv_median=549914.
If (pin>17284030321000 and pin<17284030329999) newmv_median=759781.
If (pin>17284180331000 and pin<17284180339999) newmv_median=8879485.
If (pin>17284320301000 and pin<17284320309999) newmv_median=740000.
If (pin>17284350361000 and pin<17284350369999) newmv_median=1894735.
If (pin>17284360481000 and pin<17284360489999) newmv_median=3254189.
If (pin>17293270421000 and pin<17293270429999) newmv_median=722772.
If (pin>17294090641000 and pin<17294090649999) newmv_median=591477.
If (pin>17294261211000 and pin<17294261219999) newmv_median=1548073.
If (pin>17312120651000 and pin<17312120659999) newmv_median=1744568.
If (pin>17312190451000 and pin<17312190459999) newmv_median=701813.
If (pin>17312270511000 and pin<17312270519999) newmv_median=1299492.
If (pin>17313000341000 and pin<17313000349999) newmv_median=1019884.
If (pin>17313050361000 and pin<17313050369999) newmv_median=634615.
If (pin>17313130441000 and pin<17313130449999) newmv_median=504546.
If (pin>17313130451000 and pin<17313130459999) newmv_median=711313.
If (pin>17314160441000 and pin<17314160449999) newmv_median=735669.
If (pin>17314300471000 and pin<17314300479999) newmv_median=857843.
If (pin>17321030551000 and pin<17321030559999) newmv_median=599074.
If (pin>17322020451000 and pin<17322020459999) newmv_median=815542.
If (pin>17322030471000 and pin<17322030479999) newmv_median=701804.
If (pin>17322060511000 and pin<17322060519999) newmv_median=912737.
If (pin>17322060521000 and pin<17322060529999) newmv_median=632068.
If (pin>17322162051000 and pin<17322162059999) newmv_median=511058.
If (pin>17322172171000 and pin<17322172179999) newmv_median=1585857.
If (pin>17322220471000 and pin<17322220479999) newmv_median=904068.
If (pin>17322230481000 and pin<17322230489999) newmv_median=575000.
If (pin>17322270461000 and pin<17322270469999) newmv_median=952083.
If (pin>17324020241000 and pin<17324020249999) newmv_median=9681492.
If (pin>17324030441000 and pin<17324030449999) newmv_median=1191484.
If (pin>17324030451000 and pin<17324030459999) newmv_median=1191484.
If (pin>17331010461000 and pin<17331010469999) newmv_median=818901.
If (pin>17331010471000 and pin<17331010479999) newmv_median=818898.
If (pin>17331010491000 and pin<17331010499999) newmv_median=801557.
If (pin>17331010501000 and pin<17331010509999) newmv_median=1048616.
If (pin>17331010511000 and pin<17331010519999) newmv_median=1534791.
If (pin>17331040531000 and pin<17331040539999) newmv_median=815344.
If (pin>17331040541000 and pin<17331040549999) newmv_median=845714.
If (pin>17331110491000 and pin<17331110499999) newmv_median=1413793.
If (pin>17331140601000 and pin<17331140609999) newmv_median=768385.
If (pin>17331180381000 and pin<17331180389999) newmv_median=1145311.
If (pin>17331200951000 and pin<17331200959999) newmv_median=1208565.
If (pin>17332020581000 and pin<17332020589999) newmv_median=1158007.
If (pin>17332020591000 and pin<17332020599999) newmv_median=1158007.
If (pin>17332020611000 and pin<17332020619999) newmv_median=1158007.
If (pin>17332030581000 and pin<17332030589999) newmv_median=970918.
If (pin>17332090331000 and pin<17332090339999) newmv_median=4284593.
If (pin>17332110521000 and pin<17332110529999) newmv_median=1116797.
If (pin>17333140511000 and pin<17333140519999) newmv_median=1028952.
If (pin>17333210451000 and pin<17333210459999) newmv_median=1069793.
If (pin>17334230201000 and pin<17334230209999) newmv_median=1073331.
If (pin>17334230221000 and pin<17334230229999) newmv_median=3566965.
If (pin>17334230231000 and pin<17334230239999) newmv_median=14251878.
If (pin>17334230241000 and pin<17334230249999) newmv_median=6858381.
If (pin>17334260141000 and pin<17334260149999) newmv_median=8431996.
If (pin>17341030581000 and pin<17341030589999) newmv_median=2501101.
If (pin>17341030641000 and pin<17341030649999) newmv_median=761749.
If (pin>17341030651000 and pin<17341030659999) newmv_median=761749.
If (pin>17341030681000 and pin<17341030689999) newmv_median=761749.
If (pin>17341030691000 and pin<17341030699999) newmv_median=761749.
If (pin>17341100641000 and pin<17341100649999) newmv_median=1146089.
If (pin>17341160431000 and pin<17341160439999) newmv_median=1144556.
If (pin>17341160441000 and pin<17341160449999) newmv_median=1144556.
If (pin>17341160461000 and pin<17341160469999) newmv_median=740144.
If (pin>17341201041000 and pin<17341201049999) newmv_median=1013783.
If (pin>17341211271000 and pin<17341211279999) newmv_median=800512.
If (pin>17341221151000 and pin<17341221159999) newmv_median=860000.
If (pin>17341221291000 and pin<17341221299999) newmv_median=540661.
If (pin>17342191501000 and pin<17342191509999) newmv_median=19651741.
If (pin>17342250031000 and pin<17342250039999) newmv_median=15590205.
If (pin>17343091101000 and pin<17343091109999) newmv_median=476128.
If (pin>17343091221000 and pin<17343091229999) newmv_median=481192.
If (pin>17343091231000 and pin<17343091239999) newmv_median=748600.
If (pin>17343111011000 and pin<17343111019999) newmv_median=433296.
If (pin>17343120911000 and pin<17343120919999) newmv_median=745594.
If (pin>17343120921000 and pin<17343120929999) newmv_median=3824985.
If (pin>17343190211000 and pin<17343190219999) newmv_median=505221.
If (pin>17343210421000 and pin<17343210429999) newmv_median=618893.
If (pin>17343220511000 and pin<17343220519999) newmv_median=1222891.
If (pin>17343220641000 and pin<17343220649999) newmv_median=775764.
If (pin>17343220651000 and pin<17343220659999) newmv_median=701102.
If (pin>17343220661000 and pin<17343220669999) newmv_median=729576.
If (pin>17343230691000 and pin<17343230699999) newmv_median=709779.
If (pin>17343230711000 and pin<17343230719999) newmv_median=709779.
If (pin>17343230721000 and pin<17343230729999) newmv_median=709779.
If (pin>17343270471000 and pin<17343270479999) newmv_median=706799.
If (pin>17344000911000 and pin<17344000919999) newmv_median=690431.
If (pin>17344000951000 and pin<17344000959999) newmv_median=1155349.
If (pin>17344090361000 and pin<17344090369999) newmv_median=1592150.
If (pin>17344230141000 and pin<17344230149999) newmv_median=1253385.
If (pin>17344230151000 and pin<17344230159999) newmv_median=1082005.
If (pin>17344240101000 and pin<17344240109999) newmv_median=959561.
If (pin>17351011231000 and pin<17351011239999) newmv_median=963138.
If (pin>17351011251000 and pin<17351011259999) newmv_median=629100.
If (pin>17351011261000 and pin<17351011269999) newmv_median=671487.



*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
*compute newmv_median=newmv_median+(.12*oldmv).
*if (pin=1001) newmv_median=.
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.

Compute newav = newmv_median*percent*.10.
if class=599 newav=newmv_median*percent*.10.
if class=589 newav=newmv_median*percent*.10.
if class=399 newav=newmv_median*percent*.10.
if class=499 newav=newmv_median*percent*.10.
if class=899 newav=newmv_median*percent*.10.
if class=679 newav=newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=99999999991001 newmv=99999.

Save OUTFILE = 'S:\2018 City_condo_valuations\South\condos76.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'South CONDOS' 'USING 2015-2018 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'Lake'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'South ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.






