﻿GET FILE = 'S:\2018 City_condo_valuations\North\NorthD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 
*changed saleyr to 50 so that condos that were sold in FC sales won't be used to value condo's.
*Any number could be used as long as it isn't one that is already on the file for a sale year.

if saleyr=18 amount=salepric.
if saleyr=17 amount=salepric.
if saleyr=16 amount=salepric.
if saleyr=15 amount=salepric.
*if salpric<saleamt amount=saleamt.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=74.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.

IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2018 City_condo_valuations\North\temp_t74.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2018 City_condo_valuations\North\temp_t74.sav'
                /by PIN10.
*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.

If (pin>14321010611000 and pin<14321010619999) newmv_median=2338337.
If (pin>14321010631000 and pin<14321010639999) newmv_median=2206461.
If (pin>14321020451000 and pin<14321020459999) newmv_median=3129032.
If (pin>14321040321000 and pin<14321040329999) newmv_median=1181811.
If (pin>14321050161000 and pin<14321050169999) newmv_median=18471613.
If (pin>14321070551000 and pin<14321070559999) newmv_median=3609857.
If (pin>14321080521000 and pin<14321080529999) newmv_median=1312983.
If (pin>14321120581000 and pin<14321120589999) newmv_median=2375693.
If (pin>14321130401000 and pin<14321130409999) newmv_median=2100000.
If (pin>14321130431000 and pin<14321130439999) newmv_median=2960000.
If (pin>14321130501000 and pin<14321130509999) newmv_median=1685365.
If (pin>14321260481000 and pin<14321260489999) newmv_median=1031768.
If (pin>14321270411000 and pin<14321270419999) newmv_median=2649169.
If (pin>14321270491000 and pin<14321270499999) newmv_median=1964324.
If (pin>14321340531000 and pin<14321340539999) newmv_median=1103241.
If (pin>14321340601000 and pin<14321340609999) newmv_median=1599513.
If (pin>14321340661000 and pin<14321340669999) newmv_median=2493591.
If (pin>14322060481000 and pin<14322060489999) newmv_median=1471550.
If (pin>14322060501000 and pin<14322060509999) newmv_median=1151062.
If (pin>14322060521000 and pin<14322060529999) newmv_median=3013838.
If (pin>14322060531000 and pin<14322060539999) newmv_median=2219556.
If (pin>14322060541000 and pin<14322060549999) newmv_median=2215720.
If (pin>14322060561000 and pin<14322060569999) newmv_median=5733613.
If (pin>14322070451000 and pin<14322070459999) newmv_median=1031104.
If (pin>14322070501000 and pin<14322070509999) newmv_median=1180851.
If (pin>14322120481000 and pin<14322120489999) newmv_median=1656832.
If (pin>14322130501000 and pin<14322130509999) newmv_median=1783708.
If (pin>14322140411000 and pin<14322140419999) newmv_median=743960.
If (pin>14322140441000 and pin<14322140449999) newmv_median=1128857.
If (pin>14322160471000 and pin<14322160479999) newmv_median=1170168.
If (pin>14322160561000 and pin<14322160569999) newmv_median=2308580.
If (pin>14322170501000 and pin<14322170509999) newmv_median=1145351.
If (pin>14322170521000 and pin<14322170529999) newmv_median=1185882.
If (pin>14322170541000 and pin<14322170549999) newmv_median=2365543.
If (pin>14322180531000 and pin<14322180539999) newmv_median=2205200.
If (pin>14322190481000 and pin<14322190489999) newmv_median=1213978.
If (pin>14322200511000 and pin<14322200519999) newmv_median=2025142.
If (pin>14322200521000 and pin<14322200529999) newmv_median=2678175.
If (pin>14322200531000 and pin<14322200539999) newmv_median=4939278.
If (pin>14322210511000 and pin<14322210519999) newmv_median=2664176.
If (pin>14322220501000 and pin<14322220509999) newmv_median=1565660.
If (pin>14322230371000 and pin<14322230379999) newmv_median=1290333.
If (pin>14322230391000 and pin<14322230399999) newmv_median=1255319.
If (pin>14322240471000 and pin<14322240479999) newmv_median=1553333.
If (pin>14322240481000 and pin<14322240489999) newmv_median=2254691.
If (pin>14322240621000 and pin<14322240629999) newmv_median=1139770.
If (pin>14322240701000 and pin<14322240709999) newmv_median=2353658.
If (pin>14322260531000 and pin<14322260539999) newmv_median=2256632.
If (pin>14322270511000 and pin<14322270519999) newmv_median=2343633.
If (pin>14322280481000 and pin<14322280489999) newmv_median=1913082.
If (pin>14322280531000 and pin<14322280539999) newmv_median=2943396.
If (pin>14322280551000 and pin<14322280559999) newmv_median=1175301.
If (pin>14324000901000 and pin<14324000909999) newmv_median=1685708.
If (pin>14324001091000 and pin<14324001099999) newmv_median=3796310.
If (pin>14324020261000 and pin<14324020269999) newmv_median=1325000.
If (pin>14324030711000 and pin<14324030719999) newmv_median=2590909.
If (pin>14324030741000 and pin<14324030749999) newmv_median=1596153.
If (pin>14324030841000 and pin<14324030849999) newmv_median=2916778.
If (pin>14324080661000 and pin<14324080669999) newmv_median=3693541.
If (pin>14324100591000 and pin<14324100599999) newmv_median=2209440.
If (pin>14324100621000 and pin<14324100629999) newmv_median=1550386.
If (pin>14324100681000 and pin<14324100689999) newmv_median=1912500.
If (pin>14324110791000 and pin<14324110799999) newmv_median=1494510.
If (pin>14324110851000 and pin<14324110859999) newmv_median=1046890.
If (pin>14324110901000 and pin<14324110909999) newmv_median=1311310.
If (pin>14324120661000 and pin<14324120669999) newmv_median=1388243.
If (pin>14324120721000 and pin<14324120729999) newmv_median=1840388.
If (pin>14324140591000 and pin<14324140599999) newmv_median=1654679.
If (pin>14324140691000 and pin<14324140699999) newmv_median=5968109.
If (pin>14324140721000 and pin<14324140729999) newmv_median=1488009.
If (pin>14324140751000 and pin<14324140759999) newmv_median=5729166.
If (pin>14324140771000 and pin<14324140779999) newmv_median=1535510.
If (pin>14324160711000 and pin<14324160719999) newmv_median=1946050.
If (pin>14324160731000 and pin<14324160739999) newmv_median=1105714.
If (pin>14324220371000 and pin<14324220379999) newmv_median=1186000.
If (pin>14324220381000 and pin<14324220389999) newmv_median=1863157.
If (pin>14324220391000 and pin<14324220399999) newmv_median=1138874.
If (pin>14324220431000 and pin<14324220439999) newmv_median=2636538.
If (pin>14324220441000 and pin<14324220449999) newmv_median=2132249.
If (pin>14324230541000 and pin<14324230549999) newmv_median=1983805.
If (pin>14324230551000 and pin<14324230559999) newmv_median=1982933.
If (pin>14324230591000 and pin<14324230599999) newmv_median=1861762.
If (pin>14324230611000 and pin<14324230619999) newmv_median=2177736.
If (pin>14324230621000 and pin<14324230629999) newmv_median=1603370.
If (pin>14324230651000 and pin<14324230659999) newmv_median=3780207.
If (pin>14324230661000 and pin<14324230669999) newmv_median=2441612.
If (pin>14324230691000 and pin<14324230699999) newmv_median=1417625.
If (pin>14324251241000 and pin<14324251249999) newmv_median=2016332.
If (pin>14324251361000 and pin<14324251369999) newmv_median=1003371.
If (pin>14324251381000 and pin<14324251389999) newmv_median=2523473.
If (pin>14324251391000 and pin<14324251399999) newmv_median=4881919.
If (pin>14324260671000 and pin<14324260679999) newmv_median=2500000.
If (pin>14324260691000 and pin<14324260699999) newmv_median=1181136.
If (pin>14331020371000 and pin<14331020379999) newmv_median=1480928.
If (pin>14331020401000 and pin<14331020409999) newmv_median=2746818.
If (pin>14331030251000 and pin<14331030259999) newmv_median=1604260.
If (pin>14331030291000 and pin<14331030299999) newmv_median=4390438.
If (pin>14331040761000 and pin<14331040769999) newmv_median=7131370.
If (pin>14331040791000 and pin<14331040799999) newmv_median=1787924.
If (pin>14331040861000 and pin<14331040869999) newmv_median=2925347.
If (pin>14331070461000 and pin<14331070469999) newmv_median=1429997.
If (pin>14331090411000 and pin<14331090419999) newmv_median=4515004.
If (pin>14331110501000 and pin<14331110509999) newmv_median=4423654.
If (pin>14331110511000 and pin<14331110519999) newmv_median=2925925.
If (pin>14331120541000 and pin<14331120549999) newmv_median=85300546.
If (pin>14331130321000 and pin<14331130329999) newmv_median=7810468.
If (pin>14331140471000 and pin<14331140479999) newmv_median=2558823.
If (pin>14331140491000 and pin<14331140499999) newmv_median=1160963.
If (pin>14331210771000 and pin<14331210779999) newmv_median=735294.
If (pin>14331230591000 and pin<14331230599999) newmv_median=2452380.
If (pin>14331230601000 and pin<14331230609999) newmv_median=4012605.
If (pin>14331250511000 and pin<14331250519999) newmv_median=2940512.
If (pin>14331250541000 and pin<14331250549999) newmv_median=1131865.
If (pin>14331250561000 and pin<14331250569999) newmv_median=3609083.
If (pin>14331270121000 and pin<14331270129999) newmv_median=4444776.
If (pin>14331280741000 and pin<14331280749999) newmv_median=3185888.
If (pin>14331280791000 and pin<14331280799999) newmv_median=735294.
If (pin>14331280811000 and pin<14331280819999) newmv_median=735294.
If (pin>14331280821000 and pin<14331280829999) newmv_median=735294.
If (pin>14331290811000 and pin<14331290819999) newmv_median=3313180.
If (pin>14331300611000 and pin<14331300619999) newmv_median=2808683.
If (pin>14331300631000 and pin<14331300639999) newmv_median=1129631.
If (pin>14331300641000 and pin<14331300649999) newmv_median=940000.
If (pin>14331300761000 and pin<14331300769999) newmv_median=3626943.
If (pin>14331310481000 and pin<14331310489999) newmv_median=2191205.
If (pin>14331310531000 and pin<14331310539999) newmv_median=8949416.
If (pin>14331310621000 and pin<14331310629999) newmv_median=2039106.
If (pin>14332010141000 and pin<14332010149999) newmv_median=4089635.
If (pin>14332010221000 and pin<14332010229999) newmv_median=10293159.
If (pin>14332050531000 and pin<14332050539999) newmv_median=3066708.
If (pin>14332060531000 and pin<14332060539999) newmv_median=8575197.
If (pin>14332060591000 and pin<14332060599999) newmv_median=2845262.
If (pin>14332060611000 and pin<14332060619999) newmv_median=39107220.
If (pin>14332080331000 and pin<14332080339999) newmv_median=5826236.
If (pin>14333001031000 and pin<14333001039999) newmv_median=1097368.
If (pin>14333001061000 and pin<14333001069999) newmv_median=1740281.
If (pin>14333001101000 and pin<14333001109999) newmv_median=2951817.
If (pin>14333001181000 and pin<14333001189999) newmv_median=2020000.
If (pin>14333021331000 and pin<14333021339999) newmv_median=2574497.
If (pin>14333021551000 and pin<14333021559999) newmv_median=1920000.
If (pin>14333021561000 and pin<14333021569999) newmv_median=1366853.
If (pin>14333031291000 and pin<14333031299999) newmv_median=10008040.
If (pin>14333031351000 and pin<14333031359999) newmv_median=1956521.
If (pin>14333031431000 and pin<14333031439999) newmv_median=2072513.
If (pin>14333031531000 and pin<14333031539999) newmv_median=1243997.
If (pin>14333031611000 and pin<14333031619999) newmv_median=2010510.
If (pin>14333031621000 and pin<14333031629999) newmv_median=1751088.
If (pin>14333040491000 and pin<14333040499999) newmv_median=1868421.
If (pin>14333040501000 and pin<14333040509999) newmv_median=1359208.
If (pin>14333040611000 and pin<14333040619999) newmv_median=2055862.
If (pin>14333040621000 and pin<14333040629999) newmv_median=3320659.
If (pin>14333050711000 and pin<14333050719999) newmv_median=1344820.
If (pin>14333060521000 and pin<14333060529999) newmv_median=3113797.
If (pin>14333060631000 and pin<14333060639999) newmv_median=2266666.
If (pin>14333060651000 and pin<14333060659999) newmv_median=1923748.
If (pin>14333070731000 and pin<14333070739999) newmv_median=1159040.
If (pin>14333070751000 and pin<14333070759999) newmv_median=3261936.
If (pin>14333100731000 and pin<14333100739999) newmv_median=3792025.
If (pin>14333100741000 and pin<14333100749999) newmv_median=3792025.
If (pin>14333110511000 and pin<14333110519999) newmv_median=3387653.
If (pin>14333110551000 and pin<14333110559999) newmv_median=1615364.
If (pin>14333130751000 and pin<14333130759999) newmv_median=7375000.
If (pin>14333130771000 and pin<14333130779999) newmv_median=1186838.
If (pin>14333130881000 and pin<14333130889999) newmv_median=7285843.
If (pin>14333140741000 and pin<14333140749999) newmv_median=4800000.
If (pin>14333140791000 and pin<14333140799999) newmv_median=1910008.
If (pin>14333140801000 and pin<14333140809999) newmv_median=1983908.
If (pin>14333140811000 and pin<14333140819999) newmv_median=1983908.
If (pin>14333140821000 and pin<14333140829999) newmv_median=1025536.
If (pin>14333140831000 and pin<14333140839999) newmv_median=1370164.
If (pin>14333140841000 and pin<14333140849999) newmv_median=1564935.
If (pin>14333140951000 and pin<14333140959999) newmv_median=5176327.
If (pin>14333141001000 and pin<14333141009999) newmv_median=16469111.
If (pin>14333160791000 and pin<14333160799999) newmv_median=27064516.
If (pin>14333170551000 and pin<14333170559999) newmv_median=2478987.
If (pin>14333170591000 and pin<14333170599999) newmv_median=4341230.
If (pin>14333180641000 and pin<14333180649999) newmv_median=1844239.
If (pin>14333180651000 and pin<14333180659999) newmv_median=1614039.
If (pin>14333180661000 and pin<14333180669999) newmv_median=2460248.
If (pin>14333220181000 and pin<14333220189999) newmv_median=1951151.
If (pin>14333240551000 and pin<14333240559999) newmv_median=9107459.
If (pin>14333250731000 and pin<14333250739999) newmv_median=5839545.
If (pin>14333300131000 and pin<14333300139999) newmv_median=2917246.
If (pin>14333310571000 and pin<14333310579999) newmv_median=1134211.
If (pin>14334000401000 and pin<14334000409999) newmv_median=1043948.
If (pin>14334010671000 and pin<14334010679999) newmv_median=2380952.
If (pin>14334030111000 and pin<14334030119999) newmv_median=9885486.
If (pin>14334040261000 and pin<14334040269999) newmv_median=19340489.
If (pin>14334040271000 and pin<14334040279999) newmv_median=2960000.
If (pin>14334070471000 and pin<14334070479999) newmv_median=2391722.
If (pin>14334080401000 and pin<14334080409999) newmv_median=1128164.
If (pin>14334120521000 and pin<14334120529999) newmv_median=4074726.
If (pin>14334130401000 and pin<14334130409999) newmv_median=5417827.
If (pin>14334160121000 and pin<14334160129999) newmv_median=3012137.
If (pin>14334170331000 and pin<14334170339999) newmv_median=3695526.
If (pin>14334200511000 and pin<14334200519999) newmv_median=1214955.
If (pin>14334200521000 and pin<14334200529999) newmv_median=1418750.
If (pin>14334210491000 and pin<14334210499999) newmv_median=1443267.
If (pin>14334210531000 and pin<14334210539999) newmv_median=1442751.
If (pin>17031000131000 and pin<17031000139999) newmv_median=61626271.
If (pin>17031000141000 and pin<17031000149999) newmv_median=13123380.
If (pin>17031000151000 and pin<17031000159999) newmv_median=3869346.
If (pin>17031010301000 and pin<17031010309999) newmv_median=4021483.
If (pin>17031020401000 and pin<17031020409999) newmv_median=3243528.
If (pin>17031020411000 and pin<17031020419999) newmv_median=3573469.
If (pin>17031030291000 and pin<17031030299999) newmv_median=52717459.
If (pin>17031040211000 and pin<17031040219999) newmv_median=4263891.
If (pin>17031060301000 and pin<17031060309999) newmv_median=2017336.
If (pin>17031090311000 and pin<17031090319999) newmv_median=4760010.
If (pin>17031090351000 and pin<17031090359999) newmv_median=3136936.
If (pin>17031120421000 and pin<17031120429999) newmv_median=8859649.
If (pin>17032000651000 and pin<17032000659999) newmv_median=7418224.
If (pin>17032000771000 and pin<17032000779999) newmv_median=1508886.
If (pin>17032000791000 and pin<17032000799999) newmv_median=1347184.
If (pin>17032010771000 and pin<17032010779999) newmv_median=2012513.
If (pin>17032010781000 and pin<17032010789999) newmv_median=1766629.
If (pin>17032020711000 and pin<17032020719999) newmv_median=5018817.
If (pin>17032040671000 and pin<17032040679999) newmv_median=4897388.
If (pin>17032080201000 and pin<17032080209999) newmv_median=18704929.
If (pin>17032080331000 and pin<17032080339999) newmv_median=4406243.
If (pin>17032080341000 and pin<17032080349999) newmv_median=93786941.
If (pin>17032140171000 and pin<17032140179999) newmv_median=12500000.
If (pin>17032210111000 and pin<17032210119999) newmv_median=27567588.
If (pin>17032220241000 and pin<17032220249999) newmv_median=12100701.
If (pin>17032250791000 and pin<17032250799999) newmv_median=17248215.
If (pin>17032280331000 and pin<17032280339999) newmv_median=185082600.
If (pin>17032280341000 and pin<17032280349999) newmv_median=85667006.
If (pin>17032280351000 and pin<17032280359999) newmv_median=3713104.
If (pin>17041080391000 and pin<17041080399999) newmv_median=1860158.
If (pin>17041080571000 and pin<17041080579999) newmv_median=2173361.
If (pin>17041080591000 and pin<17041080599999) newmv_median=3214285.
If (pin>17041090411000 and pin<17041090419999) newmv_median=4547817.
If (pin>17041090421000 and pin<17041090429999) newmv_median=1076611.
If (pin>17041090441000 and pin<17041090449999) newmv_median=1661725.
If (pin>17041090451000 and pin<17041090459999) newmv_median=2271889.
If (pin>17041090471000 and pin<17041090479999) newmv_median=2116000.
If (pin>17041090491000 and pin<17041090499999) newmv_median=2118854.
If (pin>17041090501000 and pin<17041090509999) newmv_median=1991193.
If (pin>17041090521000 and pin<17041090529999) newmv_median=1955935.
If (pin>17041090561000 and pin<17041090569999) newmv_median=1750448.
If (pin>17041090591000 and pin<17041090599999) newmv_median=2582832.
If (pin>17041100481000 and pin<17041100489999) newmv_median=1919353.
If (pin>17041100491000 and pin<17041100499999) newmv_median=2137546.
If (pin>17041100511000 and pin<17041100519999) newmv_median=1951544.
If (pin>17041100591000 and pin<17041100599999) newmv_median=1905625.
If (pin>17041100661000 and pin<17041100669999) newmv_median=763934.
If (pin>17041100671000 and pin<17041100679999) newmv_median=2540271.
If (pin>17041100681000 and pin<17041100689999) newmv_median=2372093.
If (pin>17041100701000 and pin<17041100709999) newmv_median=2185603.
If (pin>17041110401000 and pin<17041110409999) newmv_median=1454315.
If (pin>17041110511000 and pin<17041110519999) newmv_median=2013478.
If (pin>17041210891000 and pin<17041210899999) newmv_median=1968278.
If (pin>17041210921000 and pin<17041210929999) newmv_median=2401280.
If (pin>17041210941000 and pin<17041210949999) newmv_median=7981830.
If (pin>17041210971000 and pin<17041210979999) newmv_median=1980868.
If (pin>17041221161000 and pin<17041221169999) newmv_median=1513819.
If (pin>17041221191000 and pin<17041221199999) newmv_median=1632751.
If (pin>17041221201000 and pin<17041221209999) newmv_median=3322587.
If (pin>17041221251000 and pin<17041221259999) newmv_median=1585891.
If (pin>17041221291000 and pin<17041221299999) newmv_median=3470808.
If (pin>17041221311000 and pin<17041221319999) newmv_median=1886227.
If (pin>17041221321000 and pin<17041221329999) newmv_median=2140189.
If (pin>17041221361000 and pin<17041221369999) newmv_median=1382734.
If (pin>17041221461000 and pin<17041221469999) newmv_median=1648111.
If (pin>17042000871000 and pin<17042000879999) newmv_median=3281181.
If (pin>17042000911000 and pin<17042000919999) newmv_median=868524.
If (pin>17042001001000 and pin<17042001009999) newmv_median=2176673.
If (pin>17042010611000 and pin<17042010619999) newmv_median=1370732.
If (pin>17042020941000 and pin<17042020949999) newmv_median=2893052.
If (pin>17042020971000 and pin<17042020979999) newmv_median=1832480.
If (pin>17042020981000 and pin<17042020989999) newmv_median=1711417.
If (pin>17042021021000 and pin<17042021029999) newmv_median=1640023.
If (pin>17042021041000 and pin<17042021049999) newmv_median=2441666.
If (pin>17042030991000 and pin<17042030999999) newmv_median=1701272.
If (pin>17042040541000 and pin<17042040549999) newmv_median=1980813.
If (pin>17042050531000 and pin<17042050539999) newmv_median=2235492.
If (pin>17042050541000 and pin<17042050549999) newmv_median=3564517.
If (pin>17042050561000 and pin<17042050569999) newmv_median=2142069.
If (pin>17042050571000 and pin<17042050579999) newmv_median=1069748.
If (pin>17042050651000 and pin<17042050659999) newmv_median=6244210.
If (pin>17042110381000 and pin<17042110389999) newmv_median=2300000.
If (pin>17042120421000 and pin<17042120429999) newmv_median=5082352.
If (pin>17042120481000 and pin<17042120489999) newmv_median=4388349.
If (pin>17042120521000 and pin<17042120529999) newmv_median=1223511.
If (pin>17042120531000 and pin<17042120539999) newmv_median=4917525.
If (pin>17042120541000 and pin<17042120549999) newmv_median=1985714.
If (pin>17042150571000 and pin<17042150579999) newmv_median=6016494.
If (pin>17042170651000 and pin<17042170659999) newmv_median=3465952.
If (pin>17042171341000 and pin<17042171349999) newmv_median=3309466.
If (pin>17042180371000 and pin<17042180379999) newmv_median=2481639.
If (pin>17042180381000 and pin<17042180389999) newmv_median=2194736.
If (pin>17042180391000 and pin<17042180399999) newmv_median=1870588.
If (pin>17042180441000 and pin<17042180449999) newmv_median=2318334.
If (pin>17042180471000 and pin<17042180479999) newmv_median=19278243.
If (pin>17042180491000 and pin<17042180499999) newmv_median=7742517.
If (pin>17042230871000 and pin<17042230879999) newmv_median=4379054.
If (pin>17042240461000 and pin<17042240469999) newmv_median=2344736.
If (pin>17042240481000 and pin<17042240489999) newmv_median=2928138.
If (pin>17043050581000 and pin<17043050589999) newmv_median=30157622.
If (pin>17043070541000 and pin<17043070549999) newmv_median=67766053.
If (pin>17043170161000 and pin<17043170169999) newmv_median=4806747.
If (pin>17043170171000 and pin<17043170179999) newmv_median=8368124.
If (pin>17043170181000 and pin<17043170189999) newmv_median=4358823.
If (pin>17043200621000 and pin<17043200629999) newmv_median=1657793.
If (pin>17043200631000 and pin<17043200639999) newmv_median=1303756.
If (pin>17043241101000 and pin<17043241109999) newmv_median=3338492.
If (pin>17043241121000 and pin<17043241129999) newmv_median=5714285.
If (pin>17043310471000 and pin<17043310479999) newmv_median=7037383.
If (pin>17044040351000 and pin<17044040359999) newmv_median=31311789.
If (pin>17044130221000 and pin<17044130229999) newmv_median=5450000.
If (pin>17044140371000 and pin<17044140379999) newmv_median=2686260.
If (pin>17044150251000 and pin<17044150259999) newmv_median=1369565.
If (pin>17044150261000 and pin<17044150269999) newmv_median=1100310.
If (pin>17044180411000 and pin<17044180419999) newmv_median=1776927.
If (pin>17044220301000 and pin<17044220309999) newmv_median=5058253.
If (pin>17044220401000 and pin<17044220409999) newmv_median=10199791.
If (pin>17044350341000 and pin<17044350349999) newmv_median=192712550.
If (pin>17044360621000 and pin<17044360629999) newmv_median=37215411.
If (pin>17044400301000 and pin<17044400309999) newmv_median=1597566.
If (pin>17044400351000 and pin<17044400359999) newmv_median=1839250.
If (pin>17044400401000 and pin<17044400409999) newmv_median=1101413.
If (pin>17044420541000 and pin<17044420549999) newmv_median=1306322.
If (pin>17044480311000 and pin<17044480319999) newmv_median=6136986.
If (pin>17044490421000 and pin<17044490429999) newmv_median=4326004.
If (pin>17044500321000 and pin<17044500329999) newmv_median=2710752.
If (pin>17091170111000 and pin<17091170119999) newmv_median=4583333.
If (pin>17091200151000 and pin<17091200159999) newmv_median=8385256.
If (pin>17091210031000 and pin<17091210039999) newmv_median=6043370.
If (pin>17092020241000 and pin<17092020249999) newmv_median=1809984.
If (pin>17092030261000 and pin<17092030269999) newmv_median=1993275.
If (pin>17092560061000 and pin<17092560069999) newmv_median=5408163.
If (pin>17101090231000 and pin<17101090239999) newmv_median=12624948.
If (pin>17101110141000 and pin<17101110149999) newmv_median=164539959.
If (pin>17101320401000 and pin<17101320409999) newmv_median=10671207.
If (pin>17101350381000 and pin<17101350389999) newmv_median=793382639.
If (pin>17102020851000 and pin<17102020859999) newmv_median=10207288.
If (pin>17102140191000 and pin<17102140199999) newmv_median=38182954.



*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
*compute newmv_median=newmv_median+(.12*oldmv).
*if (pin=1001) newmv_median=.
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.

Compute newav = newmv_median*percent*.10.
if class=599 newav=newmv_median*percent*.10.
if class=589 newav=newmv_median*percent*.10.
if class=399 newav=newmv_median*percent*.10.
if class=499 newav=newmv_median*percent*.10.
if class=899 newav=newmv_median*percent*.10.
if class=679 newav=newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=99999999991001 newmv=99999.

Save OUTFILE = 'S:\2018 City_condo_valuations\North\condos74.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'North CONDOS' 'USING 2015-2018 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'Lake'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'North ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.






