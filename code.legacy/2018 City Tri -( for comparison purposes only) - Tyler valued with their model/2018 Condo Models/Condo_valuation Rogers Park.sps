﻿GET FILE = 'S:\2018 City_condo_valuations\Rogers Park\Rogers ParkD.sav'.

*PUT IN ANY UNIT'S SALE YOU WISH TO HAVE TAKEN OUT OF THE REPORT.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.
*IF (PIN=00000000000000) SALEPRIC=0.

*only needed if we use recorder file.
*if recyr10=2010 saleyr=10. 
*changed saleyr to 50 so that condos that were sold in FC sales won't be used to value condo's.
*Any number could be used as long as it isn't one that is already on the file for a sale year.

if saleyr=18 amount=salepric.
if saleyr=17 amount=salepric.
if saleyr=16 amount=salepric.
if saleyr=15 amount=salepric.
if saleyr=14 amount=salepric.

*if salpric<saleamt amount=saleamt.
create difbuild = diff(pin10, 1).
if (difbuild>0) txtpull=1.
*these two lines catch condos not starting with 1001 and make sure they upload to text file.
if (condo=1001) txtpull=1.
if (condo=4001) txtpull=1.
*these two lines catch the first condo in the data file and act as a redundency to ensure that condos make the file.

if (condo=4001) town=75.
*PUT IN ANY BUILDING THAT REQUIRES A DIFFERENT SALE YEAR OR YEARS THAN THE MAJORITY.
*IF (PIN>11071220701000 AND PIN<11071220709999) AND saleyr=03 AMOUNT=amount/0.

Sort cases by pin.

IF (GARAGE='GR') AMOUNT=amount/0.
Compute av = PRIORLAN + PRIORBLD.
Compute ratio = av/amount*100.
Compute oldmv = av/percent/.10.

Compute newmv=amount/percent.

If amount>0 sales=1.

Format oldmv (comma9.0).
Format pin10 (f10.0).
Format sales (comma9.0).
Format salepric (comma9.0).
Format newmv (comma9.0).
Format amount (comma9.0). 

Sort cases by PIN10.

Aggregate
                /outfile= 'S:\2018 City_condo_valuations\Rogers Park\temp_t75.sav'
               /break=PIN10
               /newmv_median=median(newmv).
Match files
                 /file= *
                /table='S:\2018 City_condo_valuations\Rogers Park\temp_t75.sav'
                /by PIN10.
*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR AN ENTIRE BUILDING.
*If (pin>10253030531000 and pin<10253030539999) newmv_median=1200000.
*If (pin>1000 and pin<9999) newmv_median=.

compute newmv_median=newmv_median-(.07*oldmv).

If (pin>10253030521000 and pin<10253030529999) newmv_median=1067978.
If (pin>10253030531000 and pin<10253030539999) newmv_median=1067978.
If (pin>10254060471000 and pin<10254060479999) newmv_median=540105.
If (pin>10254100151000 and pin<10254100159999) newmv_median=441062.
If (pin>10254270401000 and pin<10254270409999) newmv_median=480972.
If (pin>10254270411000 and pin<10254270419999) newmv_median=480972.
If (pin>10254270421000 and pin<10254270429999) newmv_median=480972.
If (pin>10254270431000 and pin<10254270439999) newmv_median=480972.
If (pin>10254270441000 and pin<10254270449999) newmv_median=480972.
If (pin>10254270451000 and pin<10254270459999) newmv_median=480972.
If (pin>10254270461000 and pin<10254270469999) newmv_median=480972.
If (pin>10254270471000 and pin<10254270479999) newmv_median=480972.
If (pin>10254270481000 and pin<10254270489999) newmv_median=480972.
If (pin>10254270491000 and pin<10254270499999) newmv_median=480972.
If (pin>10362000461000 and pin<10362000469999) newmv_median=2720526.
If (pin>10362010311000 and pin<10362010319999) newmv_median=712602.
If (pin>10362010331000 and pin<10362010339999) newmv_median=356940.
If (pin>10362060381000 and pin<10362060389999) newmv_median=247534.
If (pin>10362060411000 and pin<10362060419999) newmv_median=983644.
If (pin>10362060421000 and pin<10362060429999) newmv_median=3032325.
If (pin>10362070131000 and pin<10362070139999) newmv_median=3853658.
If (pin>10362070161000 and pin<10362070169999) newmv_median=1361485.
If (pin>10362080601000 and pin<10362080609999) newmv_median=1322708.
If (pin>10362080621000 and pin<10362080629999) newmv_median=594940.
If (pin>10362090431000 and pin<10362090439999) newmv_median=540972.
If (pin>10362090461000 and pin<10362090469999) newmv_median=725088.
If (pin>10362100421000 and pin<10362100429999) newmv_median=706896.
If (pin>10362100431000 and pin<10362100439999) newmv_median=514875.
If (pin>10362100441000 and pin<10362100449999) newmv_median=1262303.
If (pin>10362100451000 and pin<10362100459999) newmv_median=631840.
If (pin>10362100471000 and pin<10362100479999) newmv_median=3368421.
If (pin>10362110331000 and pin<10362110339999) newmv_median=1488886.
If (pin>10362140111000 and pin<10362140119999) newmv_median=1784968.
If (pin>10362160361000 and pin<10362160369999) newmv_median=605760.
If (pin>10362180421000 and pin<10362180429999) newmv_median=1573875.
If (pin>10362180461000 and pin<10362180469999) newmv_median=698960.
If (pin>10362280541000 and pin<10362280549999) newmv_median=1371220.
If (pin>10363110401000 and pin<10363110409999) newmv_median=544256.
If (pin>10363110411000 and pin<10363110419999) newmv_median=544256.
If (pin>10363110421000 and pin<10363110429999) newmv_median=544256.
If (pin>10363110431000 and pin<10363110439999) newmv_median=544256.
If (pin>10363110441000 and pin<10363110449999) newmv_median=544256.
If (pin>10363110451000 and pin<10363110459999) newmv_median=544256.
If (pin>10363110461000 and pin<10363110469999) newmv_median=544256.
If (pin>10363160371000 and pin<10363160379999) newmv_median=1544163.
If (pin>10363170391000 and pin<10363170399999) newmv_median=605240.
If (pin>10363170401000 and pin<10363170409999) newmv_median=605240.
If (pin>10363180341000 and pin<10363180349999) newmv_median=605240.
If (pin>10363180371000 and pin<10363180379999) newmv_median=1943190.
If (pin>10363190311000 and pin<10363190319999) newmv_median=647058.
If (pin>10363190321000 and pin<10363190329999) newmv_median=647058.
If (pin>10363200511000 and pin<10363200519999) newmv_median=610000.
If (pin>10363200521000 and pin<10363200529999) newmv_median=610000.
If (pin>10363200531000 and pin<10363200539999) newmv_median=610000.
If (pin>10363200541000 and pin<10363200549999) newmv_median=610000.
If (pin>10363200551000 and pin<10363200559999) newmv_median=610000.
If (pin>10363200561000 and pin<10363200569999) newmv_median=610000.
If (pin>10363200571000 and pin<10363200579999) newmv_median=610000.
If (pin>10363200581000 and pin<10363200589999) newmv_median=610000.
If (pin>10363200591000 and pin<10363200599999) newmv_median=610000.
If (pin>10363200601000 and pin<10363200609999) newmv_median=1191348.
If (pin>10363200611000 and pin<10363200619999) newmv_median=925559.
If (pin>10363200621000 and pin<10363200629999) newmv_median=3556618.
If (pin>10363210701000 and pin<10363210709999) newmv_median=1107748.
If (pin>10363210711000 and pin<10363210719999) newmv_median=698253.
If (pin>10363230591000 and pin<10363230599999) newmv_median=1909090.
If (pin>10363230601000 and pin<10363230609999) newmv_median=900000.
If (pin>10363250311000 and pin<10363250319999) newmv_median=610000.
If (pin>10363260461000 and pin<10363260469999) newmv_median=879672.
If (pin>10364000411000 and pin<10364000419999) newmv_median=560388.
If (pin>10364050391000 and pin<10364050399999) newmv_median=2490994.
If (pin>10364060431000 and pin<10364060439999) newmv_median=518040.
If (pin>10364130381000 and pin<10364130389999) newmv_median=307800.
If (pin>10364160391000 and pin<10364160399999) newmv_median=560000.
If (pin>10364160411000 and pin<10364160419999) newmv_median=525000.
If (pin>10364160441000 and pin<10364160449999) newmv_median=525000.
If (pin>10364160451000 and pin<10364160459999) newmv_median=525000.
If (pin>10364160461000 and pin<10364160469999) newmv_median=560000.
If (pin>10364160481000 and pin<10364160489999) newmv_median=560000.
If (pin>10364160491000 and pin<10364160499999) newmv_median=525000.
If (pin>10364160501000 and pin<10364160509999) newmv_median=525000.
If (pin>10364280341000 and pin<10364280349999) newmv_median=840000.
If (pin>10364280361000 and pin<10364280369999) newmv_median=1529115.
If (pin>10364280371000 and pin<10364280379999) newmv_median=1164705.
If (pin>11291020501000 and pin<11291020509999) newmv_median=1188683.
If (pin>11291020541000 and pin<11291020549999) newmv_median=1190359.
If (pin>11291020551000 and pin<11291020559999) newmv_median=1646061.
If (pin>11291020561000 and pin<11291020569999) newmv_median=863593.
If (pin>11291020571000 and pin<11291020579999) newmv_median=636636.
If (pin>11291020581000 and pin<11291020589999) newmv_median=1733333.
If (pin>11291030231000 and pin<11291030239999) newmv_median=1396753.
If (pin>11291030241000 and pin<11291030249999) newmv_median=699570.
If (pin>11291030261000 and pin<11291030269999) newmv_median=749570.
If (pin>11291030271000 and pin<11291030279999) newmv_median=1009116.
If (pin>11291060221000 and pin<11291060229999) newmv_median=1326056.
If (pin>11291060271000 and pin<11291060279999) newmv_median=588573.
If (pin>11291060321000 and pin<11291060329999) newmv_median=892481.
If (pin>11291060391000 and pin<11291060399999) newmv_median=892274.
If (pin>11291070391000 and pin<11291070399999) newmv_median=1742378.
If (pin>11293000231000 and pin<11293000239999) newmv_median=456894.
If (pin>11293060301000 and pin<11293060309999) newmv_median=2042553.
If (pin>11293060311000 and pin<11293060319999) newmv_median=1810814.
If (pin>11293060321000 and pin<11293060329999) newmv_median=4071246.
If (pin>11293070231000 and pin<11293070239999) newmv_median=1284242.
If (pin>11293070241000 and pin<11293070249999) newmv_median=1492158.
If (pin>11293080191000 and pin<11293080199999) newmv_median=1063829.
If (pin>11293080211000 and pin<11293080219999) newmv_median=1311263.
If (pin>11293080221000 and pin<11293080229999) newmv_median=5844155.
If (pin>11293100151000 and pin<11293100159999) newmv_median=942451.
If (pin>11293100171000 and pin<11293100179999) newmv_median=720288.
If (pin>11293110261000 and pin<11293110269999) newmv_median=3268921.
If (pin>11293110281000 and pin<11293110289999) newmv_median=925293.
If (pin>11293140421000 and pin<11293140429999) newmv_median=1777771.
If (pin>11293150161000 and pin<11293150169999) newmv_median=1224137.
If (pin>11293150171000 and pin<11293150179999) newmv_median=1289517.
If (pin>11293160241000 and pin<11293160249999) newmv_median=896444.
If (pin>11293160261000 and pin<11293160269999) newmv_median=2442896.
If (pin>11293160301000 and pin<11293160309999) newmv_median=4170817.
If (pin>11293180181000 and pin<11293180189999) newmv_median=2022798.
If (pin>11293190181000 and pin<11293190189999) newmv_median=2162500.
If (pin>11293190201000 and pin<11293190209999) newmv_median=1344086.
If (pin>11293200521000 and pin<11293200529999) newmv_median=1351351.
If (pin>11293200591000 and pin<11293200599999) newmv_median=1760739.
If (pin>11302050281000 and pin<11302050289999) newmv_median=2006557.
If (pin>11302130551000 and pin<11302130559999) newmv_median=2192745.
If (pin>11302150151000 and pin<11302150159999) newmv_median=734141.
If (pin>11302160161000 and pin<11302160169999) newmv_median=773006.
If (pin>11302160171000 and pin<11302160179999) newmv_median=814226.
If (pin>11302170201000 and pin<11302170209999) newmv_median=786969.
If (pin>11302170251000 and pin<11302170259999) newmv_median=799136.
If (pin>11303010461000 and pin<11303010469999) newmv_median=813372.
If (pin>11303020501000 and pin<11303020509999) newmv_median=1181102.
If (pin>11303020511000 and pin<11303020519999) newmv_median=881399.
If (pin>11303030681000 and pin<11303030689999) newmv_median=679675.
If (pin>11303030711000 and pin<11303030719999) newmv_median=544367.
If (pin>11303040291000 and pin<11303040299999) newmv_median=1050000.
If (pin>11303060271000 and pin<11303060279999) newmv_median=2008789.
If (pin>11303060281000 and pin<11303060289999) newmv_median=1167834.
If (pin>11303072071000 and pin<11303072079999) newmv_median=5241952.
If (pin>11303072121000 and pin<11303072129999) newmv_median=3789473.
If (pin>11303072131000 and pin<11303072139999) newmv_median=2589725.
If (pin>11303072161000 and pin<11303072169999) newmv_median=1865133.
If (pin>11303072181000 and pin<11303072189999) newmv_median=769886.
If (pin>11303072201000 and pin<11303072209999) newmv_median=897528.
If (pin>11303080271000 and pin<11303080279999) newmv_median=1466973.
If (pin>11303100301000 and pin<11303100309999) newmv_median=537000.
If (pin>11303100311000 and pin<11303100319999) newmv_median=518623.
If (pin>11303100341000 and pin<11303100349999) newmv_median=421970.
If (pin>11303100351000 and pin<11303100359999) newmv_median=1620346.
If (pin>11303110241000 and pin<11303110249999) newmv_median=1585375.
If (pin>11303110251000 and pin<11303110259999) newmv_median=659986.
If (pin>11303120221000 and pin<11303120229999) newmv_median=804003.
If (pin>11303120231000 and pin<11303120239999) newmv_median=1406699.
If (pin>11303120251000 and pin<11303120259999) newmv_median=1720837.
If (pin>11303120261000 and pin<11303120269999) newmv_median=1012500.
If (pin>11303120291000 and pin<11303120299999) newmv_median=2092457.
If (pin>11303120301000 and pin<11303120309999) newmv_median=498762.
If (pin>11303130211000 and pin<11303130219999) newmv_median=516762.
If (pin>11303150181000 and pin<11303150189999) newmv_median=495000.
If (pin>11303150191000 and pin<11303150199999) newmv_median=501351.
If (pin>11303160161000 and pin<11303160169999) newmv_median=1259997.
If (pin>11303160191000 and pin<11303160199999) newmv_median=617469.
If (pin>11303160211000 and pin<11303160219999) newmv_median=2198795.
If (pin>11303170421000 and pin<11303170429999) newmv_median=1054216.
If (pin>11303170461000 and pin<11303170469999) newmv_median=1743403.
If (pin>11303170511000 and pin<11303170519999) newmv_median=1139794.
If (pin>11303190311000 and pin<11303190319999) newmv_median=2511639.
If (pin>11303190361000 and pin<11303190369999) newmv_median=2970740.
If (pin>11303190371000 and pin<11303190379999) newmv_median=1205682.
If (pin>11303200401000 and pin<11303200409999) newmv_median=538461.
If (pin>11303200411000 and pin<11303200419999) newmv_median=3305542.
If (pin>11303220381000 and pin<11303220389999) newmv_median=5309090.
If (pin>11303220401000 and pin<11303220409999) newmv_median=1741240.
If (pin>11303220421000 and pin<11303220429999) newmv_median=1538461.
If (pin>11303220431000 and pin<11303220439999) newmv_median=1616456.
If (pin>11303230641000 and pin<11303230649999) newmv_median=865522.
If (pin>11303230661000 and pin<11303230669999) newmv_median=1238298.
If (pin>11303231061000 and pin<11303231069999) newmv_median=868843.
If (pin>11304010321000 and pin<11304010329999) newmv_median=448000.
If (pin>11304010331000 and pin<11304010339999) newmv_median=1495791.
If (pin>11304030431000 and pin<11304030439999) newmv_median=7548976.
If (pin>11304060251000 and pin<11304060259999) newmv_median=502768.
If (pin>11304070191000 and pin<11304070199999) newmv_median=1109481.
If (pin>11304080771000 and pin<11304080779999) newmv_median=461578.
If (pin>11304080781000 and pin<11304080789999) newmv_median=1855220.
If (pin>11304080871000 and pin<11304080879999) newmv_median=1367208.
If (pin>11304130291000 and pin<11304130299999) newmv_median=660160.
If (pin>11304130301000 and pin<11304130309999) newmv_median=660160.
If (pin>11304130311000 and pin<11304130319999) newmv_median=660160.
If (pin>11304140271000 and pin<11304140279999) newmv_median=1525297.
If (pin>11304140311000 and pin<11304140319999) newmv_median=4759259.
If (pin>11304150391000 and pin<11304150399999) newmv_median=4271160.
If (pin>11304180411000 and pin<11304180419999) newmv_median=1620280.
If (pin>11304190321000 and pin<11304190329999) newmv_median=3165735.
If (pin>11304200681000 and pin<11304200689999) newmv_median=1660640.
If (pin>11304200731000 and pin<11304200739999) newmv_median=1537190.
If (pin>11304200741000 and pin<11304200749999) newmv_median=550952.
If (pin>11304220281000 and pin<11304220289999) newmv_median=1005907.
If (pin>11304220301000 and pin<11304220309999) newmv_median=1005907.
If (pin>11304230351000 and pin<11304230359999) newmv_median=2227272.
If (pin>11304230361000 and pin<11304230369999) newmv_median=1506202.
If (pin>11304240301000 and pin<11304240309999) newmv_median=1299870.
If (pin>11311040441000 and pin<11311040449999) newmv_median=8199356.
If (pin>11311060231000 and pin<11311060239999) newmv_median=881294.
If (pin>11311060251000 and pin<11311060259999) newmv_median=396552.
If (pin>11311080211000 and pin<11311080219999) newmv_median=7571428.
If (pin>11311140231000 and pin<11311140239999) newmv_median=4632309.
If (pin>11311160461000 and pin<11311160469999) newmv_median=1225000.
If (pin>11311160481000 and pin<11311160489999) newmv_median=787356.
If (pin>11311160491000 and pin<11311160499999) newmv_median=699254.
If (pin>11311160511000 and pin<11311160519999) newmv_median=955833.
If (pin>11311170201000 and pin<11311170209999) newmv_median=604643.
If (pin>11311170211000 and pin<11311170219999) newmv_median=596045.
If (pin>11311170221000 and pin<11311170229999) newmv_median=596045.
If (pin>11311170231000 and pin<11311170239999) newmv_median=596045.
If (pin>11311170271000 and pin<11311170279999) newmv_median=1199533.
If (pin>11311210221000 and pin<11311210229999) newmv_median=580344.
If (pin>11311210231000 and pin<11311210239999) newmv_median=580344.
If (pin>11311210241000 and pin<11311210249999) newmv_median=580344.
If (pin>11311210251000 and pin<11311210259999) newmv_median=3750721.
If (pin>11311220251000 and pin<11311220259999) newmv_median=1222242.
If (pin>11311220291000 and pin<11311220299999) newmv_median=690000.
If (pin>11311220311000 and pin<11311220319999) newmv_median=962289.
If (pin>11311240181000 and pin<11311240189999) newmv_median=524895.
If (pin>11311240191000 and pin<11311240199999) newmv_median=1532567.
If (pin>11311240241000 and pin<11311240249999) newmv_median=956534.
If (pin>11311250171000 and pin<11311250179999) newmv_median=1360000.
If (pin>11312000331000 and pin<11312000339999) newmv_median=1596516.
If (pin>11312000351000 and pin<11312000359999) newmv_median=1825000.
If (pin>11312000361000 and pin<11312000369999) newmv_median=1460500.
If (pin>11312010711000 and pin<11312010719999) newmv_median=2720000.
If (pin>11312010811000 and pin<11312010819999) newmv_median=4834973.
If (pin>11312020291000 and pin<11312020299999) newmv_median=753049.
If (pin>11312030221000 and pin<11312030229999) newmv_median=1404255.
If (pin>11312030231000 and pin<11312030239999) newmv_median=2006979.
If (pin>11312030241000 and pin<11312030249999) newmv_median=1724137.
If (pin>11312030251000 and pin<11312030259999) newmv_median=1298467.
If (pin>11312040211000 and pin<11312040219999) newmv_median=518724.
If (pin>11312040221000 and pin<11312040229999) newmv_median=599023.
If (pin>11312040241000 and pin<11312040249999) newmv_median=3243160.
If (pin>11312050461000 and pin<11312050469999) newmv_median=839772.
If (pin>11312070331000 and pin<11312070339999) newmv_median=1514673.
If (pin>11312080291000 and pin<11312080299999) newmv_median=873493.
If (pin>11312080301000 and pin<11312080309999) newmv_median=3030905.
If (pin>11312080331000 and pin<11312080339999) newmv_median=2239353.
If (pin>11312080361000 and pin<11312080369999) newmv_median=1604278.
If (pin>11312090221000 and pin<11312090229999) newmv_median=1546414.
If (pin>11312100381000 and pin<11312100389999) newmv_median=3551691.
If (pin>11312100391000 and pin<11312100399999) newmv_median=3555334.
If (pin>11312130391000 and pin<11312130399999) newmv_median=3555334.
If (pin>11312140551000 and pin<11312140559999) newmv_median=1937406.
If (pin>11312140571000 and pin<11312140579999) newmv_median=1740551.
If (pin>11312140581000 and pin<11312140589999) newmv_median=1570459.
If (pin>11312150311000 and pin<11312150319999) newmv_median=1589682.
If (pin>11312180381000 and pin<11312180389999) newmv_median=1188035.
If (pin>11312180411000 and pin<11312180419999) newmv_median=2417302.
If (pin>11312220341000 and pin<11312220349999) newmv_median=1247361.
If (pin>11312220401000 and pin<11312220409999) newmv_median=2025881.
If (pin>11312230371000 and pin<11312230379999) newmv_median=894941.
If (pin>11312260301000 and pin<11312260309999) newmv_median=826339.
If (pin>11312260311000 and pin<11312260319999) newmv_median=1483938.
If (pin>11312260331000 and pin<11312260339999) newmv_median=1351010.
If (pin>11313010451000 and pin<11313010459999) newmv_median=570800.
If (pin>11313010461000 and pin<11313010469999) newmv_median=570800.
If (pin>11313010471000 and pin<11313010479999) newmv_median=570800.
If (pin>11313010481000 and pin<11313010489999) newmv_median=570800.
If (pin>11313010491000 and pin<11313010499999) newmv_median=570800.
If (pin>11313010501000 and pin<11313010509999) newmv_median=570800.
If (pin>11313010511000 and pin<11313010519999) newmv_median=570800.
If (pin>11313010521000 and pin<11313010529999) newmv_median=570800.
If (pin>11313010531000 and pin<11313010539999) newmv_median=570800.
If (pin>11313010541000 and pin<11313010549999) newmv_median=570800.
If (pin>11313010551000 and pin<11313010559999) newmv_median=570800.
If (pin>11313010561000 and pin<11313010569999) newmv_median=570800.
If (pin>11313010571000 and pin<11313010579999) newmv_median=570800.
If (pin>11313010581000 and pin<11313010589999) newmv_median=570800.
If (pin>11313010591000 and pin<11313010599999) newmv_median=570800.
If (pin>11313010601000 and pin<11313010609999) newmv_median=570800.
If (pin>11313020761000 and pin<11313020769999) newmv_median=570800.
If (pin>11313020771000 and pin<11313020779999) newmv_median=570800.
If (pin>11313020781000 and pin<11313020789999) newmv_median=570800.
If (pin>11313020791000 and pin<11313020799999) newmv_median=570800.
If (pin>11313020801000 and pin<11313020809999) newmv_median=570800.
If (pin>11313020811000 and pin<11313020819999) newmv_median=570800.
If (pin>11313020821000 and pin<11313020829999) newmv_median=570800.
If (pin>11313020831000 and pin<11313020839999) newmv_median=570800.
If (pin>11313020841000 and pin<11313020849999) newmv_median=570800.
If (pin>11313020851000 and pin<11313020859999) newmv_median=570800.
If (pin>11313020871000 and pin<11313020879999) newmv_median=570800.
If (pin>11313020881000 and pin<11313020889999) newmv_median=570800.
If (pin>11313020901000 and pin<11313020909999) newmv_median=570800.
If (pin>11313020911000 and pin<11313020919999) newmv_median=570800.
If (pin>11313020921000 and pin<11313020929999) newmv_median=570800.
If (pin>11313020931000 and pin<11313020939999) newmv_median=570800.
If (pin>11313020941000 and pin<11313020949999) newmv_median=570800.
If (pin>11313020951000 and pin<11313020959999) newmv_median=570800.
If (pin>11313020961000 and pin<11313020969999) newmv_median=570800.
If (pin>11313020971000 and pin<11313020979999) newmv_median=570800.
If (pin>11313030621000 and pin<11313030629999) newmv_median=570800.
If (pin>11313030631000 and pin<11313030639999) newmv_median=570800.
If (pin>11313030641000 and pin<11313030649999) newmv_median=570800.
If (pin>11313030651000 and pin<11313030659999) newmv_median=570800.
If (pin>11313030661000 and pin<11313030669999) newmv_median=570800.
If (pin>11313030671000 and pin<11313030679999) newmv_median=570800.
If (pin>11313030681000 and pin<11313030689999) newmv_median=570800.
If (pin>11313030691000 and pin<11313030699999) newmv_median=570800.
If (pin>11313030701000 and pin<11313030709999) newmv_median=570800.
If (pin>11313030711000 and pin<11313030719999) newmv_median=570800.
If (pin>11313030721000 and pin<11313030729999) newmv_median=570800.
If (pin>11313030731000 and pin<11313030739999) newmv_median=570800.
If (pin>11313030741000 and pin<11313030749999) newmv_median=570800.
If (pin>11313030751000 and pin<11313030759999) newmv_median=570800.
If (pin>11313030761000 and pin<11313030769999) newmv_median=570800.
If (pin>11313060071000 and pin<11313060079999) newmv_median=1513186.
If (pin>11313060081000 and pin<11313060089999) newmv_median=1893393.
If (pin>11313070081000 and pin<11313070089999) newmv_median=381189.
If (pin>11313080071000 and pin<11313080079999) newmv_median=806411.
If (pin>11313110091000 and pin<11313110099999) newmv_median=1108030.
If (pin>11313120451000 and pin<11313120459999) newmv_median=1059782.
If (pin>11313120461000 and pin<11313120469999) newmv_median=2071428.
If (pin>11313130381000 and pin<11313130389999) newmv_median=474078.
If (pin>11313130391000 and pin<11313130399999) newmv_median=499396.
If (pin>11313140361000 and pin<11313140369999) newmv_median=2396021.
If (pin>11313140371000 and pin<11313140379999) newmv_median=440047.
If (pin>11313160471000 and pin<11313160479999) newmv_median=1121898.
If (pin>11313160551000 and pin<11313160559999) newmv_median=888625.
If (pin>11313160561000 and pin<11313160569999) newmv_median=670315.
If (pin>11313160571000 and pin<11313160579999) newmv_median=1051956.
If (pin>11313170351000 and pin<11313170359999) newmv_median=2924835.
If (pin>11313170361000 and pin<11313170369999) newmv_median=2236135.
If (pin>11313180391000 and pin<11313180399999) newmv_median=647878.
If (pin>11313190411000 and pin<11313190419999) newmv_median=2581632.
If (pin>11314000511000 and pin<11314000519999) newmv_median=7154471.
If (pin>11314010911000 and pin<11314010919999) newmv_median=1315789.
If (pin>11314020881000 and pin<11314020889999) newmv_median=904177.
If (pin>11314130151000 and pin<11314130159999) newmv_median=2744590.
If (pin>11314140521000 and pin<11314140529999) newmv_median=1322599.
If (pin>11314140541000 and pin<11314140549999) newmv_median=940338.
If (pin>11321020171000 and pin<11321020179999) newmv_median=5153196.
If (pin>11321040341000 and pin<11321040349999) newmv_median=1564478.
If (pin>11321040371000 and pin<11321040379999) newmv_median=464916.
If (pin>11321060261000 and pin<11321060269999) newmv_median=1020001.
If (pin>11321060281000 and pin<11321060289999) newmv_median=1612529.
If (pin>11321060291000 and pin<11321060299999) newmv_median=2002613.
If (pin>11321100401000 and pin<11321100409999) newmv_median=909234.
If (pin>11321120241000 and pin<11321120249999) newmv_median=1397673.
If (pin>11321140331000 and pin<11321140339999) newmv_median=1186861.
If (pin>11321150201000 and pin<11321150209999) newmv_median=890037.
If (pin>11321150211000 and pin<11321150219999) newmv_median=1836956.
If (pin>11321160291000 and pin<11321160299999) newmv_median=1113387.
If (pin>11321160351000 and pin<11321160359999) newmv_median=851196.
If (pin>11321190191000 and pin<11321190199999) newmv_median=2023523.
If (pin>11321190201000 and pin<11321190209999) newmv_median=518265.
If (pin>11321190221000 and pin<11321190229999) newmv_median=2260000.
If (pin>11321190291000 and pin<11321190299999) newmv_median=1397812.
If (pin>11321200391000 and pin<11321200399999) newmv_median=1000000.
If (pin>11321210201000 and pin<11321210209999) newmv_median=1169279.
If (pin>11321230161000 and pin<11321230169999) newmv_median=3133462.
If (pin>11321230241000 and pin<11321230249999) newmv_median=1269587.
If (pin>11321240301000 and pin<11321240309999) newmv_median=1366800.
If (pin>11321240311000 and pin<11321240319999) newmv_median=1530810.
If (pin>11321240361000 and pin<11321240369999) newmv_median=1097435.
If (pin>11322000391000 and pin<11322000399999) newmv_median=1213120.
If (pin>11322000401000 and pin<11322000409999) newmv_median=2370000.
If (pin>11322000421000 and pin<11322000429999) newmv_median=1387451.
If (pin>11322010291000 and pin<11322010299999) newmv_median=1052727.
If (pin>11322020211000 and pin<11322020219999) newmv_median=2106000.
If (pin>11322020231000 and pin<11322020239999) newmv_median=1060606.
If (pin>11323000261000 and pin<11323000269999) newmv_median=1639982.
If (pin>11323000271000 and pin<11323000279999) newmv_median=1136363.
If (pin>11323010191000 and pin<11323010199999) newmv_median=3295855.
If (pin>11323020271000 and pin<11323020279999) newmv_median=1689858.
If (pin>11323030241000 and pin<11323030249999) newmv_median=884159.
If (pin>11323030281000 and pin<11323030289999) newmv_median=2012841.
If (pin>11323040271000 and pin<11323040279999) newmv_median=1671608.
If (pin>11323040301000 and pin<11323040309999) newmv_median=1557606.
If (pin>11323050281000 and pin<11323050289999) newmv_median=1471211.
If (pin>11323070351000 and pin<11323070359999) newmv_median=2786904.
If (pin>11323110271000 and pin<11323110279999) newmv_median=1661996.
If (pin>11323120181000 and pin<11323120189999) newmv_median=1281250.
If (pin>11323130341000 and pin<11323130349999) newmv_median=1143432.
If (pin>11323130371000 and pin<11323130379999) newmv_median=2569055.
If (pin>11323140351000 and pin<11323140359999) newmv_median=1552876.
If (pin>11323150381000 and pin<11323150389999) newmv_median=855555.
If (pin>11323170281000 and pin<11323170289999) newmv_median=1450617.
If (pin>11323170291000 and pin<11323170299999) newmv_median=1344973.
If (pin>11323210321000 and pin<11323210329999) newmv_median=482156.
If (pin>11323230161000 and pin<11323230169999) newmv_median=548786.
If (pin>11323230171000 and pin<11323230179999) newmv_median=2065850.
If (pin>11323260291000 and pin<11323260299999) newmv_median=1649305.
If (pin>11323270271000 and pin<11323270279999) newmv_median=1543695.
If (pin>11323270291000 and pin<11323270299999) newmv_median=1613475.
If (pin>11323270301000 and pin<11323270309999) newmv_median=1941885.
If (pin>11323300461000 and pin<11323300469999) newmv_median=500814.
If (pin>11324000391000 and pin<11324000399999) newmv_median=1201465.
If (pin>11324000421000 and pin<11324000429999) newmv_median=901304.
If (pin>11324000481000 and pin<11324000489999) newmv_median=3090773.
If (pin>11324010441000 and pin<11324010449999) newmv_median=2073573.
If (pin>11324010451000 and pin<11324010459999) newmv_median=2340936.
If (pin>11324030191000 and pin<11324030199999) newmv_median=1542249.
If (pin>11324040181000 and pin<11324040189999) newmv_median=758498.



*Check this building for an error with comma placement. 399.



*If (pin>1000 and pin<9999) newmv_median=.
*compute newmv_median=.9*newmv_median.
*rel whole town nemvw_median*.95 as example.
*compute newmv_median=newmv_median-(.04*oldmv).
*if (pin=1001) newmv_median=.
*If (newmv_median>.65*oldmv) newmv_median=.92*newmv_median.



Compute newav = newmv_median*percent*.10.
if class=599 newav=newmv_median*percent*.10.
if class=589 newav=newmv_median*percent*.10.
if class=399 newav=newmv_median*percent*.10.
if class=499 newav=newmv_median*percent*.10.
if class=899 newav=newmv_median*percent*.10.
if class=679 newav=newmv_median*percent*.10.

Compute newratio = newav/amount*100.
Compute increase = (newav-av)/av*100.
Compute diff = (newav-av).

Format newmv_median(comma9.0).

*USE THE FOLLOWING TO MANUALLY ADJUST MARKET VALUE FOR UNIT.
*If pin=1001 newmv=9999.

Save OUTFILE = 'S:\2018 City_condo_valuations\Rogers Park\condos75.sav'.

Set length = 50.
Table observation =class  age sales condo  amount ratio oldmv newmv_median newratio increase
                /table = PIN10 by class + age + sales + condo + amount + ratio + oldmv + newmv_median + newratio + increase
/title = 'Rogers Park CONDOS' 'USING 2013-2018 SALES'
/statistics = median(class(f3.0)'Class')
	                 median(age(f3.0) 'Age')
                  validn(sales(f3.0) 'Number of Sales')
                  validn(condo(f3.0) 'Number of Units')
                  median(amount(comma9.0) 'Median Sale Price') 
                  median(ratio(f3.2) 'Median Prior Ratio')
                  median(oldmv(comma9.0) 'Median Prior Market Value') 
                  median(newmv_median(comma9.0) 'Median Current Market Value') 
                  median(newratio(f3.2) 'Median Current Ratio')
                  median(increase(f3.1) 'Median Percent Increase').

Var label increase 'AV % INCREASE'
          diff 'AV $ INCREASE'
          amount 'SALE PRICE'
          newratio 'NEW RATIO'
          av 'OLD AV'
          newav 'NEW AV'
          PIN10 'BUILDING'
          town 'Lake'
          condo 'UNIT #'.

Format diff (comma9.0)
       increase (pct7.2)
       newav (comma9.0)
       av (comma9.0)
       amount (comma9.0).

set len =35.
Report Format = automatic list tspace(1) align(center)
/string = saledat (salemo '-' saleyr)
/Title = 'Rogers Park ASSESSED VALUES'
/Variables = condo
             saledat
             amount
             newratio
             percent
             av
             newav
             diff
             increase
/Break = pin10.






