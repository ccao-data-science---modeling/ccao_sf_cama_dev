# Comments -----------------------------------------------------------------------------------------------------
# Are COD and PRD statistics for the Chicago Triad 2018 Re-assessment accurate characterizations 
# of the overall performance of the models?
# Rob Ross, 2018-11-15, raross1217@gmail.com
# The data for this scripts comes from the Cook County Assessor
# Data can be found here: http://www.cookcountyassessor.com/Resources/CityTriennialReassessmentModelsandMethodologyReport.aspx
# I have translated the data into CSV files here: https://www.dropbox.com/sh/x8ofki55wyqzjz5/AABQf-qW6KZExxw_C5A3ai_Xa?dl=0
# I then loaded these files into a PostgreSQL Instance on my desktop terminal. SQL code for this is in the Dropbox folder.
# notes. 1) GRANTOR and GRANTEE fields have some corruption from CSV translation
# 2) I dropped a series of fields cumfileX_X because they did not line up across CSV files,
#     which made it difficult to pull into Postgres. I didn't need them anyway for this exercise.
# 3) 'filings' was sometimes string and sometimes numeric. I made always numeric with no loss of data.


# Top -----
rm(list=ls());flush.console(); theme_set(theme_bw());

# Controls ----
# Who is running this code?
user <- "Robert Ross"

# What packages will you need?
libs <- c("xlsx", "maptools","sp","foreign","fields", "mgcv", "car","RPostgreSQL"
          , "plyr","dplyr", "DBI", "RODBC", "RStata", "lattice", "RColorBrewer", 
          "ggplot2", "scales", "zoo", "leaflet", "htmlwidgets", "htmltools", "sjPlot"
          ,"gbm", "progress", "knitr", "kableExtra")

# What data to use?
data_id <- "70"
townships <- c(70)

# How many bootstrapped replications for COD, PRD?
bootstrapp_iters <- 10 

# How many trim rounds?
filter_iters <- 10
filter_setting_1 <- 0 # Starting with...

# What model and estimation proceedure? (models are in 'models' data)
form <- "f1"; method <- "OLS"; model_id="linear_f1"; type <- "log-log"; ntree <- 10000;

# What other scripts need to be loaded?
scripts <-c("2018115_reopen_HP&W_functions.r")

# Where are your files stored?
if(user=="Robert Ross"){setwd("C:/Users/Robert A Ross/Dropbox/Chicago Triad Assessment Data");}
if(user=="A new user"){setwd("---------SET ME----------");}
dirs <- list(code="C:/Users/Robert A Ross/Documents/CCAO_CAMA_SF/code.r/"
             , data="C:/Users/Robert A Ross/Dropbox/Chicago Triad Assessment Data/data/"
             , results="C:/Users/Robert A Ross/Dropbox/Chicago Triad Assessment Data/results/"
             , data_shp="C:/Users/Robert A Ross/Documents/CCAO_CAMA_SF/data/SHP/shp2016.Rdata"
             , data_shp_raw="data/SHP/ccgisdata_-_Parcels_2016.csv");
options(scipen=20);


# Refresh ----
refresh <- function(recode=TRUE){
  # Refresh packages
  check.packages <- function(libs){
    new.libs <- libs[!(libs %in% installed.packages()[, "Package"])]
    if (length(new.libs)) 
      install.packages(new.libs, dependencies = TRUE)
    sapply(libs, require, character.only = TRUE)
  }
  for (iter in 1:length(libs))
    check.packages(libs[iter]);
  
   #Refresh functions
  for (iter in 1:length(scripts)){
    source(paste0(dirs$code, scripts[iter]))
  }
  rm(iter)
}
refresh()

# stick all CSVs together to load into Postgres ----
# Note that you have to change the root directory in this file manually
# chooseStataBin()
# options("RStata.StataVersion" = 14)
# DO THIS ONCE
# stata(paste(dirs$code, "Paste files together.do", sep=""))


# ODBC Connect ----
drv <- dbDriver("PostgreSQL")
pw <- "password"

con <- dbConnect(drv, dbname = "postgres",
                 host = "localhost", port = 5432,
                 user = "postgres", password = pw)

# Integrity Checks -----

integrity_checks <- data.frame(check="", outcome="")

# 1:1 Check -----
unique_pins <- data.matrix(
  dbGetQuery(con, "SELECT * FROM
             (SELECT COUNT(pin) as pins FROM ccao_working.chi_2018_mv) AS C
              UNION ALL
              (SELECT COUNT(DISTINCT pin) as pins FROM ccao_working.chi_2018_mv) 
              UNION ALL
              (SELECT COUNT(pin) as pins FROM ccao_working.chi_2018_regt)
              UNION ALL
              (SELECT COUNT(DISTINCT pin) as pins FROM ccao_working.chi_2018_regt)  
              UNION ALL
              (SELECT COUNT(B.pin) FROM 
                ccao_working.chi_2018_mv AS A INNER JOIN ccao_working.chi_2018_regt AS B ON A.pin=B.pin
                GROUP BY A.pin HAVING COUNT(B.pin) > 1) 
              UNION ALL
              (SELECT COUNT(A.pin) FROM 
             ccao_working.chi_2018_mv AS A INNER JOIN ccao_working.chi_2018_regt AS B ON A.pin=B.pin
             GROUP BY B.pin HAVING COUNT(A.pin) > 1) 
            UNION ALL
              (SELECT COUNT(DISTINCT pin) as pins FROM ccao_working.chi_2018_regt WHERE amount1>1) 
    ")
  )
if (unique_pins[1,1]!=unique_pins[2,1]){msg <- "Bad: there are duplicates in MV file"}else{
  msg <-"Good: There are no duplicate pins in MV file"
}

integrity_checks <- rbind(integrity_checks, data.frame(check=""
                              , outcome=msg))
if (unique_pins[3,1]!=unique_pins[4,1]){msg <- "Bad: there are duplicates in REGT file"}else{
  msg <- "Good: There are no duplicate pins in REGT file"
}
integrity_checks <- rbind(integrity_checks, data.frame(check=""
                                                       , outcome=msg))
if (length(unique_pins)>5){msg <- "Bad: MV to REGT is not 1:1"}else{
  msg <- "Good: MV to REGT is 1:1"
}
integrity_checks <- rbind(integrity_checks, data.frame(check=""
                                                       , outcome=msg))

# Get modeldata -----
modeldata <- dbGetQuery(con, "
    SELECT B.*, A.mv
	FROM ccao_working.chi_2018_mv AS A
	RIGHT JOIN ccao_working.chi_2018_regt AS B
	ON A.pin=B.pin
	WHERE amount1>1
	ORDER BY A.pin 
")
if (unique_pins[5,1]!=nrow(modeldata)){msg <- "Bad query! Number of rows isn't right."}else{
  msg <- "Good query: Number of rows is consistent"
}
integrity_checks <- rbind(integrity_checks, data.frame(check=""
                                                       , outcome=msg))
killDbConnections()

# Geo-spatial Data ----
# Read in global shapefile
if (TRUE) {
  load(dirs$data_shp);
  if (FALSE) {	# RUN THIS BLOCK OF CODE ONCE TO COMPILE THE CSV INTO RDATA
    SHP <- read.csv(dirs$data_shp_raw,as.is=TRUE);
    SHP <- subset(SHP,select=c(Name,the_geom));
    save(SHP,file=dirs$data_shp);
  }
}
SHP <- subset(SHP,Name%in%modeldata$pin,select=c(Name,the_geom));
SHP <- subset(SHP,!duplicated(Name));
shpstr <- gsub("MULTIPOLYGON ","",SHP$the_geom);
shpstr <- gsub("\\(","",shpstr);
shpstr <- gsub("\\)","",shpstr);
shpstr <- strsplit(shpstr,", ");
shpxy <- lapply(shpstr,function(vec) {
  xy <- strsplit(vec," ")
  x <- sapply(xy,function(vec) vec[1]);
  y <- sapply(xy,function(vec) vec[2]);
  cbind(as.numeric(x),as.numeric(y));
});
p <- lapply(shpxy,function(mat) Polygon(mat,hole=FALSE));
p <- lapply(seq_along(p),function(iter) Polygons(list(p[[iter]]),ID=as.character(iter)));
p <- SpatialPolygons(p);
POLY <- SpatialPolygonsDataFrame(p,subset(SHP,select=-the_geom),match.ID=FALSE);
XY <- data.frame(pin=as.numeric(SHP$Name),coordinates(POLY),stringsAsFactors=FALSE);
colnames(XY) <- c("pin","X","Y");
modeldata <- merge(XY,modeldata,by="pin", all.y=TRUE)
rm(p, SHP, shpstr, shpxy, XY)

# How many pins are missing from the shapefile?
msg < paste0(sum(is.na(modeldata$X)), " pins are missing from the polygon data")
print(msg)
integrity_checks <- rbind(integrity_checks, data.frame(check=""
                                                       , outcome=msg))

# Recodes ----
valid_column_names <- make.names(names=names(modeldata), unique=TRUE, allow_ = TRUE)
names(modeldata) <- valid_column_names
class(modeldata$amount1) <- c("money", class(modeldata$amount1))
modeldata$sale_bin <- cut(modeldata$amount1, seq(0,10000000,25000), labels=1:400)
modeldata$puremarket[is.na(modeldata$puremarket)]<- 0
modeldata$multi[is.na(modeldata$multi)]<- 0
modeldata$saledate <- as.Date(modeldata$saledate,"%m/%d/%Y")
modeldata$saleyear <- as.numeric(format(modeldata$saledate, "%Y"))
modeldata$saleyear[is.na(modeldata$saleyear)]<-0;
modeldata$PPSF <- modeldata$amount1/modeldata$sqftb;
modeldata$RQOS <- 4*(2017-as.numeric(format(modeldata$saledate,"%Y"))) + (5 - ceiling(as.numeric(format(modeldata$saledate,"%m"))/3)); 
modeldata$RHOS <- factor(2*(2017-as.numeric(format(modeldata$saledate,"%Y"))) + (3 - ceiling(as.numeric(format(modeldata$saledate,"%m"))/2)))
modeldata$QUARTER <- factor(5 - ceiling(as.numeric(format(modeldata$saledate,"%m"))/3))
modeldata$MONTH <-ceiling(as.numeric(format(modeldata$saledate,"%m")))
FULLBATH_F <- factor(modeldata$fullbath)
FULLBATH_C <- modeldata$fullbath
modeldata$ROOMS_F <- factor(modeldata$rooms)
modeldata$ROOMS_C <- modeldata$rooms
NUM <- factor(modeldata$num)
EXTCON <- factor(modeldata$extcon)
FIREPL_C <- modeldata$firepl
FIREPL_F <- factor(modeldata$firepl)
modeldata$CLASS <- factor(modeldata$class)
modeldata$NBHD <- factor(modeldata$nbhd)
modeldata$GAR <- factor(modeldata$gar)
modeldata$NUM <- factor(modeldata$num)
modeldata$BSF <- modeldata$sqftb 
modeldata$LSF <- modeldata$sqftl
modeldata$AGE <- modeldata$age
modeldata$AGE2 <- modeldata$age^2
modeldata$NUM6 <- as.numeric(modeldata$NUM==6);
modeldata$basment1 <- as.numeric(modeldata$basment==1);
modeldata$ceiling2 <- as.numeric(modeldata$ceiling==2);
modeldata$garNOT7 <- as.numeric(modeldata$gar!=7);
modeldata$saledate_numeric<-as.numeric(modeldata$saledate)

# G Location factor ----
this <- subset(modeldata[, c("pin","X", "Y", "saledate", "RQOS","saleyear","MONTH" ,"amount1")], 
              !is.na(saledate) & !is.na(amount1) & amount1>1 &!is.na(X))
LOCF_MODEL <- gam(log(amount1)~s(X)+s(Y)+s(X,Y)
                  +saledate+s(MONTH)+s(saleyear)
                  ,data=modeldata, na.action = na.exclude)

# Estimate by forcing sale date to be the date we want to estimate at
this$saledate <- as.Date("12/30/2017", "%m/%d/%Y")
this$saleyear <-2017
this$MONTH <-12
this$RQOS <-1
this$LOC_PRED <- as.numeric(predict(LOCF_MODEL,newdata=this));
this$LOCF <- this$LOC_PRED/mean(this$LOC_PRED)

# Validity check
if (min(this$LOCF)>0){msg<- paste0("Good: LOCF always positive")}else{msg<- "BAD: LOCF has negative values"}
integrity_checks <- rbind(integrity_checks, data.frame(check=""
                                                       , outcome=msg))
if (mean(this$LOCF)==1){msg<- paste0("Good: LOCF mean=1")}else{msg<-"BAD: LOCF mean!=1"}
integrity_checks <- rbind(integrity_checks, data.frame(check=""
                                                       , outcome=msg))

modeldata <- merge(this[,c("pin", "LOCF")], modeldata
                   , by="pin", all.y=TRUE)
rm(this, LOCF_MODEL)
# How many pins are missing LOCF?
n <- nrow(subset(modeldata, !is.na(saledate) & !is.na(amount1) & amount1>1 &!is.na(X) & is.na(LOCF)))
if (n==0){msg <- paste0("Good: all elligible pins have LOCF assigned",
  rm(n))}else{print("BAD:",n, "elligible pins missing LCOF")}
integrity_checks <- rbind(integrity_checks, data.frame(check=""
                                                       , outcome=msg))
# Specify Models ----
f1 <- log(amount1)~log(BSF)+log(LSF)+log(AGE)+log(bedrooms)+log(LOCF)+
  log(fullbath)+log(1+halfbath)+saledate_numeric+QUARTER+rooms+NUM6+firepl+
  basment1+ceiling2+garNOT7
f2 <- amount1~BSF+LSF+AGE+AGE2+bedrooms+LOCF+
  fullbath+halfbath+saledate_numeric+QUARTER+rooms+NUM6+firepl+
  basment1+ceiling2+garNOT7-1

models <- data.frame(
  form_id=""
  , form=""
  , model_id=""
  , type=""
  , method=""
)
models <- rbind(models, data.frame(
  form_id="f1"
  , form=gsub(" ","",as.character(Reduce(paste, deparse(f1))))
  , model_id="linear_f1"
  , type="log-log"
  , method="OLS"
))
models <- subset(models, form_id!="")
models <- rbind(models, data.frame(
  form_id="f2"
  , form=gsub(" ","",as.character(Reduce(paste, deparse(f2))))
  , model_id="linear_f2"
  , type="lin-lin"
  , method="OLS"
))
models <- rbind(models, data.frame(
  form_id="f1"
  , form=gsub(" ","",as.character(Reduce(paste, deparse(f2))))
  , model_id="tree_f1"
  , type="log-log"
  , method="Random Forest"
))

# Template outcome tables ----
modeling_results <- data.frame(
  form_id="", form="", type="" , method="",model_id="", trim=NA, pct_trimmed=NA, N=NA, adj_r=NA
  , p25_price_msample=NA, p50_price_msample=NA, p75_price_msample=NA, IQR=NA, mean_ratio=NA
  , p50_ratio_msample=NA, p25_ratio_msample=NA
  , p75_ratio_msample=NA, COD=NA, COD_SE=NA, model_COD=NA, model_COD_SE=NA, P10_COD=NA, P10COD_SE=NA, P90_COD=NA
  , P90_COD_SE=NA, PRD=NA, PRD_SE=NA, model_PRD=NA, model_PRD_SE=NA, PRB=NA, PRB_SE=NA, suites=NA
  , suites_SE=NA
)
COD <- data.frame(iter=NA, trim=NA, COD=NA, p10_COD=NA, p90_COD=NA, model_COD=NA, PRD=NA
                  , model_PRD=NA) #Should remane this table at some point.
models_list <- c("linear_f1", "tree_f1", "linear_f2", "tree_f2")

# Modeling Loop ----
modeldata <- subset(modeldata, town %in% townships)
modeldata$filter_1 <-filter_setting_1
for(i in 1:filter_iters){ 
  filter_setting_1 <- i
  if(filter_setting_1>2){
    if(nrow(subset(modeldata, filter_1>=eval(filter_setting_1)))<=500){
      print("Not enough obs in next trimmed set - skipped")
      next
    }
  }
  print(paste0("Filter = ", filter_setting_1, "/", filter_iters))
  
# Initial Filter ----
# Filter > 0 when property should be included
  # Filter for ALL eligible sales and basic data integrity
  if(filter_setting_1==1){
    modeldata$filter_1<-ifelse(
      modeldata$filter_1>=0
      & modeldata$puremarket==1 
      & modeldata$saleyear>2012                               
      & modeldata$multi<1 
      & modeldata$amount1>10000 
      & modeldata$amount1<10000000
      & !is.na(modeldata$amount1)
      & !is.na(modeldata$LOCF)
      ,1,0
  )
  }  

# Estimation ----
print("Estimating Values")
# unfinished - want to pull values from models data frame
if(method=="OLS" & type=="log-log"){
model <- lm(f1,data=subset(modeldata, filter_1>=filter_setting_1))
}
if(method=="Random Forest" & type=="log-log"){
model <- gbm(f1,distribution="gaussian"
             ,interaction.depth=2
             ,n.trees=ntree
             ,data=subset(modeldata, filter_1>=filter_setting_1))
}
  
# Want CIs too at some point
modeldata$fitted_value <- NA
if(method=="OLS" & type=="log-log"){
modeldata$fitted_value <- exp(predict(model, newdata=modeldata)) 
}
if(method=="OLS" & type=="lin-lin"){
  modeldata$fitted_value <- predict(model, newdata=modeldata) 
}
if(method=="Random Forest" & type=="log-log"){
  modeldata$fitted_value <- exp(predict(model, newdata=modeldata, n.trees=ntree))
}

modeldata$fitted_value <- ifelse(modeldata$fitted_value<=10000, 10000, modeldata$fitted_value)
modeldata$residuals <- modeldata$fitted_value-modeldata$amount1

# need manually calcuated 'R-squared' if using non-parametric method
if(method=="Random Forest"){
pseudo_r <- (sum(subset(modeldata, filter_1>=filter_setting_1)$amount1^2)  - 
     sum(subset(modeldata, filter_1>=filter_setting_1)$residuals^2))/
  sum(subset(modeldata, filter_1>=filter_setting_1)$amount1^2)
}

# Ratios ----
print("Calculating Ratios")
modeldata$ratio <- NA
modeldata$ratio <-modeldata$fitted_value/modeldata$amount1

mean <- mean(subset(modeldata, ratio<10)$ratio, na.rm=TRUE)
std <- sd(subset(modeldata, ratio<10)$ratio, na.rm=TRUE)
print(paste0("Median ratio= ", round(median(modeldata$ratio,na.rm=TRUE),2)))
print(paste0("Mean ratio= ", round(mean,2), " Standard Deviation = ", round(std,2)))
modeldata$Nratio <- NA
modeldata$Nratio <-(modeldata$ratio-mean)/std
rm(mean, std)

# Re-filter ----
# Find next filter using IQR range on normalized ratio
p25 <- quantile(subset(modeldata, filter_1>=filter_setting_1)$Nratio, c(.25), names = FALSE, na.rm=TRUE)
p75 <- quantile(subset(modeldata, filter_1>=filter_setting_1)$Nratio, c(.75), names = FALSE, na.rm=TRUE)
IQR <- abs(p75-p25)

if(eval(filter_setting_1)>=1){
  print("Calculating next filter level")

  for(j in seq(4, .4, by=-.2)){ 
    # Want to identify the LEAST aggressive trim that STILL trims something
    if(nrow(subset(modeldata, modeldata$Nratio>(p25-j*IQR) & modeldata$Nratio<(p75+j*IQR)
    & modeldata$filter_1==eval(filter_setting_1)))<
      nrow(subset(modeldata, modeldata$filter_1==eval(filter_setting_1)))
    & max(modeldata$filter_1)==filter_setting_1
    ){
      # Use that to construct next filter setting, where n(filter_setting_1+1)<n(filter_setting_1)
      modeldata$filter_1<- ifelse(
        modeldata$Nratio>(p25-j*IQR) & modeldata$Nratio<(p75+j*IQR) & modeldata$filter_1>=filter_setting_1
        ,filter_setting_1+1,modeldata$filter_1)
      print(paste0(j, " used as the IQR multiplier"))
    }else{next}
  }
}
print(paste0("IQR for normalized ratio is now ", round(p25,2)," to ", round(p75,2), ", leaving "
             , nrow(subset(subset(modeldata, filter_1>=filter_setting_1+1)))
             , " observations in filter=", filter_setting_1+1))

# COD & PRD ----
print("Calculating COD and PRD statistics with bootstrapped standard errors")
set.seed(7677)
pb <- progress_bar$new(total = bootstrapp_iters)
for(i in 1:bootstrapp_iters){
  pb$tick()
  Sys.sleep(1 / bootstrapp_iters)

  #COD of bottom 10th percentile of sales price of the entire sample
  df <-subset(modeldata[,c("ratio", "amount1","filter_1")], !is.na(amount1) & amount1<quantile(subset(modeldata$amount1,!is.na(modeldata$amount1) & filter_1>=1), c(.10), names = FALSE))
  n <- sample(1:nrow(df), 1, replace=FALSE)
  s <- sample(df[, c("ratio")],n[1])
  cod_10 <- sum(abs(s-median(s))) / (n*median(s));

  #COD of top 10th percentile of sales price of the entire sample
  df <-subset(modeldata[,c("ratio", "amount1","filter_1")], !is.na(amount1) & amount1>quantile(subset(modeldata$amount1,!is.na(modeldata$amount1) & filter_1>=1), c(.90), names = FALSE))
  n <- sample(1:nrow(df), 1, replace=FALSE)
  s <- sample(df[, c("ratio")],n[1])
  cod_90 <- sum(abs(s-median(s))) / (n*median(s));
  
  
  # Overall COD of the entire sample
  df <-subset(modeldata[,c("ratio", "amount1","filter_1")], !is.na(ratio) & filter_1>=1)
  n <- sample(1:nrow(df), 1, replace=FALSE)
  s <- sample(df[, c("ratio")],n)
  cod <- sum(abs(s-median(s))) / (n*median(s));
  
  #COD within model sample
  df <-subset(modeldata[,c("ratio", "amount1","filter_1")], !is.na(ratio) & filter_1>=eval(filter_setting_1))
  n <- sample(1:nrow(df), 1, replace=FALSE)
  s <- sample(df[, c("ratio")],n[1])
  mod_COD <- sum(abs(s-median(s))) / (n*median(s));
  
  # PRD for entire sample
  df <-subset(modeldata[,c("ratio", "amount1", "filter_1")], !is.na(ratio) & filter_1>=1)
  prd <- mean(df$ratio,  na.rm = TRUE)/weighted.mean(df$ratio,df$amount1, na.rm = TRUE)
  
  # PRD for model sample
  df <-subset(modeldata[,c("ratio", "amount1", "filter_1","filter_1")], !is.na(ratio) & filter_1>=eval(filter_setting_1))
  prd_mod <- mean(df$ratio,  na.rm = TRUE)/weighted.mean(df$ratio,df$amount1, na.rm = TRUE)
  
  
  COD <- rbind(COD, data.frame(
    iter=i
    , trim=filter_setting_1
    , COD=cod*100
    , p10_COD=cod_10*100
    , p90_COD=cod_90*100
    , model_COD=mod_COD*100
    , PRD=prd
    , model_PRD=prd_mod
    ))
}
print(paste0("COD = ",round(cod,2)," PRD = ",round(prd,2)))  
COD <- subset(COD, !is.na(COD))
rm(s, n, cod, cod_10,cod_90, df, prd, prd_mod)

# Save Modeling Results ----
print("Saving modeling results")

if(method=="Random Forest"){
r <- pseudo_r
} 
if(method=="OLS"){
  r <- summary(model)$adj.r.squared
}

modeling_results <- 
rbind(modeling_results,
data.frame(
   form_id=form
  , form=gsub(" ","",as.character(Reduce(paste, deparse(f1))))
  , type=type
  , method=method
  , model_id=model_id
  , trim=filter_setting_1
  , pct_trimmed=(nrow(subset(modeldata, filter_1>=1))-nrow(subset(modeldata, filter_1>=eval(filter_setting_1))))/nrow(subset(modeldata, filter_1>=1))
  , N=nrow(subset(modeldata, filter_1>=eval(filter_setting_1)))
  , adj_r=r
  , p25_price_msample=quantile(subset(modeldata, filter_1>=filter_setting_1)$amount1, c(.25), names = FALSE, na.rm = TRUE)
  , p50_price_msample=quantile(subset(modeldata, filter_1>=filter_setting_1)$amount1, c(.50), names = FALSE, na.rm = TRUE)
  , p75_price_msample=quantile(subset(modeldata, filter_1>=filter_setting_1)$amount1, c(.75), names = FALSE, na.rm = TRUE)
  , IQR=IQR
  , mean_ratio=mean(modeldata$ratio, na.rm = TRUE)
  , p50_ratio_msample=quantile(subset(modeldata, filter_1>=filter_setting_1)$ratio, c(.50), names = FALSE, na.rm = TRUE)
  , p25_ratio_msample=quantile(subset(modeldata, filter_1>=filter_setting_1)$ratio, c(.25), names = FALSE, na.rm = TRUE)
  , p75_ratio_msample=quantile(subset(modeldata, filter_1>=filter_setting_1)$ratio, c(.75), names = FALSE, na.rm = TRUE)
  , COD=mean(subset(COD, trim=filter_setting_1)$COD, na.rm = TRUE)
  , COD_SE=sd(subset(COD, trim=filter_setting_1)$COD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$COD))
  , model_COD=mean(subset(COD, trim=filter_setting_1)$model_COD, na.rm = TRUE)
  , model_COD_SE=sd(subset(COD, trim=filter_setting_1)$model_COD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$model_COD))
  , P10_COD=mean(subset(COD, trim=filter_setting_1)$p10_COD, na.rm = TRUE)
  , P10COD_SE=sd(subset(COD, trim=filter_setting_1)$p10_COD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$p10_COD))
  , P90_COD=mean(subset(COD, trim=filter_setting_1)$p90_COD, na.rm = TRUE)
  , P90_COD_SE=sd(subset(COD, trim=filter_setting_1)$p90_COD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$p90_COD))
  , PRD=mean(subset(COD, trim=filter_setting_1)$PRD, na.rm = TRUE)
  , PRD_SE=sd(subset(COD, trim=filter_setting_1)$PRD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$PRD))
  , model_PRD=mean(subset(COD, trim=filter_setting_1)$model_PRD, na.rm = TRUE)
  , model_PRD_SE=sd(subset(COD, trim=filter_setting_1)$model_PRD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$PRD))
  , PRB=NA
  , PRB_SE=NA
  , suites=NA
  , suites_SE=NA
  )
)
if(method=="Random Forest" & type=="log-log"){
dev.off()
}
}
modeling_results <- subset(modeling_results, form!="")
print("Done")

# Estimation with CCAO trimming ----
filter_setting_1 <- 1
# Filter using fixed values reported in modeling document
if(  data_id == "70"){modeldata$filter_2<-ifelse(
    modeldata$puremarket==1 
  & modeldata$saleyear>2012                               
  & modeldata$multi<1 
  & modeldata$amount1>65000 
  & modeldata$amount1<790000
  & modeldata$saleyear>2012
  & modeldata$sqftb<9000
  & !is.na(modeldata$amount1)
  & !is.na(modeldata$LOCF)
  ,1, 0
)
}  
if(data_id == "77"){modeldata$filter_2<-ifelse(
    modeldata$puremarket==1 
  & modeldata$saleyear>2012                               
  & modeldata$multi<1 
  & modeldata$amount1>75000 
  & modeldata$amount1<790000
  & modeldata$saleyear>2012 #?? All sales?
  # & modeldata$AGE<10 
  # & modeldata$amount1/modeldata$sqftb<75 
  # & modeldata$class<95
  & !is.na(modeldata$amount1)
  & !is.na(modeldata$LOCF)
  ,1, 0
)
}  

# CCAO:Estimation ----

# Using fixed thresholds doesn't really fit well into the previous section. So, at the risk of repetative code
# I simply re-run the whole shebang with the CCAO thresholds
print("Estimating Values")
# unfinished - want to pull values from models data frame
if(method=="OLS" & type=="log-log"){
  model <- lm(f1,data=subset(modeldata, filter_2>=1))
}
if(method=="Random Forest" & type=="log-log"){
  model <- gbm(f1,distribution="gaussian"
               ,interaction.depth=2
               ,n.trees=ntree
               ,data=subset(modeldata, filter_2>=1))
}

# Want CIs too at some point
modeldata$fitted_value <- NA
if(method=="OLS" & type=="log-log"){
  modeldata$fitted_value <- exp(predict(model, newdata=modeldata)) 
}
if(method=="OLS" & type=="lin-lin"){
  modeldata$fitted_value <- predict(model, newdata=modeldata) 
}
if(method=="Random Forest" & type=="log-log"){
  modeldata$fitted_value <- exp(predict(model, newdata=modeldata, n.trees=ntree))
}

modeldata$fitted_value <- ifelse(modeldata$fitted_value<=10000, 10000, modeldata$fitted_value)
modeldata$residuals <- modeldata$fitted_value-modeldata$amount1

# need manually calcuated 'R-squared' if using non-parametric method
if(method=="Random Forest"){
  pseudo_r <- (sum(subset(modeldata, filter_2>=1)$amount1^2)  - 
                 sum(subset(modeldata, filter_2>=1)$residuals^2))/
    sum(subset(modeldata, filter_2>=1)$amount1^2)
}

# CCAO:Ratios ----
print("Calculating Ratios")
modeldata$ratio <- NA
modeldata$ratio <-modeldata$fitted_value/modeldata$amount1

mean <- mean(subset(modeldata, ratio<10)$ratio, na.rm=TRUE)
std <- sd(subset(modeldata, ratio<10)$ratio, na.rm=TRUE)
print(paste0("Median ratio= ", round(median(modeldata$ratio,na.rm=TRUE),2)))
print(paste0("Mean ratio= ", round(mean,2), " Standard Deviation = ", round(std,2)))
modeldata$Nratio <- NA
modeldata$Nratio <-(modeldata$ratio-mean)/std
rm(mean, std)

# CCAO:COD & PRD ----
print("Calculating COD and PRD statistics with bootstrapped standard errors")
set.seed(7677)
pb <- progress_bar$new(total = bootstrapp_iters)
for(i in 1:bootstrapp_iters){
  pb$tick()
  Sys.sleep(1 / bootstrapp_iters)
  
  # Overall COD of the entire sample
  df <-subset(modeldata[,c("ratio", "amount1","filter_2")], !is.na(ratio) & filter_2>=1)
  n <- sample(1:nrow(df), 1, replace=FALSE)
  s <- sample(df[, c("ratio")],n)
  cod <- sum(abs(s-median(s))) / (n*median(s));
  
  #COD of bottom 10th percentile of sales price of the entire sample
  df <-subset(modeldata[,c("ratio", "amount1","filter_2")], !is.na(amount1) & amount1<quantile(subset(modeldata$amount1,!is.na(modeldata$amount1) & filter_2>=1), c(.10), names = FALSE))
  n <- sample(1:nrow(df), 1, replace=FALSE)
  s <- sample(df[, c("ratio")],n[1])
  cod_10 <- sum(abs(s-median(s))) / (n*median(s));
  
  #COD of top 10th percentile of sales price of the entire sample
  df <-subset(modeldata[,c("ratio", "amount1","filter_2")], !is.na(amount1) & amount1>quantile(subset(modeldata$amount1,!is.na(modeldata$amount1) & filter_2>=1), c(.90), names = FALSE))
  n <- sample(1:nrow(df), 1, replace=FALSE)
  s <- sample(df[, c("ratio")],n[1])
  cod_90 <- sum(abs(s-median(s))) / (n*median(s));
  
  #COD within model sample
  df <-subset(modeldata[,c("ratio", "amount1","filter_2")], !is.na(ratio) & filter_2>=eval(filter_setting_1))
  n <- sample(1:nrow(df), 1, replace=FALSE)
  s <- sample(df[, c("ratio")],n[1])
  mod_COD <- sum(abs(s-median(s))) / (n*median(s));
  
  # PRD for entire sample
  df <-subset(modeldata[,c("ratio", "amount1", "filter_2")], !is.na(ratio) & filter_2>=1)
  prd <- mean(df$ratio,  na.rm = TRUE)/weighted.mean(df$ratio,df$amount1, na.rm = TRUE)
  
  # PRD for model sample
  df <-subset(modeldata[,c("ratio", "amount1", "filter_2","filter_2")], !is.na(ratio) & filter_2>=eval(filter_setting_1))
  prd_mod <- mean(df$ratio,  na.rm = TRUE)/weighted.mean(df$ratio,df$amount1, na.rm = TRUE)
  
  
  COD <- rbind(COD, data.frame(
    iter=i
    , trim=filter_setting_1
    , COD=cod*100
    , p10_COD=cod_10*100
    , p90_COD=cod_90*100
    , model_COD=mod_COD*100
    , PRD=prd
    , model_PRD=prd_mod
  ))
}
print(paste0("COD = ",round(cod,2)," PRD = ",round(prd,2)))  
COD <- subset(COD, !is.na(COD))
rm(s, n, cod, cod_10,cod_90, df, prd, prd_mod)

# Save Modeling Results ----
print("Saving modeling results")

if(method=="Random Forest"){
  r <- pseudo_r
} 
if(method=="OLS"){
  r <- summary(model)$adj.r.squared
}

modeling_results <- 
  rbind(modeling_results,
        data.frame(
          form_id=form
          , form=gsub(" ","",as.character(Reduce(paste, deparse(f1))))
          , type=type
          , method=method
          , model_id=model_id
          , trim=999
          , pct_trimmed=(nrow(subset(modeldata, filter_2>=0))-nrow(subset(modeldata, filter_2>=eval(filter_setting_1))))/nrow(subset(modeldata, filter_2>=0))
          , N=nrow(subset(modeldata, filter_2>=eval(filter_setting_1)))
          , adj_r=r
          , p25_price_msample=quantile(subset(modeldata, filter_2>=filter_setting_1)$amount1, c(.25), names = FALSE, na.rm = TRUE)
          , p50_price_msample=quantile(subset(modeldata, filter_2>=filter_setting_1)$amount1, c(.50), names = FALSE, na.rm = TRUE)
          , p75_price_msample=quantile(subset(modeldata, filter_2>=filter_setting_1)$amount1, c(.75), names = FALSE, na.rm = TRUE)
          , IQR=IQR
          , mean_ratio=mean(modeldata$ratio, na.rm = TRUE)
          , p50_ratio_msample=quantile(subset(modeldata, filter_2>=filter_setting_1)$ratio, c(.50), names = FALSE, na.rm = TRUE)
          , p25_ratio_msample=quantile(subset(modeldata, filter_2>=filter_setting_1)$ratio, c(.25), names = FALSE, na.rm = TRUE)
          , p75_ratio_msample=quantile(subset(modeldata, filter_2>=filter_setting_1)$ratio, c(.75), names = FALSE, na.rm = TRUE)
          , COD=mean(subset(COD, trim=filter_setting_1)$COD, na.rm = TRUE)
          , COD_SE=sd(subset(COD, trim=filter_setting_1)$COD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$COD))
          , model_COD=mean(subset(COD, trim=filter_setting_1)$model_COD, na.rm = TRUE)
          , model_COD_SE=sd(subset(COD, trim=filter_setting_1)$model_COD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$model_COD))
          , P10_COD=mean(subset(COD, trim=filter_setting_1)$p10_COD, na.rm = TRUE)
          , P10COD_SE=sd(subset(COD, trim=filter_setting_1)$p10_COD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$p10_COD))
          , P90_COD=mean(subset(COD, trim=filter_setting_1)$p90_COD, na.rm = TRUE)
          , P90_COD_SE=sd(subset(COD, trim=filter_setting_1)$p90_COD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$p90_COD))
          , PRD=mean(subset(COD, trim=filter_setting_1)$PRD, na.rm = TRUE)
          , PRD_SE=sd(subset(COD, trim=filter_setting_1)$PRD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$PRD))
          , model_PRD=mean(subset(COD, trim=filter_setting_1)$model_PRD, na.rm = TRUE)
          , model_PRD_SE=sd(subset(COD, trim=filter_setting_1)$model_PRD, na.rm = TRUE)/sqrt(length(subset(COD, trim=filter_setting_1)$PRD))
          , PRB=NA
          , PRB_SE=NA
          , suites=NA
          , suites_SE=NA
        )
  )

# Save outputs to dir ----
integrity_checks <- subset(integrity_checks, outcome!="")
save(models, file=paste0(dirs$results, "models.Rdata"))

# save(COD, file=paste0(dirs$results, "COD_PRD.Rdata"))
save(integrity_checks, file=paste0(dirs$results, "integrity_checks.Rdata"))
save(modeling_results, file=paste0(dirs$results, "modeling_results_",model_id,"_",data_id,"_",filter_iters,".Rdata"))


# Save modeling data for other processes
save(modeldata, file=paste0(dirs$data, "modeldata_", data_id, ".Rdata"))

# ID Preferred Trim ----
# Need to be able to choose between best COD or best PRD
best_trim <- list(subset(modeling_results[,c("trim", "PRD")], PRD==min(modeling_results$PRD))$trim)
msg <- paste0("The best trim setting is ", best_trim,".")
msg
integrity_checks <- rbind(integrity_checks, data.frame(check=""
                                                       , outcome=msg))
# Manually check if 999 is best trim? Likely not, but check.

# Re-Estimation for V ----
print("Estimating Values at preferred trim")
# unfinished - want to pull values from models data frame 
if(method=="OLS" & type=="log-log"){
  model <- lm(f1,data=subset(modeldata, filter_1>=best_trim))
}
if(method=="Random Forest" & type=="log-log"){
  model <- gbm(f1,distribution="gaussian"
               ,interaction.depth=2
               ,n.trees=ntree
               ,data=subset(modeldata, filter_1>=best_trim))
}

# Want CIs too at some point
modeldata$fitted_value <- NA
if(method=="OLS" & type=="log-log"){
  modeldata$fitted_value <- exp(predict(model, newdata=modeldata)) 
}
if(method=="OLS" &type=="lin-lin"){
  modeldata$fitted_value <- predict(model, newdata=modeldata) 
}
if(method=="Random Forest" &type=="log-log"){
  modeldata$fitted_value <- exp(predict(model, newdata=modeldata, n.trees=ntree)) 
}
modeldata$fitted_value <- ifelse(modeldata$fitted_value<=10000, 10000, modeldata$fitted_value)
modeldata$residuals <- modeldata$fitted_value-modeldata$amount1


# Visualize ----
summary(model)

df <- data.frame(rbind(
  data.frame(
      trim=modeling_results[,c("trim")]
      , COD=modeling_results[,c("COD")]
      , COD_SE=modeling_results[,c("COD_SE")]
      , type="Sales Ratio Data")
  ,
  data.frame(
      trim=modeling_results[,c("trim")]
      , COD=modeling_results[,c("model_COD")]
      , COD_SE=modeling_results[,c("model_COD_SE")]
      , type="Model Data")
  )
)
df <- subset(df, trim!=999)

mean <- data.frame(rbind(
  data.frame(
    stat=mean(subset(modeldata, filter_1>=1 & ratio<=10)$Nratio)
    ,group="mean")
  ,
  data.frame(
    stat=median(subset(modeldata, filter_1>=1 & ratio<=10)$Nratio)
    ,group="median")
  )
)

png(paste0(dirs$results, paste0("fitted_",model_id,"_",data_id,"_",filter_iters,".png")))
ggplot(data=subset(modeldata, amount1<1000000 & fitted_value>0 & fitted_value<1000000)
       , aes(x=amount1, y=fitted_value)) +
  theme(legend.position="bottom")+
  geom_point()+
  theme_minimal() +
  labs(title="Model fitted values vs. actual prices "
       , subtitle=(paste0("Model=",model_id, "; Township=", data_id, "; Filter=",best_trim," out of ", filter_iters))
       ,x="Sale price", y = "Fitted values")+
  geom_abline(intercept = 0, slope = 1, size = 2, color = "dark blue")
dev.off()

png(paste0(dirs$results, paste0("Nratio_", model_id,"_",data_id,"_",filter_iters, ".png"))) 
ggplot(data=subset(modeldata, filter_1>=best_trim & ratio<=10), aes(x=Nratio)) +
  geom_histogram(binwidth=.05)+
  theme_minimal()+theme(legend.position="bottom")+
  labs(
    title="Distribution of normalized ratios"
    , subtitle=(paste0("Model=",model_id, "; Township=", data_id, "; Filter=",best_trim," out of ", filter_iters))
    ,x="((Yhat/Y)-mean(Yhat/Y))/stdev(Yhat/Y)", y = "N")+
  geom_vline(data=mean, aes(xintercept=stat, color=group),linetype="dashed")
dev.off()


png(paste0(dirs$results, paste0("N_", model_id,"_",data_id,"_",filter_iters, ".png")))
ggplot(data=subset(modeling_results, trim!=999), aes(x=trim, y=pct_trimmed, group=type)) +
  geom_line()+
  theme_minimal()+theme(legend.position="bottom")+
  geom_point()+
  geom_vline(xintercept = as.numeric(best_trim[1]))+
  geom_text(aes(x=as.numeric(best_trim[1]), label="best trim", y=.5)
            , colour="blue", angle=90
            , vjust = 1.3)+
  labs(title="Plot of sample trimmed"
       , subtitle=(paste0("Model=",model_id, "; Township=", data_id))
       , x="Trim round", y = "Percent trimmed out")
dev.off()

df <- data.frame(rbind(
  data.frame(
    trim=modeling_results[,"trim"],COD=modeling_results[,"COD"], PRD=modeling_results[,"PRD"], type="Sales Ratio Data"
    )
  ,
  data.frame(
    trim=modeling_results[,"trim"],COD=modeling_results[,"model_COD"], PRD=modeling_results[,"model_PRD"], type="Modeling Data"
    )
)
)
df <- subset(df, trim!=999)
png(paste0(dirs$results, paste0("COD_",model_id,"_",data_id,"_",filter_iters ,".png")))
ggplot(data=df, aes(x=trim, y=COD, group=type,color=type)) +
  geom_line()+
  theme(legend.position="bottom", axis.text.y = element_blank())+
  geom_point()+
  theme_minimal()+
  scale_color_brewer(palette="Paired")+
  labs(title="Plot of COD as data is trimmed"
       , subtitle=(paste0("Model=",model_id, "; Township=", data_id))
       ,x="Trim round", y = "COD")+
  geom_vline(xintercept = as.numeric(best_trim[1]))+
  geom_text(aes(x=as.numeric(best_trim[1]), label="best trim", y=40)
            , colour="blue", angle=90
            , vjust = 1.2)
dev.off()

png(paste0(dirs$results, paste0("PRD_",model_id,"_",data_id,"_",filter_iters ,".png")))
ggplot(data=df, aes(x=trim, y=PRD, group=type,color=type)) +
  geom_line()+
  theme(legend.position="bottom", axis.text.y = element_blank())+
  geom_point()+
  theme_minimal()+
  scale_color_brewer(palette="Paired")+
  labs(title="Plot of PRD as data is trimmed"
       , subtitle=(paste0("Model=",model_id, "; Township=", data_id, "."))
       ,x="Trim round", y = "PRD")+
  geom_vline(xintercept = as.numeric(best_trim[1]))+
  geom_text(aes(x=as.numeric(best_trim[1]), label="best trim", y=1.4)
            , colour="blue", angle=90
            , vjust = 1.3)
dev.off()

df <- data.frame(rbind(
  data.frame(
    trim=modeling_results[,"trim"],COD=modeling_results[,"COD"]
    , PRD=modeling_results[,"PRD"], Data="Sales Ratio Data"
  )
  ,
  data.frame(
    trim=modeling_results[,"trim"],COD=modeling_results[,"model_COD"]
    , PRD=modeling_results[,"model_PRD"], Data="Modeling Data"
  )
)
)
df <- subset(df, trim==999 | trim==best_trim)
df$trim_type[df$trim==999]<-"CCAO 'hard' trim"
df$trim_type[df$trim!=999]<-paste0("Best iterative trim (",best_trim,")")
df$trim <-factor(df$trim)
png(paste0(dirs$results, paste0("COD_comparison_",model_id,"_",data_id,"_",filter_iters,".png")))
ggplot(data=df, aes(x=trim_type, y=COD, group=Data, fill=Data)) +
  geom_bar(stat="identity",position=position_dodge())+
  theme_minimal()+theme(axis.text.y = element_blank())+
  scale_color_brewer(palette="Paired")+
  labs(title="CODs with best trim vs. CCAO trim" 
       , subtitle=(paste0("Model=",model_id, "; Township=", data_id))
       ,x="Trim method and data", y = "COD")
dev.off()

png(paste0(dirs$results, paste0("PRD_comparison_",model_id,"_",data_id,"_",filter_iters,".png")))
ggplot(data=df, aes(x=trim_type, y=PRD, group=Data, fill=Data)) +
  geom_bar(stat="identity",position=position_dodge())+
  theme_minimal()+theme(axis.text.y = element_blank())+
  scale_color_brewer(palette="Paired")+
  labs(title="PRDs with best trim vs. CCAO trim"
       , subtitle=(paste0("Model=",model_id, "; Township=", data_id))
       ,x="Trim method and data", y = "COD")
dev.off()
# Bottom ----
# rm(list=ls())

scatter(modeling_results$COD)

  